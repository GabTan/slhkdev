<!-- 
    Version    : 1.6
    Company    : Sun Life Financial
    Date       : 20.JUL.2009
    Author     : Marian Baleros
    Description: Visualforce page of Fund Transaction History
    History    : 
    
	1.1		09.SEP.2009 - Marian Baleros - Added the remark due to rounding differences and updated the alignment of table values.
	1.2		10.SEP.2009	- Marian Baleros - Added the custom label for N/A value.
	1.3		14.SEP.2009 - Marian Baleros - Moved the rendered value for Interest Rate and Maturity Date, from the outputtext to column.
			17.SEP.2009 - Marian Baleros - Moved and updated the format of messages that display after search
    1.4     SIT_113 09.22.09 - Wilson Cheng
    1.5     09.25.09 Wcheng - Added header title
    1.6     10.12.09 Wcheng - added the custom label of All in transction type picklist
            SIT_154 10.13.09 Wcheng - Edit the value of the 'All' labels of transaction type
    1.7     12/03/09 JC NACHOR - removed remarks 
    1.8     06/03/10 Mikka Elepano - SR-ES-10017 Fund Transaction Enhancement (Added Remarks)
    1.9     04/03/11 Mikka Elepano - HKSCS handling of client name       
-->

<apex:page controller="FundTxnHstyViewCtrl" showHeader="false" sidebar="false" action="{!Security}">
<!-- START ADD Wcheng Enhancement 09.25.09-->
<head>
    <title>{!$Label.FTH_FUNDTRANSHIST} - {!PolicyName}</title>
</head>
<!-- END ADD Wcheng Enhancement 09.25.09-->
    <c:PortalNavigator navID="103" />
    <link href="/EXT/ext-2.2/resources/css/ext-all.css" type="text/css" rel="stylesheet"/>
    <script src="/EXT/ext-2.2/adapter/ext/ext-base.js" type="text/javascript" />
    <script src="/EXT/ext-2.2/ext-all.js" type="text/javascript" />
    <apex:pageBlock >
    <!-- <apex:sectionHeader title="{!$Label.FTH_FUNDTRANSHIST}" subtitle="{!PolicyName}"/> -->
    <div id="SectionHeader" styleClass="hkscs" style="background-color:#ffb200; color:#ffffff; border:solid 3px #ffb200; font-weight:bold; padding-left: 5px; height:35px; line-height:13pt;">
               <apex:outputLabel value="{!$Label.FTH_FUNDTRANSHIST}" styleClass="hkscs"/>
               <p></p>               
               <apex:outputLabel value="{!PolicyName}" styleClass="hkscs"/>
    </div>
    <p>&nbsp;</p> 
        <apex:form id="theform">
            <script>
                function SelectTransType(elem)
                {
                    var selElem=document.getElementById('selectTrans');
                    document.getElementById(elem).value=selElem.options[selElem.selectedIndex].value;   
                }
                
                function onPageChange(){
                   onPageChange();
                }
                
                function hideDialog(divType)
                {
                    dWin.hide();
                    //reloadBanner();
                }
                
                function onErrorDisplay(checkDateHdn, checkNumRecsHdn)
                {
                    var errMsg = '';
                    var hasErr = "false";
                    var hasErrDate = "false";
                
                    if (document.getElementById(checkDateHdn).value == 'true')
                    {
                        //START EDIT WCheng SIT_113 09.22.09
                    	var spanElem    = (document.getElementById('{!$Component.errDateMsg}').getElementsByTagName('span'));
                        var headerText  = '{!$Label.FTH_ERR_01}';
                        var headerElem  = document.getElementById(spanElem[0].id);

                        headerElem.setAttribute('class','errorMsg');
		                headerElem.setAttribute('className','errorMsg');
                        headerElem.innerHTML = '<b>'+headerText+'</b>';
                        
                       	document.getElementById('checkErrDate').style.display = '';
                       	//END EDIT WCheng SIT_113 09.22.09
                    }
                    else
                    {	
                    	document.getElementById('checkErrDate').style.display = 'none';
                    }   
                        
                    if (document.getElementById(checkNumRecsHdn).value == 'true')
                    {
                        //START ADD WCheng SIT_113 09.22.09
                        var spanElem    = (document.getElementById('{!$Component.errRecMsg}').getElementsByTagName('span'));
                        var headerText  = '{!$Label.FTH_ERR_03}';
                        var headerElem  = document.getElementById(spanElem[0].id);

                        headerElem.setAttribute('class','errorMsg');
                        headerElem.setAttribute('className','errorMsg');
                        headerElem.innerHTML = '<b>'+headerText+'</b>';
                        
                        document.getElementById('checkErrRec').style.display = '';
                        
                        //errMsg = "{!$Label.FTH_ERR_03}<br/><br/>";
                        //hasErr = "true";
                    }
                    else
                    {
                        document.getElementById('checkErrRec').style.display = 'none';
                    } 
                    //END ADD WCheng SIT_113 09.22.09
                    
                    //START DELETE WCheng SIT_113 09.22.09
                    /*if (hasErr == "true")
                    {
                        document.getElementById('errDiv').innerHTML = errMsg;
                        document.getElementById('errDiv').style.display = '';
                    }
                    else
                    {
                        document.getElementById('errDiv').style.display = 'none';
                    }*/
                    //START DELETE WCheng SIT_113 09.22.09
                }
                
                //START ADDED Wcheng Enchancement 09.23.09
                function setCurrPageNumber(pageNumberId, currPageId, newPageId, prevPageId, nextPageId)
			    {
			        document.getElementById(currPageId).value = document.getElementById(newPageId).value;
			        document.getElementById(pageNumberId).value = document.getElementById(currPageId).value;
			        document.getElementById(prevPageId).value = new Number(document.getElementById(currPageId).value) - 1;
			        document.getElementById(nextPageId).value = new Number(document.getElementById(currPageId).value) + 1;
			        onPageChange();
			    }
			    //END ADDED Wcheng Enchancement 09.23.09
            </script>
            <div class="blueLink">
            <apex:outputLink id="backLink" value="{!PolicyBack}?policyID={!PolicyNum}">{!$Label.FTH_BACK}</apex:outputLink>
            </div>
            <br/>
            
            <!-- ERROR DISPLAY -->
            <div id="checkErrDate" style="display:none">
                <apex:pageMessage id="errDateMsg" severity="error" strength="3"/>
                <apex:pageMessages />
            </div>
            <div id="checkErrRec" style="display:none">
                <apex:pageMessage id="errRecMsg" severity="warning" strength="3"/>
                <apex:pageMessages />
            </div>
            <!-- <div id="errDiv" style="display:none; color:#FF0000"></div> -->
            <apex:inputHidden id="checkDate" value="{!checkDate}" />
            <apex:inputHidden id="checkNumRecs" value="{!checkNumRecs}" />
            
            <apex:pageBlockSection title="{!$Label.FTH_SEARCHTRANS}" collapsible="false"/>
            <br/>
            
            <!-- FILTERING -->
            
            <table>
            <tr>
                <th><apex:outputLabel value="{!$Label.FTH_FUNDNAME}"/></th>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <th><apex:outputLabel value="{!$Label.FTH_TRANSTYPE}"/></th>
            </tr>
            <tr>
                <td><apex:selectList value="{!FundsName}" multiselect="false" size="1">
                <apex:selectOptions value="{!FundName}"/>
                </apex:selectList></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><select size="1" id="selectTrans" name="selectTrans" onchange="SelectTransType('{!$Component.trans}');">
                <!-- START EDIT SIT_154 Wcheng  10.13.09-->
                    <option value="{!$Label.FTH_ALL}">{!$Label.FTH_ALL}</option>
                <!-- END EDIT SIT_154 Wcheng  10.13.09-->
                    <optgroup label="{!$Label.FTH_CASHFLOWTYPE}">
                        <apex:outputText value="{!CashflowFundTransType}" escape="false"/>
                    </optgroup>
                    <optgroup label="{!$Label.FTH_UTILIZEDFUNDTYPE}">
                        <apex:outputText value="{!UtilizedFundTransType}" escape="false"/>
                    </optgroup>
                </select></td>
                <apex:inputHidden value="{!TransType}" id="trans"/>
            </tr>
            <tr><td></td></tr>
            <tr>
                <th><apex:outputLabel value="{!$Label.FTH_COVPERIOD}"/></th>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <th><apex:outputLabel value="{!$Label.FTH_SORT}"/></th>
            </tr>
            <tr>
                <td>
                    <apex:selectList value="{!FundsStartMonth}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundStartMonth}"/>
                    </apex:selectList>&nbsp;/&nbsp;
                    <apex:selectList value="{!FundsStartYear}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundStartYear}"/>
                    </apex:selectList>&nbsp;&nbsp;-&nbsp;&nbsp;  
                    <apex:selectList value="{!FundsEndMonth}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundEndMonth}"/>
                    </apex:selectList>&nbsp;/&nbsp;
                    <apex:selectList value="{!FundsEndYear}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundEndYear}"/>
                    </apex:selectList>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <apex:selectList value="{!FromSort}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundSort}"/>
                    </apex:selectList>&nbsp;&nbsp;&nbsp;&nbsp;
                    <apex:selectList value="{!SortBy}" multiselect="false" size="1">
                        <apex:selectOptions value="{!FundSortBy}"/>
                    </apex:selectList>
                </td>
            </tr>
            </table>
            <br/><br/>
            <apex:commandButton styleClass="eSvcBtn"  value="{!$Label.FTH_SEARCH}" action="{!Submit}" reRender="outPanel, checkDate, checkRecs, checkNumRecs" onclick="remShield(); javascript:window.scrollTo(0,0); showLoadingDialog();" status="status" onComplete="hideDialog('progress'); onErrorDisplay('{!$Component.checkDate}', '{!$Component.checkNumRecs}');"/>&nbsp;
            
            <!--This code below is for the fund transaction history list-->
            
            <br/>
            <br/>
            <apex:pageBlockSection title="{!$Label.FTH_SETTLEDTRANS}" collapsible="false" />
            <apex:outputPanel id="outPanel">
            <br/>
            <apex:outputLabel id="checkRecs" value="{!$Label.FTH_INFO_01}" rendered="{!checkRecs}" />
                <apex:outputPanel id="inPanel" rendered="{!disp}">
				<apex:actionFunction name="onPageChange" action="{!onPageChange}" reRender="outPanel" oncomplete="hideDialog('progress');"></apex:actionFunction>
                    
                    <apex:dataTable value="{!curFundTran}" var="dispReport" id="theTable" columnsWidth="25%,10%,18%,10%" width="100%" styleClass="inputTable" headerClass="linedHeaderRow" columnClasses="leftTableCol,rightTableCol,leftTableCol,rightTableCol,rightTableCol,rightTableCol,rightTableCol,rightTableCol" rowClasses="whiteInputRow,ltBlueInputRow">
                        <apex:column >
                            <apex:facet name="header">{!$Label.FTH_FUNDNAME}</apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.fundtransName)||(dispReport.fundtransName=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText >{!dispReport.fundtransName}</apex:outputText>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_VALDATE}</div></apex:facet>                        
                            <apex:outputText rendered="{!ISNULL(dispReport.valuateDate)||(dispReport.valuateDate=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText >{!dispReport.valuateDate}</apex:outputText>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$Label.FTH_TRANSTYPE}</apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.transactType)||(dispReport.transactType=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText >{!dispReport.transactType}</apex:outputText>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_UNITPRICE}</div></apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.funitPrice)||(dispReport.funitPrice=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText styleClass="{!IF(CONTAINS(dispReport.funitPrice,'('), 'negativeNumber', 'positiveNumber')}">{!IF(dispReport.funitPrice!=NULL, dispReport.funitPrice, $Label.FTH_NA)}</apex:outputText>
                        </apex:column>
                        <apex:column rendered="{!checkFortune}">
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_INTRATE}</div></apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.intrstRate)||(dispReport.intrstRate=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText >{!IF(dispReport.intrstRate!=NULL, dispReport.intrstRate, $Label.FTH_NA)}</apex:outputText>
                        </apex:column>
                        <apex:column rendered="{!checkFortune}">
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_MATDATE}</div></apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.maturityDate)||(dispReport.maturityDate=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputText >{!IF(dispReport.maturityDate!=NULL, dispReport.maturityDate, $Label.FTH_NA)}</apex:outputText>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_NUMOFUNITS}</div></apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.numOfUnits)||(dispReport.numOfUnits=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputLabel styleClass="{!IF(CONTAINS(dispReport.numOfUnits,'('), 'negativeNumber', 'positiveNumber')}">{!IF(dispReport.numOfUnits!=NULL, dispReport.numOfUnits, $Label.FTH_NA)}</apex:outputLabel>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header"><div class="rightAlignDiv">{!$Label.FTH_FUNDVAL}</div></apex:facet>
                            <apex:outputText rendered="{!ISNULL(dispReport.fundsValue)||(dispReport.fundsValue=='')}" escape="false">&nbsp;</apex:outputText>
                            <apex:outputLabel styleClass="{!IF(CONTAINS(dispReport.fundsValue,')'), 'negativeNumber', 'positiveNumber')}">{!dispReport.fundsValue}</apex:outputLabel>
                        </apex:column>
                    </apex:dataTable>
                    <br/>
                    <table style="width: 100%;border-collapse: collapse;">
                        <tr>
                            <!--  <td style="text-align:right;">
                                {!$Label.FTH_GOTOPAGE}&nbsp;<apex:selectList id="pageSel1" value="{!pageNum}" size="1" onchange="remShield(); showLoadingDialog(); onPageChange();" >
                                        <apex:selectOptions value="{!pagesOpt}"/>
                                </apex:selectList>
                            </td>-->
                            
                            <td align="center">
				                <apex:outputPanel styleClass="navi" id="pagination">
				                    <apex:inputHidden id="firstPageNum" value="{!firstPageNumber}"/>
                                    <apex:inputHidden id="prevPageNum" value="{!prevPageNumber}"/>
                                    <apex:inputHidden id="currPageNum" value="{!currPageNumber}"/>
                                    <apex:inputHidden id="nextPageNum" value="{!nextPageNumber}"/>
                                    <apex:inputHidden id="lastPageNum" value="{!lastPageNumber}"/>
                                    
				                    <apex:commandLink styleClass="activeNavText" rendered="{!CurrPageNumberInt > FirstPageNumberInt}" onclick="javascript:window.scrollTo(0,0); showLoadingDialog(); setCurrPageNumber('{!$Component.pageSel}', '{!$Component.currPageNum}', '{!$Component.firstPageNum}', '{!$Component.prevPageNum}', '{!$Component.nextPageNum}')" reRender="null">
				                       <apex:image styleClass="activeFirstPage" value="/s.gif" width="9" height="10"/></apex:commandLink>
				                    <apex:outputText styleClass="disabledNavText" rendered="{!IF(CurrPageNumberInt == FirstPageNumberInt, true, false)}">
				                       <apex:image styleClass="disabledFirstPage" value="/s.gif" width="9" height="10"/></apex:outputText>
				                    &nbsp;
				                    <apex:commandLink styleClass="activeNavText" rendered="{!CurrPageNumberInt > FirstPageNumberInt}" onclick="javascript:window.scrollTo(0,0); showLoadingDialog(); setCurrPageNumber('{!$Component.pageSel}', '{!$Component.currPageNum}', '{!$Component.prevPageNum}', '{!$Component.prevPageNum}', '{!$Component.nextPageNum}')" reRender="null">
				                       <apex:image styleClass="activePrevPage" value="/s.gif" width="9" height="10"/>{!$Label.Page_Previous}</apex:commandLink>
				                    <apex:outputText styleClass="disabledNavText" rendered="{!IF(CurrPageNumberInt == FirstPageNumberInt, true, false)}">
				                       <apex:image styleClass="disabledPrevPage" value="/s.gif" width="9" height="10"/><span class="disabledNavText">{!$Label.Page_Previous}</span></apex:outputText>
				                    &nbsp;
				                    <apex:selectList id="pageSel" value="{!pageNum}" size="1" onchange="javascript:window.scrollTo(0,0); showLoadingDialog(); setCurrPageNumber('{!$Component.pageSel}', '{!$Component.currPageNum}', '{!$Component.pageSel}', '{!$Component.prevPageNum}', '{!$Component.nextPageNum}')">
				                        <apex:selectOptions value="{!pagesOpt}"/>
				                    </apex:selectList>
				                    &nbsp;
				                    <apex:commandLink styleClass="activeNavText" rendered="{!CurrPageNumberInt<LastPageNumberInt}" onclick="javascript:window.scrollTo(0,0); showLoadingDialog(); setCurrPageNumber('{!$Component.pageSel}', '{!$Component.currPageNum}', '{!$Component.nextPageNum}', '{!$Component.prevPageNum}', '{!$Component.nextPageNum}')" rerender="null">
				                        {!$Label.Page_Next}<apex:image styleClass="activeNextPage" value="/s.gif" width="9" height="10"/></apex:commandLink>
				                    <apex:outputText styleClass="disabledNavText" rendered="{!IF(CurrPageNumberInt == LastPageNumberInt, true, false)}">
				                        <span class="disabledNavText">{!$Label.Page_Next}</span><apex:image styleClass="disabledNextPage" value="/s.gif" width="9" height="10"/></apex:outputText>
				                    &nbsp;
				                    <apex:commandLink styleClass="activeNavText" rendered="{!CurrPageNumberInt<LastPageNumberInt}" onclick="javascript:window.scrollTo(0,0); showLoadingDialog(); setCurrPageNumber('{!$Component.pageSel}', '{!$Component.currPageNum}', '{!$Component.lastPageNum}', '{!$Component.prevPageNum}', '{!$Component.nextPageNum}')" reRender="null">
				                        <apex:image styleClass="activeLastPage" value="/s.gif" width="9" height="10"/></apex:commandLink>
				                    <apex:outputText styleClass="disabledNavText" rendered="{!IF(CurrPageNumberInt == LastPageNumberInt, true, false)}">
				                        <apex:image styleClass="disabledLastPage" value="/s.gif" width="9" height="10"/></apex:outputText> 
				                 </apex:outputPanel>   
				             </td>
                        </tr>
                    </table>
                    
                    <apex:outputLabel id="remarks" value="{!$Label.FTH_REMARKS}"/><br/>
                    
                    <apex:outputText id="txt_remarks" value=" " rendered="{!NotRenderRemark4}">
                    <ul class="bulletedList" >
                        <li><apex:outputLabel id="remark3" value="{!$Label.FTH_REMARK_03}"/></li>
                        <!-- JC NACHOR 12/03/2009 edit remarks -->
                        <!-- <li><apex:outputLabel id="remark1" value="{!$Label.FTH_REMARK_01} {!fundYearPrevious}" /><br/></li> -->
                        <li><apex:outputLabel id="remark1" value="{!$Label.FTH_REMARK_01} {!fundYearPrevious} {!$Label.FTH_REMARK_01b}" /><br/></li>
                        <!-- JC NACHOR 12/03/2009 remove remarks -->
                        <!-- <li><apex:outputLabel id="remark2" value="{!$Label.FTH_REMARK_02}" rendered="{!IF(renderRemark3==true,true,false)}"/></li> -->
                        
                    </ul>
                    </apex:outputText>
                    
                     <apex:outputText id="txt_remarks1" value=" " rendered="{!RenderRemark4}">
                     <ul class="bulletedList" >
                     	 <li><apex:outputLabel id="remark3b" value="{!$Label.FTH_REMARK_03}"/></li>
                        <!-- JC NACHOR 12/03/2009 edit remarks -->
                        <!-- <li><apex:outputLabel id="remark1" value="{!$Label.FTH_REMARK_01} {!fundYearPrevious}" /><br/></li> -->
                        <li><apex:outputLabel id="remark1b" value="{!$Label.FTH_REMARK_01} {!fundYearPrevious} {!$Label.FTH_REMARK_01b}" /><br/></li>
                        <!-- JC NACHOR 12/03/2009 remove remarks -->
                        <!-- <li><apex:outputLabel id="remark2" value="{!$Label.FTH_REMARK_02}" rendered="{!IF(renderRemark3==true,true,false)}"/></li> -->
                     	 <!-- MIKKA ELEPANO 06/03/2010 SR-ES-10017 Fund Transaction Enhancement remarks -->  
                         <li><apex:outputLabel id="remark4" value="{!$Label.FTH_REMARK_04}" /></li> 
                     </ul>
                     </apex:outputText>
                </apex:outputPanel>
            </apex:outputPanel>
        </apex:form>
    </apex:pageBlock>
    <div id="layout-win" class="x-hidden">
	<div class="x-window-header" style="color:white">{!$Label.CHM_PreAckHeader}</div>
	<c:EServicesModalDialogs />
	</div>
</apex:page>