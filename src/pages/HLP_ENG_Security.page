<!--
Version      : 1.0
Company      : Sun Life Financial
Date         : July 24, 2009
Author       : JC Nachor
Description  : Displays the Security Information used in Online Help (English)
History : 
          [Version after modification] <Date Modified - Author - Short description of modification>

-->
<apex:page showheader="false" sidebar="false">
  <!-- c:PortalNavigator navID="400"/ -->
  <apex:stylesheet value="{!URLFOR($Resource.ESW2Style,'newESUIStyle.css')}" />
  <div class="disclaimDiv">
    <apex:pageBlock >
      <div>
        <table width="100%" ><tbody>
          <tr>
            <td>
              <!-- span style="font-weight:bold;font-size:16.0pt">Security</span -->
              <apex:pageBlockSection collapsible="false" title="Security Information" />
                <br/>
                <span>At Sun Life Financial, we take the security and confidentiality of your information seriously. While there is always some risk when doing business using the Internet, we have implemented measures to reduce these risks, such as security policies and standards and strong security technology.</span>
                <br/><br/><br/>
              <!-- span style="font-style:italic;font-weight:bold;font-size:14.0pt">How we protect your information</span -->
              <apex:pageBlockSection collapsible="false" title="How we protect your information" />
                <br/>
                <b>Data Encryption</b>
                  <br/><br/>
                  As a Sun Life Financial customer, once you have logged into a Sun Life Financial site (successfully entering your User ID and password) all web communications and transactions are encrypted between your browser and Sun Life Financial. For your security, you will not be able to authenticate to Sun Life Financial sites if your browser does not support a minimum of 128-bit encryption.  In addition, hard drive encryption is installed on all Sun Life Financial laptops.
                  <br/><br/>
                  We recommend that you use one of the following supported browser versions:
                  <ol>
                    <li>Microsoft Internet Explorer, version 6 or higher - <a href="http://www.microsoft.com/downloads/en/default.aspx" target="_blank"><b>Microsoft Download Centre</b></a></li>
                    <li>Mozilla Firefox, version 2 or higher – <a href="http://www.mozilla.com/en-US/firefox/ie.html" target="_blank"><b>Mozilla Firefox Download Centre</b></a></li>
                  </ol>
                  <br/>
                  Note: Beta or test versions of browsers are not supported.
                  <br/><br/><br/>
                <b>Message Integrity</b>
                  <br/><br/>
                  Our websites use Secure Sockets Layer (SSL), the Internet security standard, to help ensure the integrity and confidentiality of online transactions. The use of SSL helps to prevent information being tampered with during transmission across the Internet.
                  <br/><br/><br/>
                <b>Authentication</b>
                  <br/><br/>
                  Sun Life Financial will only grant access to your account if the proper Access ID and Password are entered.  When you have successfully logged in, your browser will establish a secure SSL connection between your computer and Sun Life Financial servers. Your operating system has been designed to display a security symbol (e.g. a lock) once you have a secure connection.
                  <br/><br/>
                  If you double click on the lock symbol, you will be able view our site certificate. The site certificate is authorized by a third party (VeriSign) that validates the identity of web site. As the e-Services is using a vendor, SalesForce, to provide the online service, you will see the certificate is issued to salesforce.com. If you click on the Issuer Statement button, it will direct to you VeriSign's web site where you can obtain further information about site certificates.
                  <br/><br/><br/><br/>
              <!-- span style="font-style:italic;font-weight:bold;font-size:14.0pt">How you protect your information</span -->              
              <apex:pageBlockSection collapsible="false" title="How you can protect your information" />
                <br/>
                <b>Data Encryption</b>
                  <br/><br/>
                  To ensure your web browser has the strongest encryption to the Sun Life Financial web site, you should use the most current version of the web browser.
                  <br/><br/>
                  We recommend that you use one of the following supported browser versions: 
                  <ol>
                    <li>Microsoft Internet Explorer, version 6 or higher - <a href="http://www.microsoft.com/downloads/en/default.aspx" target="_blank"><b>Microsoft Download Centre</b></a></li>
                    <li>Mozilla Firefox, version 2 or higher – <a href="http://www.mozilla.com/en-US/firefox/ie.html" target="_blank"><b>Mozilla Firefox Download Centre</b></a></li>
                  </ol>
                  <br/>
                <b>Password Protection</b>
                  <br/><br/>
                  Your password is your proof of identity. Using a strong password helps you to protect your identity. A strong password has at least eight characters comprised of a combination of numbers and letters.
                  <br/><br/>
                  It is your responsibility to keep your Sun Life Financial site sign-on information confidential and to prevent its unauthorized use. We recommend you change your password on a regular basis using the appropriate function provided by our sites to do so. Additionally:
                  <ul>
                    <li>Do not leave your computer unattended while connected to Sun Life Financial sites</li>
                    <li>Do not share your passwords with anyone</li>
                    <li>Do not use the same password twice</li>
                    <li>Do not write your passwords down</li>
                    <li>Do not store your passwords in a computer file</li>
                    <li>Do not use personal or identifying information as your password, for example, birth date or social insurance number</li>
                  </ul>
                  <br/>
                <b>Use and Keep Anti-Virus Software Up-to-Date</b>
                  <br/><br/>
                  Your computer can become infected with a virus by opening e-mail attachments, downloading information from websites and sharing files. Installing and using anti-virus software can be an effective way to prevent your computer and the information stored on it from becoming infected. Anti-virus software is designed to detect viruses, cleanse them and ensures they do not spread. With new viruses being created every day, it is important to keep your anti-virus software up to date. Many programs will send your computer automatic updates when a new virus is discovered.
                  <br/><br/><br/>
                <b>Use and Keep Anti-Spyware Software Up-to-Date</b>
                  <br/><br/>
                  Spyware is software installed on your computer without your knowledge or consent. It uses different techniques to collect or track personal information. One technique - called key logging - records keystrokes to capture sensitive data such as user IDs and passwords. Once this information is collected, it can be used to access your online accounts.
                  <br/><br/>
                  There are a number of ways spyware can be installed on your computer - through e-mail attachments or downloading software such as music or video file sharing programs. Anti-spyware software is designed to detect these programs and remove them from your computer. It is important to keep your anti-spyware software up-to-date to ensure threats are identified before they are able to infect your computer.
                  <br/><br/><br/>
                <b>Keep Your Computer's Operating System Up-to-Date</b>
                  <br/><br/>
                  To help to protect your computer from Internet threats, it is important to keep your operating system up-to-date. You can do this by installing the latest operating system security updates and patches provided by your manufacturer. These updates can be sent to you automatically, often provided by operating system manufacturers free of charge.
                  <br/><br/><br/>
            </td>
          </tr>
        </tbody></table>
      </div>
    </apex:pageBlock>
  </div>    
</apex:page>