/*
Version : 
Company : Sun Life Financial
Date    : 03.August.2009
Author  : Wilson Cheng
Description : This is used for saving/updating the owner of the address.
History : 
*/

trigger address_before_insert_trigger on Address__c (before insert) {
    PolicyOwnership.changeAddressOwner(Trigger.new);
}