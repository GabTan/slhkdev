trigger attachment_before_insert on Attachment (before insert) {
	System.debug('Trigger: Attachment Size is:'+Trigger.new.size());
	if (Trigger.isBefore && Trigger.isInsert)
	{
		System.debug('Trigger: Attachment Size is:'+Trigger.new.size());
		StatementManager.updateAttachmentOwner(Trigger.new);
	}
}