/*
Create By: Benjo Malabanan
Date Created; 03/22/2012
Notes: If new transaction created, insert record in Transaction Data Hist Object 
*/

trigger transaction_after_insert_trigger on Transaction__c (after insert) {
	
	// Verify if the action is associated with the right event
    if (Trigger.isAfter && Trigger.isInsert) {
    	
    	List<Transaction_Data_Hist__c> listTransDataHistInsert = new List<Transaction_Data_Hist__c>();
    	
    	for (Transaction__c T : Trigger.new ){
    		
    		//Populate new record for Transaction_Data_Hist__c
    		Transaction_Data_Hist__c objTransDataHist = new Transaction_Data_Hist__c();
    		objTransDataHist.Transaction_Code__c = T.Name;
    	    objTransDataHist.Transaction_Data__c = T.TxnData__c;
    	    objTransDataHist.TransLastModifiedbyID__c = T.LastModifiedById;
    	    listTransDataHistInsert.add(objTransDataHist);
    	}
    	
    	//Insert new record for Transaction_Data_Hist__c
    	insert listTransDataHistInsert;
    }

}