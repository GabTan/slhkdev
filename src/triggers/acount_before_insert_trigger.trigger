/* 

Version        : 1.0 
Company        : Sun Life Financial 
Date           : 07.JAN.2010
Author         : JC Nachor
Description    : Trigger before insert of Account

History        : 1.1  -  01/08/2010  -  JC NACHOR - added validation to guarantee that the email has a value
                 1.2  -  12/02/2011  -  Mikka Elepano - remove email validation PL-ES-11003
                          
*/
trigger acount_before_insert_trigger on Account (before insert) {
    
    //START Wcheng 09.03.09
    //w. clavero 20090929: change of ownership no longer applicable
    //PolicyOwnership.changeAccountOwner(Trigger.New);
    //END Wcheng 09.03.09
    
    //START MIKKA 12.05.2011: 
      //Remove null email validation
      //Instead placed a validation that if Client has an existing User account, null email address not allowed                        
      
      //if (Trigger.isBefore && Trigger.isInsert){
      // JC NACHOR - added validation to guarantee that the email has a value   
      //for(Account acc:Trigger.new){
        //if(acc.PersonEmail == null || acc.PersonEmail == ''){
          //acc.addError(Label.TRI_EMAIL_REQ + ' ' + acc.Client_Id__c);
        //}
      //}
    //}  
    //START MIKKA 12092011
    /*
    Map <id,Account> acctMap = new Map <id,Account>();
    
         if(Trigger.isBefore && Trigger.isInsert){
         	 for(Account acc:Trigger.new){
         	 	acctMap.put(acc.PersonContactId ,acc);
         	 }
         }
         
    List<User> users2Check = [Select Id,ContactId,Email
                              from User
                              where ContactId in: acctMap.keySet()];
    if(users2Check.size()> 0){                         
    	for(Account acc: Trigger.new){
    		for (User u: users2Check){
    			if((acc.PersonEmail == null || acc.PersonEmail == '')&&
    		    	acc.PersonContactId == u.ContactId && 
    		    	u.ContactId != null && acc.PersonContactId !=null){
    		    	acc.addError(Label.TRI_EMAIL_VALIDATION + ' '+ acc.Client_Id__c);
    		    	System.debug('PersonContactId:'+ acc.PersonContactId);
    		    	System.debug('UserContactId:'+ u.ContactId);
    		    }
    		}
    	}
    } 
    //END MIKKA 12092011
/*
    if (Trigger.isBefore && Trigger.isInsert)
    {
        // Transfer the flag -- e_SMS_Indicator__pc to SQP SMS outpOu Flag on Person Accounts
        // Default the SqwarePegSMS__SMS_Task_Activity__c to true
        // Select a.SqwarePegSMS__SMS_Task_Activity__pc, a.SqwarePegSMS__SMS_Opt_Out__pc, a.SqwarePegSMS__SMS_Message__pc, a.SqwarePegSMS__SMS_Flag__pc From Account a
        for (Account acc: Trigger.new)
        {
            if (acc.IsPersonAccount)
            {
                acc.SqwarePegSMS__SMS_Task_Activity__pc = true;
                // e_SMS_Indicator__pc is rx message - opt out is opposite!
                acc.SqwarePegSMS__SMS_Opt_Out__pc = !acc.e_SMS_Indicator__pc;
            }
        }   
    }
    */
}