/* 

Version        : 1.0 
Company        : Sun Life Financial 
Date           : 07.JAN.2010
Author         : JC Nachor
Description    : Trigger to update the user object when the client record is updated

History        : 
                          
*/ 
trigger account_after_update_trigger on Account (after update) {

  // Verify if the action is associated with the right event
  if (Trigger.isAfter && Trigger.isUpdate) {
  	
  	Map<Id, Account> acctMap = new Map<Id, Account>();
  	
  	// for every update in account
    for (Account acc: Trigger.new) {
    	// if the account is a person account & the old and new email values are different
      if (acc.IsPersonAccount 
          
          && acc.PersonEmail != Trigger.oldMap.get(acc.Id).PersonEmail 
        
          && acc.PersonContactId != null) {

        acctMap.put(acc.PersonContactId, acc);

      }
    }
    
    List<User> users2Update = [select Id, ContactId, Email 
                               from User 
                               where ContactId IN: acctMap.keySet()];
                               
    for(User u: users2Update){
    	u.Email = acctMap.get(u.ContactId).PersonEmail;
    }         
    
    if(users2Update.size() > 0){
    	Database.update(users2Update);
    }                               	    	   
    
  }

}