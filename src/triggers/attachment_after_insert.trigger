trigger attachment_after_insert on Attachment (after insert) {
	
	if (Trigger.isAfter && Trigger.isInsert)
	{
		// StatementManager.updateStatements(Trigger.newMap); 
		
		StatementManager.upsertStatements(Trigger.newMap);
	}
}