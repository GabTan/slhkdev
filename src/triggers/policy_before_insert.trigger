trigger policy_before_insert on Policy__c (before insert) {
	/*
	// Trigger to update all positions being inserted with the correct Owner (Customer Portal user)
	// if it exists as an Active User in the system
	*/
	
	// Call to a static method passing Trigger.new variable	
	if (Trigger.isBefore && Trigger.isInsert) 
	PolicyOwnership.updatePolicyOwner(Trigger.new); 
}