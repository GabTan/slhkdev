trigger user_after_update on User (after update) {
	
	// Verify if the action is associated with the right event
	if (Trigger.isAfter && Trigger.isUpdate) {
		Id[] userIDList=new ID[0];
		Id[] userIDList2 =new ID[0];
		for(user u: Trigger.new) {
			// Checks the following conditions before calling the business method
			// 1. User is a Customer Portal User (to be verified if using UserType=='PowerCustomerSuccess' is valid)
			// 2. There is an associated Contact record for the Customer portal user
			// 3. The user is active now
			// 4. The user was inactive earlier 
			// Statements 1, 2, 3 and 4 above together means a customer portal user is being activated
			System.debug('Checking the criteria while user update: User Type is:'+u.UserType+ ' & ContactId is: '+u.ContactId + ' & old status is:'+(Trigger.oldMap.get(u.Id)).IsActive);
			if ((u.UserType=='PowerCustomerSuccess')&&(u.ContactId!=null)&&(u.IsActive==True)&&((Trigger.oldMap.get(u.Id)).IsActive==False)) {
			//if ((u.UserType=='PowerCustomerSuccess')&&(u.ContactId!=null)&&(u.IsActive==True)) {
					//PolicyOwnership.updatePolicyOwner(u.Id);
					System.debug('Met the criteria while user update');
					userIDList.add(u.Id);
			}
			else {
		   	  userIDList2.add(u.Id);
		   }
		}
		  
		
		if(userIDList.size()>0) 
		{
			System.debug('Calling the updatePolicyPwner method');
			PolicyOwnership.updatePolicyOwner(userIDList);
		}
		
		if(userIDList2.size()>0) 
		{
			System.debug('Calling the ValidateInactivePolicyOwners method');
			PolicyOwnership.ValidateInactivePolicyOwners(userIDList2);
		}
	}
}