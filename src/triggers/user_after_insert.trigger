trigger user_after_insert on User (after insert) {
    
    // Verify if the action is associated with the right event
    if (Trigger.isAfter && Trigger.isInsert) {
        // For each user
        Id[] userIDList=new ID[0];
        
        //Start ADD Wcheng 8/17/09
        List<Id> userId = new List<Id>();
        //End ADD Wcheng 8/17/09
        
        for(user u: Trigger.new) {
            // Check whether the new user being created is an Active customer portal user 
            if ((u.UserType=='PowerCustomerSuccess')&&(u.ContactId!=null)&&(u.IsActive==True)) {
                    //PolicyOwnership.updatePolicyOwner(u.Id);
                    userIDList.add(u.Id);
            }
            
            //Start ADD Wcheng 8/17/09
            userId.add(u.Id);
            //End ADD Wcheng 8/17/09
        }
        if(userIDList.size()>0) PolicyOwnership.updatePolicyOwner(userIDList);
        
        //Start ADD Wcheng 8/17/09
        if(userId.size()>0)
        {
            ExtensionUser.insertUserExtension(userId);
        }
        //End ADD Wcheng 8/17/09
        
       /*w. clavero: no longer applicable 
       //w.clavero 20090922 - change the ownership of the portal user's account
       PolicyOwnership.updateUserAccountOwnership(Trigger.new);
       */
    }
}