trigger policy_before_update on Policy__c (before update) {
	/*
	// Trigger to update all positions being updated with the correct Owner (Customer Portal user)
	// if it exists as an Active User in the system
	*/
	// Call to a static method passing Trigger.new variable
	if (Trigger.isBefore && Trigger.isUpdate) 
	{
		System.debug('In policy_before_update trigger');
		PolicyOwnership.updatePolicyOwner(Trigger.new); 	
		PolicyOwnerShip.sendReminders(Trigger.newMap, Trigger.oldMap);
	}
}