trigger statement_before_insert on Statement__c (before insert) {
if((Trigger.IsBefore)&&(Trigger.IsInsert)) {
		StatementManager.updateStatementFields(Trigger.new);
	}
}