/* 

Version        : 1.0 
Company        : Sun Life Financial 
Date           : 07.JAN.2010
Author         : JC Nachor
Description    : Trigger before update of Account

History        : 1.1  -  01/08/2010  -  JC NACHOR - added validation to guarantee that the email has a value
                 1.2  -  12/02/2011  -  Mikka Elepano - remove email validation PL-ES-11003
                          
*/ 
trigger account_before_update_trigger on Account (before update) {

    
    if (Trigger.isBefore && Trigger.isUpdate)
    {
    	// JC NACHOR - added validation to guarantee that the email has a value   
    	// MIKKA ELEPANO Start 12-02-2011 
    	//   - disable null email validation for PL-ES-11003 
    	//for(Account acc:Trigger.new){
    	//	if(acc.PersonEmail == null || acc.PersonEmail == ''){
    	//		acc.addError(Label.TRI_EMAIL_REQ + ' ' + acc.Client_Id__c);
    	//	}
    	//}
    	Map<Id,Account> acctMap = new Map<Id, Account>();
    	    if(Trigger.isBefore && Trigger.isUpdate){
    	    	for(Account acc:Trigger.new){
    	    		acctMap.put(acc.PersonContactId,acc);
    	    	}
    	    	
    	    List<User> users2Check = [Select Id,ContactId,Email
    	                              from User
    	                              where ContactId IN: acctMap.keySet()];          
    	    if(users2Check.size()> 0){ 
    	    	for (Account acc:Trigger.new){
    	    		for (User u:users2Check){
    	    			if (acc.PersonContactId == u.ContactId && (acc.PersonEmail == null || acc.PersonEmail == '')){
    	    				acc.addError(Label.TRI_EMAIL_VALIDATION +' '+ acc.Client_Id__c);
    	    			}
    	    		}	
    	    	}
    	    }
    	    
    	    
    	    }
    	 // Mikka 12/02/2011 - End
    	
    	// This is the code to reset the Password_Last_Changed_date when the 
      // E Service Stats changes to resend...
        PolicyOwnership.resetPortalUserLastPasswordChangedDate(Trigger.oldMap, Trigger.newMap);
    }
    
    //START Wcheng 09.03.09
    //w. clavero 20090929: change of ownership no longer applicable
    //PolicyOwnership.changeAccountOwner(Trigger.New);
    //END Wcheng 09.03.09
        
/*
    if (Trigger.isBefore && Trigger.isUpdate)
    {
        for (Account acc: Trigger.new)
        {
            if (acc.IsPersonAccount)
            {
                System.debug('Update to Person Account');
                if (acc.Send_SMS__pc)
                {
                    System.debug('Sending SMS');
                    // Requested to send an SMS             
                    acc.Send_SMS__pc = false;
                    // Set this flag back to false.
                    
                    // Default this falg to true - always
                    acc.SqwarePegSMS__SMS_Task_Activity__pc = true;
                    
                    // Send SMS Indictor is oppossite of Opt Out Flag
                    acc.SqwarePegSMS__SMS_Opt_Out__pc = !acc.e_SMS_Indicator__pc;
                    
                    // Transfer the text from Contact to Sqware Peg field.
                    acc.SqwarePegSMS__SMS_Message__pc = acc.SMS_Text__pc;
                    // Set to true to tell SqwarePeg to send the message
                    // TODO - remove testing!
                    acc.SqwarePegSMS__SMS_Flag__pc = true;
                }
                else
                {
                    System.debug('Unsetting flags');
                  // reset these flags - so as not to resend messages!!!
                  acc.SqwarePegSMS__SMS_Message__pc = null;
                  acc.SqwarePegSMS__SMS_Flag__pc = false;
                }
            }
        }
    }
  */  
}