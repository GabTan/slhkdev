/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 17.AUG.2009
Author		: W. Clavero
Description	: Test Coverage for the CurrencyCodeHandler Apex Class.
History		:
*/
@isTest
private class CurrencyCodeHandler_Test {

	static testMethod void testCurrencyCodeHandler() {
		List<Config_Translation_Lookups__c> curList = new List<Config_Translation_Lookups__c>();
		curList.add(new Config_Translation_Lookups__c(Code_Type__c='SYS_LOOKUP_MST', Code__c='CURRENCY_AAA', Language__c='en_US', Description__c='AAA'));
		curList.add(new Config_Translation_Lookups__c(Code_Type__c='SYS_LOOKUP_MST', Code__c='CURRENCY_BBB', Language__c='en_US', Description__c='BBB'));
		curList.add(new Config_Translation_Lookups__c(Code_Type__c='SYS_LOOKUP_MST', Code__c='CURRENCY_CCC', Language__c='en_US', Description__c='CCC'));
		insert curList;
				
		List<String> enC = CurrencyCodeHandler.getENCurrencyCodes();
		Map<String, String> trC = CurrencyCodeHandler.getTranslatedCurrencyCodes();
	}
}