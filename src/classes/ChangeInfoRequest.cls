/*
Version : 1.0
Company : Sun Life Financial
Date    : 21.July.2009
Author  : Wilson Cheng
Description : This is used for temporary container for saving and retrieving xml data for
              change contact info.
History : 
*/

public class ChangeInfoRequest
{
    public string RequestType {get; set;}
    public string ReferenceNo {get; set;}
    public string PolicyNo {get; set;}
    public string SubmissionDateTime {get; set;}
    public string HomePhone {get; set;} 
    public string HomePhoneChange {get; set;}
    public string OfficePhone {get; set;}
    public string OfficePhoneChange {get; set;}
    public string MobilePhone {get; set;}
    public string MobilePhoneChange {get; set;}
    public string EmailAddValue {get; set;}
    public string EmailAddChange {get; set;}
    public string AddressChange {get; set;}
    public string AddressLine1 {get; set;}
    public string AddressLine2 {get; set;}
    public string AddressLine3 {get; set;}
    public string AddressLine4 {get; set;}
    public string AddressLine5 {get; set;}
    public string AddressCity {get; set;}
    public string AddressProv {get; set;}
    public string AddressZip {get; set;}
    public string AddressCountry {get; set;}
    public list<string> Policies {get; set;}
    
    public ChangeInfoRequest()
    {
    	Policies = new List<string>();
    }
}