/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 17.AUG.2009
Author		: W. Clavero
Description	: Test Coverage for the ChangePasswordContainerCtrl Apex Class.
History		: 
*/
@isTest
private class ChangePasswordContainerCtrl_Test {

    static testMethod void testChangePasswordContainerCtrl() {
        PageReference pgRef = Page.ChangePasswordContainer;
        Test.setCurrentPage(pgRef);
        ApexPages.currentPage().getParameters().put('reg', '1');
        ChangePasswordContainerCtrl ctrlr = new ChangePasswordContainerCtrl();
    }
}