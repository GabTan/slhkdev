/*
Version        : 1.0 
Company        : Sun Life Financial 
Date           : 01.FEB.2010
Author         : Gilbert de Leon
Description    : Controller in redirecting the Customer Portal user to an appropriate page
History        : 
                 version 1.1 - JC NACHOR     02/01/10 - Change locking out logic. Compare sysdate against Resend Date instead of Last Login Date
*/
public with sharing class RedirectCPController {    
    public String strOrgId = System.Userinfo.getOrganizationId();
    private String strCPURL;          
    static final String strTargetURL = '/home/home.jsp';
    private String strPortalId;
    public String strSessionId = System.Userinfo.getSessionId();
    public String strProcess = 'Redirect2CP';
    private String strURL = '';       
    private Double doubConfigGracePeriod; 
    // The Override variables are for the testMethods
    public static Boolean bOverride1 {get; set;} 
    public static Boolean bOverride2 {get; set;}
    public static Boolean bOverride3 {get; set;}
    public static Boolean bOverride4 {get; set;}     
    private String strUserId = System.UserInfo.getUserId();
    User u = [select Id, Last_Disclaimer_Accept_Time__c, Last_Login_Date__c, Last_Password_Change_Date__c, 
                First_Login_Date__c, Days_Since_Reset__c, Resend_Date__c, CreatedDate, Locked__c
                from user where id = :strUserId limit 1];
    User_Extension__c[] userExtensions = [select id, Reset__c, User_Deactivated__c from User_Extension__c  where User__c = :u.id limit 1];            
    Config_Global_Settings__c[] configSettings = [select USR_First_Login_AGP__c, USR_Forgot_Pwd_Retry_Interval__c, USR_Max_Login_Attempts__c,
    										CPURL__c, PortalId__c
                                            from Config_Global_Settings__c
                                            order by CreatedDate desc
                                            limit 1];   
    public RedirectCPController()
    { 
        bOverride1 = false;
        bOverride2 = false;
        bOverride3 = false;
        bOverride4 = false;
        strCPURL = configSettings[0].CPURL__c;
        strPortalId = configSettings[0].PortalId__c;
    }
    public void doDMLUpdates()
    {
        String strValue = validateEntry();
        if((strValue == 'AccountLocked' && u.Locked__c == false) || bOverride1)
        {
                    String strLockedReason = 'No Login After '+ String.valueOf(configSettings[0].USR_First_Login_AGP__c) + ' Days';
                    User updateUser = new User(id = u.id, Locked__c = true, Locked_Date__c = System.now(), 
                                                Lock_Reason__c = strLockedReason);   
                if(!bOverride2)
                {                                                               
                    Database.update(updateUser);
                }           
                if((userExtensions.size()>0) || bOverride1)
                {
                    User_Extension__c updateUserExtension = new User_Extension__c(id = userExtensions[0].id, User__c = u.id, User_Deactivated__c = true);
                    Database.update(updateUserExtension);  
                }
                else
                {
                    User_Extension__c updateUserExtension = new User_Extension__c(User__c = u.id, User_Deactivated__c = true);
                    Database.insert(updateUserExtension);
                }   
        }
        if(strValue == 'Redirect2CP')
        {
        	User updateUser = new User(id = u.id, Last_Login_Date__c = System.now());
            if(!bOverride2)
            {                                                               
                Database.update(updateUser);
            }        	
        } 
    }   
    public String validateEntry()
    {
        doubConfigGracePeriod = Double.valueOf(configSettings[0].USR_First_Login_AGP__c)*1000*60*1440;
        if((userExtensions.size()> 0 && userExtensions[0].User_Deactivated__c == true) || bOverride1)  
        {
            strProcess = 'Deactivated';         
        } 

        if((u.Last_Login_Date__c != null)|| bOverride1)
        {                    	
            if((u.Resend_Date__c > u.Last_Login_Date__c) || bOverride1) 
            {
                /* START EDIT JC NACHOR 02/01/10 - - Change locking out logic. Compare sysdate against Resend Date instead of Last Login Date */            	
                //if(bOverride1 || (System.now().getTime() - u.Last_Login_Date__c.getTime() > doubConfigGracePeriod)) 
                if(bOverride1 || (System.now().getTime() - u.Resend_Date__c.getTime() > doubConfigGracePeriod))
                /* END EDIT JC NACHOR 02/01/10 - - Change locking out logic. Compare sysdate against Resend Date instead of Last Login Date */
                {           
                    strProcess = 'AccountLocked';
                }
            }  
        }   
        //   u.Last_Login_Date__c == null  
        else
        {
	     	if((u.Resend_Date__c != null)|| bOverride1) 
	     	{       
	            if((System.now().getTime() - u.Resend_Date__c.getTime() > doubConfigGracePeriod)|| bOverride1) 
	            {           
	                strProcess = 'AccountLocked';
	            }
	     	}       	
        }   
        if((u.Locked__c == true) || (bOverride1 && !bOverride4))
        {
            strProcess = 'Deactivated';
        }         
        return strProcess;              
    }
    public PageReference getRedirect()    
    { 
        PageReference pref;  
        strProcess = validateEntry();
        strURL = strCPURL;
        strURL += '/secur/frontdoor.jsp?';
        strURL += 'sid=' + strSessionId + '&';
        strURL += 'orgId='+ strOrgId +'&';                                  
        if((strProcess == 'Redirect2CP' && u.First_Login_Date__c != null) || bOverride1)
        {
            strURL += 'portalId=' + strPortalId;
            pref = new PageReference(strURL);
        }
        else if((strProcess == 'Redirect2CP' && u.First_Login_Date__c == null) || bOverride2)
        {
            strURL += 'portalId=' + strPortalId + '&';
            strURL += 'retURL='+ 'apex/FirstTimeAuthentication'; 
            pref = new PageReference(strURL);       
        }
        else if ((strProcess == 'AccountLocked' || strProcess == 'Deactivated' ) || bOverride3)
        {
            pref = new PageReference(Site.getPrefix() + '/AccountLocked');  
        }
        return pref;        
    }
    
    public String getRedirectURL()    
    { 
        strProcess = validateEntry();
        strURL = strCPURL;
        strURL += '/secur/frontdoor.jsp?';
        strURL += 'sid=' + strSessionId + '&';
        strURL += 'orgId='+ strOrgId +'&';                                  
        if((strProcess == 'Redirect2CP' && u.First_Login_Date__c != null) || bOverride1)
        {
            strURL += 'portalId=' + strPortalId;
        }
        else if((strProcess == 'Redirect2CP' && u.First_Login_Date__c == null) || bOverride2)
        {
            strURL += 'portalId=' + strPortalId + '&';
            strURL += 'retURL='+ 'apex/FirstTimeAuthentication'; 
        }
        else if ((strProcess == 'AccountLocked' || strProcess == 'Deactivated' ) || bOverride3)
        {
            strURL = Site.getPrefix() + '/AccountLocked';  
        }
        return strURL;        
    }       
}