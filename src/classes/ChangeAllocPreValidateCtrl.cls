/*
Version     : 1.4
Company     : Sun Life Financial
Date        : 28.JULY.2009
Author      : Dorothy Anne L. Baltazar
Description : Controller for Change of Fund Allocation transaction prevalidations.
History     : 
              1.1 <9/15/09 - Robert Andrew Almedina - added code that directs the page to home page when cancel button is clicked>
              1.2 - 10.Oct.2009 - WCheng - IL-ES-00036 - Added method which handles the reject button navigation
              1.3 - 24.Nov.2009 - JC NACHOR - add parameter filtering to prevent cross site scripting
              1.4 - 25.NOV.2009 - W. Clavero - Updated for additional coverage
*/
public with sharing class ChangeAllocPreValidateCtrl {
    TransactionValidator CMTxnValidator = new TransactionValidator();
    public String homeURL {get; set;}
    public String chmURL {get; set;}
    public String policyNumber {get; set;}
    
    //For checking if Transaction is valid
    public Boolean errorCastIron;
    public Boolean errorExistRequest;
    public Boolean errorSinglePremium; 
    public Boolean errorLapseStartDate;
    
    //For displaying error messages on the page; initially set to false
    public Boolean renderErrorCastIron {get; set;}
    public Boolean renderErrorExistRequest {get; set;}
    public Boolean renderErrorSinglePremium {get; set;}
    public Boolean renderErrorLapseStartDate {get; set;}
    
    //public String policyNumber;
    public String acctClientId;
    //public String userId = userInfo.getUserId();
    public String tempURL {get; set;}
    
    public String prevPage = System.currentPageReference().getParameters().get('prv');
    
    public String getPolicyNumber()
    {
        policyNumber = System.currentPageReference().getParameters().get('policyID');
        return policyNumber;
    }

    //START IL-ES-00036 Wcheng 10/09/09
    public String getNavTxnLog()
    {
        string espParam;
        
        if(prevPage == '1')
        {
            espParam = ESPortalNavigator.NAV_URL_HOM;
        }
        else if (prevPage == '2')
        {
            espParam = ESPortalNavigator.NAV_URL_HOM_POLICY+'?policyID='+getpolicyNumber();
        }
        else
        {
            espParam = ESPortalNavigator.NAV_URL_HOM_POLICY+'?policyID='+getpolicyNumber();
        }
        
        return espParam;
    }
    //START IL-ES-00036 Wcheng 10/09/09
    
    
    public String getAcctClientId()
    {
        Policy__c clientID = [SELECT Client__r.Client_ID__c from Policy__c where Policy_Number__c =: policyNumber LIMIT 1];
        if (clientID != NULL)
        {
            acctClientId = clientID.Client__r.Client_ID__c;
        }
        return acctClientId;
    }
    
    // JC NACHOR 11/24/09 - determines if the current user has rights to the Policy being accessed 
    public PageReference Security() 
    {
        // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
        PageReference pRefRedirect     = Page.NoAccessToPolicy;
        PageReference pProfileNoAccess = Page.UserAccessError;
        
        if(ApexPages.currentPage().getParameters().size() != 2){
          return pRefRedirect;
        }
        
        String policyId = ApexPages.currentPage().getParameters().get('policyID');            

        if(ESAccessVerifier.isCurrentUsersPolicy((policyId==null)?'':policyId) 
        && (ApexPages.currentPage().getParameters().get('prv') != null)
        && (ApexPages.currentPage().getParameters().get('prv').length() == 1)){
          return null;
        } else {
          return pRefRedirect;
        }
    }   
    
    public ChangeAllocPreValidateCtrl()
    {
        String pId;
        getPolicyNumber();
        
        String transType = 'CM';
        String transStatus = 'Submitted';
        
        errorCastIron = !(CMTxnValidator.checkCastIron());
        errorExistRequest = CMTxnValidator.checkExistRequest(policyNumber, transType, transStatus);
        errorSinglePremium = CMTxnValidator.checkSinglePremium(policyNumber);
        errorLapseStartDate = CMTxnValidator.checkLapseStartDate(policyNumber);
        
        if ((errorCastIron == false) && (errorExistRequest == false) && (errorSinglePremium == false) && (errorLapseStartDate == false))
        {
            renderErrorCastIron         = false;
            renderErrorExistRequest     = false;
            renderErrorSinglePremium    = false;
            renderErrorLapseStartDate   = false;
            tempURL = ESPortalNavigator.NAV_URL_CHM_NOTES;
            tempURL = tempURL + '?policyID=' + policyNumber;
            
        }
        else
        {
            renderErrorCastIron         = errorCastIron;
            renderErrorExistRequest     = errorExistRequest;
            renderErrorSinglePremium    = errorSinglePremium;
            renderErrorLapseStartDate   = errorLapseStartDate;
        }
        
        //Returns the URL for Accept & Reject buttons in Important Notes page
        chmURL = ESPortalNavigator.NAV_URL_CHM_INPUT;
        chmURL = chmURL + '?policyID=' + policyNumber;
        //homeURL = ESPortalNavigator.NAV_URL_SVC_TXN;
        homeURL = ESPortalNavigator.NAV_URL_HOM; // directs the page to the home page when reject button is clicked
    }
    
    public String getChmURL()
    {
        return chmURL;
    }
    
    public String getHomeURL()
    {
        return homeURL;
    }
    
    public static testMethod void testChangeAllocPreValidateCtrl()
    {
        Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
        Map<String,Schema.RecordTypeInfo>AccountMapByName = AccountSchema.getRecordTypeInfosByName();
        Schema.RecordTypeInfo AccountRT = AccountMapByName.get('Person Account');
        
        String PersonAccountTypeId = '';
        if(AccountRT != null){
             PersonAccountTypeId =AccountRT.getRecordTypeId();
        }
        
        Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
                PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
                FirstName='AcME', LastName='GX', RecordTypeId =  PersonAccountTypeId, 
                Client_Id__c = '!@#$%^&*()', Client_ID__pc = 'abcde12345'
                );
                    
        Insert accountSuccessTest;      
        
        Schema.DescribeSObjectResult PolicySchema = Schema.SObjectType.Policy__c;
        Map<String,Schema.RecordTypeInfo>PolicyMapByName = PolicySchema.getRecordTypeInfosByName();
        Schema.RecordTypeInfo PolicyRT = PolicyMapByName.get('Traditional');
            
        String TradPolicyTypeId = '';
        if(PolicyRT != null){
            TradPolicyTypeId =PolicyRT.getRecordTypeId();
        }
            
        Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountSuccessTest.id, policy_number__c = '887888877',
                                        RecordTypeId = TradPolicyTypeId, Policy_Status_Picklist_Hidden__c = '1', 
                                        Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
                                        Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
                                        Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
                                        Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
                                        Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
                                        Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
                                        Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = '1',
                                        Branch_Number__c = '234', Account_Number__c = 'Account 1', 
                                        Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
                                        Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
                                        Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
                                        Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
                                        Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
                                        Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
                                        Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
                                        Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
                                        Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
                                        Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
                                        Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
                                        Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
                                        Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
                                        Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
                                        Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00                                    
                                        );      
        insert policySuccessTest;
        
        Transaction__c transTempCM = new Transaction__c(Reference_Number__c ='Ref0123456CM',
                                                        Request_Type__c ='S0001',
                                                        CurrencyIsoCode ='HKD',
                                                        SORStatus__c = 'Submitted',
                                                        Policy_ID__c = policySuccessTest.Policy_Number__c,
                                                        Active__c = true,
                                                        Client_Id__c = accountSuccessTest.Client_Id__c,
                                                        Client__c = accountSuccessTest.Id,
                                                        TxnData__c ='<Txn RequestType="S0001" ReferenceNo="ECM-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
                            '<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
                            '<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
                            '<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
                            '<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
        insert transTempCM;
        
        Transaction__c transTempCP = new Transaction__c(Reference_Number__c ='Ref0123456CP',
                                                        Request_Type__c ='CHGALLOCAT',
                                                        CurrencyIsoCode ='HKD',
                                                        SORStatus__c = 'Submitted',
                                                        Policy_ID__c = policySuccessTest.Policy_Number__c,
                                                        Active__c = true,
                                                        Client_Id__c = accountSuccessTest.Client_Id__c,
                                                        Client__c = accountSuccessTest.Id,
                                                        TxnData__c ='<Txn RequestType="CHGALLOCAT" ReferenceNo="ECP-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
                            '<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
                            '<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
                            '<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
                            '<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
        insert transTempCP;
        
        Transaction__c transTempFS = new Transaction__c(Reference_Number__c ='Ref0123456FS',
                                                        Request_Type__c ='FUNDSW',
                                                        CurrencyIsoCode ='HKD',
                                                        SORStatus__c = 'Submitted',
                                                        Policy_ID__c = policySuccessTest.Policy_Number__c,
                                                        Active__c = true,
                                                        Client_Id__c = accountSuccessTest.Client_Id__c,
                                                        Client__c = accountSuccessTest.Id,
                                                        TxnData__c ='<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
                            '<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
                            '<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
                            '<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
                            '<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
        insert transTempFS;
        
        Fund_Transaction_History__c fundTrans = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000123',
                                                     Policy__c=policySuccessTest.Id, 
                                                     Fund_Code__c='BEAEH', Transaction_Type__c='LSI', 
                                                     Fund_Price_Date__c=System.today(), Maturity_Date__c=System.today(), 
                                                     Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75 );
        insert fundTrans;
        
        pageReference pg = page.ChangeAllocPreValidation;
        Test.setCurrentPage(pg);
            
        ApexPages.currentPage().getParameters().put('policyID',policySuccessTest.policy_number__c);
        
        ChangeAllocPreValidateCtrl temp = new ChangeAllocPreValidateCtrl();
        String home = temp.getHomeURL();
        String chm  = temp.getChmURL();
        temp.policyNumber = temp.getPolicyNumber();
        String tempUserId = accountSuccessTest.Client_Id__c;
        String tempId = temp.getAcctClientId();
        
        
        TransactionValidator tempTV = new TransactionValidator();
        tempTV.checkCPExistRequest(tempUserId, 'CP', 'Submitted');
        tempTV.checkNoActiveInforcePolicy(tempUserId);
        
        tempTV.checkExistRequest(temp.policyNumber, 'FS', 'Submitted');
        tempTV.checkExistRequest(temp.policyNumber, 'CM', 'Submitted');
        tempTV.checkLapseStartDate(temp.policyNumber);
        tempTV.checkNonES(temp.policyNumber);
        tempTV.checkPendingFundTxn(temp.policyNumber);
        tempTV.checkSinglePremium(temp.policyNumber);
        
        temp.Security();
        temp.getNavTxnLog();
    } 
    
}