/*
Company     : Sun Life Financial
Date        : 08/14/2009
Author      : W. Clavero
Description : Controller for the TransactionOnlineHelp page.
History     : 
*/
public class TransactionOnlineHelpCtrl {

	public String helpURL {get; set;}
	
	public TransactionOnlineHelpCtrl() {
	    if (Userinfo.getLanguage().equals(ESPortalNavigator.LANG_CHI_TRADITIONAL)) {
            this.helpURL = ESPortalNavigator.NAV_URL_HLP_SVC_CHI;
	    } else {
	        this.helpURL = ESPortalNavigator.NAV_URL_HLP_SVC_ENG;
	    }	
	}

}