/*
	Version	   : 1.0
    Company    : Sun Life Financial
    Date       : 28.AUG.2009
    Author     : W. Clavero
    Description: Test Coverage for the FundTxnHstyCustomSortMerge Apex Class.
    History    : 
    
    [Version after modification] <Date Modified - Author - Short description of modification>
    
*/
@isTest
private class FundTxnHstyCustomSortMerge_Test {

    static testMethod void testFundTxnHstyCustomSortMerge() {
        //adjust limit for test cov
        FundTxnHstyCustomSortMerge fthSort = new FundTxnHstyCustomSortMerge();
		fthSort.maxListSize = 5;        
        
        //sort fund txn hsty records
        List<FundTxnHstyValues> fthVals = new List<FundTxnHstyValues>();
        List<List<FundTxnHstyValues>> fthList = new List<List<FundTxnHstyValues>>();
        
        for (Integer i = 0; i < 23; i++) {
        	FundTxnHstyValues fthRec = new FundTxnHstyValues('fundName' + i, System.today() , 'tx' + i, '0.00', '0.00', '01/01/2009', '1', '0.00');
        	if ((fthVals.size() == 7) || (i == 6) || (i == 15)) {
        		fthList.add(fthVals);
        		fthVals = new List<FundTxnHstyValues>();
        	}
        	fthVals.add(fthRec);
        }
        fthList.add(fthVals);
        //fund name asc
        fthList = fthSort.sortFundTxnHstyRecords(fthList, FundTxnHstyCustomSortMerge.COLUMN_FUND_NAME, true);
        //fund name desc
        fthList = fthSort.sortFundTxnHstyRecords(fthList, FundTxnHstyCustomSortMerge.COLUMN_FUND_NAME, false);

        //txn type asc
        fthList = fthSort.sortFundTxnHstyRecords(fthList, FundTxnHstyCustomSortMerge.COLUMN_TXN_TYPE, true);
        //txn type desc
        fthList = fthSort.sortFundTxnHstyRecords(fthList, FundTxnHstyCustomSortMerge.COLUMN_TXN_TYPE, false);
        
        
        //sort fund names
        List<Display_Translator> dtList = new List<Display_Translator>(); 
        for (Integer i = 0; i < 12; i++) {
        	Display_Translator dtRec = new Display_Translator('CODE_TYPE', 'CODE_VALUE_' + i);
        	dtList.add(dtRec);
        }
        
        //asc
		dtList = fthSort.translateFundNameSort(dtList, FundTxnHstyCustomSortMerge.COLUMN_TRANSLATE_FUND, true);        
        //desc     
		dtList = fthSort.translateFundNameSort(dtList, FundTxnHstyCustomSortMerge.COLUMN_TRANSLATE_FUND, false);              
    }
}