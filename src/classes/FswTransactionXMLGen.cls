/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Ryan Dave Wilson
Description	: A class for generating XML for Fund Switching.
History	:
	[1.1] <22.JUL.2009 - Ryan Wilson - Added the writeFSWxml method and test coverage>

*/

public with sharing class FswTransactionXMLGen {
	public SwitchOutRequest fsSwitchOut = new SwitchOutRequest();
	public SwitchInRequest fsSwitchIn = new SwitchInRequest();
	public FundSwitchRequest FSWrequest = new FundSwitchRequest();
	Decimal amnt;
	
	public String writeFSWxml(FundSwitchRequest fsXml){
		XmlStreamWriter fswXml = new XmlStreamWriter();
		
		fswXml.writeStartElement(null,TransactionsReqConstant.TXN,null); //start of Txn element
		fswXml.writeAttribute(null,null,TransactionsReqConstant.REQUEST_TYPE,fsXml.reqType);
		fswXml.writeAttribute(null,null,TransactionsReqConstant.REFERENCE_NO,fsXml.refNum);
		fswXml.writeAttribute(null,null,TransactionsReqConstant.POLICY_NO,fsXml.policyNo);
		fswXml.writeAttribute(null,null,TransactionsReqConstant.SUBMISSION_DATE_TIME,fsXml.subDateTime);
		
		fswXml.writeStartElement(null,TransactionsReqConstant.SW_OUT,null); //start of SwitchOut element
		for(integer i=0; i < fsXml.switchOut.size(); i++){
			fswXml.writeStartElement(null,TransactionsReqConstant.FUND,null); //start of Fund Element
			fswXml.writeAttribute(null,null,TransactionsReqConstant.CODE,fsXml.switchOut[i].code);
			fswXml.writeAttribute(null,null,TransactionsReqConstant.NAME,XMLDataHandler.escapeXMLString(fsXml.switchOut[i].name));
			fswXml.writeAttribute(null,null,TransactionsReqConstant.PERCENT,fsXml.switchOut[i].percent);
			fswXml.writeEndElement(); //End of fund element
		}
		fswXml.writeEndElement(); //End of SwitchOut Element
		
		fswXml.writeStartElement(null,TransactionsReqConstant.SW_IN,null); //start of SwitchIn element
		for(integer x=0; x < fsXml.switchIn.size(); x++){
			fswXml.writeStartElement(null,TransactionsReqConstant.FUND,null); //start of Fund Element
			fswXml.writeAttribute(null,null,TransactionsReqConstant.CODE,fsXml.switchIn[x].code);
			fswXml.writeAttribute(null,null,TransactionsReqConstant.NAME,XMLDataHandler.escapeXMLString(fsXml.switchIn[x].name));
			fswXml.writeAttribute(null,null,TransactionsReqConstant.PERCENT,fsXml.switchIn[x].percent);
			fswXml.writeAttribute(null,null,TransactionsReqConstant.ESTIMATED_AMOUNT,fsXml.switchIn[x].estiAmount);
			fswXml.writeAttribute(null,null,TransactionsReqConstant.CREATE_NEW,(fsXml.switchIn[x].createNew == True? TransactionsReqConstant.YES: TransactionsReqConstant.NO));
			fswXml.writeEndElement(); //End of fund element
		}
		fswXml.writeEndElement(); //End of SwitchIn Element
		fswXml.writeEndElement(); //End of Txn Element
		
		String XmlOutput = fswXml.getXmlString();
		System.debug('*WRITE XML: '+XmlOutput.length());
		System.debug('*WRITE XML: '+XmlOutput.replace('<', '«').replace('>', '»'));
		fswXml.close();
		return XmlOutput;
	}
	
	public FundSwitchRequest readFSWxml(String fsRequest){
		System.debug('*TestXML: '+fsRequest);
		XmlStreamReader reader = new XmlStreamReader(fsRequest);
		
		while(reader.hasNext()){
			if(reader.getEventType() == XmlTag.START_ELEMENT){
				if (TransactionsReqConstant.TXN == reader.getLocalName()){
					FSWrequest.reqType = reader.getAttributeValue(null,TransactionsReqConstant.REQUEST_TYPE);
					FSWrequest.refNum = reader.getAttributeValue(null,TransactionsReqConstant.REFERENCE_NO);
					FSWrequest.policyNo = reader.getAttributeValue(null,TransactionsReqConstant.POLICY_NO);
					FSWrequest.subDateTime = reader.getAttributeValue(null,TransactionsReqConstant.SUBMISSION_DATE_TIME);		
						
				while(reader.hasNext()){
						if(reader.getEventType() == XmlTag.END_ELEMENT){
							if(TransactionsReqConstant.TXN == reader.getLocalName()){
								break;
							}
						}
						if(reader.getEventType() == XmlTag.START_ELEMENT){
							if(TransactionsReqConstant.SW_OUT == reader.getLocalName()){
								parseSwitchOut(reader);
							}
							if(TransactionsReqConstant.SW_IN == reader.getLocalName()){
								parseSwitchIn(reader);
							}
						}
						reader.next();
						
					}
				}
				else{
					break;
				}
			}
			reader.next();
			
		}
		System.debug('*SwitchOutFunds: '+FSWrequest);
		return FSWrequest;
	}
	
	public void parseSwitchOut(XmlStreamReader reader){
		while(reader.hasNext()){
			if(reader.getEventType() == XmlTag.END_ELEMENT)
			{
				if(TransactionsReqConstant.SW_OUT == reader.getLocalName()){
					break;
				}
				
			}
			if(reader.getEventType() == XmlTag.START_ELEMENT){
				if(TransactionsReqConstant.FUND == reader.getLocalName()){
					SwitchOutRequest fsSwitchOut = new SwitchOutRequest();
					fsSwitchOut.code = reader.getAttributeValue(null,TransactionsReqConstant.CODE);
					fsSwitchOut.name = XMLDataHandler.unescapeXMLString(reader.getAttributeValue(null,TransactionsReqConstant.NAME));
					fsSwitchOut.percent = reader.getAttributeValue(null,TransactionsReqConstant.PERCENT);
					FSWrequest.switchOut.add(fsSwitchOut);
					//System.debug('*SwitchOutFunds: '+FSWrequest);
				}
			}
			reader.next();
		}
		
	}
	
	public void parseSwitchIn(XmlStreamReader reader){
		
		while(reader.hasNext()){
			if(reader.getEventType() == XmlTag.START_ELEMENT){
				if(TransactionsReqConstant.FUND == reader.getLocalName()){
					SwitchInRequest fsSwitchIn = new SwitchInRequest();
					fsSwitchIn.code = reader.getAttributeValue(null,TransactionsReqConstant.CODE);
					fsSwitchIn.name = XMLDataHandler.unescapeXMLString(reader.getAttributeValue(null,TransactionsReqConstant.NAME));
					fsSwitchIn.percent = reader.getAttributeValue(null,TransactionsReqConstant.PERCENT);
					//fsSwitchIn.estAmntDecimal = decimal.valueOf(reader.getAttributeValue(null,TransactionsReqConstant.ESTIMATED_AMOUNT));
					amnt = decimal.valueOf(reader.getAttributeValue(null,TransactionsReqConstant.ESTIMATED_AMOUNT));
					fsSwitchIn.estAmntDecimal = amnt.setScale(2, System.RoundingMode.HALF_DOWN);
					fsSwitchIn.estiamount = ESAmountFormatter.FormatAmount(decimal.valueOf(reader.getAttributeValue(null,TransactionsReqConstant.ESTIMATED_AMOUNT)));
					fsSwitchIn.createNew = (reader.getAttributeValue(null,TransactionsReqConstant.CREATE_NEW) == TransactionsReqConstant.YES? True: False);
					FSWrequest.switchIn.add(fsSwitchIn);
					//System.debug('*SwitchInFunds: '+FSWrequest);
				}
			}
			if(reader.getEventType() == XmlTag.END_ELEMENT)
			{
				if(TransactionsReqConstant.SW_IN == reader.getLocalName()){
					break;
				}
				
			}
			reader.next();
		}
		
	}
	
	static testMethod void FswTransactionXMLGen_test(){
		FswTransactionXMLGen FswXmlGen = new FswTransactionXMLGen();
		FundSwitchRequest FSWvar = new FundSwitchRequest();
		SwitchOutRequest fsSwOut = new SwitchOutRequest();
		
		String sampleXml = '<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life%COMMA% China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678" createnew="N"/></switchin></Txn>';
		
		FswXmlGen.readFSWxml(sampleXml);
		System.debug('*Test XML: '+FswXmlGen);
		
		//insert values on the FundSwitchRequest variables to test the write XML method.
		String expectResult = '<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="FSW1" name="Fund1" percent="70"></fund></switchout>'+
							'<switchin><fund code="FSW2" name="Fund2" percent="50" estamount="1234" createnew="Y"></fund>'+
							'<fund code="FSW3" name="Fund3" percent="50" estamount="4323" createnew="N"></fund></switchin></Txn>';
		
		FSWvar.reqType = 'FUNDSW';
		FSWvar.refNum = 'EFS-298000005-090421';
		FSWvar.policyNo = '1234567890';
		FSWvar.subDateTime = '21/04/2009 13:10:52';
		fsSwOut.code = 'FSW1';
		fsSwOut.name = 'Fund1,sample';
		fsSwOut.percent = '70';
		FSWvar.switchOut.add(fsSwOut);
		SwitchInRequest fsSwInA = new SwitchInRequest();
		fsSwInA.code = 'FSW2';
		fsSwInA.name = 'Fund2';
		fsSwInA.percent = '50';
		fsSwInA.estiamount = '1234';
		fsSwInA.createNew = True;
		FSWvar.switchIn.add(fsSwInA);
		SwitchInRequest fsSwInB = new SwitchInRequest();
		fsSwInB.code = 'FSW3';
		fsSwInB.name = 'Fund3';
		fsSwInB.percent = '50';
		fsSwInB.estiamount = '4323';
		fsSwInB.createNew = False;
		FSWvar.switchIn.add(fsSwInB);
		
		String result = FswXmlGen.writeFSWxml(FSWvar);
		if(expectResult == result){
			System.debug('TRUE');
		}else{
			System.debug('FALSE');
		}
	}

}