public class PasswordChangedController {

	public String esHomePage {get; set;}

    public PasswordChangedController()
    {
		
    }
    
    public PageReference passwordChanged()
    {  
        User u = [select Id, Last_Disclaimer_Accept_Time__c, LastLoginDate, Last_Password_Change_Date__c
                    from user where id = :UserInfo.getUserId()];
                     
        u.Last_Password_Change_Date__c = System.now();  
        update u;
        //wlc 20090812: redirect to new eServices Home page
        esHomePage = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_MAIN);
        
        return null;
        // Update the User -
        // Return to the home page
        //PageReference pRef = new PageReference('/home/home.jsp');
        //pRef.setRedirect(true);
        //return pRef;
    }
    
    public static TestMethod void testPasswordChangedController()
    {
    	PasswordChangedController controller = new PasswordChangedController();
    	controller.passwordChanged();
    }
}