/* Custom Beneficiary Place Holder */
 	public class customBeneficiaryObject
 	{
 		public String strBeneficiaryName {get; set;}
 		public String strDesignation {get; set;}
 		public String strRevocable {get; set;}
 		public String strRelationshipToInsured {get; set;}
 		public String strPercentage {get; set;}
 		public String strTrusteeName {get; set;}
 		public customBeneficiaryObject(String strInputBeneficiaryName, String strInputDesignation, String strInputRevocable, 
 						String strInputRelationshipToInsured, Double doubleInputPercentage, String strInputTrusteeName)
 		{
 			if (strInputBeneficiaryName != null)
 			{
 				strBeneficiaryName = strInputBeneficiaryName;
 			}
 			else
 			{
 				strBeneficiaryName = 'N/A';
 			}
 			if (strInputDesignation != null)
 			{
 				strDesignation = strInputDesignation;
 			}
 			else
 			{
 				strDesignation = 'N/A';
 			}
 			if (strInputRevocable != null)
 			{
 				strRevocable = strInputRevocable;
 			}
 			else
 			{
 				strRevocable = 'N/A';
 			}
 			if(strInputRelationshipToInsured != null)
 			{
 				strRelationshipToInsured = strInputRelationshipToInsured;
 			}
 			if (doubleInputPercentage !=null && doubleInputPercentage>0)
 			{
 				strPercentage = String.valueOf(doubleInputPercentage.format()) + '%';
 			}
 			else
 			{
 				strPercentage = 'N/A';
 			}
 			if (strInputTrusteeName != null)
 			{
 				strTrusteeName = strInputTrusteeName;
 			}	
 			else
 			{
 				strTrusteeName = '';
 			}
 		}	
 	}