/*
Version		: 1.2
Company		: Sun Life Financial
Date		: 30.JULY.2009
Author		: Dorothy Anne L. Baltazar
Description	: Controller for Transaction Log History.
History		: 
	1.1		23.SEP.2009 - W. Clavero - Added code for the new look of pagination links.
	1.2     28.OCT.2009 - R. Angeles - Added admin portal check on constructor and checking of policyId parameter
*/

public with sharing class TxnLogHistoryCtrl {
	public List<TxnLogList> myLogList {get; set;}
	
	public String acctClientId = null;
	public String userId = userInfo.getUserId();
	
	public String formatDate {get; set;}
	public String tempURL {get; set;}
	
	public Boolean renderTxnLog {get; set;}
	
	Map<String, List<TxnLogList>> TLH_Map = new Map<String, List<TxnLogList>>();
	Integer MaxRecPerPage;
	public List<SelectOption> pageList {get; set;}
	
	public String PageNumber {get; set;}
	
	public Integer numberOfPages {get; set;}
	
	public Integer getCurrentPageNum() {
		return Integer.valueOf(PageNumber);
	}
	
	public String getAcctClientId()
	{
		try
		{
			User userClientId = [Select u.Client_Id__c From User u where u.Id = :userId];
			List<Account> acctList = new List<Account>();
			acctList = [Select a.Client_Id__c, a.Client_Id__pc, a.RecordType.name From Account a where a.Client_Id__c = :userClientId.Client_Id__c];
			if (acctList.size()>0)
			{
				if (acctList[0].Client_Id__c!=null)
				{
					acctClientId = acctList[0].Client_Id__c;
				}
			}
		}
		catch (Exception e)
		{
			
		}
		return acctClientId;
	}

	Config_Global_Settings__c globalConfig;

	public PageReference Security(){ //R Angeles 10292009 - admin portal check
		PageReference pProfileNoAccess = Page.UserAccessError;
		Config_Utilities CU = new Config_Utilities();
		Boolean isPortalUser = CU.isPortalUser(globalConfig.SLF_Portal_Profile_Names__c);
		if(isPortalUser){
			return null;
		}
		else{
			return pProfileNoAccess;
		}
	}

	public TxnLogHistoryCtrl()
	{	
		try
		{
			globalConfig = [Select c.SLF_Portal_Profile_Names__c, c.TXN_Maximum_Records_per_Page__c From Config_Global_Settings__c c LIMIT 1];
			MaxRecPerPage = Integer.valueOf(globalConfig.TXN_Maximum_Records_per_Page__c);
			PageNumber = '1';
			pageList = new List<SelectOption>();
			
			//R. Angeles 10282009 - if Portal User check Client access via logged in User
			String s = ApexPages.currentPage().getParameters().get('defID');
			if (s != null) {
				acctClientId = s;
			} else {
				acctClientId = getAcctClientId();
			}
			
			
			List<Transaction__c> myTransactions = [SELECT Submission_Date_Time__c, SORStatus__c, Request_Type__c, Reference_Number__c, Policy_ID__c, CreatedDate, Completed__c, Active__c, Client_Id__c, Id 
							  						 FROM Transaction__c t
						    					    WHERE (Client_Id__c = :acctClientId AND Client_Id__c != NULL) AND Active__c = true AND (CreatedDate = THIS_YEAR OR CreatedDate = LAST_YEAR)
						  						 ORDER BY CreatedDate desc, Request_Type__c
						  						 	LIMIT 1000];
			if (myTransactions.size()>0)
			{
				myLogList = new List<TxnLogList>();
				myLogList = getMyLogList(myTransactions);
				/* This is to compute the number of pages that will be available for the user */ 
				
				numberOfPages = myLogList.size()/MaxRecPerPage; 				// this is the number of pages available for the user
				
				if (Math.mod(myLogList.size(), MaxRecPerPage) != 0)
				{
					numberOfPages++;
				}

				/* end computation of number of pages */ 
				
				/* This part creates the list for page selection on the VF page */
				
				for (Integer i=1; i<=numberOfPages; i++)
				{
					pageList.add(new SelectOption(i.format(),i.format()));
				}
				
				/* this is the loop for the map items */
				for (Integer i=0; i<numberOfPages; i++)
				{
					List<TxnLogList> TLHWrappedDisplay = new List<TxnLogList>(); // temp list
					/* This is the loop for the List items */
					for (Integer x=0; x<MaxRecPerPage; x++)
					{
						Integer index = (i * MaxRecPerPage) + x;
						
						if (index < myLogList.size())
						{
							TLHWrappedDisplay.add(myLogList[index]); //add to loglist
						}
						else
						{
							break; // end the loop if index is already greater than the main list's size
						}
					}
					TLH_Map.put((i+1).format(), TLHWrappedDisplay); // Insert to the map the page number and the list of records to display for that page number
				}
				if (TLHForDisplay == null)
				{
					getTLHForDisplayfromMap(); // called for the initial display of the table
				}

			}
			/*
			else
			{
				renderTxnLog = false;
			}
			this is the loop for the map items */
			
		}
		catch (Exception a)
		{
		}
	}
	
	List<TxnLogList> TLHForDisplay;
	
	public void getTLHForDisplayfromMap()
	{
		TLHForDisplay = TLH_Map.get(PageNumber);	
	}

	public List<TxnLogList> getTLHForDisplay()
	{
		if(TLHForDisplay != null){
			if(TLHForDisplay.size() == 0){
				TLHForDisplay = null;
			}
		}
		return TLHForDisplay;
	}
	
	public string getChangeInfoNav()
	{
		string navURL;
		
		navURL = ESPortalNavigator.NAV_URL_CPP_PDF;
		
		return navURL;
	}

	public string getFundSwitchNav()
	{
		string navURL;
		
		navURL = ESPortalNavigator.NAV_URL_FSW_PDF;
		
		return navURL;
	}
	
	public string getChangeAllocNav()
	{
		string navURL;
		
		navURL = ESPortalNavigator.NAV_URL_CHM_PDF;
		
		return navURL;
	}
	
	public String formatDateTime(Datetime theDate)
	{
		formatDate = '';
		if(theDate!=null)
		{
			formatDate = theDate.format('dd/MM/yyyy, H:mm');
		}
		return formatDate;
	}
	
	public String formatDate(Datetime theDate)
	{
		formatDate = '';
		if(theDate!=null)
		{
			formatDate = theDate.format('dd/MM/yyyy');
		}
		return formatDate;
	}
	
	/*copy contents of myTransactions to myLogList*/
	public List<TxnLogList> getMyLogList(List<Transaction__c> myTxnLog)
	{	
		Display_Translator[] reqStatus = new Display_Translator[]{};
		Map<String, String> reqStatusMap = new Map<String, String>();
		string tempString;
		
		List<TxnLogList> newList = new List<TxnLogList>();
		
		for (Integer j=0; j<myTxnLog.size(); j++)
		{
			tempString = myTxnLog[j].SORStatus__c != NULL ? myTxnLog[j].SORStatus__c.toUpperCase(): NULL;
			reqStatus.add(new Display_Translator(TransactionsReqConstant.TXN_STATUS, tempString));
		}
		
		reqStatusMap = Display_Translator.TranslateMap(reqStatus);
		
		for (Integer i=0; i<myTxnLog.size(); i++)
		{
			TxnLogList txnItem = new TxnLogList();
			
			txnItem.strId				= myTxnLog[i].Id;
			txnItem.strRefNo			= myTxnLog[i].Reference_Number__c;
			txnItem.strPolicyID 		= myTxnLog[i].Policy_ID__c;
			txnItem.strSubmitDate 		= formatDateTime(myTxnLog[i].Submission_Date_Time__c);
			txnItem.strChgRequestType 	= myTxnLog[i].Request_Type__c;
			txnItem.strRequestStatus 	= (myTxnLog[i].SORStatus__c != NULL ? reqStatusMap.get(myTxnLog[i].SORStatus__c.toUpperCase()): NULL);
			txnItem.strCompletedDate 	= formatDate(myTxnLog[i].Completed__c);
			newList.add(txnItem);
		}
		return newList;
	}
	
	public static testmethod void testTxnLogHistoryCtrl() {
		Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
    	Map<String,Schema.RecordTypeInfo>AccountMapByName = AccountSchema.getRecordTypeInfosByName();
    	Schema.RecordTypeInfo AccountRT = AccountMapByName.get('Person Account');
    	
    	String PersonAccountTypeId = '';
        if(AccountRT != null){
        	 PersonAccountTypeId =AccountRT.getRecordTypeId();
        }
        
        Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
				PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
				FirstName='AcME', LastName='GX', RecordTypeId =  PersonAccountTypeId, 
				Client_Id__c = '!@#$%^&*()', Client_ID__pc = 'abcde12345'
				);
					
		Insert accountSuccessTest;		
		
		Schema.DescribeSObjectResult PolicySchema = Schema.SObjectType.Policy__c;
    	Map<String,Schema.RecordTypeInfo>PolicyMapByName = PolicySchema.getRecordTypeInfosByName();
    	Schema.RecordTypeInfo PolicyRT = PolicyMapByName.get('Traditional');
    		
    	String TradPolicyTypeId = '';
        if(PolicyRT != null){
        	TradPolicyTypeId =PolicyRT.getRecordTypeId();
        }
			
		Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountSuccessTest.id, policy_number__c = '887888877',
										RecordTypeId = TradPolicyTypeId, Policy_Status_Picklist_Hidden__c = '1', 
										Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
										Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
										Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
										Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
										Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
										Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
										Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = '1',
										Branch_Number__c = '234', Account_Number__c = 'Account 1', 
										Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
										Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
										Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
										Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
										Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
										Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
										Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
										Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
										Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
										Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
										Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
										Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
										Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
										Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
										Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00									
										);      
		insert policySuccessTest;
		
		Transaction__c transTempCM = new Transaction__c(Reference_Number__c ='Ref0123456CM',
														Request_Type__c ='S0001',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="S0001" ReferenceNo="ECM-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempCM;
		
		Transaction__c transTempCP = new Transaction__c(Reference_Number__c ='Ref0123456CP',
														Request_Type__c ='CHGALLOCAT',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="CHGALLOCAT" ReferenceNo="ECP-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempCP;
		
		Transaction__c transTempFS = new Transaction__c(Reference_Number__c ='Ref0123456FS',
														Request_Type__c ='FUNDSW',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempFS;
		
		PageReference pgRef = Page.TxnLogHistory;
		Test.setCurrentPage(pgRef);

		ApexPages.currentPage().getParameters().put('defID', accountSuccessTest.Client_Id__c);
		
		TxnLogHistoryCtrl temp = new TxnLogHistoryCtrl();
		temp.acctClientId = accountSuccessTest.Client_Id__c;

		List<TxnLogList> tempList = temp.getTLHForDisplay();
		String CPNav = temp.getChangeInfoNav();
		String CMNav = temp.getChangeAllocNav();
		String FSNav = temp.getFundSwitchNav();
		String tempId2 = temp.getAcctClientId();
		
	}					
}