/*
Version : 1.0
Company : Sun Life Financial
Date    : 21.July.2009
Author  : Wilson Cheng
Description : This is used for saving and retrieving data of xml data which will be used by 
              acknowledgement page of change contact info.
History : 
*/

Public Class ChangeInfoXMLGen
{
    public string writeCPPXML(ChangeInfoRequest formatTOXML)
    {
    	XmlStreamWriter writeXML = new XmlStreamWriter();
    	string outputXML;
    	
    	writeXML.writeStartElement(null, TransactionsReqConstant.TXN, null); //Start of TXN element
	    	writeXML.writeAttribute(null, null, TransactionsReqConstant.REQUEST_TYPE, formatTOXML.RequestType);
	    	writeXML.writeAttribute(null, null, TransactionsReqConstant.REFERENCE_NO, formatTOXML.ReferenceNo);
	    	writeXML.writeAttribute(null, null, TransactionsReqConstant.POLICY_NO, formatTOXML.PolicyNo);
	    	writeXML.writeAttribute(null, null, TransactionsReqConstant.SUBMISSION_DATE_TIME, formatTOXML.SubmissionDateTime);
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.HOME_PHONE, null); //Start of home phone element
		    	if(formatTOXML.HomePhone != NULL)
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.HomePhone);
		    	}
		    	else
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
		    	}
		    	writeXML.writeAttribute(null, null, TransactionsReqConstant.NO_CHANGE, formatTOXML.HomePhoneChange);
	    	writeXML.writeEndElement();//End of home phone element
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.OFFICE_PHONE, null); //Start of office phone element
		    	if(formatTOXML.OfficePhone != NULL)
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.OfficePhone);
		    	}
		    	else
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
		    	}
		    	writeXML.writeAttribute(null, null, TransactionsReqConstant.NO_CHANGE, formatTOXML.OfficePhoneChange);
	    	writeXML.writeEndElement();//End of office phone element
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.MOBILE_PHONE, null); //Start of mobile phone element
		    	if(formatTOXML.MobilePhone != NULL)
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.MobilePhone);
		    	}
		    	else
		    	{
		    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
		    	}
		    	writeXML.writeAttribute(null, null, TransactionsReqConstant.NO_CHANGE, formatTOXML.MobilePhoneChange);
	    	writeXML.writeEndElement();//End of mobile phone element
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.EMAIL_ADDRESS, null); //Start of email address element
		    	writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.EmailAddValue);
		    	writeXML.writeAttribute(null, null, TransactionsReqConstant.NO_CHANGE, formatTOXML.EmailAddChange);
	    	writeXML.writeEndElement();//End of email address element
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.ADDRESS, null); //Start of address element
	    	    writeXML.writeAttribute(null, null, TransactionsReqConstant.NO_CHANGE, formatTOXML.AddressChange);
	    		writeXML.writeStartElement(null, TransactionsReqConstant.LINE1, null); //Start of line1 element
	    			if(formatTOXML.AddressLine1 != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressLine1.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of line1 element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.LINE2, null); //Start of line2 element
	    			if(formatTOXML.AddressLine2 != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressLine2.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of line2 element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.LINE3, null); //Start of line3 element
	    			if(formatTOXML.AddressLine3 != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressLine3.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of line3 element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.LINE4, null); //Start of line4 element
	    			if(formatTOXML.AddressLine4 != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressLine4.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of line4 element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.LINE5, null); //Start of line5 element
	    			if(formatTOXML.AddressLine5 != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressLine5.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of line5 element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.CITY, null); //Start of city element
	    			if(formatTOXML.AddressCity != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressCity.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of city element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.PROV, null); //Start of province element
	    			if(formatTOXML.AddressProv != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.AddressProv.toUpperCase());
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of province element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.ZIP, null); //Start of zip element
	    			if(formatTOXML.AddressZip != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, XMLDataHandler.escapeXMLString(formatTOXML.AddressZip.toUpperCase()));
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of zip element
	    		
	    		writeXML.writeStartElement(null, TransactionsReqConstant.COUNTRY, null); //Start of country element
	    			if(formatTOXML.AddressCountry != NULL)
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.AddressCountry.toUpperCase());
			    	}
			    	else
			    	{
			    		writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, TransactionsReqConstant.EMPTY_STRING);
			    	}
	    		writeXML.writeEndElement();//END of country element
	    	writeXML.writeEndElement();//End of address element
	    	
	    	writeXML.writeStartElement(null, TransactionsReqConstant.POLICIES, null); //Start of policies element
	    		for(integer i = 0; i < formatTOXML.Policies.size(); i++)
	    		{
	    			writeXML.writeStartElement(null, TransactionsReqConstant.POLICIES_NUM, null); //Start of policy no. element
	    			writeXML.writeAttribute(null, null, TransactionsReqConstant.VALUE, formatTOXML.Policies[i]);
	    			writeXML.writeEndElement();//End of policy no. element
	    		}
	    	writeXML.writeEndElement();//End of policies element
    	writeXML.writeEndElement();//End of Txn element
    	
    	outputXML = writeXML.getXmlString();
    	writeXML.close();
    	
    	system.debug('outputXML deb:'+ outputXML.replace('<', '«').replace('>', '»'));
    	return outputXML;
    }
    
    public ChangeInfoRequest readCPPXML(string dataInfo)
    {
    	ChangeInfoRequest changeInfoData = new ChangeInfoRequest();
    	XmlStreamReader changeInfo = new XmlStreamReader(dataInfo);
    	
		while(changeInfo.hasNext()) 
		{
			// To check if the xml has Txn
			if (changeInfo.getEventType() == XmlTag.START_ELEMENT) 
			{
				if (TransactionsReqConstant.TXN == changeInfo.getLocalName()) 
				{
					changeInfoData = parseContactInfo(changeInfo);
					break;
				}
				else
				{
					break;
				}
			}
			changeInfo.next();
		}
		
		return changeInfoData;
    }
    
    public ChangeInfoRequest parseContactInfo(XmlStreamReader changeInfo)
    {
    	ChangeInfoRequest tempInfoData = new ChangeInfoRequest();
    	//TransactionsReqConstant constantValue;
    	string stringPolicyNo;
    	
    	//get all attribute in the element of Txn
        tempInfoData.RequestType = changeInfo.getAttributeValue(null, TransactionsReqConstant.REQUEST_TYPE);
        tempInfoData.ReferenceNo = changeInfo.getAttributeValue(null, TransactionsReqConstant.REFERENCE_NO);
 	    tempInfoData.PolicyNo = changeInfo.getAttributeValue(null, TransactionsReqConstant.POLICY_NO);
 	    tempInfoData.SubmissionDateTime = changeInfo.getAttributeValue(null, TransactionsReqConstant.SUBMISSION_DATE_TIME);

    	system.debug('tempInfoData.submissionDateTime deb:'+ tempInfoData.submissionDateTime);
    	
    	while(changeInfo.hasNext())
    	{
    		if (changeInfo.getEventType() == XmlTag.END_ELEMENT) 
    		{
				//check if the end element is the parent Txn 
				if(TransactionsReqConstant.TXN == changeInfo.getLocalName())
				{
					//out of the loop
					break;	
				}
			} 
			//check if the start element is the same as below
			if (changeInfo.getEventType() == XmlTag.START_ELEMENT) 
			{
				if (TransactionsReqConstant.HOME_PHONE == changeInfo.getLocalName()) 
				{
					tempInfoData.HomePhone = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
					tempInfoData.HomePhoneChange = changeInfo.getAttributeValue(null, TransactionsReqConstant.NO_CHANGE);
				}
				else if (TransactionsReqConstant.OFFICE_PHONE == changeInfo.getLocalName())
				{
					tempInfoData.OfficePhone = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
					tempInfoData.OfficePhoneChange = changeInfo.getAttributeValue(null, TransactionsReqConstant.NO_CHANGE);
				}
				else if(TransactionsReqConstant.MOBILE_PHONE == changeInfo.getLocalName())
				{
					tempInfoData.MobilePhone = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
					tempInfoData.MobilePhoneChange = changeInfo.getAttributeValue(null, TransactionsReqConstant.NO_CHANGE);
				}
				else if(TransactionsReqConstant.EMAIL_ADDRESS == changeInfo.getLocalName())
				{
					tempInfoData.EmailAddValue = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
					tempInfoData.EmailAddChange = changeInfo.getAttributeValue(null, TransactionsReqConstant.NO_CHANGE);
				}
				//START
				else if(TransactionsReqConstant.ADDRESS == changeInfo.getLocalName())
                {
                    tempInfoData.AddressChange = changeInfo.getAttributeValue(null, TransactionsReqConstant.NO_CHANGE);
                }
				//END
				else if(TransactionsReqConstant.LINE1 == changeInfo.getLocalName())
				{
					tempInfoData.AddressLine1 = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.LINE2 == changeInfo.getLocalName())
				{
					tempInfoData.AddressLine2 = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.LINE3 == changeInfo.getLocalName())
				{
					tempInfoData.AddressLine3 = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.LINE4 == changeInfo.getLocalName())
				{
					tempInfoData.AddressLine4 = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.LINE5 == changeInfo.getLocalName())
				{
					tempInfoData.AddressLine5 = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.CITY == changeInfo.getLocalName())
				{
					tempInfoData.AddressCity = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.PROV == changeInfo.getLocalName())
				{
					tempInfoData.AddressProv = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
				}
				else if(TransactionsReqConstant.ZIP == changeInfo.getLocalName())
				{
					tempInfoData.Addresszip = XMLDataHandler.unescapeXMLString(changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE));
				}
				else if(TransactionsReqConstant.COUNTRY == changeInfo.getLocalName())
				{
					tempInfoData.AddressCountry = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
				}
				else if(TransactionsReqConstant.POLICIES_NUM == changeInfo.getLocalName())
				{
					stringPolicyNo = changeInfo.getAttributeValue(null, TransactionsReqConstant.VALUE);
    				
					tempInfoData.Policies.add(stringPolicyNo);
				}
			}
			
			//continue parsing the xml
			changeInfo.next();
    	}
    	
    	return tempInfoData;
    }
    
    public 
    
    static testMethod void testParser() {
		ChangeInfoXMLGen demo = new ChangeInfoXMLGen();
		ChangeInfoRequest tempData = new ChangeInfoRequest();
		String str = '<Txn RequestType="S0001" ReferenceNo="ECP-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
		             '<homephone value="21119999" nochange="false"/>'+
					 '<officephone value="*" nochange="false"/>'+
					 '<mobilephone value="97778888" nochange="false"/>'+
					 '<emailaddress value="peterchan@mailbox.com.hk" nochange="true"/>'+
					 '<address>'+
					 '<line1 value="Flat 123"></line1>'+
					 '<line2 value="5th floor"/>'+
					 '<line3 value="House ABC"/>'+
					 '<line4 value="Salisbury Road"/>'+
					 '<line5 value="Tsim Tsa Tsui"/>'+
					 '<city value="Hong Kong City"/>'+
					 '<prov value="Hong Kong"/>'+
					 '<zip value="1234"/>'+
					 '<country value="Hong Kong"/>'+
					 '</address>'+
					 '<policies>'+
					 '<policyno value="1234567891"/>'+
					 '<policyno value="1234567892"/>'+
					 '<policyno value="1234567893"/>'+
					 '</policies>'+
					 '</Txn>';
		ChangeInfoRequest info = demo.readCPPXML(str);
		//System.debug(info.size());
		System.debug('change contact info deb:'+info);
		
		tempData.RequestType = 'S0001';
    	tempData.ReferenceNo = 'ECP-298000005-090421';
    	tempData.PolicyNo = '1234567890';
    	tempData.SubmissionDateTime = '21/04/2009 13:10:52';
	    tempData.HomePhone = '21119999';
        tempData.HomePhoneChange = 'true';
        tempData.OfficePhone = '';
        tempData.OfficePhoneChange = 'true';
        tempData.MobilePhone = '97778888';
        tempData.MobilePhoneChange = 'true';
	    tempData.EmailAddValue = 'peterchan@mailbox.com.hk';
	    tempData.EmailAddChange = 'true';
	    tempData.AddressChange = 'false';
	    tempData.AddressLine1 = 'Flat 123';
	    tempData.AddressLine2 = '5th floor';
	    tempData.AddressLine3 = 'House ABC';
	    tempData.AddressLine4 = 'Salisbury Road';
	    tempData.AddressLine5 = 'Tsim Tsa Tsui';
	    tempData.AddressCity = 'Hong Kong City';
	    tempData.AddressProv = 'Hong Kong';
	    tempData.AddressZip = '1234';
	    tempData.AddressCountry = 'Hong Kong';
	    tempData.Policies.add('1234567891');
	    tempData.Policies.add('1234567892');
	    tempData.Policies.add('1234567893');
	    
	    demo.writeCPPXML(tempData);
	    
	    tempData.RequestType = 'S0001';
        tempData.ReferenceNo = 'ECP-298000005-090421';
        tempData.PolicyNo = '1234567890';
        tempData.SubmissionDateTime = '21/04/2009 13:10:52';
        tempData.HomePhone = NULL;
        tempData.HomePhoneChange = 'true';
        tempData.OfficePhone = NULL;
        tempData.OfficePhoneChange = 'true';
        tempData.MobilePhone = NULL;
        tempData.MobilePhoneChange = 'true';
        tempData.EmailAddValue = 'peterchan@mailbox.com.hk';
        tempData.EmailAddChange = 'true';
        tempData.AddressChange = 'true';
        tempData.AddressLine1 = NULL;
        tempData.AddressLine2 = NULL;
        tempData.AddressLine3 = NULL;
        tempData.AddressLine4 = NULL;
        tempData.AddressLine5 = NULL;
        tempData.AddressCity = NULL;
        tempData.AddressProv = NULL;
        tempData.AddressZip = NULL;
        tempData.AddressCountry = NULL;
        tempData.Policies.add('1234567891');
        tempData.Policies.add('1234567892');
        tempData.Policies.add('1234567893');
        
        demo.writeCPPXML(tempData);
	}
}