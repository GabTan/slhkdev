/*
Version : 1.4
Company : Sun Life Financial
Date    : 27.July.2009
Author  : Wilson Cheng
Description : This is used for displaying the claim history section in NewPolicyInquiry page.
History : 
          Version 1.1 <09/08/09 - Wcheng - SIT_50>
          Version 1.2 <09/17/09 - Wcheng - SIT_118>
          Version 1.3 <09/30/09 - Wcheng - Added handling the "A, J, H, or T" prefix wherein if the inputted prefix is any one of 
                                           them, then it should disregard the suffix>
          Version 1.4 <10/05/09 - Wcheng - Added additional checking if the attachment of one claims has more than 1 attachement.
                                           If it has more than 1, the latest updated attachment will be shown in the claims history page.
          version 1.5 - 12/03/09 - JC NACHOR - used the value in Config Global Settings in deducting years from the current year
          version 1.6 - 06/09/10 - JC NACHOR - Production Issue - Operation too large workaround                                           
*/

public with sharing class ClaimsHistoryController 
{
	public ClaimsHistoryController(NewPolicyInquiry controller)
    {
    } 
	
	public string policyNum = ApexPages.currentPage().getParameters().get('policyID');
	
	//START Edit Wcheng SIT_118 09/17/09 
	public string zeroRecords{get; set;}
	
	// JC NACHOR 12/03/2009 - made into an instace variable
	private Config_Global_Settings__c globalConfig;
	
	//END Edit Wcheng SIT_118 09/17/09 
	
    public CustomClaimsStatus[] getClaimsStatusValues()
    {
        CustomClaimsStatus[] claimValue = new CustomClaimsStatus[]{};
        Display_Translator[] claimTypeList = new Display_Translator[]{};
        Display_Translator[] claimStatusList = new Display_Translator[]{};
        Display_Translator[] payMethodList = new Display_Translator[]{};
        Map<string, string> claimTypeContainer = new Map<string, string>();
        Map<string, string> statusValueContainer = new Map<string, string>();
        Map<string, string> payMethodValueContainer = new Map<string, string>();
        Set<string> claimsId = new Set<string>();
        Set<string> inforceCode = new Set<string>{'1', '2', '3', '4'};
        Map<string, Id> attachmentContainer = new Map<string, Id>();
        Map<String, String> currencyMap = CurrencyCodeHandler.getTranslatedCurrencyCodes();
        String strCurrency;
        string concatPreSuff;
        string clmNo;
        string clmType;
        string clmStat;
        string pAmnt;
        string pMethod;
        string apprvDate;
        string mClaims;
        Id pNotice;
        string claimSuffix;
        string claimPrefix;
        integer indexNum;
        set<string> attachmentParentId = new set<string>();
        datetime queryDate = datetime.newInstance(Date.today().year() - 1, 1, 1);
        
        List<Claims_History__c> claimHistData = [Select Id, Approve_Rejected_Date__c, Benefits_Amount__c, Claims_No__c, Claims_Prefix__c, 
                                                 Claims_Status__c, Claims_Suffix__c, Payment_Method__c, CurrencyIsoCode from Claims_History__c 
                                                 where Active__c = true and Policy__r.Policy_Number__c =: policyNum AND
                                                 (((Claims_Status__c =: TransactionsReqConstant.CSH_ADMITTED AND Submission_Date__c >:queryDate) OR 
                                                 (Claims_Status__c =: TransactionsReqConstant.CSH_DECLINED AND Submission_Date__c >:queryDate)) OR
                                                 ((Claims_Status__c =: TransactionsReqConstant.CSH_PROCESSING) OR 
                                                 (Claims_Status__c =: TransactionsReqConstant.CSH_PENDING))) AND
                                                 Policy__r.Policy_Status_Hidden__c IN: inforceCode
                                                 ORDER BY Claims_No__c DESC];
                                                 
       //START EDIT Wcheng enhancement 09.30.09
       // JC NACHOR 12/03/2009 - made into an instace variable
       globalConfig = [Select CSH_Claims_Type__c
                                                // JC NACHOR 12/03/2009 - used the value in Config Global Settings in deducting years from the current year     
                                                , Remark_Year_Offset__c 
                                                 From Config_Global_Settings__c limit 1];
       //END EDIT Wcheng enhancement 09.30.09
        
        //START Edit Wcheng SIT_118 09/17/09
        if (claimHistData.size() == 0)
        {
        	zeroRecords = TransactionsReqConstant.CSH_TRUE;
        }
        else
        {
        	zeroRecords = TransactionsReqConstant.CSH_FALSE;
        }
        //START Edit Wcheng SIT_118 09/17/09
        
        //put the claims id in a set
        for(Claims_History__c tempClaimId:claimHistData)
        {
        	claimsId.add(tempClaimId.Id);    	
        }
        
        //START Wcheng Enhancement 10.05.09
        // JC NACHOR - Production Issue - Operation too large workaround
        List<Attachment> attachmentRecord = new List<Attachment>();
        if(claimsId.size() > 0){
          attachmentRecord = [Select Id, ParentId From Attachment where ParentId IN:claimsId order by ParentId asc, LastModifiedDate desc];
        }
        
        //container for the file attachment
        for(Attachment tempAttachment: attachmentRecord)
        {
        	if(!attachmentParentId.contains(tempAttachment.ParentId))
        	{
        		attachmentContainer.put(tempAttachment.ParentId, tempAttachment.Id);
                attachmentParentId.add(tempAttachment.ParentId);
        	}
        }
        //END Wcheng Enhancement 10.05.09
        
        for(Claims_History__c tempHistData: claimHistData)
        {
        	claimPrefix = NULL;
            claimSuffix = NULL;
        	//get the description for the claim status
            Display_Translator tempStatus = new Display_Translator(TransactionsReqConstant.CSH_STATUS, tempHistData.Claims_Status__c);
            claimStatusList.add(tempStatus);
                    
            //get the description for the pay method
            Display_Translator tempPayMethod = new Display_Translator(TransactionsReqConstant.CSH_PAYMETHOD, tempHistData.Payment_Method__c);
            payMethodList.add(tempPayMethod);          
            
            //START EDIT Wcheng enhancement 09.30.09
            //To check whether the prefix is part of the list to ignore suffix
            indexNum = globalConfig.CSH_Claims_Type__c.indexOf(tempHistData.Claims_Prefix__c, 0);
            
            //if its greater than -1, then it should take the prefix disregarding the suffix
            if(indexNum > -1)
            {
            	claimPrefix = tempHistData.Claims_Prefix__c;
            	claimSuffix = NULL;
            }
            else
            {
            	claimPrefix = tempHistData.Claims_Prefix__c;
                claimSuffix = tempHistData.Claims_Suffix__c;
            }

            //get the description for the claim type
        	if(claimSuffix != NULL && claimPrefix != NULL)
            {
                concatPreSuff = claimPrefix +'_'+ claimSuffix;
            }
            else if (claimSuffix == NULL && claimPrefix != NULL)
            {
                concatPreSuff = claimPrefix;
            }
            else
            {
                concatPreSuff = NULL;
            }
            //END EDIT Wcheng enhancement 09.30.09
            
            Display_Translator tempClaimType = new Display_Translator(TransactionsReqConstant.CSH_CLAIMTYPE, concatPreSuff);
            claimTypeList.add(tempClaimType);   
        }
        
        statusValueContainer = Display_Translator.TranslateMap(claimStatusList);
        payMethodValueContainer = Display_Translator.TranslateMap(payMethodList);
        claimTypeContainer = Display_Translator.TranslateMap(claimTypeList);
        
        for(integer x = 0; x < claimHistData.size(); x++)
        {
        	claimPrefix = NULL;
            claimSuffix = NULL;
        	//START EDIT Wcheng enhancement 09.30.09
            //To check whether the prefix is part of the list to ignore suffix
            indexNum = globalConfig.CSH_Claims_Type__c.indexOf(claimHistData[x].Claims_Prefix__c, 0);
            
            //if its greater than 1, then it should take the prefix disregarding the suffix
            if(indexNum > -1)
            {
                claimPrefix = claimHistData[x].Claims_Prefix__c;
                claimSuffix = NULL;
            }
            else
            {
                claimPrefix = claimHistData[x].Claims_Prefix__c;
                claimSuffix = claimHistData[x].Claims_Suffix__c;
            }
        	
        	
            if(claimSuffix != NULL)
            {
                clmNo = claimPrefix +''+ claimHistData[x].Claims_No__c +''+ claimSuffix;
            }
            else
            {
                clmNo = claimPrefix +''+ claimHistData[x].Claims_No__c;	
            }
            
            //to get the claim type
            if(claimSuffix != NULL && claimPrefix != NULL)
            {
                concatPreSuff = claimPrefix +'_'+ claimSuffix;
            }
            else if (claimSuffix == NULL && claimPrefix != NULL)
            {
                concatPreSuff = claimPrefix;
            }
            else
            {
                concatPreSuff = NULL;
            }
            //END EDIT Wcheng Enhancement 09.30.09
            
            clmType = claimTypeContainer.get(concatPreSuff);
            mClaims = checkMinorClaim(claimHistData[x].Claims_Prefix__c, claimHistData[x].Claims_Suffix__c);
            //check if the claim status is a minor claim
            if(mClaims == TransactionsReqConstant.CSH_TRUE)
            {
            	//get the attachment Id
            	pNotice = attachmentContainer.get(claimHistData[x].Id);
            }
            else
            {
            	pNotice = NULL;
            }
            
            clmStat = statusValueContainer.get(claimHistData[x].Claims_Status__c);
            if(claimHistData[x].Benefits_Amount__c != NULL)
            {
            	strCurrency = currencyMap.get(claimHistData[x].CurrencyIsoCode);
                pAmnt = strCurrency+' '+ESAmountFormatter.FormatAmount(claimHistData[x].Benefits_Amount__c);
            }
            else
            {
            	pAmnt = null;
            }   
            pMethod = payMethodValueContainer.get(claimHistData[x].Payment_Method__c);
            if(claimHistData[x].Approve_Rejected_Date__c != NULL)
            {
            	//START Wcheng SIT_50 09/09/09
                apprvDate = claimHistData[x].Approve_Rejected_Date__c.format(TransactionsReqConstant.CSH_APPROVE_REJECTED_DATE, TransactionsReqConstant.HK_TIME_ZONE);
                //END Wcheng SIT_50 09/09/09
            }
            else
            {
                apprvDate = NULL;	
            }
            
            //add new record to the list
            claimValue.add(new CustomClaimsStatus(clmNo, clmType, clmStat, pAmnt, pMethod, apprvDate, mClaims, pNotice));
        }       
                                                                     
        return claimValue;
    }
    
    public string checkMinorClaim(string prefix, string suffix)
    {
    	//A, H, J or T or has a Claims Prefix of FR and Claims Suffix of CB
        string isMinorClaim;
        if(((prefix == TransactionsReqConstant.CSH_A || prefix == TransactionsReqConstant.CSH_H) || 
          (prefix == TransactionsReqConstant.CSH_J || prefix == TransactionsReqConstant.CSH_T)) || 
          (prefix == TransactionsReqConstant.CSH_FR && suffix == TransactionsReqConstant.CSH_CB))
        {
        	isMinorClaim = TransactionsReqConstant.CSH_TRUE;
        }
        else
        {
            isMinorClaim = TransactionsReqConstant.CSH_FALSE;	
        }
        
        return isMinorClaim;        	
    }
    
    public string getPaymentHistory()
    {
        string NavURL;
        
        NavURL = ESPortalNavigator.NAV_URL_HOM_PYH;
        
        return NavURL;
    }
    
    public string getTransHistory()
    {
        string NavURL;
        
        NavURL = ESPortalNavigator.NAV_URL_HOM_FTH;
        
        return NavURL;
    }
    
    public string getPolicyNum()
    {
        return 	policyNum;
    }
    
    public string getCurrentYear()
    {
    	  // JC NACHOR 12/03/2009 - used the value in Config Global Settings in deducting years from the current year
        //string currentYear = dateTime.now().addYears(-1).format(TransactionsReqConstant.CSH_YYYY);
        string currentYear = dateTime.now().addYears(-(globalConfig.Remark_Year_Offset__c.intValue())).format(TransactionsReqConstant.CSH_YYYY);
        return currentYear;	
    }
    
    public boolean getDispFooter()
    {
    	boolean yearFooter;
    	
    	if(integer.valueOf(dateTime.now().format(TransactionsReqConstant.CSH_YYYY)) >= 2010)
    	{
    		yearFooter = true;
    	}
    	else
    	{
    	    yearFooter = false;	
    	}
    	
    	return yearFooter;
    }
    
     static testMethod void testClaimsHistoryController() 
     {
     	NewPolicyInquiry temp = new NewPolicyInquiry();
     	ClaimsHistoryController historyTest = new ClaimsHistoryController(temp);
     	Policy__c policyTemp = new Policy__c();
     	Claims_History__c historyTemp = new Claims_History__c();
     	Claims_History__c historyTemp2 = new Claims_History__c();
     	RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Traditional'];
     	
     	policyTemp.Name = 'testPolicy';
     	policyTemp.CurrencyIsoCode = 'HKD';
     	policyTemp.Active__c = true;
     	policyTemp.Policy_Number__c = '1234';
     	policyTemp.RecordTypeId = recordID.Id;
     	policyTemp.Policy_Status_Hidden__c = '1';
     	
     	insert policyTemp;
     	
     	//Select c.Submission_Date__c, c.Policy__c, c.Payment_Method__c, c.Claims_Suffix__c, 
     	//c.Claims_Status__c, c.Claims_Prefix__c, c.Claims_No__c, c.Claim_Number__c From Claims_History__c c
     	historyTemp.Submission_Date__c = datetime.now();
     	historyTemp.Policy__c = policyTemp.Id;
     	historyTemp.Payment_Method__c = 'AC';
     	historyTemp.Claims_Prefix__c = 'A';
     	historyTemp.Claims_Suffix__c = '';
     	historyTemp.Claims_Status__c = '2';
     	historyTemp.Claims_No__c ='1234567';
     	historyTemp.Claim_Number__c = '123456';
     	
     	insert historyTemp;
     	
     	historyTemp2.Submission_Date__c = datetime.now();
        historyTemp2.Policy__c = policyTemp.Id;
        historyTemp2.Payment_Method__c = 'AC';
        historyTemp2.Claims_Prefix__c = 'SD';
        historyTemp2.Claims_Suffix__c = 'PD';
        historyTemp2.Claims_Status__c = '3';
        historyTemp2.Claims_No__c ='1234568';
        historyTemp2.Benefits_Amount__c = 10.0;
        historyTemp2.Claim_Number__c = '123455';
     	
     	insert historyTemp2;
     	
     	historyTest.PolicyNum = ''+policyTemp.Policy_Number__c+'';
     	
     	historyTest.getClaimsStatusValues();
     	historyTest.getPaymentHistory();
     	historyTest.getPolicyNum();
     	historyTest.getCurrentYear();
     	historyTest.getDispFooter();
     	historyTest.getTransHistory(); 
     	historyTest.zeroRecords = 'true';    	
     }
}