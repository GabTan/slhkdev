/*
Version        : 1.1 
Company        : Sun Life Financial 
Date           : 09.JUL.2009
Author         : Simon U. Esguerra
Description    : Hover on policy number mini page.
History        : 
	1.1		23.SEP.2009 - W.Clavero - Removed the sharing restrictions. 
   
    */
public without sharing class miniPolicyCtrlExt {
	Config_Utilities CU = new Config_Utilities();
	public String FirstInsured {get;set;}
	public String SecondInsured {get;set;}
	public String PersonAccountTypeId;
	public String AddressDisplay {get;set;}
	
	public miniPolicyCtrlExt(ApexPages.StandardController stdController){
		Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
    	Map<String,Schema.RecordTypeInfo> rtAccountMapByName = AccountSchema.getRecordTypeInfosByName();
    	Schema.RecordTypeInfo AccountRT = rtAccountMapByName.get('Person Account');
    	
    	if(AccountRT != null)
        	PersonAccountTypeId = AccountRT.getRecordTypeId();

	}  
	public miniPolicyCtrlExt(){

	}
	public void policyHover(){
		Id idCCOFirstInsuredName;
        List<ID> idCCOSecondInsuredName = new List<ID>();
        List<Account> PersonAccountFirst = new List<Account>();
        List<Account> PersonAccountSecond = new List<Account>();  
		List<Coverage__c> CoverageList= new List<Coverage__c>();
		List<Address__c> ListAddress = new List<Address__c>();	
		List<Policy__c> ListPolicy = new List<Policy__c>();
		Set<String> AddressOwnerId = new Set<String>();
		String FullAddress = '';
		Policy__c userPolicy;
		String PolicyId= ApexPages.currentPage().getParameters().get('PolicyID');
		
		if(PolicyId!=null){
			CoverageList = [select id, name, Plan_Code__c, Coverage_Number__c, Coverage_Face_Amount__c, Coverage_Sum_Insured_Amt__c, 
                                                    X1st_Life_Insured__c, X2nd_Life_Insured__c, Coverage_Status__c, Coverage_Maturity_Date__c, 
                                                    Plan_Type__c, Supplemental_Benefit_Indicator__c, Plan_Class__c, Coverage_Inforce_Date__c
                                                    from Coverage__c where Policy__r.Policy_Number__c = :PolicyId 
                                                    and Coverage_Status__c in ('1', '2', '3', '4')
                                                    order by Name limit 1];
            
            ListPolicy = [Select p.Policy_Owner_Address_Id__c, p.Id From Policy__c p where Policy_Number__c = :PolicyId and Policy_Owner_Address_Id__c!='' Limit 1];
            system.debug('ListPolicy '+ListPolicy);
            system.debug('PolicyId '+PolicyId);
            if(listPolicy.size()>0){
	            for(Policy__c LP:ListPolicy){
	            	AddressOwnerId.add(LP.Policy_Owner_Address_Id__c);
	            }
	            System.debug('xxxxxxxxxxxxxxxxxxx '+AddressOwnerId);
	            System.debug('xxxxxxxxxxxxxxxxxxx '+ListPolicy[0].Policy_Owner_Address_Id__c);
										
				ListAddress = [select id, name, Full_Address__c, Address_Status__c, County_Code__c, 
									Address_1__c, Address_2__c, Address_3__c, Address_4__c,
									Address_5__c, Address_6__c, Address_7__c, Address_8__c	
									from Address__c where Address_Id__c in :AddressOwnerId
									and  Address_Status__c = 'C'];

	            if(ListAddress.size()>0){
					System.debug('xxxxxxxxxxxxxx '+ListAddress[0].Full_Address__c);
					AddressDisplay = ListAddress[0].Full_Address__c;
				}
            }else{
            	AddressDisplay = '';
            }
            
            System.debug('xxxxxxxxxxxxxxxxxxx2 '+ListAddress);						
		}
		if(CoverageList.size()>0){
			if(CoverageList[0].X1st_Life_Insured__c!=null){
				idCCOFirstInsuredName = CoverageList[0].X1st_Life_Insured__c;
				PersonAccountFirst = [select id, LastName, FirstName, Name, RecordTypeId from Account
                                    where id = :idCCOFirstInsuredName and recordTypeId =:PersonAccountTypeId];
				if(PersonAccountFirst.size()>0){
					FirstInsured = CU.formatName(PersonAccountFirst[0].FirstName, PersonAccountFirst[0].LastName);
				}
			}	
		}
	}
	
	public static testMethod void miniPolicyCtrlExt_test(){
			
			Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
    		Map<String,Schema.RecordTypeInfo>AccountMapByName = AccountSchema.getRecordTypeInfosByName();
    		Schema.RecordTypeInfo AccountRT = AccountMapByName.get('Person Account');
    		
    		String PersonAccountTypeId = '';
        	if(AccountRT != null){
        		 PersonAccountTypeId =AccountRT.getRecordTypeId();
        	}
        	
        	Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
					PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
					FirstName='AcME', LastName='GX', RecordTypeId =  PersonAccountTypeId, 
					Client_Id__c = '!@#$%^&*()', Client_ID__pc = 'abcde12345'
					);
					
			Insert accountSuccessTest;
			
			Schema.DescribeSObjectResult PolicySchema = Schema.SObjectType.Policy__c;
    		Map<String,Schema.RecordTypeInfo>PolicyMapByName = PolicySchema.getRecordTypeInfosByName();
    		Schema.RecordTypeInfo PolicyRT = PolicyMapByName.get('Traditional');
    		
    		String TradPolicyTypeId = '';
        	if(PolicyRT != null){
        		TradPolicyTypeId =PolicyRT.getRecordTypeId();
        	}
        	
        	Address__c addressSuccessTest = new Address__c(Address_Id__c = 'abc123',
											Address_1__c = 'a', Address_2__c = 'a', Address_3__c = 'a', Address_4__c = 'a',
											Address_5__c = 'a', Address_6__c = 'a', Address_7__c = 'a', Address_8__c = 'a',
											Address_Status__c = 'C');
			insert addressSuccessTest;	
			
			Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountSuccessTest.id, policy_number__c = '887888877',
											RecordTypeId = TradPolicyTypeId, Policy_Status_Picklist_Hidden__c = '1', 
											Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
											Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
											Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
											Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
											Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
											Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
											Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = '1',
											Branch_Number__c = '234', Account_Number__c = 'Account 1', 
											Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
											Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
											Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
											Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
											Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
											Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
											Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
											Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
											Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
											Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
											Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
											Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
											Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
											Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
											Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00									
											);      
			insert policySuccessTest;
			
			Coverage__c coverageSuccessTest = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'SLCH0A', Coverage_Number__c = '01', Name='COV01',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest;
					Coverage__c coverageSuccessTest2 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'LD1H0A', Coverage_Number__c = '02', Name='COV02',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest2;	
			Coverage__c coverageSuccessTest3 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
												Plan_Code__c = 'FJPU0B', Coverage_Number__c = '03', Name='COV03',
												Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest3;
			Coverage__c coverageSuccessTest4 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
												Plan_Code__c = 'ABCDEF', Coverage_Number__c = '04', Name='COV04',
												Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest4;	
			Coverage__c coverageSuccessTest5 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
												Plan_Code__c = 'ECEHAA', Coverage_Number__c = '05', Name='COV05',
												Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest5;
			Coverage__c coverageSuccessTest6 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
												Plan_Code__c = 'RISH0A', Coverage_Number__c = '06', Name='COV06',
												Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest6;
			Coverage__c coverageSuccessTest7 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
												Plan_Code__c = 'AB2H1A', Coverage_Number__c = '07', Name='COV07',
												Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
			insert coverageSuccessTest7;
			
			pageReference pg = page.miniPolicyWindow;
			Test.setCurrentPage(pg);
			
			ApexPages.StandardController myPHW = new ApexPages.StandardController(policySuccessTest);
			ApexPages.currentPage().getParameters().put('PolicyID',policySuccessTest.Policy_Number__c);
			
			miniPolicyCtrlExt PC = new miniPolicyCtrlExt(myPHW);
			
			PC.policyHover();
			
	}
}