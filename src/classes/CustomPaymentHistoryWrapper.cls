/* 
Version     : 1.2
Company     : Sun Life Financial
Date        : 13.JUL.2009
Author      : Robert Andrew Almedina
Description : This class acts as a container for the paymet history and the amount to be displayed in the right format
History :
            1.1   <09/08/09 - Robert Andrew Almedina - bug #39 Recoded the displayed Usage value when the payment type is "Credit Card" or "Bank">
            1.2   <03.DEC.2009 - W. Clavero - Removed the call to Display_Translator to prevent it from reaching the Limit.>
*/ 

public with sharing class CustomPaymentHistoryWrapper {

    public Payment_History__c PaymentHistory { get; set; }
    public String Amount { get; set; }
    public String RcvdDate {get; set;}
    public String Usage {get; set;}
    public String Means {get; set;}
    public String myCurrency {get; set;}
    
    /* Class constructor */
    public CustomPaymentHistoryWrapper(Payment_History__c paramPYH, Decimal paramAmount, Datetime paramDateTime, String paramCurrency) 
    {
        PaymentHistory  = paramPYH;
        RcvdDate = paramDateTime.format('dd/MM/yyyy');
        myCurrency = paramCurrency;
        
        /*Decimal amt = paramAmount;
        amt = amt.setScale(2, System.RoundingMode.HALF_DOWN);
        Amount = amt.toPlainString();
        String decimalSeparator = Amount.substring(Amount.length()-3, Amount.length()-2);
        String a = paramAmount.format();

        if (a.contains(decimalSeparator))
        {
            Amount = a.substring(0, a.lastIndexOf(decimalSeparator)) + Amount.substring(amt.toPlainString().lastIndexOf(decimalSeparator));
        }
        else
        {
            Amount = a + Amount.substring(amt.toPlainString().lastIndexOf(decimalSeparator));
        }*/
        
        Amount = ESAmountFormatter.FormatAmount(paramAmount);

        
        /* w. clavero 20091203: SOQL execution removed to prevent the code from reaching the limit
        if (PaymentHistory.Payment_Type__c == 'Bank')
        {
            Usage = Display_Translator.genericTranslation('PYH_PAYUSAGE', 'RNW_PREM'); // for bug #39
            Means = Display_Translator.genericTranslation('PYH_PAYMETHOD', 'BNK_AP');
        }
        else if (PaymentHistory.Payment_Type__c == 'Credit Card')
        {
            Usage = Display_Translator.genericTranslation('PYH_PAYUSAGE', 'RNW_PREM'); // for bug #39
            Means = Display_Translator.genericTranslation('PYH_PAYMETHOD', 'CRC_AP');
        }
        else
        {
            Usage = Display_Translator.genericTranslation('PYH_PAYUSAGE', PaymentHistory.Usage_Code__c);
            Means = Display_Translator.genericTranslation('PYH_PAYMETHOD', PaymentHistory.Means_Type__c);
        }
        */
    }

}