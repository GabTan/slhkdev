/* Custom Fund Place Holder */
	//RBA 07062009 - added NA Value on the constructor
 	public class customFundObject
 	{
 		public String strFundName {get; set;}
 		public String strCurrencyISOCode {get; set;}
 		public String strCurrentAllocation {get; set;}
 		public String strNumberOfUnits {get; set;}
 		public String strFundPriceDate {get; set;}
 		public String strFundPrice {get; set;}
 		public String strInterestRate {get; set;}
 		public String strMaturityDate {get; set;}
 		public String strFundValue {get; set;}
 		public String strFundCode {get; set;}
 		public String strPendingIndicator {get; set;}
 		public Date dateValuationDate {get; set;}
 		public customFundObject(String strInputFundName, String strInputCurrencyISOCode, Decimal decInputCurrentAllocation, 
 								Decimal decInputNumberOfUnits, Date dateInputFundPriceDate, Decimal decInputFundPrice,
								Decimal decInputInterestRate, Date dateInputMaturityDate, Decimal decInputFundValue, 
								String strInputFundCode, String strInputPendingIndicator, Date dateInputValuationDate)
 		{
			/* Fund Code */
	 		if(strInputFundCode != null)
	 		{
	 			strFundCode = strInputFundCode;
	 		}
	 		else
	 		{
	 			strFundCode = 'N/A';
	 		}			 			
 			/* Fund Name */
 			if (strInputFundName != null)
 			{
	  			strFundName = strInputFundName;
 			}
 			else
 			{
 				strFundName = 'N/A';
 			}
 			/* Fund Currency */
	 		strCurrencyISOCode = strInputCurrencyISOCode;
	 		/* Current Allocation */
	 		strCurrentAllocation = '0%';
	 		if (strFundCode != null)
	 		{	 	
	 			if(dateInputMaturityDate != null &&  
	 			(strFundCode.startsWith('GF') == true || strFundCode.startsWith('GS') != true )
	 			)
	 			{	
	 				strCurrentAllocation = '100%';	
	 			}
	 			else
	 			{
	 				if(decInputCurrentAllocation != null)
	 				{
	 					strCurrentAllocation = String.valueOf(decInputCurrentAllocation.format()) + '%';
	 				}
	 			}
	 		}
 			else
 			{
 				if(decInputCurrentAllocation != null)
 				{
 					strCurrentAllocation = String.valueOf(decInputCurrentAllocation.format()) + '%';
 				}
 			}	 		
	 		/* Pending Indicator */
	 		Boolean bHasPendingIndicator = false;
	 		strPendingIndicator = '';
	 		if(strInputPendingIndicator != null && strInputPendingIndicator == 'N')
	 		{
	 			strPendingIndicator = '<b>*</b>';
	 			bHasPendingIndicator = true;
	 		}	 		
	 		/* Number of Units */
	 		strNumberOfUnits = '';
	 		if(!bHasPendingIndicator)
	 		{
		 		if(decInputNumberOfUnits != null)
		 		{
		 			strNumberOfUnits = fundCurrencyFormat(decInputNumberOfUnits);
		 		}
	 		}
	 		else
	 		{
		 		if(decInputNumberOfUnits != null)
		 		{
		 			strNumberOfUnits = fundCurrencyFormat(decInputNumberOfUnits);
		 		}	 		
	 		}
	 		/* Fund Price Date (Valuation Date) */
	 		if(dateInputFundPriceDate != null)
	 		{
	 				strFundPriceDate = String.valueOf(dateInputFundPriceDate.format());
	 		}
	 		else
	 		{
	 			strFundPriceDate = 'N/A';
	 		}
	 		/* Fund Price */
	 		if(decInputFundPrice != null && decInputFundPrice > 0)  
	 		{
	 			if(
	 				(strFundCode.substring(0,2).toUpperCase() != 'GF' || strFundCode.substring(0,2).toUpperCase() != 'GS' 
	 					|| strNumberOfUnits != 'N/A' ) && decInputNumberOfUnits > 0)
	 			{
	 				strFundPrice = fundCurrencyFormat(decInputFundPrice);
	 			}
	 			else
	 			{
	 				strFundPrice = 'N/A';
	 			}
	 		}
	 		else 
	 		{
	 			strFundPrice = 'N/A';
	 		}
	 		/* Maturity Date */
	 		if (dateInputMaturityDate != null)
	 		{
	 			strMaturityDate = String.valueOf(dateFormat(dateInputMaturityDate));
	 		}
	 		else
	 		{
	 			strMaturityDate = 'N/A';
	 		}
	 		/* Fund Value */
	 		strFundValue = '';
	 		if(decInputFundValue != null)
	 		{
	 			strFundValue = currencyFormat(decInputFundValue);	 
	 		}
	 		/* Interest Rate */
	 		if(dateInputMaturityDate == null && dateInputValuationDate == null)
	 		{
				strInterestRate = '';
	 		}
	 		else 
	 		{
	 			if (decInputInterestRate != null)
		 		{ 
		 			strInterestRate = String.valueOf(decInputInterestRate.format()) + '%';
		 		}
		 		else
		 		{
		 			strInterestRate = 'N/A';
		 		}
	 		}	 		
 		} 
		/* Generic Date Format */
		public static String dateFormat(Date dateInput)
		{
			return String.valueOf(dateInput.format());
		}
		/* Generic Currency Format */
		public static String currencyFormat(Decimal decInput)
		{ 
			Boolean bHasNegative = false;
			String strValue;
			String strDecimalSegment = '.00';
			String strIntegerSegment = '0';
			Integer iCommaCount = 0;
			strValue = String.valueOf(decInput.format());
			if(String.valueOf(decInput).contains('.'))
			{
				strIntegerSegment = strValue.split('\\.', 0)[0];			
				strDecimalSegment = String.valueOf(decInput).substring(String.valueOf(decInput).indexOf('.'), String.valueOf(decInput).length());	
				if (strDecimalSegment.length()> 3)
				{
					strDecimalSegment = strDecimalSegment.substring(0, 3);
				}
				else if(strDecimalSegment.length() == 2)
				{
					strDecimalSegment += '0';
				}		
			}
			else
			{
				strIntegerSegment = strValue;
			}
			strValue = strIntegerSegment + strDecimalSegment;	
			return strValue;
		}
		/* Fund Price Currency Format */
		public static String fundCurrencyFormat(Decimal decInput)
		{ 
			Boolean bHasNegative = false;
			String strValue;
			String strDecimalSegment = '.0000';
			String strIntegerSegment = '0';
			Integer iCommaCount = 0;
			strValue = String.valueOf(decInput);
			if(String.valueOf(decInput).contains('.'))
			{
				strIntegerSegment = strValue.split('\\.', 0)[0];			
				strDecimalSegment = String.valueOf(decInput).substring(String.valueOf(decInput).indexOf('.'), String.valueOf(decInput).length());											
				if (strDecimalSegment.length() > 5)
				{
					strDecimalSegment = strDecimalSegment.substring(0, 5);
				}
			}
			else
			{
				strIntegerSegment = strValue;
			}
			strIntegerSegment = String.valueOf(Decimal.valueOf(strIntegerSegment).format());
			strValue = strIntegerSegment + strDecimalSegment;	
			return strValue;
		}				
 	}