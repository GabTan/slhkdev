/*
Version		: 1.1
Company     : Sun Life Financial
Date        : 19.AUG.2009
Author      : W. Clavero
Description : Handles the sorting of the Fund Categories for display.
History     : 
              1.1	W. Clavero - 05.NOV.2009 - New fund category sequence.
              1.2 	JC NACHOR - 10.NOV.2009 - added getSortedFundCategoryNames method to implement a dynamic fund category assignment for the funds of a plan
                                            that is needed in CR for SunAchiever funds          
              1.3	W. Clavero - 13.NOV.2009 - New handling for incorrect data sequencing.     
*/
public class FundCategoryHandler {

  public static final String FUND_CATEGORY = 'FUND_CATEGORY';

	public static Map<String, String> getSortedFundCategories() {
		Map<String, String> fundCategories = new Map<String, String>();
		/* w. clavero 20091105: sequence has been changed
		//("display sequence", "fund category code")
		fundCategories.put('1', '1');
		fundCategories.put('2', '2');
		fundCategories.put('3', '10');
		fundCategories.put('4', '3');
		fundCategories.put('5', '4'); 
		fundCategories.put('6', '5');
		fundCategories.put('7', '6');
		fundCategories.put('8', '7');
		fundCategories.put('9', '8');
		fundCategories.put('10', '11'); 
		fundCategories.put('11', '9');
		fundCategories.put('12', '12');
		fundCategories.put('13', '13');
		fundCategories.put('14', '14');
		*/

		//("display sequence", "fund category code")
		fundCategories.put('1', '2');
		fundCategories.put('2', '10');
		fundCategories.put('3', '3');
		fundCategories.put('4', '4'); 
		fundCategories.put('5', '5');
		fundCategories.put('6', '6');
		fundCategories.put('7', '7');
		fundCategories.put('8', '8');
		fundCategories.put('9', '11'); 
		fundCategories.put('10', '9');
		fundCategories.put('11', '12');
		fundCategories.put('12', '1');

		return fundCategories;	
	}
	
  /*
   * generates a sorted list of Category Names based on a sequence and Category Code retrieved from the Config_Plan_Fund_Map
   * input: Map<Integer, String> where the key is the Category Sequence and the value is the Category Code from the Config_Plan_Fund_Map based on the current plan 
   * return: Map<String, String> that represents sorted list of Category Names. the key is the Category Code and value is the Category Name retrieved from Config_Translation_Lookups 
   *
   
  public static List<String> getSortedFundCategoryNames(Map<Integer, String> mapSequenceCategoryCode) {
    //Map<String, String> mapSortedCategoryCodeCategoryName = new Map<String, String>();
    List<String> listSortedCategoryCodeCategoryName = new List<String>(); 
    Map<String, String> mapCategoryCodeCategoryName = new Map<String, String>();
    List<Integer> listSequence = new List<Integer>();
    List<Display_Translator> listDisplayTransaltor = new List<Display_Translator>();        
    
    // Transfer the key of the map to a List so that it can be sorted       
    listSequence.addAll(mapSequenceCategoryCode.keySet());
    listSequence.sort();
    
    // retrieve the Category Name in Config_Translation_Lookups and store in a Map
    for(String strCategoryCode : mapSequenceCategoryCode.values()){
      listDisplayTransaltor.add(new Display_Translator(FUND_CATEGORY, strCategoryCode));
    } 
    mapCategoryCodeCategoryName = Display_Translator.TranslateMap(listDisplayTransaltor);
    
    // loop the sorted sequence and put the Categoy Code and Category Name in a Map
    for(Integer nSeq: listSequence){
      String strCategoryCode = mapSequenceCategoryCode.get(nSeq);
      //mapSortedCategoryCodeCategoryName.put(strCategoryCode, mapCategoryCodeCategoryName.get(strCategoryCode));
      listSortedCategoryCodeCategoryName.add(strCategoryCode + '|' + mapCategoryCodeCategoryName.get(strCategoryCode));     
    }
    
    return listSortedCategoryCodeCategoryName;
  }  	
  */
  
  public static List<String> getSortedFundCategoryNames(Map<String, Integer> mapSequenceCategoryCode) {
  
    List<String> listSortedCategoryCodeCategoryName = new List<String>(); 
    Map<String, String> mapCategoryCodeCategoryName = new Map<String, String>();
    List<Integer> listSequence = new List<Integer>();
    List<Display_Translator> listDisplayTranslator = new List<Display_Translator>();        
    Map<Integer, List<String>> seqToCodeMap = new Map<Integer, List<String>>(); 
    
    //create a map of the sequence to the fund categories and the list of categories for translation
    for (String fundCat: mapSequenceCategoryCode.keySet()) {
    	Integer seq = mapSequenceCategoryCode.get(fundCat);
    	if (seqToCodeMap.containsKey(seq)) {
    		seqToCodeMap.get(seq).add(fundCat);	
    	} else {
    		List<String> fcl = new List<String>();
    		fcl.add(fundCat);
    		seqToCodeMap.put(seq, fcl);
    	}
    	
    	listDisplayTranslator.add(new Display_Translator(FUND_CATEGORY, fundCat));
    }
    
    //sort the sequences
    listSequence.addAll(seqToCodeMap.keySet());
    listSequence.sort();
       
	//get the translated fund category descriptions
    mapCategoryCodeCategoryName = Display_Translator.TranslateMap(listDisplayTranslator);
    
    // loop the sorted sequence and put the Categoy Code and Category Name in a Map
    for(Integer nSeq: listSequence){
    	List<String> categories = seqToCodeMap.get(nSeq);
    	for (String strCategoryCode: categories) {
    		listSortedCategoryCodeCategoryName.add(strCategoryCode + '|' + mapCategoryCodeCategoryName.get(strCategoryCode));	
    	}
    }
    
    return listSortedCategoryCodeCategoryName;
  }  	
}