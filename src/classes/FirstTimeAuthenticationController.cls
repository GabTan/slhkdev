/*
Version        : 1.0 
Company        : Sun Life Financial 
Date           : 01.AUG.2009
Author         : Gilbert de Leon
Description    : First Time Authentication Controller
History        : 
                 version 1.1 - JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
                 version 1.2 - JC NCAHOR 12/03/09 - uses custom labels in display instead of a variable since the value in the variable is not being translated properly                 
*/
public without sharing class FirstTimeAuthenticationController {
    public String strSecurityAnswer {get; set;}           
    public static String strSecurityQuestion {get; set;} 
    public static String strHelpInformation {get; set;}
    public String strMaxNumberOfAttempts {get; set;}    
    public static String strUserNameError {get; set;}
    public static String strSecurityAnswerError {get; set;}  
    public String strInstanceSecurityQuestion {get; set;} 
    // The Override variables are for the testMethods
    public Boolean bOverride1 {get; set;} 
    public Boolean bOverride2 {get; set;} 
    public Boolean bOverride3 {get; set;} 
    public static Integer intSecurityGen {get; set;}
    public Integer intMaxNumberOfAttempts {get; set;}
    public Integer intNumberOfFailedAttempts {get; set;}
    public Double doubFailedAttempts {get; set;}
    public Config_Global_Settings__c[] configInstanceSettings {get; set;}
    public FirstTimeAuthenticationController (){
        bOverride1 = false; 
        bOverride2 = false; 
        bOverride3 = false;     
        ForgotPasswordController.generateSecurityQuestion();
        strSecurityQuestion = ForgotPasswordController.strSecurityQuestion;
        strInstanceSecurityQuestion = ForgotPasswordController.strSecurityQuestion;
        strHelpInformation = ForgotPasswordController.strHelpInformation;
        // JC NACHOR 12/03/2009 - assign the value generated to be usable in FirstTimeAuthentication screen
        intSecurityGen = ForgotPasswordController.intSecurityGen;
    }
    public String checkSecurityAnswer(String sUsername, String sSecurityQuestion, String sSecurityAnswer)
    {
        String strReturnMessage = '';
        sSecurityQuestion = String.escapeSingleQuotes(sSecurityQuestion); 
        sUsername = String.escapeSingleQuotes(sUsername); 
        List<String> strListValue = new List<String>();
        User[] currentUser = [select id, Client_Id__c, IsActive, Locked__c
                              from User
                              where Username = :sUsername
                              limit 1];               
        String strClientId; 
        if(!bOverride3)
        {   
            if((currentUser.size()>0) || bOverride1)
            {   
                User_Extension__c[] currentUserExtensions = [
                                        select id, User__c, Reset__c, User_Deactivated__c
                                        from User_Extension__c
                                        where User__c = :currentUser[0].id
                                        limit 1];                   
                strClientId = currentUser[0].Client_Id__c;
                if(!bOverride2)
                {
                    if((!currentUser[0].IsActive || currentUser[0].Locked__c) || bOverride1)
                    {
                        strReturnMessage = Label.Your_account_is_not_yet_activated;
                        return strReturnMessage;
                    }
                }
                if((currentUserExtensions.size()>0) || bOverride1)
                {
                    if((bOverride1 || currentUserExtensions[0].User_Deactivated__c))
                    {
                        strReturnMessage = Label.Your_account_is_not_yet_activated;
                        return strReturnMessage;                    
                    }            
                }
            }
            else
            {
                strReturnMessage = Label.Your_username_was_not_found;
                return strReturnMessage;
            }
        }
        // JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
        //if((sSecurityQuestion.equalsIgnoreCase('Legal I.D.')) || bOverride1)
        if(sSecurityQuestion.equalsIgnoreCase(Label.Legal_ID)) 
        {
            Account[] accts =  [select id, Client_Id__c  
                                from Account  
                                where (Client_ID__pc = :strClientId
                                or Client_ID__c = :strClientId)
                                and HKID__pc = :sSecurityAnswer.trim()
                                limit 1];                               
            if(accts.size() == 0)
            {               
                strReturnMessage = Label.PBM_INV_LEGALID;
                return strReturnMessage;
            }
        }
        // JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
        //else if(sSecurityQuestion.equalsIgnoreCase('Policy I.D.'))     
        else if(sSecurityQuestion.equalsIgnoreCase(Label.Policy_Id)) 
        {
            Account[] accts =  [select id, Client_Id__c 
                                from Account  
                                where (Client_ID__pc = :strClientId
                                or Client_ID__c = :strClientId) limit 1];
            if(accts.size()>0)
            {                   
                Policy__c[] policies = [select id, Policy_Number__c
                                        from Policy__c 
                                        where Policy_Status_Hidden__c in ('1', '2', '3', '4')
                                        and Client__c = :accts[0].id
                                        and Active__c = true];          
                Boolean bMatch = false;                     
                for(Policy__c policy : policies)
                {
                    if(policy.Policy_Number__c!= null && policy.Policy_Number__c.equalsIgnoreCase(sSecurityAnswer.trim()))
                    {
                        bMatch = true;
                        break;
                    }
                }
                if(!bMatch)
                {
                    strReturnMessage = Label.PBM_INV_POLID;               
                }
            }
        }
        /*
        else
        {
            strReturnMessage = sSecurityQuestion + ':[sSecurityQuestion]';
            strReturnMessage += sSecurityAnswer + ':[sSecurityAnswer]';
            strReturnMessage += sUsername + ':[sUsername]';
        }
        */
        return strReturnMessage;
    }  
    public PageReference doAuthentication() 
    {
            // Validations Start
            configInstanceSettings = [select USR_First_Login_AGP__c, USR_Forgot_Pwd_Retry_Interval__c, USR_Max_Login_Attempts__c,
            									SitesURL__c
                                                from Config_Global_Settings__c
                                                order by CreatedDate desc
                                                limit 1];          
            PageReference pref; 
            String strLockedReason = '';
            String strUserName = System.Userinfo.getUserName();     
            User[] users = [select id, name, Locked__c, Failed_Attempts__c,
                            Resend_Date__c, Lock_Reason__c, IsActive,
                            Locked_Date__c, First_Login_Date__c,
                            Days_Since_Reset__c
                            from User 
                            where username = : strUserName 
                            limit 1]; 
        if(!bOverride3)
        {                                               
            if((configInstanceSettings.size()>0 && configInstanceSettings[0].USR_Max_Login_Attempts__c != null)|| bOverride1)
            {
                strMaxNumberOfAttempts = configInstanceSettings[0].USR_Max_Login_Attempts__c;
            }                       
            strSecurityAnswerError = '';
            intMaxNumberOfAttempts = 0;
            intNumberOfFailedAttempts = 0;
            doubFailedAttempts = 0;
            intMaxNumberOfAttempts = Integer.valueOf(strMaxNumberOfAttempts);
            if((users[0].Failed_Attempts__c == null) || bOverride1)
            {
                intNumberOfFailedAttempts = 0;
            }
            else if((users[0].Failed_Attempts__c != null) || bOverride2)
            {
                intNumberOfFailedAttempts = Integer.valueOf(String.valueOf(users[0].Failed_Attempts__c));   
            }
            if((intNumberOfFailedAttempts >= intMaxNumberOfAttempts-1) || bOverride2)
            {
                    strLockedReason = 'Authentication Failed';
                    doubFailedAttempts = 1;
                    if((users[0].Failed_Attempts__c != null) || bOverride1)
                    {
                        doubFailedAttempts = Double.valueOf(String.valueOf(users[0].Failed_Attempts__c))+1;
                    }               
                    User updateUser = new User(id = users[0].id, Locked__c = true, Locked_Date__c = System.now(), 
                                                Failed_Attempts__c = doubFailedAttempts, 
                                                Lock_Reason__c = strLockedReason);                                      
                    Database.update(updateUser);
                    User_Extension__c[] userExtensions = [select id from User_Extension__c where User__c = :users[0].id limit 1];
                    if((userExtensions.size()>0)|| bOverride1)
                    {
                        User_Extension__c updateUserExtension = new User_Extension__c(id = userExtensions[0].id, User__c = users[0].id, 
                                                                        User_Deactivated__c = true);
                        Database.update(updateUserExtension);  
                    }
                    else
                    {
                        User_Extension__c updateUserExtension = new User_Extension__c(User__c = users[0].id, User_Deactivated__c = true);
                        Database.insert(updateUserExtension);
                    }          
                    pref = new PageReference(configInstanceSettings[0].SitesURL__c + '/AccountLocked');
                    pref.setRedirect(true); 
                    return pref;            
            }   
            if((strSecurityAnswer.trim().length()<1) || bOverride1)
            {
                strSecurityAnswerError = Label.You_must_enter_value; 
            } 
            else
            {                           
                strSecurityAnswerError = checkSecurityAnswer(strUserName, strInstanceSecurityQuestion, strSecurityAnswer);
            }
            if((strSecurityAnswerError.trim().length()>1) || bOverride1)
            {                 
                strSecurityAnswerError = Label.CP_Error + strSecurityAnswerError + Label.Please_review_the_information_below;
            } 
        // Validations End 
        }
        try
        {
            Boolean bSuccess = false;
            if ((strSecurityAnswerError.trim() == '') || bOverride1)
            {
                bSuccess = true;
            }           
            if ((bSuccess)  || bOverride1)
            { 
                Double doubResetFailedAttempts = 0;
                User updateUser = new User(id = users[0].id, First_Login_Date__c = System.now(), 
                                            Last_Login_Date__c = System.now(),
                                            Failed_Attempts__c = doubResetFailedAttempts);
                Database.update(updateUser);
                /* JC NACHOR 08/12/2009 changed URL of home page on successful authentication */
                //String strURL = '/home/home.jsp';
                String strURL = '/apex/Disclaimer';
                pref = new PageReference(strURL);                               
                pref.setRedirect(true); 
                return pref;            
            }
            else
            {
                
                Id idUpdateUser = users[0].id;
                doubFailedAttempts = 1;
                if((users[0].Failed_Attempts__c != null)|| bOverride1)
                {
                    doubFailedAttempts = Double.valueOf(String.valueOf(users[0].Failed_Attempts__c))+1;
                }           
                User updateUser = new User(id = idUpdateUser, 
                                        Failed_Attempts__c = doubFailedAttempts);
                Database.update(updateUser);
                
            }
        }
        catch(Exception e)
        {
            strSecurityAnswerError = 'Exception: ' + String.valueOf(e);
            return null;
        }       
        return null;        
    }    
}