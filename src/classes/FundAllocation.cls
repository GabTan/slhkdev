/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 22.JUL.2009
Author		: Ryan Dave Wilson
Description	: Storing Attribute values for Funds Allocated.
History	:

*/

public class FundAllocation {
	public String code {get; set;}
	public String name {get; set;}
	public String percent {get; set;}
	public Boolean createNew {get; set;}
}