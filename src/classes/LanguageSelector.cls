public class LanguageSelector {
    //RBA 07022009 - Updated constructor to accomodate standard controller initialization
    
    Boolean reload=false;
    String currentLang;
    //public LanguageSelector(ApexPages.StandardController stdController){
    //}
        
    public LanguageSelector(NewPolicyInquiry controller) {
        currentLang=getCurrentUserLanguage();
    }
    
    public LanguageSelector() {
        currentLang=getCurrentUserLanguage();
    }
     
    public Boolean getReload() {
        return this.reload;
    }
      
    public String getCurrentLang() {
        return this.currentLang;
    }

    public PageReference updateLanguage(String tlang) {
        User u=new User(id=UserInfo.getUserId());
        u.LanguageLocaleKey=tlang;
        update u;
        currentLang=tlang;
        reload=true;
        return null;
    }
    public PageReference updateLanguageEN() {
        return updateLanguage('en_US');
        //return null;
    }
    public PageReference updateLanguageCN() {
        return updateLanguage('zh_CN');
        //return null;
    }
    public PageReference updateLanguageTW() {
        return updateLanguage('zh_TW');
        //return null;
    }
    public string getCurrentUserLanguage() {
        return [select LanguageLocaleKey from User where Id=:Userinfo.getuserId()].LanguageLocalekey;
    }
    
    public static testMethod void testChangeLanguage() {
        LanguageSelector lgselect=new LanguageSelector();
        User u;
        PageReference pageref;
        pageref=lgselect.updateLanguageEN();
        String lang=lgselect.getCurrentUserLanguage();
        System.debug('User Language is:'+lang);
        System.assert(lgselect.getCurrentLang()==lang, 'Incorrect Language');
        System.assert(lgselect.getreload()==true, 'Reload flag not set');

        
        pageref=lgselect.updateLanguageCN();
        lang=lgselect.getCurrentUserLanguage();
        System.debug('User Language is:'+lang);
        System.assert(lgselect.getCurrentLang()==lang, 'Incorrect Language');
        
        pageref=lgselect.updateLanguageTW();
        lang=lgselect.getCurrentUserLanguage();
        System.debug('User Language is:'+lang);
        System.assert(lgselect.getCurrentLang()==lang, 'Incorrect Language');
    }
    
    //S - MARSO - 20130523 - SR-ES-13005
    public PageReference logout()
    {        
        User u = [select Client_Id__c, Last_Disclaimer_Accept_Time__c from user where id = :UserInfo.getUserId()];
        Account a = [select Id, Opt_Out__c, Opt_Out_DT__c, Opt_Out_Prev__c, Opt_Out_DT_Prev__c from Account where Client_Id__c = :u.Client_Id__c];
        
    if (u.Last_Disclaimer_Accept_Time__c < a.Opt_Out_DT_Prev__c)
    {
        a.Opt_Out_Prev__c = a.Opt_Out__c;
            a.Opt_Out_DT_Prev__c = a.Opt_Out_DT__c;

            update a;
    }
        
        PageReference pg = new PageReference('/secur/logout.jsp');
        pg.setRedirect(true);         
        return pg;
        
    }
    //E - MARSO - 20130523 - SR-ES-13005
}