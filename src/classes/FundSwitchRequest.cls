/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Ryan Dave Wilson
Description	: Storing Attribute values for Txn.
History	:

*/

public class FundSwitchRequest {
	public String reqType {get; set;}
	public String refNum {get; set;}
	public String policyNo {get; set;}
	public String subDateTime {get; set;}
	
	public List<SwitchOutRequest> switchOut {get; set;}
	public List<SwitchInRequest> switchIn {get; set;}
	
	public FundSwitchRequest(){
		switchOut = new List<SwitchOutRequest>();
		switchIn = new List<SwitchInRequest>();
	}
}