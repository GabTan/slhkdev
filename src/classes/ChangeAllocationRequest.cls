/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 22.JUL.2009
Author		: Ryan Dave Wilson
Description	: Storing Attribute values for Txn CHM requirement.
History	:

*/

public class ChangeAllocationRequest {
	public String reqType {get; set;}
	public String refNum {get; set;}
	public String policyNo {get; set;}
	public String subDateTime {get; set;}
	
	public List<FundAllocation> fundAlloc {get; set;}
	
	public ChangeAllocationRequest(){
		fundAlloc = new List<FundAllocation>();
	}
}