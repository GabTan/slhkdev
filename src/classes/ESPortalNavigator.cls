/*
Version		: 1.3
Company     : Sun Life Financial
Date        : 15.JUL.2009
Author      : W. Clavero
Description : User interface handler and constants.
History     : 
              version 1.1 JC NACHOR 	09/22/09 - comment out custom change password page. reuse old code.
              version 1.2 JC NACHOR 	09/24/09 - added error page when user tries to access another's Policy
              version 1.3 Robby Angeles 10/27/09 - removed reference to Inbox tab (MENU_IDX_INBOX).
              version 1.4 JC NACHOR 12/01/09 - added menu item element for Contact Us
*/
public class ESPortalNavigator {
    //delimiters 
    public static final String MENU_SEPARATOR           = '|';
    public static final String PARAM_SEPARATOR          = ';';  
    
    //language
    public static final String LANG_CHI_TRADITIONAL     = 'zh_TW';
    public static final String LANG_ENG_USA             = 'en_US';
    
    //main menu
    public static final Integer MENU_IDX_HOME           = 0;
    //public static final Integer MENU_IDX_INBOX          = 1; R Angeles 10272009 - removed reference
    public static final Integer MENU_IDX_SERVICES       = 1;
    public static final Integer MENU_IDX_HELP           = 2;
    public static final Integer MENU_IDX_DOWNLOAD       = 3;
    public static final Integer MENU_IDX_ACCOUNT        = 4;
    // JC NACHOR 12/01/09 - added menu item element for Contact Us
    public static final Integer MENU_IDX_CONTACT_US     = 5;
    
    //services menu
    public static final Integer MENU_IDX_SUB_TXN        = 0;
    public static final Integer MENU_IDX_SUB_CPP        = 1;
    public static final Integer MENU_IDX_SUB_FSW        = 2;
    public static final Integer MENU_IDX_SUB_CHM        = 3;
    //help menu
    public static final Integer MENU_IDX_SUB_SEC        = 0;
    public static final Integer MENU_IDX_SUB_SVC        = 1;

    //portal navigation IDs
    public static final Integer NAV_HOM_MAIN            = 100;
    public static final Integer NAV_HOM_WELCOME         = 100;  
    public static final Integer NAV_HOM_POLICY          = 101;
    public static final Integer NAV_HOM_PYH             = 102;
    public static final Integer NAV_HOM_FTH             = 103;
    
    public static final Integer NAV_INB_MAIN            = 200;
    
    public static final Integer NAV_SVC_MAIN            = 300;
    public static final Integer NAV_SVC_TXN             = 300;

    public static final Integer NAV_SVC_CPP_NOTES       = 310;
    public static final Integer NAV_SVC_CPP_INPUT       = 311;
    public static final Integer NAV_SVC_CPP_FINAL       = 312;
    public static final Integer NAV_SVC_CPP_PDF         = 313;

    public static final Integer NAV_SVC_FSW_POLICY      = 320;
    public static final Integer NAV_SVC_FSW_NOTES       = 321;
    public static final Integer NAV_SVC_FSW_INPUT       = 322;
    public static final Integer NAV_SVC_FSW_FINAL       = 323;
    public static final Integer NAV_SVC_FSW_PDF         = 324;

    public static final Integer NAV_SVC_CHM_POLICY      = 330;
    public static final Integer NAV_SVC_CHM_NOTES       = 331;
    public static final Integer NAV_SVC_CHM_INPUT       = 332;
    public static final Integer NAV_SVC_CHM_FINAL       = 333;
    public static final Integer NAV_SVC_CHM_PDF         = 334;
    
    public static final Integer NAV_HLP_MAIN            = 400;
    public static final Integer NAV_HLP_SEC             = 400;
    public static final Integer NAV_HLP_SVC             = 401;

    public static final Integer NAV_DWN_MAIN            = 500;
    
    public static final Integer NAV_ACC_MAIN            = 600;
    
    // JC NACHOR 12/01/09 - added menu item element for Contact Us
    public static final Integer NAV_CUS_MAIN            = 700;

    //portal navigation URLs
    public static final String NAV_URL_MAIN             = '/apex/TabbedMainPage';
    public static final String NAV_URL_MAIN_PARAMS      = 'nav';
    
    public static final String NAV_URL_HOM              = '/apex/EServicesHome';
    public static final String NAV_URL_HOM_POLICY       = '/apex/NewPolicyInquiry';
    public static final String NAV_URL_HOM_PYH          = '/apex/PaymentHstyView';
    public static final String NAV_URL_HOM_FTH          = '/apex/FundTxnHstyView';
    
    public static final String NAV_URL_SVC_TXN          = '/apex/TxnLogHistory';
    
    public static final String NAV_URL_CPP_NOTES        = '/apex/ChangeInfoPreValidation';
    public static final String NAV_URL_CPP_INPUT        = '/apex/ChangeContactInfo';
    public static final String NAV_URL_CPP_FINAL        = '/apex/ChangeInfoAcknowledge';
    public static final String NAV_URL_CPP_PDF          = '/apex/ChangeInfoAcknowledgePDF';

    public static final String NAV_URL_FSW_POLICY       = '/apex/FundSwitchPoliciesList';
    public static final String NAV_URL_FSW_NOTES        = '/apex/FundSwitchImportantNotes';
    public static final String NAV_URL_FSW_INPUT        = '/apex/FundSwitchInput';
    public static final String NAV_URL_FSW_FINAL        = '/apex/FSWAcknowledgement';
    public static final String NAV_URL_FSW_PDF          = '/apex/FSWAcknowledgementPDF';
    
    public static final String NAV_URL_CHM_POLICY       = '/apex/CHM_PoliciesView';
    public static final String NAV_URL_CHM_NOTES        = '/apex/ChangeAllocImportanNotes';
    public static final String NAV_URL_CHM_INPUT        = '/apex/ChangeAllocInput';
    public static final String NAV_URL_CHM_FINAL        = '/apex/CHMAcknowledgement';
    public static final String NAV_URL_CHM_PDF          = '/apex/CHMAcknowledgementPDF';


	public static final String NAV_URL_HLP_SEC_MAIN     = '/apex/SecurityOnlineHelp';
	public static final String NAV_URL_HLP_SVC_MAIN     = '/apex/TransactionOnlineHelp';
	
    public static final String NAV_URL_HLP_SEC_ENG      = '/apex/HLP_ENG_Security';
    public static final String NAV_URL_HLP_SEC_CHI      = '/apex/HLP_CHI_Security';

    public static final String NAV_URL_HLP_SVC_ENG      = '/apex/HLP_ENG_Services';
    public static final String NAV_URL_HLP_SVC_CHI      = '/apex/HLP_ENG_Services';
    
    
    //public static final String NAV_URL_ACC_CHPWD        = '/apex/ChangePasswordContainer';
    // JC NACHOR 09/22/09 - comment out custom change password page. reuse old code.
    //public static final String NAV_URL_ACC_CHPWD        = '/apex/NewChangePassword';
    public static final String NAV_URL_ACC_CHPWD        = '/apex/ChangePasswordContainer';
    public static final String CHPWD_REG_EVENT_VAL      = '1';
    public static final String CHPWD_REG_EVENT          = '?reg=1';
    public static final String CHPWD_FIRST_EVENT        = '?reg=0';
    public static final String NAV_URL_ACC_LOGOUT       = '/secur/logout.jsp';
    
    public static final String NAV_URL_DWN_DOWNLOADS    = '/apex/DownloadCorner';
    
    // JC NACHOR 09/25/09 - added error page when user tries to access another's Policy
    public static final String NAV_URL_ERR_NO_ACCESS_POLICY    = '/apex/NoAccessToPolicy';
    
    // JC NACHOR 12/01/09 - added menu item element for Contact Us
    public static final String NAV_URL_CUS_HOTLINE    = '/apex/ESContactUs';
    

    //split unicode strings for menu items
    public static List<String> getMenuItems(String combinedItems) {
        List<String> menuItems = new List<String>();
        String tempItems = combinedItems;
        Integer i = tempItems.indexOf(MENU_SEPARATOR);
        
        while (i > -1) {
            String menuItem = tempItems.substring(0, i);
            tempItems = tempItems.substring(i + 1);
            menuItems.add(menuItem);
            i = tempItems.indexOf(MENU_SEPARATOR);
        }
        menuItems.add(tempItems);
        
        return menuItems;
    }
/*
    //retrieve the URL for a main page menu item
    public static String getPortalNavURL(Integer navigationID) {
        String cpURL = NAV_URL_MAIN + '?' + NAV_URL_MAIN_PARAMS + '=' + navigationID;
        return cpURL;
    }
*/    
    //retrieve the URL for a main page menu item
    public static String getCustomerPortalNavURL(Integer navigationID) {
        String subURL;
        Integer subIndex = Math.mod(navigationID, NAV_HOM_MAIN);
        
        //services
        if ((navigationID - subIndex) == NAV_SVC_MAIN) {
            if (navigationID < NAV_SVC_CPP_NOTES) { 
                subURL = NAV_URL_SVC_TXN; //TXN
            } else if (navigationID < NAV_SVC_FSW_POLICY) { //CPP
                if (navigationID == NAV_SVC_CPP_INPUT) {
                    subURL = NAV_URL_CPP_INPUT;
                } else if (navigationID == NAV_SVC_CPP_FINAL) {
                    subURL = NAV_URL_CPP_FINAL;
                } else if (navigationID == NAV_SVC_CPP_PDF) {
                    subURL = NAV_URL_CPP_PDF;
                } else {
                    subURL = NAV_URL_CPP_NOTES;
                }
            } else if (navigationID < NAV_SVC_CHM_POLICY) { //FSW
                if (navigationID == NAV_SVC_FSW_NOTES) {
                    subURL = NAV_URL_FSW_NOTES;
                }else if (navigationID == NAV_SVC_FSW_INPUT) {
                    subURL = NAV_URL_FSW_INPUT;
                } else if (navigationID == NAV_SVC_FSW_FINAL) {
                    subURL = NAV_URL_FSW_FINAL;
                } else if (navigationID == NAV_SVC_FSW_PDF) {
                    subURL = NAV_URL_FSW_PDF;
                } else {
                    subURL = NAV_URL_FSW_POLICY;
                }
            } else { //CHM
                if (navigationID == NAV_SVC_CHM_NOTES) {
                    subURL = NAV_URL_CHM_NOTES;
                }else if (navigationID == NAV_SVC_CHM_INPUT) {
                    subURL = NAV_URL_CHM_INPUT;
                } else if (navigationID == NAV_SVC_CHM_FINAL) {
                    subURL = NAV_URL_CHM_FINAL;
                } else if (navigationID == NAV_SVC_CHM_PDF) {
                    subURL = NAV_URL_CHM_PDF;
                } else {
                    subURL = NAV_URL_CHM_POLICY;
                }
            }
        } else if (navigationID == NAV_ACC_MAIN) {
            subURL = NAV_URL_ACC_CHPWD;
        } else if ((navigationID - subIndex) == NAV_HLP_MAIN) {
            if (navigationID == NAV_HLP_SVC) {
                subURL = NAV_URL_HLP_SVC_MAIN;
            } else {
                subURL = NAV_URL_HLP_SEC_MAIN;
            }           
            /*
            if (Userinfo.getLanguage().equals(LANG_CHI_TRADITIONAL)) {
                if (navigationID == NAV_HLP_SVC) {
                    subURL = NAV_URL_HLP_SVC_CHI;
                } else {
                    subURL = NAV_URL_HLP_SEC_CHI;
                }           
            } else {
                if (navigationID == NAV_HLP_SVC) {
                    subURL = NAV_URL_HLP_SVC_ENG;
                } else {
                    subURL = NAV_URL_HLP_SEC_ENG;
                }           
            } */
        } else if (navigationID == NAV_DWN_MAIN) {
            subURL = NAV_URL_DWN_DOWNLOADS;
        // JC NACHOR 12/01/09 - added menu itme element for Contact Us
        } else if (navigationID == NAV_CUS_MAIN) {
        	  subURL = NAV_URL_CUS_HOTLINE;    
        } else {  //default to home
            subURL = NAV_URL_HOM;
        }
        
        return subURL;
    }   
/*    
    //redirect the main page to the menu tab
    public static PageReference redirectToPage(Integer navigationID) {
        PageReference pgRef = new PageReference(getCustomerPortalNavURL(navigationID));
        pgRef.setRedirect(true);
        return pgRef;
    }

    //retrieve the URL to load in the main page's iframe
    public static String getSubPageURL(Integer navigationID, String paramValues) {
        String subURL;
        Integer subIndex = Math.mod(navigationID, NAV_HOM_MAIN);
        
        //services
        if ((navigationID - subIndex) == NAV_SVC_MAIN) {
            if (navigationID < NAV_SVC_CPP_NOTES) { 
                subURL = NAV_URL_SVC_TXN; //TXN
            } else if (navigationID < NAV_SVC_FSW_POLICY) { //CPP
                if (navigationID == NAV_SVC_CPP_INPUT) {
                    subURL = NAV_URL_CPP_INPUT;
                } else if (navigationID == NAV_SVC_CPP_FINAL) {
                    subURL = NAV_URL_CPP_FINAL;
                } else if (navigationID == NAV_SVC_CPP_PDF) {
                    subURL = NAV_URL_CPP_PDF;
                } else {
                    subURL = NAV_URL_CPP_NOTES;
                }
            } else if (navigationID < NAV_SVC_CHM_POLICY) { //FSW
                if (navigationID == NAV_SVC_FSW_NOTES) {
                    subURL = NAV_URL_FSW_NOTES;
                }else if (navigationID == NAV_SVC_FSW_INPUT) {
                    subURL = NAV_URL_FSW_INPUT;
                } else if (navigationID == NAV_SVC_FSW_FINAL) {
                    subURL = NAV_URL_FSW_FINAL;
                } else if (navigationID == NAV_SVC_FSW_PDF) {
                    subURL = NAV_URL_FSW_PDF;
                } else {
                    subURL = NAV_URL_FSW_POLICY;
                }
            } else { //CHM
                if (navigationID == NAV_SVC_CHM_NOTES) {
                    subURL = NAV_URL_CHM_NOTES;
                }else if (navigationID == NAV_SVC_CHM_INPUT) {
                    subURL = NAV_URL_CHM_INPUT;
                } else if (navigationID == NAV_SVC_CHM_FINAL) {
                    subURL = NAV_URL_CHM_FINAL;
                } else if (navigationID == NAV_SVC_CHM_PDF) {
                    subURL = NAV_URL_CHM_PDF;
                } else {
                    subURL = NAV_URL_CHM_POLICY;
                }
            }
        } else if ((navigationID - subIndex) == NAV_ACC_MAIN) {
            subURL = NAV_URL_ACC_CHPWD;
        } else {  //default to home
            subURL = NAV_URL_HOM;
        }
        
        if (paramValues != null) {
            subURL += (paramValues.equals('') ? '' : '?' + paramValues);
        }
        
        return subURL;
    }
    */ 
}