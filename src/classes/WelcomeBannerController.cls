public class WelcomeBannerController {
    
    Config_Marketing__c activeBanner;
    Integer bannerIndex;
    Integer bannerIndexMax;
    Integer bannerURLIndex = 0;
    Boolean bFirstLoad=True;
    String bannerId; 
    String bannerURL;
    String gotoURL;
    
    List<string> bannerURLList = new List<String>();
    
    List<Config_Marketing__c> bannerList = new List<Config_Marketing__c>();
    List<Config_Marketing__c> attachmentBannerList = new List<Config_Marketing__c>();
    public WelcomeBannerController(){ 	 
    	 Double bannerDuration;
    	/*Query all for config marketing object*/
	        bannerList = [Select Name, URL__c, Language__c, Id, Effective_Start_Date__c, Effective_End_Date__c, Display_Duration__c, 
	                     (Select Id, Name From Attachments order by LastModifiedDate) 
	                     From Config_Marketing__c where Effective_Start_Date__c <= TODAY and Effective_End_Date__c >=TODAY];  
	                     
	        /*get default banner if list is empty*/
	        if(bannerList.size()==0){
	            bannerList = [Select Name, URL__c, Language__c, Id, Effective_Start_Date__c, Effective_End_Date__c, Display_Duration__c, 
	                     (Select Id, Name From Attachments) 
	                     From Config_Marketing__c where Name='Default Banner'];
	        }
    	 
    	 
	        /*Check if banner object contains an attachment*/
	        for (Config_Marketing__c banner:bannerList){
				if(banner.Attachments.size()>0){
					attachmentBannerList.add(banner);
					bannerURLList.add(banner.URL__c); 
				}
	        }
	        
	        if(attachmentBannerList.size()>0){
	        	bannerIndex=0;
				bannerIndexMax = attachmentBannerList.size()-1;
				bannerDuration = attachmentBannerList[bannerIndex].Display_Duration__c * 1000;
		    	Config_Marketing__c tempBanner = new Config_Marketing__c(Name=attachmentBannerList[bannerIndex].Name, URL__c=attachmentBannerList[bannerIndex].URL__c, Id=attachmentBannerList[bannerIndex].Id, Display_Duration__c=bannerDuration);
		        bannerId = attachmentBannerList[bannerIndex].Attachments[0].Id;
		        activeBanner=tempBanner;
		        gotoURL=attachmentBannerList[bannerIndex].URL__c;     	
	        }
       
    }
    public String getBannerDisplay(){
    	String bannerPath;
    	try{
    		bannerPath='/servlet/servlet.FileDownload?file='+bannerId;
    	}Catch(exception e){
    	 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR,'No Marketing Banners are available as of the moment ');
            ApexPages.addMessage(myMsg);
    	}
    	return bannerPath;
    }
    public double getDisplayDuration(){
    	double display;
    	try{
    		display = activeBanner.Display_Duration__c;
    	}Catch(exception e){
    	 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR,'No Marketing Banners are available as of the moment ');
            ApexPages.addMessage(myMsg);
    	}
    	return display;
    }
    public String nextBannerDisplay(){
    	Double bannerDuration;
    	bannerURL=null;
    	System.debug('bannerIndex '+bannerIndex);
    	try{
	    	if(bannerIndex<bannerIndexMax){
	    		bannerIndex+=1;
	    	}else{
	   			bannerIndex=0;
	    	}
	    	system.debug('bannerURL '+bannerURL);
	    	bannerDuration = attachmentBannerList[bannerIndex].Display_Duration__c * 1000;
	    	Config_Marketing__c tempBanner = new Config_Marketing__c(Name=attachmentBannerList[bannerIndex].Name, URL__c=attachmentBannerList[bannerIndex].URL__c, Id=attachmentBannerList[bannerIndex].Id, Display_Duration__c=bannerDuration);
	        bannerId = attachmentBannerList[bannerIndex].Attachments[0].Id;
	        activeBanner=tempBanner;
	        system.debug('$$$ '+activeBanner.URL__c);
	        gotoURL=attachmentBannerList[bannerIndex].URL__c;
	        system.debug('$$$2 '+gotoURL);
	    }Catch(exception e){
    	 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR,'No Marketing Banners are available as of the moment ');
            ApexPages.addMessage(myMsg);
    	}
    	return null;
    }
    public string clickRate(){
    	Marketing_Click_Rate__c MCR = new Marketing_Click_Rate__c (Banner_Name__c = activeBanner.Name, Client_ID__c=userInfo.getUserId());
    	insert MCR;
    	return null;
    }
    
    public string getgotoURL(){
    	return gotoURL;
    	
    }
    public void setgotoURL(String x){
    	gotoURL = x;
    }
    
    public boolean getrenderBanner(){
    	if(bannerList.size()>0){
    		return true;
    	}
    	return false;
    }
    
    public static testMethod void banner_test(){
    	WelcomeBannerController banner = new WelcomeBannerController();
    
    	banner.getBannerDisplay();
    	banner.getDisplayDuration();
    	banner.nextBannerDisplay();
    	banner.clickRate();
    	banner.getgotoURL();
    	banner.setgotoURL('http://yahoo.com');
    	
    }
}