/*
Version		: 1.4
Company		: Sun Life Financial
Date		: 23.JUL.2009
Author		: Ryan Dave Wilson
Description	: Fund Switching acknowledgement page controller.
History	:
	[1.1] <30.JUL.2009 - Ryan Wilson   - added methods for getting the URL for Navigating between pages>
	[1.2] <28.OCT.2009 - Robby Angeles - added method to check security for admin portal users (if page is Acknowledgement PDF)
	[1.3] <10.NOV.2009 - Robby Angeles - added config utility parseExtCode method to parse external fund code from TxnData name attribute.>
	[1.4] <24.NOV.2009 - Robby Angeles - updated in lieu of recent changes to target controller.
	      <24.NOV.2009 - JC NACHOR - add parameter filtering to prevent cross site scripting
*/

public without sharing class FSWAcknowledgementCtrl{
  // JC NACHOR - change to String instead of Id
	//Id transId;
	String transId;
	Display_Translator[] switchOutFunds = new Display_Translator[]{};
	Display_Translator[] switchInFunds = new Display_Translator[]{};
	Map<string, string> swOutFundName = new Map<string, string>();
	Map<string, string> swInFundName = new Map<string, string>();
	String fundNameSWIN;
	String fundNameSWOUT;
	
	Config_Utilities CU = new Config_Utilities();
	
    // JC NACHOR - global flag to determine if transaction is accessible to the user
    public boolean isValidTxn = true;	
	
    public PageReference Security() 
    {
	      Boolean isPortalUser = true;
	      // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
	      //PageReference pRefRedirect = Page.NoAccessToPolicy;
	      PageReference pRefRedirect = new Pagereference('/apex/NoAccessToPolicy?type=transaction');
	      String strId = ApexPages.currentPage().getParameters().get('transId');            
	      pRefRedirect.setRedirect(true);
	      //R.Angeles bypass security if logged in user is admin and current page is PDF 
	      String pageRef = ApexPages.currentPage().getURL();
	      this.transId   = ApexPages.currentPage().getParameters().get('transId');
	      if(pageRef.indexOf('PDF') != -1){
	           isPortalUser = CU.isPortalUser('');
	      }
	      if(isPortalUser){ 
	      	
        // START JC NACHOR - add parameter filtering to prevent cross site scripting
        if(strId.length() > 18 && strId.length() < 15){
          isValidTxn = false;
          return pRefRedirect;
        }  	      	
	      	
		      if(ESAccessVerifier.isCurrentUsersTransaction((strId==null)?'':strId)){ 
		        return null;
		      } else {
		      	isValidTxn = false;
		        return pRefRedirect;
		      }
	      }
	      else{
	      		return null;
	      }
	      //END
    } 	
    
	public FundSwitchRequest getFSWxmlData(){
		
		if(isValidTxn) {
		
		SwitchInRequest fswSWIn = new SwitchInRequest();
		SwitchInRequest fswSpace = new SwitchInRequest();
		FswTransactionXMLGen fswXmlMethod = new FswTransactionXMLGen();
		Decimal swIntoDecimal = 0.0;
		FundSwitchRequest fswXmlOutput = new FundSwitchRequest();
		
		Transaction__c xmldata = [Select id, Policy_ID__c, Client_Id__c, TxnData__c from Transaction__c where id=: transId];
		
		fswXmlOutput = fswXmlMethod.readFSWxml(xmldata.TxnData__c);
		//fswXmlOutput = fswXmlMethod.readFSWxml(sampleXml);
		
		for(integer x=0; x<fswXmlOutput.switchOut.size(); x++){
			switchOutFunds.add(new Display_Translator(TransactionsReqConstant.FUND_NAME_DT, fswXmlOutput.switchOut[x].code));	
		}
		for(integer y=0; y<fswXmlOutput.switchIn.size(); y++){
			switchInFunds.add(new Display_Translator(TransactionsReqConstant.FUND_NAME_DT, fswXmlOutput.switchIn[y].code));	
		}
		
		swOutFundName = Display_Translator.TranslateMap(switchOutFunds);
		swInFundName = Display_Translator.TranslateMap(switchInFunds);
		
		for(integer cnt=0; cnt<fswXmlOutput.switchOut.size(); cnt++){
			fundNameSWOUT = swOutFundName.get(fswXmlOutput.switchOut[cnt].code);
			fswXmlOutput.switchOut[cnt].name = fundNameSWOUT + CU.parseExtCode(fswXmlOutput.switchOut[cnt].name);
		}
		for(integer ctr=0; ctr<fswXmlOutput.switchIn.size(); ctr++){
			fundNameSWIN = swInFundName.get(fswXmlOutput.switchIn[ctr].code);
			fswXmlOutput.switchIn[ctr].name = fundNameSWIN + CU.parseExtCode(fswXmlOutput.switchIn[ctr].name);
		}
		
		fswSpace.percent = '*';
		fswXmlOutput.switchIn.add(fswSpace);
		fswSWIn.name ='Total';
		fswSWIn.percent = '100';
		for(integer i=0; i<fswXmlOutput.switchIn.size()-1; i++){
			swIntoDecimal += fswXmlOutput.switchIn[i].estAmntDecimal;
			//fswXmlOutput.switchIn[i].estAmntDecimal;
		}
		//swIntoDecimal = swIntoDecimal.setScale(2,System.RoundingMode.HALF_DOWN);
		fswSWIn.estiamount = ESAmountFormatter.FormatAmount(swIntoDecimal);
		fswXmlOutput.switchIn.add(fswSWIn);
		
		return fswXmlOutput;
		
		} else {
			return null;
		}
	}
	
	public String getClientName(){
		
		if(isValidTxn) {
		
		Transaction__c AcctName = [Select Client__r.Id, Client__r.FirstName, Client__r.LastName from Transaction__c where Id =:transId];
		//User uName = [Select FirstName, Id, LastName from User where Id=: UserInfo.getUserId()];
		
        string usrName;
        
        usrName = CU.formatName(AcctName.Client__r.FirstName, AcctName.Client__r.LastName);
        //usrName = formattedName.formatName(uName.FirstName, uName.LastName);

        return usrName;
        
		} else {
			return null;
		}
	}
	
	public String getNavPreValidation(){
		String espParam = ESPortalNavigator.NAV_URL_CHM_POLICY;
		return espParam;
	}
	
	public String getNavTxnLog(){
		String espParam = ESPortalNavigator.NAV_URL_SVC_TXN;
		return espParam;
	}
	
  // JC NACHOR - change to String instead of Id
	//public ID getTransactionId(){
	public String getTransactionId(){
		return transId;
	}
	
	public String getLanguage(){
		return UserInfo.getLanguage();
	}
	
	public static testMethod void FSWAcknowledgementCtrl_test(){
		PageReference pg = Page.FSWAcknowledgement;
		Test.setCurrentPage(pg);
		PageReference pgPdf = Page.FSWAcknowledgementPDF;
		Test.setCurrentPage(pgPdf);
		RecordType transacType;
		try{
			transacType =[Select Id, Name from RecordType where Name ='Person Account'];
		}catch(Exception e){
			system.debug('Error while retrieving record type. '+e);
		}
		FSWAcknowledgementCtrl fswTest = new FSWAcknowledgementCtrl();
		Account accTemp = new Account(LastName='TestAccount', CurrencyIsoCode='HKD', Client_ID__pc='Test0002', PersonEmail = 'a@b.com',
										RecordTypeId=transacType.Id);
		insert accTemp;
		Transaction__c transTemp = new Transaction__c(Reference_Number__c ='Ref0123456',
														Request_Type__c ='FUNDSW',
														CurrencyIsoCode ='HKD',
														Client__c =accTemp.Id,
														TxnData__c ='<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTemp;
		System.currentPageReference().getParameters().put('transId', transTemp.Id);
		fswTest.transId = transTemp.Id;
		
		fswTest.Security();
		fswTest.getFSWxmlData();
		fswTest.getClientName();
		fswTest.getNavPreValidation();
		fswTest.getTransactionId();
		fswTest.getNavTxnLog();
		fswTest.getLanguage();
	}
}