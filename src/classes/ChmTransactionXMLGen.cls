/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 22.JUL.2009
Author		: Ryan Dave Wilson
Description	: A class for generating XML for Change Allocation.
History	:

*/

public with sharing class ChmTransactionXMLGen{
	public ChangeAllocationRequest CHMrequest = new ChangeAllocationRequest();
	public FundAllocation chmFund = new FundAllocation();
	
	public String writeCHMxml(ChangeAllocationRequest chXml){
		XmlStreamWriter chmXml = new XmlStreamWriter();
		
		chmXml.writeStartElement(null,TransactionsReqConstant.TXN,null); //start of Txn element
		chmXml.writeAttribute(null,null,TransactionsReqConstant.REQUEST_TYPE,chXml.reqType);
		chmXml.writeAttribute(null,null,TransactionsReqConstant.REFERENCE_NO,chXml.refNum);
		chmXml.writeAttribute(null,null,TransactionsReqConstant.POLICY_NO,chXml.policyNo);
		chmXml.writeAttribute(null,null,TransactionsReqConstant.SUBMISSION_DATE_TIME,chXml.subDateTime);
		
		for(integer i=0; i < chXml.fundAlloc.size(); i++){
			chmXml.writeStartElement(null,TransactionsReqConstant.FUND,null); //start of Fund Element
			chmXml.writeAttribute(null,null,TransactionsReqConstant.CODE,chXml.fundAlloc[i].code);
			chmXml.writeAttribute(null,null,TransactionsReqConstant.NAME,XMLDataHandler.escapeXMLString(chXml.fundAlloc[i].name));
			chmXml.writeAttribute(null,null,TransactionsReqConstant.PERCENT,chXml.fundAlloc[i].percent);
			chmXml.writeAttribute(null,null,TransactionsReqConstant.CREATE_NEW,(chXml.fundAlloc[i].createNew == True? TransactionsReqConstant.YES: TransactionsReqConstant.NO));
			chmXml.writeEndElement(); //End of fund element
		}
		
		chmXml.writeEndElement(); //End of Txn Element
		
		String XmlChOutput = chmXml.getXmlString();
		System.debug('*WRITE XML: '+XmlChOutput.length());
		System.debug('*WRITE XML: '+XmlChOutput.replace('<', '«').replace('>', '»'));
		chmXml.close();
		return XmlChOutput;
	}
	
	public ChangeAllocationRequest readCHMxml(String chmAlloc){
		XmlStreamReader reader = new XmlStreamReader(chmAlloc);
		
		while(reader.hasNext()){
			if(reader.getEventType() == XmlTag.START_ELEMENT){
				if (TransactionsReqConstant.TXN == reader.getLocalName()){
					CHMrequest.reqType = reader.getAttributeValue(null,TransactionsReqConstant.REQUEST_TYPE);
					CHMrequest.refNum = reader.getAttributeValue(null,TransactionsReqConstant.REFERENCE_NO);
					CHMrequest.policyNo = reader.getAttributeValue(null,TransactionsReqConstant.POLICY_NO);
					CHMrequest.subDateTime = reader.getAttributeValue(null,TransactionsReqConstant.SUBMISSION_DATE_TIME);
					
					while(reader.hasNext()){
						if(reader.getEventType() == XmlTag.END_ELEMENT){
							if(TransactionsReqConstant.TXN == reader.getLocalName()){
								break;
							}
						}
						if(reader.getEventType() == XmlTag.START_ELEMENT){
							if(TransactionsReqConstant.FUND == reader.getLocalName()){
								FundAllocation chmFund = new FundAllocation();
								chmFund.code = reader.getAttributeValue(null,TransactionsReqConstant.CODE);
								chmFund.name = XMLDataHandler.unescapeXMLString(reader.getAttributeValue(null,TransactionsReqConstant.NAME));
								chmFund.percent = reader.getAttributeValue(null,TransactionsReqConstant.PERCENT);
								chmFund.createNew = (reader.getAttributeValue(null,TransactionsReqConstant.CREATE_NEW) == TransactionsReqConstant.YES? True: False);
								CHMrequest.fundAlloc.add(chmFund);
							}
						}
						reader.next();
					}
				}
				else{
					break;
				}
			}
			reader.next();
		}
		System.debug('*FundAllocation: '+CHMrequest);
		return CHMrequest;
	}
	static testMethod void ChmTransactionXMLGen_test(){
		ChmTransactionXMLGen ChmXmlGen = new ChmTransactionXMLGen();
		ChangeAllocationRequest CHMvar = new ChangeAllocationRequest();
		FundAllocation chmFund = new FundAllocation();
		String sampleXml = '<Txn RequestType="CHGALLOCAT" ReferenceNo="ECM-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="25" createnew="Y"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="25" createnew="N"/>'+
							'<fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="20" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="30" createnew="N"/></Txn>';
		ChmXmlGen.readCHMxml(sampleXml);
		System.debug('*Test CHMXML: '+CHMvar);
		
		//insert values on the FundSwitchRequest variables to test the write XML method.
		String expectResult = '<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<fund code="FSW1" name="Fund1" percent="25" createnew="Y"></fund>'+
							'<fund code="FSW2" name="Fund2" percent="50" createnew="N"></fund>'+
							'<fund code="FSW3" name="Fund3" percent="25" createnew="Y"></fund></Txn>';
		
		CHMvar.reqType = 'FUNDSW';
		CHMvar.refNum = 'EFS-298000005-090421';
		CHMvar.policyNo = '1234567890';
		CHMvar.subDateTime = '21/04/2009 13:10:52';
		FundAllocation chmFundA = new FundAllocation();
		chmFundA.code = 'FSW1';
		chmFundA.name = 'Fund1';
		chmFundA.percent = '25';
		chmFundA.createNew = True;
		CHMvar.fundAlloc.add(chmFundA);
		FundAllocation chmFundB = new FundAllocation();
		chmFundB.code = 'FSW2';
		chmFundB.name = 'Fund2';
		chmFundB.percent = '50';
		chmFundB.createNew = False;
		CHMvar.fundAlloc.add(chmFundB);
		FundAllocation chmFundC = new FundAllocation();
		chmFundC.code = 'FSW3';
		chmFundC.name = 'Fund3';
		chmFundC.percent = '25';
		chmFundC.createNew = True;
		CHMvar.fundAlloc.add(chmFundC);
		
		String result = ChmXmlGen.writeCHMxml(CHMvar);
		if(expectResult == result){
			System.debug('TRUE');
		}else{
			System.debug('FALSE');
		}
	}
}