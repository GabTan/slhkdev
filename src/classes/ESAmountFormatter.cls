/* 
Company        : Sun Life Financial 
Date           : July 23, 2009 
Author         : Robert Andrew Almedina 
Description    : This class acts as an amount formatter (###,###,###.##)
History        : <Date of Update>        <Author of Update>        <Short description of update>
 1.0             02-03-2011              Mikka Elepano				Change the algorithm of Format Amount to cater the correct rounding of unit values 

*/ 


public class ESAmountFormatter {

	/*public static String FormatAmount(Decimal paramAmount)
	{
		Decimal amt = paramAmount;
		System.debug('Param Amount'+ paramAmount);
        amt = amt.setScale(2, System.RoundingMode.HALF_DOWN);
        
        System.debug('amtt'+ amt);
        String Amount = amt.toPlainString();
        String decimalSeparator = Amount.substring(Amount.length()-3, Amount.length()-2);
		String a = paramAmount.format();
		System.debug('FormatAMount:'+ a);

		if (a.contains(decimalSeparator))
		{
			Amount = a.substring(0, a.lastIndexOf(decimalSeparator)) + Amount.substring(amt.toPlainString().lastIndexOf(decimalSeparator));
		}
		else
		{
			Amount = a + Amount.substring(amt.toPlainString().lastIndexOf(decimalSeparator));
		}
		System.debug('Amount:'+ Amount);
		return Amount;
	}*/

	public static String FormatAmount(Decimal paramAmount) {
	
    //paramAmount =  1000000.906;//0.566  5.106 100.106 1.490 0.576 100000.499 100000.666 1000000.504
	Decimal base;
	Integer WholeNum;
	String Amount;
	String cond = null;
	
	//System.debug('Param Amount'+ paramAmount);
	
	Decimal WholeNum1 = Integer.valueOf(WholeNum);
    
	base = (paramAmount * 100);
    //System.debug('Param Amount'+ paramAmount);
    
    WholeNum = base.intValue();
    System.debug('WholeNum'+ WholeNum);
    Amount = String.valueOf(base);
    //System.debug('Amount: ' + Amount);
   
    Integer pos = Amount.indexOf('.',0)+1;    
    //System.debug('Decimal Separator:' + pos);
   
    cond = Amount.substring(pos, pos+1);
    //System.debug('number condition: ' + cond);
  
    if (Integer.valueOf(cond) > 5) {
    	WholeNum++;
    	}

    Decimal base1 = WholeNum;
    base1 = base1.divide(100, 2);
   
    //System.debug('base1 : ' + base1);
   
    Amount = base1.format();
    //for catering two decimal places in the screen
    if (Amount.contains('.')){
    	//get the last chars after decimal point
    	 String lastDecimal = Amount.substring(Amount.lastIndexOf('.'));
    	 //System.debug('Last Decimal : ' + lastDecimal);
    	     // if the chars after the decimal point is less than equal to 2
    	     if(lastDecimal.length()<=2){
    	     	Amount = Amount + '0';
    	     	}
    }
    else {
    	  // add decimal(.) + 00
    	  Amount = Amount + '.00';
    	 }
    
    return Amount;
    }

	//mjcac: 11-21-2012: modified to properly handle rounding off 
	public static String FormatAmountScale(Decimal paramAmount)	{
		Decimal ldAmount, ldWholeNo;
		String lsAmount, lsDecimalPoint, lsWholeNo;
		
		ldAmount = paramAmount;
        ldAmount = ldAmount.setScale(4, System.RoundingMode.HALF_DOWN);
        lsAmount = ldAmount.toPlainString();
        
        lsDecimalPoint = lsAmount.substring(lsAmount.length()-5, lsAmount.length()-4);
		lsWholeNo = lsAmount.substring(0, lsAmount.lastIndexOf(lsDecimalPoint));
		ldWholeNo = decimal.valueOf(lsWholeNo);
		lsWholeNo = ldWholeNo.format();
		
		lsAmount = lsWholeNo + lsAmount.substring(ldAmount.toPlainString().lastIndexOf(lsDecimalPoint));
		
		return lsAmount;
	}

}