/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 13.AUG.2009
Author		: W. Clavero
Description	: Redirects the user to the appropriate page after the Change Password event.
History		:
*/ 
public class ChangePasswordRedirectorCtrl {

    public String redirURL {get; set;}
    
    public ChangePasswordRedirectorCtrl() {
        String chPwdType = ApexPages.currentPage().getParameters().get('reg');
        
        if (chPwdType.equals(ESPortalNavigator.CHPWD_REG_EVENT_VAL)) {
            this.redirURL = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_MAIN);
        } else {
            this.redirURL = ESPortalNavigator.NAV_URL_ACC_LOGOUT;
        }
    }

}