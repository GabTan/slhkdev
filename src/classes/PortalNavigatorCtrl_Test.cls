/*
Version     : 1.0
Company     : Sun Life Financial
Date        : 17.AUG.2009
Author      : W. Clavero
Description : Test Coverage for the PortalNavigator Apex Class.
History     : 
*/
@isTest
private class PortalNavigatorCtrl_Test {

    static testMethod void testPortalNavigatorCtrl() {
        PageReference pgRef = Page.EServicesHome;
        Test.setCurrentPage(pgRef);
        CustomerPortalWelcomeController ctrlr = new CustomerPortalWelcomeController();
        PortalNavigatorCtrl compCtrlr = new PortalNavigatorCtrl();
        
        compCtrlr.navID = 100;
        compCtrlr.showMenu = true;
        String s = compCtrlr.getMenuStyle();
        s = compCtrlr.getHomeTabURL();
        s = compCtrlr.getInboxTabURL();
        s = compCtrlr.getServicesTabURL();
        s = compCtrlr.getHelpTabURL();
        s = compCtrlr.getDownloadTabURL();
        s = compCtrlr.getAccountTabURL();

        s = compCtrlr.getCHMSubURL();
        s = compCtrlr.getCPPSubURL();
        s = compCtrlr.getFSWSubURL();
        s = compCtrlr.getTXNSubURL();
        
        s = compCtrlr.getHelpSECSubURL();
        s = compCtrlr.getHelpSVCSubURL();
        
        
        Boolean b = compCtrlr.getIsHomeActive();
        b = compCtrlr.getIsInboxActive();
        b = compCtrlr.getIsServicesActive();
        b = compCtrlr.getIsHelpActive();
        b = compCtrlr.getIsDownloadActive();
        b = compCtrlr.getIsAccountActive();
        
        b = compCtrlr.getPolicyRender();
        b = compCtrlr.getServicesRender();
        b = compCtrlr.getHelpRender();
        
        b = compCtrlr.getIsCHMSubActive();
        b = compCtrlr.getIsCPPSubActive();
        b = compCtrlr.getIsFSWSubActive();
        b = compCtrlr.getIsTXNSubActive();
        b = compCtrlr.getIsHelpSECSubActive();
        b = compCtrlr.getIsHelpSVCSubActive();     
        
        List<Profile> up = [Select Id From Profile Where Name = 'SLF eServices Customer'];
        
        if (up.size() > 0) {
            //portal user simulation
            List<User> usr = [Select Id, Name From User 
                                Where IsActive = true 
                                And UserType = 'PowerCustomerSuccess' 
                                And Client_Id__c != null 
                                And ProfileId = :up.get(0).Id 
                                Limit 1];
            
            if (usr.size() > 0) {
                User u = usr.get(0);
                System.runAs(u) {
                    PageReference portalPage = Page.EServicesHome;
                    Test.setCurrentPage(portalPage);
                    CustomerPortalWelcomeController welcomectrlr = new CustomerPortalWelcomeController();
                    PortalNavigatorCtrl portalCtrlr = new PortalNavigatorCtrl();
                    
                    portalCtrlr.isvalidforFS();
                    portalCtrlr.isvalidforCM();
                }
            }        
        }
    }
}