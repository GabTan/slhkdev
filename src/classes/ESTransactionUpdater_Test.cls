/*
    Company    : Sun Life Financial
    Date       : 7/22/2009
    Author     : Marian Baleros
    Description: Test Coverage for ESTransactionUpdater WebService
    History    : 
    
    <Date Modified>    <Developer Name>     <Short description of modification>
    7/23/2009           Marian Baleros      Added scenarios and record insert
    7/24/2009           Marian Baleros      Separated different scenarios into different methods
    
*/

public class ESTransactionUpdater_Test
{
    public static testmethod void scenario1Test()
    {   
        /*Instantiate*/
        Policy__c policyId = [select Id from Policy__c limit 1];
        ESTransactionUpdater esTransactionTest = new ESTransactionUpdater();
        String transIdNull='';
        
        /*Scenario 1*/
        Boolean testPollDatescenario1 = ESTransactionUpdater.updatePollingDate(null);
        System.debug('------ '+testPollDatescenario1);
        Boolean testInProcessscenario1 = ESTransactionUpdater.updateToInProcess(null);
        System.debug('------ '+testInProcessscenario1);
        
        /*Scenario 5
        for (Transaction__c[] txnQuer : [select Id from Transaction__c where Id=:txnNewInsert7.Id for update])
        {
            Boolean yhan8 = ESTransactionUpdater.updatePollingDate(transIdConcatValid);
            System.debug('------ '+yhan8);
            Boolean yhan9 = ESTransactionUpdater.updateToInProcess(transIdConcatValid);
            System.debug('------ '+yhan9);
        }*/
            
    }
    public static testmethod void scenario2Test()
    {   
        /*Instantiate*/
        Policy__c policyId = [select Id from Policy__c limit 1];
        ESTransactionUpdater esTransactionTest = new ESTransactionUpdater();
        String transIdInvalid ='12345,34562,78906';
        
        /*Scenario 2*/
        Boolean testPollDatescenario2 = ESTransactionUpdater.updatePollingDate(transIdInvalid);
        System.debug('------ '+testPollDatescenario2);
        Boolean testInProcessscenario2 = ESTransactionUpdater.updateToInProcess(transIdInvalid);
        System.debug('------ '+testInProcessscenario2);
    }
    
    public static testmethod void scenario3Test()
    {
        /*Instantiate*/
        Policy__c policyId = [select Id from Policy__c limit 1];
        ESTransactionUpdater esTransactionTest = new ESTransactionUpdater();
        
        /*insert new records for Scenario 3*/
        Transaction__c txnNewInsert1 = new Transaction__c (Reference_Number__c='00001', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='In Process');
        insert txnNewInsert1;
        Transaction__c txnNewInsert2 = new Transaction__c (Reference_Number__c='00002', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Close');
        insert txnNewInsert2;
        Transaction__c txnNewInsert3 = new Transaction__c (Reference_Number__c='00003', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='In Process');
        insert txnNewInsert3;
        
        String transIdValid = txnNewInsert1.Id+','+txnNewInsert2.Id+','+txnNewInsert3.Id;
        
        /*Scenario 3*/
        Boolean testPollDatescenario3 = ESTransactionUpdater.updatePollingDate(transIdValid);
        System.debug('------ '+testPollDatescenario3);
        Boolean testInProcessscenario3 = ESTransactionUpdater.updateToInProcess(transIdValid);
        System.debug('------ '+testInProcessscenario3);
    }
    
    public static testmethod void scenario4Test()
    {
        /*Instantiate*/
        Policy__c policyId = [select Id from Policy__c limit 1];
        ESTransactionUpdater esTransactionTest = new ESTransactionUpdater();
        
        /*insert new records for Scenario 4*/
        Transaction__c txnNewInsert1 = new Transaction__c (Reference_Number__c='00004', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Open');
        insert txnNewInsert1;
        Transaction__c txnNewInsert2 = new Transaction__c (Reference_Number__c='00005', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Close');
        insert txnNewInsert2;
        Transaction__c txnNewInsert3 = new Transaction__c (Reference_Number__c='00006', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Open');
        insert txnNewInsert3;
        
        String transIdConcatInvalid = txnNewInsert1.Id+','+txnNewInsert2.Id+','+txnNewInsert3.Id;
        
        /*Scenario 4*/
        Boolean testPollDatescenario4 = ESTransactionUpdater.updatePollingDate(transIdConcatInvalid);
        System.debug('------ '+testPollDatescenario4);
        Boolean testInProcessscenario4 = ESTransactionUpdater.updateToInProcess(transIdConcatInvalid);
        System.debug('------ '+testInProcessscenario4);
    }
    
    public static testmethod void scenario5Test()
    {
        /*Instantiate*/
        Policy__c policyId = [select Id from Policy__c limit 1];
        ESTransactionUpdater esTransactionTest = new ESTransactionUpdater();
        
        /*insert new records for Scenario 5*/
        Transaction__c txnNewInsert1 = new Transaction__c (Reference_Number__c='00007', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Open');
        insert txnNewInsert1;
        Transaction__c txnNewInsert2 = new Transaction__c (Reference_Number__c='00008', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Open');
        insert txnNewInsert2;
        Transaction__c txnNewInsert3 = new Transaction__c (Reference_Number__c='00009', Policy__c=policyId.Id, Request_Type__c='CP', CommStatus__c='Open');
        insert txnNewInsert3;
        
        String transIdConcatSuccess = txnNewInsert1.Id+','+txnNewInsert2.Id+','+txnNewInsert3.Id;
        
        /*Scenario 5*/
        Boolean testPollDatescenario5 = ESTransactionUpdater.updatePollingDate(transIdConcatSuccess);
        System.debug('------ '+testPollDatescenario5);
        Boolean testInProcessscenario5 = ESTransactionUpdater.updateToInProcess(transIdConcatSuccess);
        System.debug('------ '+testPollDatescenario5);
    }
}