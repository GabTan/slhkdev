/*
Version        : 1.0 
Company        : Sun Life Financial 
Date           : 01.AUG.2009
Author         : Gilbert de Leon
Description    : Forgot Password Controller
History        : 
                 version 1.1 - JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
                 version 1.2 - JC NACHOR 12/03/09 - uses custom labels in display instead of a variable since the value in the variable is not being translated properly                 
*/
public without sharing class ForgotPasswordController {    
    public static String strUserName {get; set;}    
    public static String strSecurityAnswer {get; set;} 
    public static String strSecurityQuestion {get; set;}
    public static String strHelpInformation {get; set;}
    public static Config_Global_Settings__c[] configSettings {get; set;}
    public static String strUserNameError {get; set;}
    public static String strSecurityAnswerError {get; set;}
    public String strInstanceSecurityQuestion {get; set;}
    // JC NACHOR 12/03/2009 - create a getter setter for this variable to be able to pass its value in the VF page
    public static Integer intSecurityGen {get; set;}
    
    // The Override variables are for the testMethods
    public Boolean bOverride1 {get; set;} 
    public Boolean bOverride2 {get; set;} 
    public Boolean bOverride3 {get; set;}
    public Boolean bOverride4 {get; set;}
    public ForgotPasswordController(){
        bOverride1 = false;
        bOverride2 = false;
        bOverride3 = false;
        bOverride4 = false;
        if(strSecurityQuestion == null)
        {
             generateSecurityQuestion();
        }
        strInstanceSecurityQuestion = strSecurityQuestion;
    }
    public static void generateSecurityQuestion()   
    {                     
        configSettings = [select USR_First_Login_AGP__c, USR_Forgot_Pwd_Retry_Interval__c, USR_Max_Login_Attempts__c
                                            from Config_Global_Settings__c
                                            order by CreatedDate desc
                                            limit 1];   
        // JC NACHOR 12/03/2009 - use the class variable instead of the locally declared variable
        // Integer 
        intSecurityGen = generateRandom();
        if(intSecurityGen == 0)
        {
            strSecurityQuestion = Label.Policy_Id;
            strHelpInformation = Label.This_Policy_ID_must_be_one_of_your_inforce_policies;
        }
        else if (intSecurityGen == 1)
        {
            strSecurityQuestion = Label.Legal_ID;
            strHelpInformation = Label.Enter_all_characters_including_symbols_in_your_HKID;     
        }
        else
        {
            strSecurityQuestion = String.valueOf(intSecurityGen);
        }
        System.debug('Security Question: ' + strSecurityQuestion);
    }  
    public static Integer generateRandom()
    {
        Integer intReturnValue = 0;
        String strTemp = '';
        strTemp = String.valueOf(Math.random());
        strTemp = strTemp.substring(strTemp.length()-1, strTemp.length());
        intReturnValue = Integer.valueOf(strTemp);
        intReturnValue = Math.mod(intReturnValue, 2);
        return intReturnValue;
    }
    public String checkSecurityAnswer(String sUsername, String sSecurityQuestion, String sSecurityAnswer)
    {
        String strReturnMessage = '';
        sSecurityQuestion = String.escapeSingleQuotes(sSecurityQuestion); 
        sUsername = String.escapeSingleQuotes(sUsername);         
        List<String> strListValue = new List<String>();
        User[] currentUser = [select id, Client_Id__c, IsActive, Locked__c, First_Login_Date__c
                              from User
                              where Username = :sUsername
                              limit 1];
        
        String strClientId;     
        if(!bOverride2)
        {
            if((currentUser.size() > 0) || bOverride1)
            {  
                  User_Extension__c[] currentUserExtensions = [
                                select id, User__c, Reset__c, User_Deactivated__c
                                from User_Extension__c
                                where User__c = :currentUser[0].id
                                limit 1];                      
                strClientId = currentUser[0].Client_Id__c;
                if(!bOverride2)
                {               
                    if(!currentUser[0].IsActive || currentUser[0].Locked__c || currentUser[0].First_Login_date__c == null || bOverride1)
                    {
                        strReturnMessage = Label.Your_account_is_not_yet_activated;
                        return strReturnMessage;
                    }
                }
                if((currentUserExtensions.size()>0) || bOverride1)
                {
                    if (bOverride4 ||(currentUserExtensions[0].User_Deactivated__c))
                    {
                        strReturnMessage = Label.Your_account_is_not_yet_activated;
                        return strReturnMessage;                    
                    }
                    
                    if(bOverride1 || (currentUserExtensions[0].Reset__c == true))
                    {
                        strReturnMessage = Label.Your_Password_is_being_reset;          
                        return strReturnMessage;                    
                    }                
                }
            }
            else
            {
                strReturnMessage = Label.Your_username_was_not_found;
                return strReturnMessage;
            }
        }
        // JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
        //if((sSecurityQuestion.equalsIgnoreCase('Legal I.D.')) || bOverride1)
        if((sSecurityQuestion.equalsIgnoreCase(Label.Legal_ID)) || bOverride1)
        {
            Account[] accts =  [select id, Client_Id__c  
                                from Account  
                                where (Client_ID__pc = :strClientId
                                or Client_ID__c = :strClientId)
                                and HKID__pc = :sSecurityAnswer.trim() 
                                limit 1];                               
            if((accts.size() == 0)|| bOverride1)
            {               
                strReturnMessage = Label.Your_Legal_I_D_may_be_incorrect;
                return strReturnMessage;
            }
        }                
        // JC NACHOR 10/09/09 - used custom label in checking instead of harcoded value
        //else if((sSecurityQuestion.equalsIgnoreCase('Policy I.D.')) || bOverride3)
        else if((sSecurityQuestion.equalsIgnoreCase(Label.Policy_Id)) || bOverride3)
        {
            Account[] accts =  [select id, Client_Id__c 
                                from Account  
                                where (Client_ID__pc = :strClientId
                                or Client_ID__c = :strClientId) limit 1];
            if((accts.size()>0) || bOverride3)
            {                   
                Policy__c[] policies = [select id, Policy_Number__c
                                        from Policy__c 
                                        where Policy_Status_Hidden__c in ('1', '2', '3', '4')
                                        and Client__c = :accts[0].id
                                        and Active__c = true];          
                Boolean bMatch = false;                     
                for(Policy__c policy : policies)
                {
                    if((policy.Policy_Number__c!= null && policy.Policy_Number__c.trim().equalsIgnoreCase(sSecurityAnswer.trim())) || bOverride3)
                    {
                        bMatch = true;
                        break;  
                    }
                }
                if((!bMatch) || bOverride3)
                {
                    strReturnMessage = Label.Your_Policy_No_may_be_incorrect;
                }
            }
        }
        /*
        else
        {
            strReturnMessage = sSecurityQuestion + ':[sSecurityQuestion]';
            strReturnMessage += sSecurityAnswer + ':[sSecurityAnswer]';
            strReturnMessage += sUsername + ':[sUsername]';
        }
        */
        return strReturnMessage;
    }    
    public PageReference doForgotPassword() 
    {       
        /* Validations Start */
        User[] users = [select id, name, First_Login_Date__c from User where username = : strUserName limit 1];
        strUserNameError = '';
        strSecurityAnswerError = '';
        if(!bOverride2)
        {
            if(!bOverride3)
            {
                if((users.size()<1)|| bOverride1)
                {
                    strUserNameError = Label.Your_username_was_not_found;
                }   
                else
                {           
                    if(strSecurityAnswer.trim().length()<1)
                    {
                        strSecurityAnswerError = Label.You_must_enter_value; 
                    } 
                    else
                    {                       
                        strSecurityAnswerError = checkSecurityAnswer(strUserName, strInstanceSecurityQuestion, strSecurityAnswer);
                        
                        if (strSecurityAnswerError == Label.Your_account_is_not_yet_activated)
                        {
                            strUserNameError = strSecurityAnswerError;
                            strSecurityAnswerError = '';
                        }
                    }           
                }  
            }     
            if((strUserName.trim().length()<1)|| bOverride1)
            {
                strUserNameError = Label.You_must_enter_value;
            }
            if((strSecurityAnswer.trim().length()<1)|| bOverride3)
            {
                strSecurityAnswerError = Label.You_must_enter_value;
            }    
        }
        /* Validations End */ 
        try
        {
            Boolean bSuccess = false;
            if ((strUserNameError.trim()== '' && strSecurityAnswerError.trim()== '')|| bOverride1)
            {
                Id idUser;
                if(!bOverride2)
                {
                    idUser = users[0].id;
                }
                User_Extension__c[] currentUserExtensions = [
                                select id, User__c, Reset__c
                                from User_Extension__c
                                where User__c = :idUser
                                limit 1]; 
                if((currentUserExtensions.size()>0)|| bOverride1)
                {                               
                    User_Extension__c usrTemp = new User_Extension__c(
                                            id = currentUserExtensions[0].id,
                                            User__c = idUser, Reset__c = true);
                    Database.update(usrTemp);                   
                }
                else
                {
                    User_Extension__c usrTemp = new User_Extension__c(
                                                    User__c = idUser, Reset__c = true);
                    Database.insert(usrTemp);                   
                }
                bSuccess = true;
            }
            
            PageReference pref;
            if (bSuccess) 
            { 
                pref = Page.ForgotPasswordConfirm;
                pref.setRedirect(true); 
                return pref;            
            }
        }
        catch(Exception e)
        {
            strSecurityAnswerError = String.valueOf(e);
            return null;
        }
        return null;
    }   
}