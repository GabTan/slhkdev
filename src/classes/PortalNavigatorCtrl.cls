/*
Version		: 1.4
Company		: Sun Life Financial
Date		: 30.JUL.2009
Author		: W. Clavero
Description	: Controller for the PortalNavigator component.
History		: 
		1.1		29.SEP.09   - W. Cheng   - Enhancement of the download corner. I added new URL for the Chinese download corner
		1.2		15.OCT.2009 - W. Clavero - Changed the query conditions for Fund Switch and Change Alloc (IL-ES-00043).
		1.3     27.OCT.2009 - R. Angeles - Removed reference to Inbox tab
		1.4     28.OCT.2009 - R. Angeles - Added admin portal checks
		1.5   01.DEC.2009 - JC NACHOR - added menu item element for Contact Us
*/
public class PortalNavigatorCtrl {
	Config_Utilities CU = new Config_Utilities();
	Config_Global_Settings__c globalConfig;
	Boolean isPortalUser;
	public Integer navID {get; set;}
	public Boolean showMainMenu {get; set;}
	public Boolean showMenu {get; set;}
	//main menu	
	public String homeTab {get; set;}
	public String inboxTab {get; set;}
	public String servicesTab {get; set;}
	public String helpTab {get; set;}
	public String downloadTab {get; set;}
	public String accountTab {get; set;}
  // JC NACHOR 12/01/09 - added menu item element for Contact Us	
	public String contactUsTab {get; set;}
	//services sub-menu
	public String transactionSubMenu {get; set;}
	public String contactInfoSubMenu {get; set;}
	public String fundSwitchSubMenu {get; set;}
	public String fundAllocSubMenu {get; set;}
	//help sub-menu
	public String helpSecuritySubMenu {get; set;}
	public String helpServicesSubMenu {get; set;}

	public PortalNavigatorCtrl() {
		//build the menu
		globalConfig = [Select SLF_Portal_Profile_Names__c, DWN_English_URL__c, DWN_Chinese_URL__c From Config_Global_Settings__c Limit 1];
		isPortalUser = CU.isPortalUser(globalConfig.SLF_Portal_Profile_Names__c);
		//R Angeles 10292009 - look and feel item # 11 if admin portal user hide main menus
		if(isPortalUser){
			setupNavMenu();
			showMainMenu = true;
		}
		else{
			showMainMenu = false;
		}	
	}
	
	public String getMenuStyle() {
		if (showMenu) {
			return 'display: inline;';
		} else {
			return 'display: none;';
		}
	}
	
	private void setupNavMenu() {
		//get the main tabs
		List<String> mainTabs = ESPortalNavigator.getMenuItems(System.Label.TAB_MainMenu);
		if (mainTabs.size() > 0) {
			homeTab = mainTabs.get(ESPortalNavigator.MENU_IDX_HOME);
			//inboxTab = mainTabs.get(ESPortalNavigator.MENU_IDX_INBOX);
			servicesTab = mainTabs.get(ESPortalNavigator.MENU_IDX_SERVICES);
			helpTab = mainTabs.get(ESPortalNavigator.MENU_IDX_HELP);
			downloadTab = mainTabs.get(ESPortalNavigator.MENU_IDX_DOWNLOAD);
			accountTab  = mainTabs.get(ESPortalNavigator.MENU_IDX_ACCOUNT);
      // JC NACHOR 12/01/09 - added menu item element for Contact Us			
			contactUsTab = mainTabs.get(ESPortalNavigator.MENU_IDX_CONTACT_US);
		}
		//get the services sub tabs
		List<String> svcSubs = ESPortalNavigator.getMenuItems(System.Label.TAB_ServicesSubMenu);
		if (svcSubs.size() > 0) {
			transactionSubMenu = svcSubs.get(ESPortalNavigator.MENU_IDX_SUB_TXN);
			contactInfoSubMenu = svcSubs.get(ESPortalNavigator.MENU_IDX_SUB_CPP);
			fundSwitchSubMenu = svcSubs.get(ESPortalNavigator.MENU_IDX_SUB_FSW);
			fundAllocSubMenu = svcSubs.get(ESPortalNavigator.MENU_IDX_SUB_CHM);
		}	
		//get the help sub tabs
		List<String> hlpSubs = ESPortalNavigator.getMenuItems(System.Label.TAB_HelpSubMenu);
		if (hlpSubs.size() > 0) {
			helpSecuritySubMenu = hlpSubs.get(ESPortalNavigator.MENU_IDX_SUB_SEC);
			helpServicesSubMenu = hlpSubs.get(ESPortalNavigator.MENU_IDX_SUB_SVC);
		}	
	}	
	//main URLs
	public String getHomeTabURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_MAIN);
	}

	public String getInboxTabURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_INB_MAIN);
	}

	public String getServicesTabURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_MAIN);
	}
	
	public String getHelpTabURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_MAIN);
	}
	
	public String getDownloadTabURL() {
		String downloadURL;
		//START ADD Wcheng Enhancement 09.29.09                            
                                    
        if(UserInfo.getLanguage() == 'zh_TW')
        {
            downloadURL = globalConfig.DWN_Chinese_URL__c;
        }
        else
        {
            downloadURL = globalConfig.DWN_English_URL__c;
        }
        //START ADD Wcheng Enhancement 09.29.09
		//return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_DWN_MAIN)
		return downloadURL;
	}
	
	public String getAccountTabURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_ACC_MAIN) + ESPortalNavigator.CHPWD_REG_EVENT;
	}
	
	// JC NACHOR 12/01/09 - added menu item element for Contact Us
  public String getContactUsTabURL() {
    return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_CUS_MAIN);
  }	
	
	//main styles	
	public Boolean getIsHomeActive() {
		return ((this.navID >= ESPortalNavigator.NAV_HOM_MAIN) && (this.navID <= ESPortalNavigator.NAV_HOM_FTH));
	}

	public Boolean getIsInboxActive() {
		return (this.navID == ESPortalNavigator.NAV_INB_MAIN);
	}

	public Boolean getIsServicesActive() {
		return ((this.navID >= ESPortalNavigator.NAV_SVC_MAIN) && (this.navID <= ESPortalNavigator.NAV_SVC_CHM_FINAL));
	}
	
	public Boolean getIsHelpActive() {
		return ((this.navID >= ESPortalNavigator.NAV_HLP_MAIN) && (this.navID <= ESPortalNavigator.NAV_HLP_SVC));
	}
	
	public Boolean getIsDownloadActive() {
		return (this.navID == ESPortalNavigator.NAV_DWN_MAIN);
	}
	
	public Boolean getIsAccountActive() {
		return (this.navID == ESPortalNavigator.NAV_ACC_MAIN);
	}
	
  // JC NACHOR 12/01/09 - added menu item element for Contact Us
  public Boolean getIsContactUsActive() {
    return (this.navID == ESPortalNavigator.NAV_CUS_MAIN);
  }
  	
	//sub-menu display
	public Boolean getPolicyRender() {
		return (this.navID == ESPortalNavigator.NAV_HOM_POLICY); 	
	}
	
	public Boolean getServicesRender() {
		return ((this.navID >= ESPortalNavigator.NAV_SVC_MAIN) && (this.navID <= ESPortalNavigator.NAV_SVC_CHM_FINAL));
	}

	public Boolean getHelpRender() {
		return ((this.navID >= ESPortalNavigator.NAV_HLP_MAIN) && (this.navID <= ESPortalNavigator.NAV_HLP_SVC));
	}
	//sub-menu URLs 
	public String getTXNSubURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_TXN);
	}
	
	public String getCPPSubURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_NOTES);
	}

	public Boolean isvalidforFS(){
		Boolean isValid = false;
		 
		try { 
			User curUser = [Select Client_Id__c From User Where id = :UserInfo.getUserId()];
			if(curUser.Client_Id__c != null){
				if(curUser.Client_Id__c != ''){
					List<Account> curAccount = [Select Id, Client_Id__c From Account Where Client_Id__c = :curUser.Client_Id__c LIMIT 1];
					if(curAccount.size() > 0){
						/* w. clavero 20091015: changed query criteria
						List<Policy__c> curPolicies = [Select Id From Policy__c Where Client__c = :curAccount[0].Id
									   					AND Policy_Status_Hidden__c in ('1','2','3','4')
									   					AND RecordType.Name = 'UL'
									   					AND Active__c = true];
					    */
						List<Policy__c> curPolicies = [Select Id From Policy__c Where Client__c = :curAccount[0].Id
									   					AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES 
									   					AND Policy_Insurance_Type__c in :TransactionsReqConstant.POLICY_UL_CODES 
									   					AND Active__c = true];
						if(curPolicies.size() > 0) isValid = true;	
					}
				}
			}
		} catch (QueryException qe) {
			
		}
		return isValid;
	}

	//w. clavero 20091015: added new validation function for change alloc
	public Boolean isvalidforCM(){
		Boolean isValid = false;
		 
		try { 
			User curUser = [Select Client_Id__c From User Where id = :UserInfo.getUserId()];
			if(curUser.Client_Id__c != null){
				if(curUser.Client_Id__c != ''){
					List<Account> curAccount = [Select Id, Client_Id__c From Account Where Client_Id__c = :curUser.Client_Id__c LIMIT 1];
					if(curAccount.size() > 0){
						List<Policy__c> curPolicies = [Select Id From Policy__c Where Client__c = :curAccount[0].Id
									   					AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES  
									   					AND Policy_Insurance_Type__c in :TransactionsReqConstant.POLICY_UL_CODES 
									   					AND Billing_Method__c != :TransactionsReqConstant.POLICY_SINGLE_PREMIUM 
									   					AND Active__c = true];
						if(curPolicies.size() > 0) isValid = true;	
					}
				}
			}
		} catch (QueryException qe) {
			
		}
		return isValid;
	}

	public String getFSWSubURL() {
		Boolean isFSValid; 
		if(CU.isPortalUser('') == false){
			return '/apex/UserAccessError';
		}
		else{
			isFSValid = isvalidforFS();	
		}
		if(isFSValid){
			return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_POLICY);
		}
		else{
			return '#';
		}
	}
	
	public String getCHMSubURL() {
		Boolean isCHMValid; 
		if(CU.isPortalUser('') == false){
			return '/apex/UserAccessError';
		}
		else{
			isCHMValid = isvalidforCM();	
		}
		
		if(isCHMValid){
			return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_POLICY);
		}
		else{
			return '#';
		}
	}

	public String getHelpSECSubURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_SEC);
	}

	public String getHelpSVCSubURL() {
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_SVC);
	}
	//sub-menu styles
	public Boolean getIsTXNSubActive() {
		return (this.navID == ESPortalNavigator.NAV_SVC_TXN);
	}
	
	public Boolean getIsCPPSubActive() {
		return ((this.navID >= ESPortalNavigator.NAV_SVC_CPP_NOTES) && (this.navID <= ESPortalNavigator.NAV_SVC_CPP_FINAL));
	}

	public Boolean getIsFSWSubActive() {
		return ((this.navID >= ESPortalNavigator.NAV_SVC_FSW_POLICY) && (this.navID <= ESPortalNavigator.NAV_SVC_FSW_FINAL));
	}

	public Boolean getIsCHMSubActive() {
		return ((this.navID >= ESPortalNavigator.NAV_SVC_CHM_POLICY) && (this.navID <= ESPortalNavigator.NAV_SVC_CHM_FINAL));
	}

	public Boolean getIsHelpSECSubActive() {
		return (this.navID == ESPortalNavigator.NAV_HLP_SEC);
	}

	public Boolean getIsHelpSVCSubActive() {
		return (this.navID == ESPortalNavigator.NAV_HLP_SVC);
	}
}