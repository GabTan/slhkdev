public class LoginAsPortalUserController {

	private String accountId = null;
	private Account acc = null;
	private String orgId = UserInfo.getOrganizationId();
	private String portalId = '060300000002KUS'; // Not available anywhere- must hardcode :(
	private User portalUser = null;
	private String message;
	private String accountName;
	private String renderViewPortalUserButton;
	
	public LoginAsPortalUserController()
	{
		acc = [Select PersonContactId, Name, IsPersonAccount, Id From Account where Id = :getAccountId()];
		accountName = acc.Name;
	}
	
	public PageReference Back()
	{
		PageReference pRef = new PageReference('/'+getAccountId());
        pRef.setRedirect(true);
            
        return pRef;
	}
	
	public void setMessage(String s)
	{
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public void setAccountName(String s)
	{
	}
	
	public String getAccountName()
	{
		return accountName;
	}
	
	public String getRenderViewPortalUserButton()
	{
		if (portalUser == null)
		{
			return 'false';
		}
		else
		{
			return 'true';
		}
	}
	
	public void setRenderViewPortalUserButton(String s)
	{
		
	}
	
	
	public PageReference redirect()
	{
		if (acc.IsPersonAccount && null != acc.PersonContactId)
		{
			List<User> users = [select Id, Name, IsActive, UserType from User where ContactId = :acc.PersonContactId];
			
			if (users.size() != 0)
			{
				portalUser = users.get(0);
				if (portalUser.IsActive && portalUser.UserType == 'PowerCustomerSuccess')
				{
					// All go for Login
					// /servlet/servlet.su?oid=00D30000000KbT8&suportaluserid=00530000001YwEx&retURL=%2F0033000000UjxbP&targetURL=%2Fhome%2Fhome.jsp&suportalid=060300000002KUS
					
					String url = '/servlet/servlet.su?oid=' + orgId + '&suportaluserid='+portalUser.Id+'&retURL=%2F'+getAccountId()+'&targetURL=%2Fhome%2Fhome.jsp&suportalid='+portalId;
					PageReference pRef = new PageReference(url);
					pRef.setRedirect(true);
			        return pRef;
				}
				else
				{
					message = 'does not have an active portal user';
				}
			}
			else
			{
				message = 'does not have an associated portal user';
			}
		}
		else
		{
			message = 'is not a Person Account';
		}
	
		return null;
	}
	
	private String getAccountId()
	{
		if (null == accountId)
		{
			accountId = ApexPages.currentPage().getParameters().get('id');
		}
		
		return accountId;
	}
	
	public PageReference ViewUser()
	{
		if (null == portalUser)
		{
			return null;	
		}
		
		PageReference pRef = new PageReference('/'+portalUser.Id);
		pRef.setRedirect(true);
		return pRef;
	}
	
	public static TestMethod void testLoginAsPortalUser()
	{
		// Create new account
        Account a=new Account(recordtypeid='012300000006IcS', LastName='Test Person Account', Client_ID__pc='C0001XAA', PersonEmail='a@b.com');
        insert a;
        
        Test.setCurrentPage(new PageReference('/apex/LoginAsPortalUser?id='+a.Id));
		LoginAsPortalUserController controller = new LoginAsPortalUserController();
		
		controller.ViewUser();
		controller.getAccountId();
		controller.redirect();
		controller.Back();
		
		controller.getAccountName();
		controller.setAccountName('');
		
		controller.setMessage('');
		controller.getMessage();

		controller.setRenderViewPortalUserButton('');	
		controller.getRenderViewPortalUserButton();
		
		controller.portalUser = [select id from user limit 1];
		controller.getRenderViewPortalUserButton();
		controller.ViewUser();
	}
}