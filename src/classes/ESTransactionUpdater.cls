/*
    Version    : 1.1
    Company    : Sun Life Financial
    Date       : 7/22/2009
    Author     : Marian Baleros
    Description: WebService for Transaction Model
    History    :     
        1.0    7/24/2009       Marian Baleros - Added functionality for parsing and checking if more than the record limit
        1.1    06.OCT.2009     W. Clavero - Fixed logic for parsing multiple IDs.
*/

global class ESTransactionUpdater
{
    public static final String ID_SEPARATOR        = ',';
    
    public static final Integer MAX_LIST_ITEMS     = 1000;  //max no. of items in a List 
    public static final Integer MAX_REC_MULTIPLIER = 10;    //10 * 1,000 = 10,000
    
    public static Boolean useDebugLog = true;               // set to true to create debugging logs, otherwise set to false;

    public static void insertTxnLog(String logDesc){
        if (useDebugLog) {
            ESTxn_WS_Log__c wsl = new ESTxn_WS_Log__c();
            wsl.Log_Detail__c = '@' + System.now().format('yyyy.MM.dd.HH.mm.ss') + ':: ' + logDesc;
            insert wsl; 
        }
    }

    WebService static Boolean updatePollingDate (String transID)
    {
        insertTxnLog('<updatePollingDate> transID = ' + transID);
        Boolean updateRan = false;
        if (transID!=null)
        {   
            List<List<String>> idList = parseIDs(transID);
            Savepoint sp = Database.setSavepoint();
            insertTxnLog('<updatePollingDate> Save Point.');
            if (idList.size()<=MAX_REC_MULTIPLIER)
            {   
                for (List<String> idL: idList)
                {
                    Set<String> idSet = new Set<String>();
                    try {
                        idSet.addAll(idL);
                        insertTxnLog('<updatePollingDate> Query = Select Id, Polling_Date_Time__c from Transaction__c where Id IN (' + idSet + ') and CommStatus__c=' + TransactionsReqConstant.COMM_STAT_OPEN);
                        List<Transaction__c> trans = [Select Id, Polling_Date_Time__c from Transaction__c where Id IN: idSet and CommStatus__c=:TransactionsReqConstant.COMM_STAT_OPEN];  
                        if ((trans.size()==idL.size()) && (trans.size() > 0))
                        {
                        
                            Datetime dtNow = System.now();
                            for (Transaction__c transTemp: trans)
                            {
                                transTemp.Polling_Date_Time__c = dtNow;
                            }
                            
                            Database.SaveResult[] transUpdatePollingDate = Database.update(trans, true);
                            if (transUpdatePollingDate[0].isSuccess()==false)
                            {
                                insertTxnLog('<updatePollingDate> FAILED! Unsuccessful UPDATE operation.');
                                Database.rollback(sp);
                                return false;
                            }
                            
                            if (updateRan == false) {
                                updateRan = true;
                            }
                                                     
                        }
                        else
                        {
                            insertTxnLog('<updatePollingDate> FAILED! The no. of param IDs and query rows do not match >>> [no. of IDs: ' +  idL.size() + '] [no. of query rows: ' + trans.size() + ']');
                            if (updateRan) {
                                Database.rollback(sp);
                            }
                            
                            return false;
                        }
                    } catch (QueryException qe) {
                        return false;
                    } catch (DmlException dmle) {
                        return false;
                    } catch (Exception e) {
                        return false;
                    }
                }
                insertTxnLog('<updatePollingDate> SUCCESS! Transaction Polling Dates were updated: ' + transID);
                return true;
            }
            else
            {
                insertTxnLog('<updatePollingDate> FAILED! Too many records: ' + transID);
                return false;
            }
        }
        else
        {
            insertTxnLog('<updatePollingDate> FAILED! Transaction ID param is null.');
            return false;
        }
    }

    WebService static Boolean updateToInProcess (String transID)
    {
        insertTxnLog('<updateToInProcess> transID = ' + transID);
        Boolean updateRan = false;
        if (transID!=null)
        {   
            List<List<String>> idList = parseIDs(transID);
            Savepoint sp = Database.setSavepoint();
            insertTxnLog('<updateToInProcess> Save Point.');
            if (idList.size()<=MAX_REC_MULTIPLIER)
            {
                for (List<String> idL: idList)
                {
                    Set<String> idSet = new Set<String>();
                    try {
                        idSet.addAll(idL);
                        insertTxnLog('<updateToInProcess> Query = Select Id, Polling_Date_Time__c from Transaction__c where Id IN (' + idSet + ') and Polling_Date_Time__c!=NULL and CommStatus__c=' + TransactionsReqConstant.COMM_STAT_OPEN);
                        List<Transaction__c> trans = [Select Id, CommStatus__c from Transaction__c where Id IN: idSet and Polling_Date_Time__c!=NULL and CommStatus__c=:TransactionsReqConstant.COMM_STAT_OPEN];    
                        if ((trans.size()==idL.size()) && (trans.size() > 0))
                        {
                            for (Transaction__c transTemp: trans)
                            {
                                transTemp.CommStatus__c = TransactionsReqConstant.COMM_STAT_IN_PROCESS;
                            }

                            Database.SaveResult[] transUpdatePollingDate = Database.update(trans, true);
                            if (transUpdatePollingDate[0].isSuccess()==false)
                            {
                                insertTxnLog('<updateToInProcess> FAILED! Unsuccessful UPDATE operation.');
                                Database.rollback(sp);
                                return false;
                            }   
                            
                            if (updateRan == false) {
                                updateRan = true;
                            }
                            
                        }
                        else
                        {
                            insertTxnLog('<updateToInProcess> FAILED! The no. of param IDs and query rows do not match >>> [no. of IDs: ' +  idL.size() + '] [no. of query rows: ' + trans.size() + ']');
                            if (updateRan) {
                                Database.rollback(sp);
                            }
                            return false;
                        }
                    } catch (QueryException qe) {
                        return false;
                    } catch (DmlException dmle) {
                        return false;
                    } catch (Exception e) {
                        return false;
                    }
                }
                insertTxnLog('<updateToInProcess> SUCCESS! Transaction Polling Dates were updated: ' + transID);
                return true;
            }
            else
            {
                insertTxnLog('<updateToInProcess> FAILED! Too many records: ' + transID);
                return false;
            }
        }
        else
        {
            insertTxnLog('<updateToInProcess> FAILED! Transaction ID param is null.');
            return false;
        }
        
    }
    
    public static List<List<String>> parseIDs(String transID)
    {
        List<List<String>> idMoreList = new List<List<String>>();
        List<String> idList = new List<String>();
        String tempItems = transID;
        Integer i = tempItems.indexOf(ID_SEPARATOR);
        
        if (i > -1) {
            while (i > -1)
            {
                if (idList.size() == MAX_LIST_ITEMS)
                {
                    idMoreList.add(idList);
                    idList = new List<String>();
                }
    
                String txnId = tempItems.substring(0, i);
                tempItems = tempItems.substring(i + 1);
                idList.add(txnId);
                i = tempItems.indexOf(ID_SEPARATOR);
            }
            
            //w.clavero 20091006: add the last ID to the list
            if (tempItems.length() > 0) {
                if (idList.size() == MAX_LIST_ITEMS)
                {
                    idMoreList.add(idList);
                    idList = new List<String>();
                }
                idList.add(tempItems);
            }
                
        } else {
            idList.add(transID);
        }
        
        //add the last batch of IDs
        if (idList.size() > 0) {
            idMoreList.add(idList);
        }
        
        return idMoreList;
    }
       
}