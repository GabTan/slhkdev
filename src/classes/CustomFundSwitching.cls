/* 

		Version		: 1.1
		Company		: Sun Life Financial
		Date		: AUG.2009
		Author		: Robby Angeles
		Description	: Source class for Funds per Category used in Fund Switching Switch In section
		              and Fund Allocation Input section.
		History	    : 
				      1.1  -  10.NOV.2009 - Robby Angeles  CR: Internal and External Fund References, added attribute fswFSExtFundCode
				      1.2  -  19.OCT.2011 - M.Elepano      SR-ES-11033 First State Fund Closure Change  
		
*/ 
public class CustomFundSwitching {
    
    public Boolean isGIF                        {get; set;}
    public String  fswFSFundName                {get; set;}
    public String  fswFundId                    {get; set;}
    public String  fswFSFundCode                {get; set;}
    public String  fswFSExtFundCode             {get; set;}
    public String  fswTranslatedFundName        {get; set;}
    public String  fswFSFundCatId               {get; set;}
    public String  fswFSFundPrice               {get; set;}
    public Integer fswFSSwitchVal               {get; set;}
    public String  fswStrFSSwitchVal            {get; set;}
    public Decimal fswEstSwitchValAmt           {get; set;}
    public String  fswstrEstSwitchValAmt        {get; set;}
    public String  fswFundAllocPercentage       {get; set;}
    public String  fswFundAllocPercentageTemp   {get; set;}
    //Mikka 10/17/2011
    public String isFundClosure   				{get; set;}
    public Integer CurrAllocPercentage          {get; set;}
    //Mikka - End 10/17/2011
    

    public CustomFundSwitching(String fswFundId, String fswFSFundName, 
        String fswFSFundCode, String fswFSExtFundCode,String fswFSFundCatId, String fswFSFundPrice, 
        Integer fswFSSwitchVal, Decimal fswEstSwitchValAmt, String fswstrEstSwitchValAmt, 
        String fswFundAllocPercentage, Boolean isGIF,String isFundClosure,Integer CurrAllocPercentage){
        
        this.fswFundId                  = fswFundId;
        this.fswFSFundName              = fswFSFundName;
        this.fswFSFundCode              = fswFSFundCode;
        this.fswFSExtFundCode           = fswFSExtFundCode;
        this.fswFSFundCatId             = fswFSFundCatId; 
        this.fswFSFundPrice             = fswFSFundPrice;
        this.fswFSSwitchVal             = fswFSSwitchVal;
        fswStrFSSwitchVal               = String.valueOf(fswFSSwitchVal)=='0'?'':String.valueOf(fswFSSwitchVal);
        this.fswEstSwitchValAmt         = fswEstSwitchValAmt;
        this.fswstrEstSwitchValAmt      = fswstrEstSwitchValAmt;
        this.fswFundAllocPercentage     = fswFundAllocPercentage; 
        this.fswFundAllocPercentageTemp = fswFundAllocPercentage;
        this.isGIF                      = isGIF;
        this.isFundClosure              = isFundClosure;
        this.CurrAllocPercentage        = CurrAllocPercentage;
        /*
        this.fswFundId                  = fswFundId!=null?fswFundId:'';
        this.fswFSFundName              = fswFSFundName!=null?fswFSFundName:'';
        this.fswFSFundCode              = fswFSFundCode!=null?fswFSFundCode:'';
        this.fswFSFundCatId             = fswFSFundCatId!=null?fswFSFundCatId:'';
        this.fswFSFundPrice             = fswFSFundPrice;
        this.fswFSSwitchVal             = fswFSSwitchVal;
        fswStrFSSwitchVal               = String.valueOf(fswFSSwitchVal)=='0'?'':String.valueOf(fswFSSwitchVal);
        this.fswEstSwitchValAmt         = fswEstSwitchValAmt;
        this.fswFundAllocPercentage     = fswFundAllocPercentage!=null?fswFundAllocPercentage:'';
        this.fswFundAllocPercentageTemp = fswFundAllocPercentage!=null?fswFundAllocPercentage:'';*/
    }
   
}