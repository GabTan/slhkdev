/*
Version 	: 1.4
Company		: Sun Life Financial
Date		: 03.JULY.2009
Author		: Dorothy Anne L. Baltazar
Description	: Class containing methods for Transaction Pre-Validations.
History		: 
			1.1 JC NACHOR 08/12/2009 added error checking if user have no active inforce Policy
			
			1.2 Robby Angeles 09/30/2009 changed checking of fund transaction history/fund detail record from '1' value to 'P', line 103
			1.3 15.OCT.2009 - W. Clavero - Updated the checkSinglePremium method (IL-ES-00043).
			1.4 19.NOV.2009 - deleted the SORStatus in the query of checkNonES function
*/
public class TransactionValidator {	
	public Boolean checkCastIron()
	{	
		Boolean isCastIronAvailable;
		List<Config_Global_Settings__c> configGlobalSettings = new List<Config_Global_Settings__c>();
		
		configGlobalSettings = [SELECT TXN_Is_Cast_Iron_Available__c 
								  FROM Config_Global_Settings__c 
								 WHERE TXN_Is_Cast_Iron_Available__c = true];
		
		if (configGlobalSettings.size()>0)
		{
			isCastIronAvailable = true;
		}
		else
		{
			isCastIronAvailable = false;
		}
		return isCastIronAvailable;
	}
	
	public Boolean checkCPExistRequest(String acctClientId, String transType, String transStatus)
	{
		Boolean isCPExistRequest;
		List<Transaction__c> txnDetails = new List<Transaction__c>();
		
		txnDetails = [SELECT TxnType__c, SORStatus__c, OwnerId, Client_Id__c, Policy_ID__c, Active__c 
						FROM Transaction__c 
					   WHERE Client_Id__c = :acctClientId AND TxnType__c = :transType AND SORStatus__c = :transStatus AND Active__c = true];
		if (txnDetails.size()>0)
		{
			isCPExistRequest = true;
		}
		else
		{
			isCPExistRequest = false;
		}
		return isCPExistRequest;
	}
	
	/* JC NACHOR 08/12/2009 - added validation in checking if the user have an active inforce Policy 
	 * before allowing the user to change his / her contact information 
	 */
	public Boolean checkNoActiveInforcePolicy(String strUserId) 
	{
		Boolean hasNoActiveInforcePolicy = false;
		List<Policy__c> listPolicies = new List<Policy__c>();
		
		/* w. clavero 20091015: unified with other batch2 pages to use constant
		listPolicies = [SELECT Id 
		                FROM Policy__c
		                WHERE OwnerId = :strUserId AND Active__c = true AND 
		                  (Policy_status_hidden__c = '1' OR Policy_Status_Hidden__c = '2' OR Policy_Status_Hidden__c = '3' OR Policy_Status_Hidden__c = '4')];
		*/
		listPolicies = [SELECT Id 
		                FROM Policy__c
		                WHERE OwnerId = :strUserId 
		                AND Active__c = true 
		                AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES];
		                  
	  	if (listPolicies.size()==0)
	    {
	      hasNoActiveInforcePolicy = true;
	    }
	    else
	    {
	      hasNoActiveInforcePolicy = false;
	    }
	    return hasNoActiveInforcePolicy;
	}
	
	public Boolean checkExistRequest(String policyNumber, String transType, String transStatus)
	{
		Boolean isExistRequest;
		List<Transaction__c> txnDetails = new List<Transaction__c>();
		
		txnDetails = [SELECT TxnType__c, SORStatus__c, OwnerId, Client_Id__c, Policy_ID__c, Active__c 
						FROM Transaction__c 
					   WHERE Policy_ID__c = :policyNumber AND TxnType__c = :transType AND SORStatus__c = :transStatus AND Active__c = true];
				   
		if (txnDetails.size()>0)
		{
			isExistRequest = true;
		}
		else
		{
			isExistRequest = false;
		}
		return isExistRequest;
	}
	
	public Boolean checkPendingFundTxn(String policyNumber)
	{
		Boolean isPendingFundTxn;
		List<Fund_Transaction_History__c> fundTxnHistoryDetails = new List<Fund_Transaction_History__c>();
		
		fundTxnHistoryDetails = [SELECT Transaction_Date__c, Reversal_Code__c, Policy__r.Policy_Number__c, Policy__c, Fund_Price_Date__c, Active__c, Fund_Detail__c 
								 FROM Fund_Transaction_History__c f 
								 WHERE Policy__r.Policy_Number__c = :policyNumber AND Fund_Detail__c = 'P' AND Active__c = true];
		
		if (fundTxnHistoryDetails.size()>0)
		{
			isPendingFundTxn = true;
		}
		else
		{
			isPendingFundTxn = false;
		}
		return isPendingFundTxn;
	}
	
	public Boolean checkLapseStartDate(String policyNumber)
	{
		Boolean isLapseStartDate;
		List<Policy__c> policyDetails = new List<Policy__c>();
		
		policyDetails = [SELECT UL_lapse_start_date__c, Policy_Number__c, Active__c 
						   FROM Policy__c p 
						  WHERE Policy_Number__c = :policyNumber AND UL_lapse_start_date__c != null AND Active__c = true];
		
		if (policyDetails.size()>0)
		{
			isLapseStartDate = true;
		}
		else
		{				  
			isLapseStartDate = false;
		}
		return isLapseStartDate;
	}
	
	public Boolean checkNonES(String policyNumber)
	{
		Boolean isNonES;
		List<Transaction_Non_ES__c> txnNonESDetails = new List<Transaction_Non_ES__c>();
		
		//START Wcheng enhancement 11/19/09
		txnNonESDetails = [SELECT SORStatus__c, Policy__r.Policy_Number__c, Active__c, Policy__c 
									 FROM Transaction_Non_ES__c t
									WHERE Policy__r.Policy_Number__c = :policyNumber AND Active__c = true];
		//END Wcheng enhancement 11/19/09
									
		if (txnNonESDetails.size()>0)
		{
			isNonES = true;
		}
		else
		{
			isNonES = false;
		}					
		return isNonES;
	}
	
	public Boolean checkSinglePremium(String policyNumber)
	{
		Boolean isSinglePremium;
		List<Policy__c> policyDetails = new List<Policy__c>();
		/* w. clavero 20091015: change the condition for single premium		
		policyDetails = [SELECT Payment_Mode__c, Policy_Number__c, Active__c 
						   FROM Policy__c p 
						  WHERE Policy_Number__c = :policyNumber AND Payment_Mode__c = '00' AND Active__c = true];
		*/
		
		policyDetails = [SELECT Payment_Mode__c, Policy_Number__c, Active__c 
						   FROM Policy__c p 
						  WHERE Policy_Number__c = :policyNumber AND Billing_Method__c = :TransactionsReqConstant.POLICY_SINGLE_PREMIUM AND Active__c = true];
					  
		if (policyDetails.size()>0)
		{
			isSinglePremium = true;
		}
		else
		{
			isSinglePremium = false;	
		}
		return isSinglePremium;
	}
}