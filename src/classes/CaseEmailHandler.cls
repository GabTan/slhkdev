global class CaseEmailHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {

        //create result for email operation
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        //contact shell
        Contact con = new Contact();
        Contact[] fuzzy = new Contact[]{};


        try {
            con = [Select Id from Contact where email = :email.fromAddress];
        } catch (Exception e) {

            String domain = '%'+ email.fromAddress.substring(email.fromAddress.indexOf('@')+1)+'%';
            try{
            fuzzy = [Select AccountId from Contact where email like :domain];
            Con.lastName = email.fromAddress.substring(1,email.fromAddress.indexOf('@'));
            Con.AccountId = fuzzy[0].AccountId;
            Con.email = email.fromAddress;
        }   catch(Exception d){
                Con.lastName = email.fromAddress.substring(1,email.fromAddress.indexOf('@'));
                Con.email = email.fromAddress;
                Con.AccountId = '0013000000M9NIU'; /******* CHANGE THIS LINE*******/
            }
            insert Con;
        }


        //Creating case shell
        Case c = new Case();

        //populating case fields
        c.ContactId = con.Id;

        c.Status = 'New';
        c.Subject = email.subject;
        c.Description = email.plainTextBody;
        c.RecordTypeId = '012300000001cbL'; /******** CHANGE THIS LINE********/

        //putting the case in the system
        insert c;

        //I'm saying that it worked, and that A new case was created.
        result.message='New Case Created';
        result.success = true;
        return result;
    }
}