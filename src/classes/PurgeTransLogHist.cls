/* 
Version     : 1.1
Company     : Sun Life Financial
Date        : 3/23/2012
Author      : Benjo Malabanan
Description : Class to purge data of Transaction_Data_Hist__c every 15 days
History     : 
*/
global class PurgeTransLogHist implements Schedulable{
    
   global void execute(SchedulableContext ctx) {
      
         //datetime myDateTime = datetime.now().addDays(-1);
        datetime myDateTime = datetime.now().addDays(-2);
        string mydtstring = mydatetime.format('yyyy-MM-dd') + 'T00:00:00Z';  // eg:1
        
        string TransQuery;
        TransQuery = 'SELECT Id FROM  Transaction_Data_Hist__c WHERE ';
        TransQuery += 'CreatedDate < '+ mydtstring + ' limit 10000 ' ;
        system.debug(TransQuery );

        List<Transaction_Data_Hist__c> existing =database.query(TransQuery);


      //Delete records      
      delete existing;
   } 
}