public class PolicyViewController {

	Boolean showX = true;
	Policy__c policy = null;
	String policyId = null;
	public PolicyViewController()
	{
		
	}
	
	public Boolean getShowX()
	{
		showX = !showX;
		return showX;
	}
	
	public String getPolicyId()
 	{
 		if (null == policyId)
 		{
 			policyId = System.CurrentPageReference().getParameters().get('policyId');
 		}
 		
 		return policyId;
 	}
 	
 	public Policy__c getPolicy()
 	{
 		if (policy == null)
 		{
 			// policy = [Select UL_lapse_start_date__c, Total_Mode_Premium__c, Status_Change_Date__c, Special_Instruction__c, Single_Premium_Amount__c, Restrict_Billing_Code_2__c, Relationship_to_Insured__c, Regular_Contribution__c, RecordTypeId, Previous_Premium_Mode__c, Previous_Policy_Status__c, Previous_Mode_Premium_Amount__c, Previous_Annual_Premium__c, Policy_Rejection_Code__c, Policy_Promotion_Code_Campaign_Code__c, Policy_Premium_Type_Code__c, Policy_Issue_Date__c, Policy_Insurance_Type__c, Policy_Inforce_Date__c, Policy_Credit_Card_Status__c,Policy_Credit_Card_Reject_Reason__c, Policy_Credit_Card_Reject_Reason_Descrip__c, Policy_Country_Code__c, Policy_Comments_Remarks__c, Policy_Cease_Reason__c, Policy_Cease_Date__c, Payment_Mode__c, Payment_Method__c, Paid_to_Date__c, OwnerId, Outstanding_Disbursement_Amount__c, Next_Autopay_Date__c, Name, Min_Requirement_on_Initial_Deposit__c, MayEdit, Maximum_Deposit_Amount__c, Mandatory_Contribution__c, Mandatory_Contribution_End__c, Last_Autopay_Date__c, IsLocked, Initial_Lump_Sum_Contribution__c, Initial_Deposit__c, Id, HKID_no__c, Dividend_Option__c, Debit_Date__c, Death_Benefit__c, Death_Benefit_Option__c, DDA_Limit__c, CurrencyIsoCode, Credit_Card_Number__c, Credit_Card_Last_Autopay_Date__c, Credit_Card_Holder_Name__c, CreatedDate, CreatedById, Client__c, Branch_Number__c, Billing_Method__c, Bank_Response_Status__c, Bank_Number__c, Bank_Name__c, Bank_Acount_Holder_Name__c, Autopay_Status__c, Autopay_Rejected_Fatal_Indicator__c, Autopay_Reject_Reason_Description__c, Application_Sign_Date__c, Application_Received_Date__c, Application_Form_ID__c, Annualized_Premium__c, Account_Status__c, Account_Number__c, Absolute_Premium_Discount_Amount__c, AFYP__c, AFYP3__c, AFYP2__c From Policy__c where Id = :getPolicyId()];
 		}
 		
 		return policy;
 	}
 		
}