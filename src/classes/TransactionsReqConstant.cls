/*
Version : 1.2
Company : Sun Life Financial
Date    : 27.July.2009
Author  : Ryan Wilson, Wilson Cheng
Description : This is used as constants for CPP, FSW, and CSH.
History : 
         Version 1.1 <09/08/09 - Wcheng - SIT_50>
         1.2	15.OCT.2009 - W. Clavero - Added new policy constants (IL-ES-00043)
*/
public class TransactionsReqConstant
{
    //for both CPP and FSW
    public static final string TXN = 'Txn';
    public final static string REQUEST_TYPE = 'RequestType';
    public final static string REFERENCE_NO = 'ReferenceNo';
    public final static string POLICY_NO = 'PolicyNo';
    public final static string SUBMISSION_DATE_TIME = 'SubmissionDateTime';
    
    //for change contact info.
    public final static string VALUE = 'value';
    public final static string NO_CHANGE = 'nochange';
    public final static string HOME_PHONE = 'homephone';
	public final static string OFFICE_PHONE = 'officePhone';
	public final static string MOBILE_PHONE = 'mobilephone';
	public final static string EMAIL_ADDRESS = 'emailaddress';
	public final static string LINE1 = 'line1';
	public final static string LINE2 = 'line2';
	public final static string LINE3 = 'line3';
	public final static string LINE4 = 'line4';
	public final static string LINE5 = 'line5';
	public final static string CITY = 'city';
	public final static string PROV = 'prov';
	public final static string ZIP = 'zip';
	public final static string COUNTRY = 'country';
	public final static string POLICIES= 'policies';
	public final static string POLICIES_NUM= 'policyno';
	public final static string EMPTY_STRING='';
	public final static string ADDRESS ='address';
	public final static string ALL_POLICIES = 'X';
	
	//for View Claim History page
	public static final string CSH_STATUS = 'CSH_STATUS';
	public static final string CSH_PAYMETHOD = 'CSH_PAYMETHOD';
	public static final string CSH_CLAIMTYPE = 'CSH_CLAIMTYPE';
	public static final string CSH_ADMITTED = '2';
	public static final string CSH_DECLINED = '3';
	//START EDIT Wcheng SIT_50 09/08/09
	public static final string CSH_APPROVE_REJECTED_DATE = 'dd/MM/yyyy';
	public static final string HK_TIME_ZONE = 'Asia/Hong_Kong';
	//END EDIT Wcheng SIT_50 09/08/09
	public static final string CSH_A = 'A';
	public static final string CSH_H = 'H';
	public static final string CSH_J = 'J';
	public static final string CSH_T = 'T';
	public static final string CSH_FR = 'FR';
	public static final string CSH_CB = 'CB';
	public static final string CSH_TRUE = 'true';
	public static final string CSH_FALSE = 'false';
	public static final string CSH_PROCESSING = '1';
	public static final string CSH_PENDING = '4';
	public static final string CSH_YYYY = 'yyyy';
    
    //for fundswitching And Change of Fund Allocation
    public static final string SW_OUT = 'switchout';
    public static final string SW_IN = 'switchin';
    public static final string FUND = 'fund';
    public static final string NAME = 'name';
    public static final string CODE = 'code';
    public static final string PERCENT = 'percent';
    public static final string ESTIMATED_AMOUNT = 'estamount';
    public static final string CREATE_NEW = 'createnew';
    public static final string YES = 'Y';
    public static final string NO = 'N';
    public static final string FUND_NAME_DT = 'FUND_NAME';
    
    //for Transaction model
    public static final string COMM_STAT_OPEN = 'Open';
    public static final string COMM_STAT_IN_PROCESS= 'In Process';
    
    //for Transaction log
    public static final string TXN_STATUS = 'TXN_STATUS';
    
    //policy inforce status codes
    public static final Set<String> POLICY_INFORCE_STATUSES = new Set<String>{'1', '2', '3', '4'};
    
    //policy UL type codes
    public static final Set<String> POLICY_UL_CODES = new Set<String>{'A', 'B', 'C', 'D', 'E', 'F',  
    																	'G', 'H', 'I', 'J', 'K', 'L', 
    																	'M', 'N', 'O', 'P', 'Q', 'R', 
    																	'S', 'T', 'U', 'V', 'W', 'X', 
    																	'Y', 'Z'};
    																	
    //policy single premium code
    public static final String POLICY_SINGLE_PREMIUM = 'P';
    
}