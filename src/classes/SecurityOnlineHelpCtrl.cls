/*
Company     : Sun Life Financial
Date        : 08/14/2009
Author      : W. Clavero
Description : Controller for the SecurityOnlineHelp page.
History     : 
*/
public class SecurityOnlineHelpCtrl {

	public String helpURL {get; set;}
	
	public SecurityOnlineHelpCtrl() {
	    if (Userinfo.getLanguage().equals(ESPortalNavigator.LANG_CHI_TRADITIONAL)) {
            this.helpURL = ESPortalNavigator.NAV_URL_HLP_SEC_CHI;
	    } else {
	        this.helpURL = ESPortalNavigator.NAV_URL_HLP_SEC_ENG;
	    }	
	}

}