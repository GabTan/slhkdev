/*

Version        : 1.0 
Company        : Sun Life Financial 
Date           : 22.JUL.2009
Author         : Robert Andrew Almedina
Description    : This class acts as a controller for the Pre Acknowledgement pop up dialog
History        : 
                1.1 09.SEP.2009 - Robert Andrew Almedina - 		  Declared Asia/Hongkong as the base time for the time to be displayed
                
                1.3 14.SEP.2009 - Robby Angeles 		 - TC 1 - Updated DateNow value to System.Now reference
                
                2.0 14.SEP.2009 - Robby Angeles  				- Remove reference to DateNow variable.
                												  Class now of no use
                
*/ 

public class TXN_PreAckViewCtrl {

	//public String DateNow {get;set;}
	
	public TXN_PreAckViewCtrl()
	{
		//DateNow = DateTime.now().format('dd/MM/yyyy HH:mm', 'Asia/Hong_Kong');
	}
	//public String getconfirmDateTime(){
    //    return System.now().format('dd/MM/yyyy HH:mm', 'Asia/Hong_Kong');    
    //}
}