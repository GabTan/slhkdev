/*
Version		: 1.0
Company 	: Sun Life Financial
Date    	: 24.SEP.2009
Author  	: W. Clavero
Description : Verifies the access of a user to a record. 
History 	: 
*/

public without sharing class ESAccessVerifier {
	//checks if a user has access to Policy
	public static Boolean isUsersPolicy(String policyNumber, ID userID) {
		Boolean isValid = false;
		
		try {
			List<User> usr = [Select Id, Client_Id__c From User Where Id = :userID];
			User aUser;
			if (usr.size() > 0) {
				if (usr.get(0).Client_Id__c == null) {
					return false;
				} else {
					aUser = usr.get(0);
				}
			} else {
				return false;
			}
			
			List<Account> clientRec = [Select Id, Client_Id__c From Account Where Client_Id__c = :aUser.Client_Id__c];
			if (clientRec.size() == 0) {
				return false;
			}
			
			List<Policy__c> policyRec = [Select Id, Policy_Number__c, Client__c, OwnerId From Policy__c Where Policy_Number__c = :policyNumber];
			if (policyRec.size() > 0) {
				Policy__c pol = policyRec.get(0);
				if ((pol.OwnerId == userID) && (pol.Client__c == clientRec.get(0).Id)) {
					isValid = true;
				}
			}
		} catch (QueryException qe) {
			return false;
		}
		return isValid;
	}
	//check if the current user has access to a Policy
	public static Boolean isCurrentUsersPolicy(String policyNumber) {
		return isUsersPolicy(policyNumber, Userinfo.getUserId());
	}
	//checks if a user has access to a Client(Account)
	public static Boolean isUsersClient(String clientID, ID userID) {
		Boolean isValid = false;
		
		try {
			List<User> usr = [Select Id, Client_Id__c From User Where Id = :userID];
			if (usr.size() > 0) {
				if (usr.get(0).Client_Id__c == null) {
					return false;
				}
			} else {
				return false;
			}
			
			List<Account> clientRec = [Select Id, Client_Id__c From Account Where Client_Id__c = :clientID];
			if (clientRec.size() > 0) {
				Account cl = clientRec.get(0);
				if (cl.Client_Id__c == null) {
					return false;
				} else if (cl.Client_Id__c.equals(usr.get(0).Client_Id__c)) {
					isValid = true;
				}			
			}
		} catch (QueryException qe) {
			return false;
		}
		return isValid;	
	}
	//check if the current user has access to a Client
	public static Boolean isCurrentUsersClient(String clientID) {
		return isUsersClient(clientID, Userinfo.getUserId());
	}
	//check if the user has access to a Transaction record
	public static Boolean isUsersTransaction(String txnID, ID userID) {
		Boolean isValid = false;
		
		if(txnID.length() < 15 || txnID.length() > 18) {
			return false;
		}
		
		try {
			List<User> usr = [Select Id, Client_Id__c From User Where Id = :userID];
			if (usr.size() == 0) {
				return false;
			}
			
			List<Transaction__c> txnRec = [Select Id, OwnerId From Transaction__c Where Id = :txnID];
			if (txnRec.size() > 0) {
				Transaction__c txn = txnRec.get(0);
				if (txn.OwnerId == userID) {
					isValid = true;
				}			
			}
		} catch (QueryException qe) {
			return false;
		} catch (Exception e) {
      return false;
    }
		return isValid;	
	}
	//check if the current user has access to a Transaction record
	public static Boolean isCurrentUsersTransaction(String txnID) {
		return isUsersTransaction(txnID, Userinfo.getUserId());
	}	
}