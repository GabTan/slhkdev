/*
	Version	   : 1.0
    Company    : Sun Life Financial
    Date       : 31.JUL.2009
    Author     : Marian Baleros
    Description: Class for the Fund Transaction History fields.
    History    : 
    
    [Version after modification] <Date Modified - Author - Short description of modification>
    
*/

public class FundTxnHstyValues
{
	public String fundtransName {get; set;}
	public String valuateDate {get; set;}
	public String transactType {get; set;}
	public String funitPrice {get; set;}
	public String intrstRate {get; set;}
	public String maturityDate {get; set;}
	public String numOfUnits {get; set;}
	public String fundsValue {get; set;}
	
	public FundTxnHstyValues(String fundName,Date valDate,String tranType,String unitPrice,String intRate,String matDate,String numUnits,String fundValue)
	{
		this.fundtransName = fundName;
		if(valDate != null){
			this.valuateDate = valDate.format();
		}
		else{
			this.valuateDate = null;
		}
		this.valuateDate = valDate.format();
		this.transactType = tranType;
		this.funitPrice = unitPrice;
		this.intrstRate = intRate;
		if(matDate != null){
			//this.maturityDate = matDate.format();
			this.maturityDate = matDate;
		}
		else{
			this.maturityDate = null;
		}
		this.numOfUnits = numUnits;
		this.fundsValue = fundValue;
		
	}
}