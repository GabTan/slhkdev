public class SLFTriggerTests {

    public static TestMethod void testFileNameParse()
    {
        String quarterlyStatement =  '201006451_20060307-20060906.pdf';
        String annualStatement = '030183907.2008.pdf';
        
        System.debug(quarterlyStatement +' is Pdf '+StatementManager.isPdf(quarterlyStatement));
        System.assert(StatementManager.isPdf(quarterlyStatement),quarterlyStatement+ ' is a PDF');
        
        System.assert(!StatementManager.isPdf('pdf.txt'), 'pdf.txt Should not be a PDF!');
    
        quarterlyStatement = StatementManager.removePdfExtension(quarterlyStatement);
        
        System.debug('Removed Extension - '+quarterlyStatement+' Is Quarterly '+StatementManager.isQuarterlyStatement(quarterlyStatement));
        
        if (StatementManager.isQuarterlyStatement(quarterlyStatement))
        {       
            // parseDate('20060930');
            StatementManager.getQuarterlyStartDate(quarterlyStatement);
            StatementManager.getQuarterlyEndDate(quarterlyStatement);
        }
        
        if (StatementManager.isPdf(annualStatement))
        {
            annualStatement = StatementManager.removePdfExtension(annualStatement);
            
            if (StatementManager.isAnnualStatement(annualStatement))
            {
                System.debug('Found Annual Statement '+annualStatement);
                System.debug('Start '+StatementManager.getAnnualStartDate(annualStatement));
                System.debug('End '+StatementManager.getAnnualEndDate(annualStatement));
            }
        }
    }
    
    public static testMethod void testAttachment() 
    {
        // Policy__c p=[select p.id from Policy__c p, p.Client__r a where a.recordtypeid='012300000006IcS' limit 1];
        Account accountTest = [Select Id,Client_Id__c,OwnerId from Account where IsCustomerPortal = TRUE and e_Registration_Status__pc= 'Create' and Owner.IsActive = TRUE limit 1];
        Policy__c p = new Policy__c(Name='Test Pol',client__c = accountTest.Id, OwnerId = accountTest.OwnerId  );
        insert p;
        
        Attachment a1=new Attachment(contenttype='application/pdf', name='201006451_20060307-20060906.pdf', parentid=p.Id, body=Blob.valueOf('hello'));
        Attachment a2=new Attachment(contenttype='application/pdf', name='030183907.2008.pdf', parentid=p.Id, body=Blob.valueOf('hello'));
        
        Attachment[] atList=new Attachment[]{a1, a2};
        
        insert atList;

        Statement__c[] statements=[select id, Name, Attachment_Id__c, policy__c from Statement__c where Attachment_Id__c IN (:a1.Id, :a2.Id) ];
        
        System.assert(statements.size()==2, 'Statement Insert Failed');
        
         
        for(Statement__c stmt: statements) {
            System.debug('Statement Policy ID:'+stmt.policy__c);
            System.debug('Statement Name'+stmt.Name);
            System.debug('Statement Attchment Id'+stmt.Attachment_Id__c);
            System.assert(stmt.policy__c==p.Id, 'failed');
        }
        
        // fire the delete attachment
        delete atList;
        
        statements=[select id, Attachment_Deleted__c  from Statement__c where Attachment_Id__c IN (:a1.Id, :a2.Id) ];
        
        // check if Attachment Delete Trigger and updateStatementOnDelete method worked fine
        for(Statement__c stmt: statements) {
            System.assert(stmt.Attachment_Deleted__c==true, 'Statement Delete Operation Failed');
        }
    }
    
    public static testmethod void testPolicyOwnerUpdate() {
        
        Policy__c policyobj;
        //Initialize a user object
        //Select a Customer Portal user for Test Case Scenarios
        User u=[Select u.Id, c.AccountId from User u, u.Contact c where u.ContactId!=null and IsActive=True limit 1];    
        //User u=[Select u.Id, c.AccountId from User u, u.Contact c where u.Id=''];
        
        // These statements check the following methods:
        // Check Trigger: Policy Before Insert
        // Check updatePolicyOwner(Policy__c[]) method
        System.debug('User Id is:'+u.Id);
        System.debug('User Account Id is:'+u.Contact.AccountId);
        
        Policy__c policyObj1=new Policy__c(Client__c=u.Contact.AccountId, Name='P0001');
        Policy__c policyObj2=new Policy__c(Client__c=u.Contact.AccountId, Name='P0002');
        Policy__c[] policyList=new Policy__c[]{policyObj1, policyObj2}; 

        insert policyList;
        
        policyobj=[select id, ownerid from Policy__c where id=:policyObj1.Id];
                
        System.debug('Policy Id is:'+policyObj.Id);
        
        System.assert(policyObj.ownerId==u.Id, 'Failed! Policy update failed');
        System.assert(policyObj.ownerId==u.Id, 'Failed! Policy update failed');
        
        // These statements check the following methods:
        // Check Trigger: Policy Before Update
        // Check updatePolicyOwner(Policy__c[]) method

        // Create new account
        Account a=new Account(recordtypeid='012300000006IcS', LastName='Test Person Account', Client_ID__pc='C0001X', PersonEmail='a@b.com');
        // Database.upsert(a,Account.Client_ID__c);
        insert a;
        update a;
        
        a=[Select a.PersonContactId from Account a where Id=:a.Id];
        // Update policy accounts for testing purposes
        policyObj1.Client__c=a.Id;
        policyObj2.Client__c=a.Id;
        update policyList;
        
        User us=[Select u.Id, c.AccountId from User u, u.Contact c where u.ContactId!=null and u.IsActive=true limit 1];
        PolicyOwnership.updatePolicyOwner(new ID[]{us.Id});
    }
    
    public static testmethod void testInsertUser()
    {
        // Sys Admin Profile - 00e30000000w2RE
        // Custom Portal ProfileId='00e30000000w2RG'
        //comment starts here 06/21/2010
        //User u = new User(ProfileId='00e30000000w2RE', username='xFZk@xFZk.com',LastName='Last',email='xFZk@xFZk.com',alias='test', TimeZoneSidKey='Asia/Hong_Kong',LocaleSidKey='ar',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US');
        //insert u;
    }
    
    public static testmethod void testUpdateUser()
    {
        // Sys Admin Profile - 00e30000000w2RE
        // Custom Portal ProfileId='00e30000000w2RG'
        User us=[Select u.Id, c.AccountId from User u, u.Contact c where u.ContactId!=null and u.IsActive=true limit 1];
        update us;
    }
    
    public static TestMethod void testSendReminder()
    {
        /*
        // Find a Portal User
        User portaluser = [Select id,ContactId from User where UserType = 'PowerCustomerSuccess' and ContactId != null and Contact.IsPersonAccount = true Limit 1];
        
        // Find their Client record
        Account client = [Select id from Account where PersonContactid = :portalUser.ContactId];

        // Create a Policy
        Policy__c pol = new Policy__c(Client__c=client.Id, Name='P0001',Policy_Number__c='2266990022',Premium_Due_Date__c=System.today());
        insert pol;
        
        // Update the reminder flag
        pol.Send_15_Day_Payment_Due_Reminder__c = true;
        update pol;
        
        Policy__c check = [select Send_15_Day_Payment_Due_Reminder__c, Send_31_Day_Payment_Due_Reminder__c, X31_Day_Payment_Due_Reminder_Sent__c, X15_Day_Payment_Due_Reminder_Sent__c from Policy__C where Id = :pol.Id];
        
        System.assert(check.Send_15_Day_Payment_Due_Reminder__c == false,'15 Reminder Should be false');
        System.assert(check.X15_Day_Payment_Due_Reminder_Sent__c == true,'15 Reminder Should Sent be true');
        System.assert(check.Send_31_Day_Payment_Due_Reminder__c == false,'31 Reminder Should be false');
        System.assert(check.X31_Day_Payment_Due_Reminder_Sent__c == false,'31 Reminder Should Sent be false');
        
        pol.Send_31_Day_Payment_Due_Reminder__c = true;
        update pol;
        
        check = [select Send_15_Day_Payment_Due_Reminder__c, Send_31_Day_Payment_Due_Reminder__c, X31_Day_Payment_Due_Reminder_Sent__c, X15_Day_Payment_Due_Reminder_Sent__c from Policy__C where Id = :pol.Id];
        System.assert(check.Send_15_Day_Payment_Due_Reminder__c == false,'15 Reminder Should be false');
        System.assert(check.X15_Day_Payment_Due_Reminder_Sent__c == true,'15 Reminder Should Sent be true');
        System.assert(check.Send_31_Day_Payment_Due_Reminder__c == false,'31 Reminder Should be false');
        System.assert(check.X31_Day_Payment_Due_Reminder_Sent__c == true,'31 Reminder Should Sent be true');
        
        pol.Premium_Due_Date__c = pol.Premium_Due_Date__c.addDays(1);
        update pol;

        check = [select Send_15_Day_Payment_Due_Reminder__c, Send_31_Day_Payment_Due_Reminder__c, X31_Day_Payment_Due_Reminder_Sent__c, X15_Day_Payment_Due_Reminder_Sent__c from Policy__C where Id = :pol.Id];
        System.assert(check.Send_15_Day_Payment_Due_Reminder__c == false,'15 Reminder Should be false');
        System.assert(check.X15_Day_Payment_Due_Reminder_Sent__c == false,'15 Reminder Should Sent be false');
        System.assert(check.Send_31_Day_Payment_Due_Reminder__c == false,'31 Reminder Should be false');
        System.assert(check.X31_Day_Payment_Due_Reminder_Sent__c == false,'31 Reminder Should Sent be false');
        */
    }
    
    public static TestMethod void testAccountStatusUpdate()
    {
        // Find an account linked to an active portal user - who has a non null
                // Find a Portal User
        User portaluser = [Select id,ContactId from User where UserType = 'PowerCustomerSuccess' and ContactId != null and Contact.IsPersonAccount = true Limit 1];
        
        // Find their Client record
        Account client = [Select id from Account where PersonContactid = :portalUser.ContactId];
        
        client.e_Registration_Status__pc = 'Create';
        update client;
        
        //client.e_Registration_Status__pc = 'Resend';
        //update client;
    }
    
    public static TestMethod void testAccountStatusResendUpdate()
    {
        // Find an account linked to an active portal user - who has a non null
                // Find a Portal User
        User portaluser = [Select id,ContactId from User where UserType = 'PowerCustomerSuccess' and ContactId != null and Contact.IsPersonAccount = true Limit 1];
        
        // Find their Client record
        Account client = [Select id from Account where PersonContactid = :portalUser.ContactId];
        
        client.e_Registration_Status__pc = 'Resend';
        update client;
    }
    
}