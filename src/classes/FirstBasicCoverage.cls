/* 

		Version		: 1.2
		Company		: Sun Life Financial
		Date		: SEP.10.2009
		Author		: Robby Angeles
		Description	: custom class that returns the plan code for the first basic coverage of a policy
		History		:	<Build> - <Change Date> - <Author> - <Test Cycle> - <Bug #> - <Description>
					    1.1 03.SEP.2009 - Robby Angeles - TC 1 - Created
					    
					    1.2 08.OCT.2009 - Robby Angeles - Changed retrieval logic for plan code (email from J.Cheung: first basic coverage 
					    								  is the coverage for the policy under context with a Name of COV01).  
	

*/

public without sharing class FirstBasicCoverage {
	
	List<Coverage__c> polCovs = new List<Coverage__c>();
	String policyId = '';
	
	public FirstBasicCoverage(String pId){
		policyId = pId;	
	}
	public String getFirstBasicCoverageValue() {
        String strPolicyPlanCode = '';
        try{
	        polCovs = [Select id, Name, Plan_Code__c 
		   			   From Coverage__c 
		               Where Policy__r.Policy_Number__c = :policyId
		               And Name = 'COV01'
		               And Coverage_Status__c in ('1', '2', '3', '4') 
		               And Active__c = true
		               Limit 1];
		    if(polCovs.size() > 0){
		    	strPolicyPlanCode = polCovs[0].Plan_Code__c;
		    }
        }
	    catch(exception e){
	    	
	    }
	    if(strPolicyPlanCode == null)strPolicyPlanCode = '';
        return strPolicyPlanCode;  
    }        
}