/* 
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Robert Andrew Almedina
Description	: This class acts as a test coverage for the Payment History view page controller
History	:

*/ 

public class PaymentHstyViewCtrl_Test {

	static testMethod void myUnitTest() {
		
		Test.setCurrentPageReference(new PageReference('Page.PaymentHstyView'));
		
		List<Policy__c> Policy 	= new List<Policy__c>();
		Policy = [Select p.X31_Day_Payment_Due_Reminder_Sent__c, p.X15_Day_Payment_Due_Reminder_Sent__c, p.UL_lapse_start_date__c, p.Total_Value__c, p.Total_Paid_up_Additions__c, p.Total_Mode_Premium__c, p.SystemModstamp, p.Surrender_Value__c, p.Status_Change_Date__c, p.Special_Instruction__c, p.Single_Premium_Amount__c, p.Send_31_Day_Payment_Due_Reminder__c, p.Send_15_Day_Payment_Due_Reminder__c, p.Restrict_Billing_Code_Picklist__c, p.Restrict_Billing_Code_2__c, p.Relationship_to_Insured__c, p.Regular_Contribution__c, p.RecordTypeId, p.Previous_Premium_Mode__c, p.Previous_Policy_Status__c, p.Previous_Mode_Premium_Amount__c, p.Previous_Annual_Premium__c, p.Premium_Suspense__c, p.Premium_Reduction_Indicator__c, p.Premium_Due_Date__c, p.Premium_Deposit_Funds__c, p.Premium_Deposit_Funds_Interest__c, p.Premium_Deposit_Funds_Interest_Rate__c, p.Premium_Deposit_Fund_Bal_Formula__c, p.Premium_Deposit_Fund_Average_Balance__c, p.Policy_Status_Picklist_Hidden__c, p.Policy_Status_Hidden__c, p.Policy_Status_Display_Formula__c, p.Policy_Rejection_Code__c, p.Policy_Promotion_Code_Campaign_Code__c, p.Policy_Premium_Type_Code__c, p.Policy_Payor_Address_Id__c, p.Policy_Owner_Address_Id__c, p.Policy_Number__c, p.Policy_Issue_Date__c, p.Policy_Insurance_Type__c, p.Policy_Inforce_Date__c, p.Policy_Credit_Card_Status__c, p.Policy_Credit_Card_Reject_Reason__c, p.Policy_Credit_Card_Reject_Reason_Descrip__c, p.Policy_Country_Code__c, p.Policy_Comments_Remarks__c, p.Policy_Cease_Reason__c, p.Policy_Cease_Date__c, p.Plan_Code__c, p.Payor__c, p.Payment_Mode__c, p.Payment_Mode_Picklist__c, p.Payment_Method__c, p.Payment_Method_Picklist__c, p.Paid_to_Date__c, p.Paid_Up_Addition_Cash_Value__c, p.OwnerId, p.Outstanding_Other_Loan__c, p.Outstanding_Other_Loan_Interest__c, p.Outstanding_Disbursement_Amount__c, p.Outstanding_Cash_Loan__c, p.Outstanding_Cash_Loan_Interest__c, p.Outstanding_APL__c, p.Outstanding_APL_Interest__c, p.One_Year_Term_Purchased_with_Dividend__c, p.Next_Autopay_Date__c, p.Name, p.Miscellaneous_Suspense__c, p.Min_Requirement_on_Initial_Deposit__c, p.MayEdit, p.Maximum_Loan__c, p.Maximum_Deposit_Amount__c, p.Maturity_Dividend_Surrendered__c, p.Maturity_Dividend_Death_Claim__c, p.Maturity_Balance__c, p.Maturity_Balance_Indicator__c, p.Mandatory_Contribution__c, p.Mandatory_Contribution_End__c, p.Loan_Interest_Rate__c, p.Loan_Balance_Formulae__c, p.Life_to_date_premium__c, p.Last_Autopay_Date__c, p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, p.IsLocked, p.IsDeleted, p.Initial_Lump_Sum_Contribution__c, p.Initial_Deposit__c, p.Id, p.HKID_no__c, p.E_Statment_Indicator__c, p.E_Billing_Indicator__c, p.ECE_JCI_Balance__c, p.Dividend_Year__c, p.Dividend_Option__c, p.Dividend_Last_Declared__c, p.Dividend_Average_Balance__c, p.Debit_Date__c, p.Death_Benefit__c, p.Death_Benefit_Option__c, p.Death_Benefit_Option_Picklist__c, p.DDA_Limit__c, p.CurrencyIsoCode, p.Credit_Card_Number__c, p.Credit_Card_Number_Masked__c, p.Credit_Card_Last_Autopay_Date__c, p.Credit_Card_Holder_Name__c, p.CreatedDate, p.CreatedById, p.Coverage_Unit_Type_Code__c, p.Coupon_Fund__c, p.Coupon_Fund_Super_Dividend_Bal_Formula__c, p.Coupon_Fund_Interest__c, p.Coupon_Fund_Interest_Rate__c, p.Coupon_Fund_Average_Balance__c, p.Contractual_Payout_Method__c, p.Client__c, p.Cash_Value__c, p.Branch_Number__c, p.Billing_Method__c, p.Bank_Response_Status__c, p.Bank_Number__c, p.Bank_Name__c, p.Bank_Acount_Holder_Name__c, p.Autopay_Status__c, p.Autopay_Rejected_Fatal_Indicator__c, p.Autopay_Reject_Reason_Description__c, p.Application_Sign_Date__c, p.Application_Received_Date__c, p.Application_Form_ID__c, p.Annualized_Premium__c, p.Accumulated_Dividend__c, p.Accumulated_Dividend_Interest__c, p.Accumulated_Dividend_Interest_Rate__c, p.Account_Status__c, p.Account_Number__c, p.Account_Number_Masked__c, p.Absolute_Premium_Discount_Amount__c, p.AFYP__c, p.AFYP3__c, p.AFYP2__c From Policy__c p LIMIT 1];
		
		System.currentPageReference().getParameters().put('policyID', Policy[0].Policy_Number__c);

		PaymentHstyViewCtrl ctrlTest = new PaymentHstyViewCtrl();
    	
    	ctrlTest.getPHForDisplayfromMap();
    	ctrlTest.getPHForDisplay();
    	ctrlTest.gettempURLCancel();
    	ctrlTest.getPolicyName();
    	ctrlTest.Security();
    	
    	Double a = 10;
    	ctrlTest.AmountFormatter(a);
    	
	}
    	
	static testMethod void myUnitTest2() {
		PaymentHstyViewCtrl ctrlTest = new PaymentHstyViewCtrl();
	
	}

}