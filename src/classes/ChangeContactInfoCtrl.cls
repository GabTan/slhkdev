/*
Version        : 2.0 
Company        : Sun Life Financial 
Date           : 01.AUG.2009
Author         : Simon U. Esguerra
Description    : Change Contact Information Controller
History        : 
                 version 1.1 - JC NACHOR  	 09/09/09 - in Reference Number Creation, truncate Policy Number to 10 characters if its length is > 10
                                                   	  - used HK format in displaying Date Time fields
                 version 1.2 - JC NACHOR   	 09/17/09 - trimmed spaces in address fields
                                                      - replaced '/' by ' ' in displaying address    
                                                      - trimmed spaces in address fields                                               
                 version 1.3 - JC NACHOR 	 09/29/09 - when Mobile Number is null, set oldMobileNumber to '' instead of setting oldHomeNumber to ''
                 version 1.4 - JC NACHOR 	 09/30/09 - removed adding of custom label for ERROR and WARNING for error messages
                 version 1.5 - JC NACHOR 	 10/02/09 - check all the checkboxes when the change is only for phone and email (IL-ES-00034 Issue)
                 version 1.6 - JC NACHOR 	 10/05/09 - changed '<deleted>' string to a custom label
                             - JC NACHOR  	 10/12/09 - used userPoliciesFirstCov instead of userPolicices
                             - JC NACHOR 	 10/13/09 - rolled back "used userPoliciesFirstCov instead of userPolicices"
                 version 1.7 - JC NACHOR 	 10/15/09 - added client id in URL checking
                 version 1.8 - JC NACHOR     10/20/09 - moved the security checking to be performed in security method insted of retirevePolicies method to fix error in Test Method
                                                      - added client side validation because salesforce automatically escapes script tags that may lead to invalid values.
				 version 1.9 - Robby Angeles 10/26/09 - Robby Angeles - Updated Security method. Checking for the user profile and bypasses the method if user is not a customer portal type.
				 version 2.0 - Wilson Cheng  11/20/09 - Checks the global settings to determine if the check for SU logins is required.
				 version 2.1 - JC NACHOR    12/03/2009 -checked the current language of the user before importing the language (Added intLanguageFlag and getIntLanguageFlag() to pass the data to the page)
				 version 2.2 - JC NACHOR   12/08/09 - retrieve Date Time on Submit to be dispalyed on Confirmation page
				             - JC NACHOR 12/11/2009 - used userPoliciesWithId instead of userPolicies 
				 version 2.3 - FER ATAS  22/05/2012 - ensure that NullPointerException will not be encountered in case clientId is NULL (added clientId !=null filter)
 			
*/
public with sharing class ChangeContactInfoCtrl {
	public List<customPolicyObject> MyPolicies{get;set;}
	public String transactId = '';
	public String ClientId;
	public String clientFirstName {get; set;}
	public String clientLastName {get; set;}
	public String URLNav  {get; set;}
	public Boolean validated {get; set;}
	public String mobileNumber  {get; set;}
	public String homeNumber {get; set;}
	public String officeNumber  {get; set;}
	public String emailAddress {get; set;}
	public String oldMobileNumber  {get; set;}
	public String oldHomeNumber {get; set;}
	public String oldOfficeNumber  {get; set;}
	public String oldEmailAddress {get; set;}
 
	// JC NACHOR 09/17/09 - trim address fields
	//public String address1 {get; set;}
  //public String address2 {get; set;} 
  //public String address3 {get; set;} 
  //public String address4 {get; set;} 
  //public String address5 {get; set;}
  //public String address6 {get; set;}
  //public String address7 {get; set;} 
  //public String address8 {get; set;}	
	public String address1;
 	public String address2; 
 	public String address3; 
 	public String address4; 
 	public String address5;  	
 	public String address6;
 	public String address7; 
 	public String address8;
 	public String country {get; set;}
 	public String province {get; set;}
 	public String countryTemp {get; set;}
 	public String provinceTemp {get; set;}  
 	
 	public String DateNow {get;set;}
 	public Boolean renderAllSelected {get;set;}
 	public Boolean renderWarning {get;set;}
 	public Boolean renderError {get;set;}
 	public Boolean renderNoChange {get;set;}
 	
 	// JC NACHOR 12/08/09 - retrieve Date Time on Submit to be dispalyed on Confirmation page
 	public String submitDateTime; 	
 	
 	// JC NACHOR 12/03/09 - added flag to determine language to be used in Hover page
 	public Integer intLanguageFlag;
 	public final Integer ENGLISH_FLG = 0;
 	public final Integer CHINESE_FLG = 1;
 	public final String ENGLISH_CODE = 'en_US';
 	public final String CHINESE_CODE = 'zh_TW';
  public Integer getIntLanguageFlag(){  	
  	intLanguageFlag = ENGLISH_FLG;  	
  	if(CHINESE_CODE == UserInfo.getLanguage()) {
  		intLanguageFlag = CHINESE_FLG;
  	}   	
    return intLanguageFlag;
  }   
  public void setIntLanguageFlag(Integer anIntLanguageFlag){
    intLanguageFlag = anIntLanguageFlag;
  }  	
 	 
 	// JC NACHOR 12/08/09 - retrieve Date Time on Submit to be dispalyed on Confirmation page 
  public String getSubmitDateTime(){
      return System.now().format('dd/MM/yyyy HH:mm', 'Asia/Hong_Kong');    
  } 	 
  public void setSubmitDateTime(String aString) {
  	  submitDateTime = aString;
  }
 	
 	// JC NACHOR 09/17/09 - trim address fields
 	public String getAddress1(){
 		return address1;
 	} 	
 	public void setAddress1(String anAddress){
    address1 = anAddress.trim();
  }  
  public String getAddress2(){
    return address2;
  }  
  public void setAddress2(String anAddress){
    address2 = anAddress.trim();
  }  
  public String getAddress3(){
    return address3;
  }   
  public void setAddress3(String anAddress){
    address3 = anAddress.trim();
  }  
  public String getAddress4(){
    return address4;
  }  
  public void setAddress4(String anAddress){
    address4 = anAddress.trim();
  }    
  public String getAddress5(){
    return address5;
  }     
  public void setAddress5(String anAddress){
    address5 = anAddress.trim();
  }  
  public String getAddress6(){
    return address6;
  }  
  public void setAddress6(String anAddress){
    address6 = anAddress.trim();
  }  
  public String getAddress7(){
    return address7;
  }   
  public void setAddress7(String anAddress){
    address7 = anAddress.trim();
  }  
  public String getAddress8(){
    return address8;
  }  
  public void setAddress8(String anAddress){
    address8 = anAddress.trim();
  }      
	
	public List<Address__c> policyAddressList = new List<Address__c>();
	public List<customPolicyObject> listCPO = new List<customPolicyObject>();
	public Account myAccount;
	public boolean polIsChecked = false;
	public Config_Utilities CU = new Config_Utilities();
	
	public String   tempURLAcknowledgement;
	public String   tempURLCancel;
	
	public Boolean renderCPErrorExistRequest   {get;set;}
	public Boolean renderErrorCastIron  	 {get;set;}
	public Boolean preconfirmError 	 		 {get;set;}
	
	//START Wcheng enhancement 11/20/09
    public Boolean checkForSU {get; set;}
    //END Wcheng enhancement 11/20/09
	
	public ChangeContactInfoCtrl(ApexPages.StandardController stdController){

		DateTime myDate = DateTime.now();
		// JC NACHOR 09/09/09 - used HK format
		//DateNow = myDate.format('dd/MM/yyyy H:mm');
		DateNow = myDate.format('dd/MM/yyyy HH:mm', 'Asia/Hong_Kong');
		List<Account> ListAccount = new List<Account>();
		
		List<Transaction__c> ListTransaction = new List<Transaction__c>();
		Contact tempC = new Contact();
		try{
			if (ApexPages.currentPage().getParameters().get('ClientId') != null){
				ClientId = ApexPages.currentPage().getParameters().get('ClientId');
				
					ListAccount = [Select Phone, PersonMobilePhone, PersonHomePhone, PersonEmail, LastName, Id, 
									FirstName, Client_Id__c From Account a where Client_Id__c = :ClientId  limit 1];
					
					if(ListAccount.size()>0){
						myAccount = ListAccount[0];
						
						clientFirstName = myAccount.FirstName;
						clientLastName = myAccount.LastName;
						
						if(myAccount.PersonHomePhone!=null){
							homeNumber = myAccount.PersonHomePhone;
							oldHomeNumber = myAccount.PersonHomePhone;
							if(oldHomeNumber==null){
								oldHomeNumber='';
							}
						}
						if(myAccount.PersonMobilePhone!=null){
							mobileNumber = myAccount.PersonMobilePhone;
							oldMobileNumber = myAccount.PersonMobilePhone;
							//if(oldMobileNumber==null){
							//	oldMobileNumber='';
							//}
						}
						if(myAccount.Phone!=null){
							officeNumber = myAccount.Phone;
							oldOfficeNumber = myAccount.Phone;
							if(oldOfficeNumber==null){
								oldOfficeNumber='';
							}
						}
						if(myAccount.PersonEmail!=null){
							emailAddress = myAccount.PersonEmail;
							oldEmailAddress = myAccount.PersonEmail;
							
						}
					}
			}
		}catch (exception e){
			
		}		
	}
	public ChangeContactInfoCtrl(){
	}
	
	
	public PageReference security() {
		
	  // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
    PageReference pRefRedirect     = new Pagereference('/apex/NoAccessToPolicy?type=client');
    PageReference pProfileNoAccess = Page.UserAccessError;
    pRefRedirect.setRedirect(true);
    pProfileNoAccess.setRedirect(true);
    //R Angeles - 10262009 - Added Transaction page security check on user profile if user is none customer portal
  
    Config_Global_Settings__c globalConfig = [Select SLF_Portal_Profile_Names__c, Check_For_SU_Logins__c From Config_Global_Settings__c Limit 1];
    Boolean isPortalUser 				   = CU.isPortalUser(globalConfig.SLF_Portal_Profile_Names__c);
    
    //START Wcheng enhancement 11/20/09
    if (globalConfig != null) {
      	checkForSU = globalConfig.Check_For_SU_Logins__c;
    } else {
      	checkForSU = true;
    }
    //END Wcheng enhancement 11/20/09
    
    if(isPortalUser){
	    String clientId = ApexPages.currentPage().getParameters().get('ClientId');            
	  
	    // START JC NACHOR - add parameter filtering to prevent cross site scripting
	    // 22 MAY 2012 (FATAS) - added the initial checking (clientId !=null) which will 
	    //	ensure that NullPointerException will not be encountered in case clientID is NULL
	    // System.debug(clientId);
	    if(clientId !=null && clientId.length() > 15){
	      return pRefRedirect;
	    }   
	  
	    if(!ESAccessVerifier.isCurrentUsersClient((clientId==null)?'':clientId)){	    		    
	      return pRefRedirect;	      
	    } 	    		    	
	    
	    return retrieveMyPolicies();	    		    
    }
    else{
  	    return pProfileNoAccess;
    }
  //END R Angeles 10262009 
}
	
public PageReference retrieveMyPolicies() {
		
        // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
//        PageReference pRefRedirect = new Pagereference('/apex/NoAccessToPolicy?type=client');
//        pRefRedirect.setRedirect(true);
//        String clientId = ApexPages.currentPage().getParameters().get('ClientId');            
      
//        if(!ESAccessVerifier.isCurrentUsersClient((clientId==null)?'':clientId)){
//          return pRefRedirect;
//        }	
		
		try{
			MyPolicies = getUserPolicies();
		}catch (exception e){
		
		}
		return null;
	}

	public List<customPolicyObject> getUserPolicies(){
		try{
			// JC NACHOR 10/12/2009 - used userPoliciesFirstCov instead of userPolicies
			// rollback
			// JC NACHOR 12/11/2009 - used userPoliciesWithId instead of userPolicies
			//listCPO = CU.userPolicies();
			listCPO = CU.userPoliciesWithId();
			//listCPO = CU.userPoliciesFirstCov();
			for(customPolicyObject CPO:listCPO){
				if(CPO.boolIsChecked == false){
					CPO.boolIsChecked = true;
				}
			}
			
		}catch(exception e){
			
		}
		system.debug('listCPO '+listCPO);
		return listCPO;
	}
	
	public String checkForChanges(){
		Integer polCount= 0;
		
		if(validatePage()==true){
			for(customPolicyObject tempCPO:listCPO){
				if(tempCPO.boolIsChecked == true){
					polCount++;
				}
			}
			if(listCPO.size()>polCount){
				renderAllSelected = false;
			}else{
				renderAllSelected = true;
			}
			if(getDisplayAddrOne()==''&& getDisplayAddrTwo()==''&&getDisplayAddrThree()==''&&getDisplayAddrFour()==''){
				renderNoChange = true;
			}else{
				renderNoChange = false;
			}	
			
			validated = true;
			showWarnings();
			return null;
		}
		validated = false;
		showWarnings();
		return null;
	}

	public Boolean validatePage(){
		polIsChecked = false;
		ApexPages.Message MsgErr = new ApexPages.Message(ApexPages.severity.ERROR ,System.Label.CPP_ERROR);
		ApexPages.Message MsgSpace = new ApexPages.Message(ApexPages.severity.ERROR ,' ');
		ApexPages.Message Msg3;
		ApexPages.Message Msg4;
		ApexPages.Message Msg5;
		ApexPages.Message Msg6;
		
		ApexPages.Message invalidValueHomeMsg;
		ApexPages.Message invalidValueOfficeMsg;
		ApexPages.Message invalidValueMobileMsg;
		ApexPages.Message invalidValueAdd1Msg;
		ApexPages.Message invalidValueAdd2Msg;
		ApexPages.Message invalidValueAdd3Msg;
		ApexPages.Message invalidValueAdd4Msg;
		ApexPages.Message invalidValueAdd5Msg;
		ApexPages.Message invalidValueAdd6Msg;
		
		if(oldOfficeNumber==null){
			oldOfficeNumber='';
		}
		if(oldHomeNumber==null){
			oldHomeNumber='';
		}
		if(oldMobileNumber==null){
			// JC NACHOR 09/29/09 - set oldMobileNumber to '' instead of setting oldHomeNumber to ''
			//oldHomeNumber='';
			oldMobileNumber='';
		}

		if(homeNumber==''&& officeNumber=='' && mobileNumber==''){
			Msg3 = new ApexPages.Message(ApexPages.severity.ERROR ,System.Label.CPP_ERR_003);
			//apexPages.addMessage(MsgErr);
			ApexPages.addMessage(Msg3);
			ApexPages.addMessage(MsgSpace);		
			return false;
			
		}
		
		for(CustomPolicyObject listCbo:listCPO){
			system.debug('listCbo.boolIsChecked '+listCbo.boolIsChecked);
			if(listCbo.boolIsChecked == true){
				 polIsChecked=true;
				 break;
			}else{
				polIsChecked = false;
			}
		}
		system.debug('polIsChecked '+polIsChecked);
		
		if(homeNumber==oldHomeNumber && mobileNumber==oldMobileNumber && officeNumber==oldOfficeNumber && emailAddress==oldEmailAddress ){
			if(address1!='' && address6!=''){
				if(polIsChecked == false ){
					Msg4 = new ApexPages.Message(ApexPages.severity.ERROR ,System.Label.CPP_ERR_010);
					//apexPages.addMessage(MsgErr);
					apexPages.addMessage(Msg4);
					ApexPages.addMessage(MsgSpace);	
					return false;
				}
			}
		}
		if(homeNumber==oldHomeNumber && mobileNumber==oldMobileNumber && officeNumber==oldOfficeNumber && emailAddress==oldEmailAddress ){
			if(address1=='' && address6==''){
				Msg5 = new ApexPages.Message(ApexPages.severity.ERROR ,System.Label.CPP_ERR_005);
				//apexPages.addMessage(MsgErr);
				apexPages.addMessage(Msg5);
				ApexPages.addMessage(MsgSpace);	
				return false;
			}
		}
		
		// JC NACHOR 10/20/09 - check if values returned to the server side is indeed valid
		boolean isValidValues = true;
    homeNumber = truncateBasedOnMaxLength(homeNumber, 50);
    officeNumber = truncateBasedOnMaxLength(officeNumber, 50);
    mobileNumber = truncateBasedOnMaxLength(mobileNumber, 50);
    address1 = truncateBasedOnMaxLength(address1, 25);
    address2 = truncateBasedOnMaxLength(address2, 25);
    address3 = truncateBasedOnMaxLength(address3, 25);
    address4 = truncateBasedOnMaxLength(address4, 25);
    address5 = truncateBasedOnMaxLength(address5, 25);
    address6 = truncateBasedOnMaxLength(address6, 25);
    
    if(!validatePhone(homeNumber)){
    	invalidValueHomeMsg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_013);
      apexPages.addMessage(invalidValueHomeMsg);
      isValidValues = false;
    } 
    if(!validatePhone(officeNumber)){
      invalidValueOfficeMsg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_014);
      apexPages.addMessage(invalidValueOfficeMsg);
      isValidValues = false;
    } 
    if(!validatePhone(mobileNumber)){
      invalidValueMobileMsg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_015);
      apexPages.addMessage(invalidValueMobileMsg);
      isValidValues = false;
    }    
    if(!validateAddress(address1)){
      invalidValueAdd1Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_016);
      apexPages.addMessage(invalidValueAdd1Msg);
      isValidValues = false;
    }        
    if(!validateAddress(address2)){
      invalidValueAdd2Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_017);
      apexPages.addMessage(invalidValueAdd2Msg);
      isValidValues = false;
    }   
    if(!validateAddress(address3)){
      invalidValueAdd3Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_018);
      apexPages.addMessage(invalidValueAdd3Msg);
      isValidValues = false;
    }   
    if(!validateAddress(address4)){
      invalidValueAdd4Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_019);
      apexPages.addMessage(invalidValueAdd4Msg);
      isValidValues = false;
    }   
    if(!validateAddress(address5)){
      invalidValueAdd5Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_020);
      apexPages.addMessage(invalidValueAdd5Msg);
      isValidValues = false;
    }   
    if(!validateAddress(address6)){
      invalidValueAdd6Msg  = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_021);
      apexPages.addMessage(invalidValueAdd6Msg);
      isValidValues = false;
    }                       
    if(!isValidValues) {
    	ApexPages.addMessage(MsgSpace);
    	return false;
    }
     		
		
		if(homeNumber!=oldHomeNumber || mobileNumber!=oldMobileNumber || officeNumber!=oldOfficeNumber || emailAddress!=oldEmailAddress){
			if(address1=='' && address6==''){
				if(polIsChecked!=true){
					//listCPO[0].boolIsChecked = true;
					countryTemp = country;
					provinceTemp = province;
					//return true;
				}else{
          //ApexPages.Message MsgPhone = new ApexPages.Message(ApexPages.severity.ERROR ,System.Label.CPP_WARN_001);
          //apexPages.addMessage(MsgPhone);
          //ApexPages.addMessage(MsgSpace);
          //return false;
					countryTemp = country;
					provinceTemp = province;
					//return true;
				}
				// JC NACHOR 10/02/09 - check all the checkboxes 
				for(integer i=0; i<listCPO.size(); i++){
					listCPO[i].boolIsChecked = true;
				}
				return true;
			}else if(address1!='' && address6!=''){
				if(polIsChecked!=true){
					Msg6 = new ApexPages.Message(ApexPages.severity.ERROR, System.Label.CPP_ERR_010);
					//apexPages.addMessage(MsgErr);
					apexPages.addMessage(Msg6);
					ApexPages.addMessage(MsgSpace);	
					return false;
				}

			}
		}  
		
		if(Msg3==null && Msg4==null && Msg5==null){
			//Validation passed
			countryTemp = country;
			provinceTemp = province;
			return true;
		}
		//Validation failed
		return false;
		
	}
		
  private String truncateBasedOnMaxLength(String aString, integer aLength){
    if(aString != null && aString.length() > aLength) {
        aString = aString.substring(0, aLength);
    }  
    return aString;      
  }	
  
  private boolean validatePhone(String aString){
  	boolean isValid = true;
    
    if(aString != null){
      isValid = !(Pattern.matches('.*[;\\s\\\\|\\*@!:\\u0100-\\uffff]+.*', aString));
    }  
    
    return isValid;
  }
  private boolean validateAddress(String aString){
    boolean isValid = true;
    
    if(aString != null){
      isValid = !(Pattern.matches('.*[;\\|\\*@!:\\u0100-\\uffff]+.*', aString));    
    }
    
    return isValid;
  }  
	
	public void showWarnings(){
		ApexPages.Message MsgWrng = new ApexPages.Message(ApexPages.severity.WARNING ,System.Label.CPP_WARN);
		ApexPages.Message MsgSpace = new ApexPages.Message(ApexPages.severity.WARNING ,' ');
		ApexPages.Message Msg;
		ApexPages.Message Msg1;
		ApexPages.Message Msg2;	
		
		String homeNo = getChangedHomeNo();
		String officeNo = getChangedOfficeNo();
		String mobileNo = getChangedMobileNo();
		
		System.debug('homeNo '+homeNo);
		System.debug('officeNo '+officeNo);	
		System.debug('mobileNo '+mobileNo);	
		
		// JC NACHOR 10/05/2009 - changed '<deleted>' string to a custom label
		//if(homeNo=='<deleted>' || officeNo=='<deleted>' || mobileNo=='<deleted>' ){
		if(homeNo==System.Label.CPP_LBL_DELETED || officeNo==System.Label.CPP_LBL_DELETED || mobileNo==System.Label.CPP_LBL_DELETED ){
			//ApexPages.addMessage(MsgWrng);
					
			//if(homeNo=='<deleted>'){
			if(homeNo==System.Label.CPP_LBL_DELETED){
				Msg = new ApexPages.Message(ApexPages.severity.WARNING ,System.Label.CPP_WARN_001);
				ApexPages.addMessage(Msg);
			}
			//if(officeNo=='<deleted>'){
			if(officeNo==System.Label.CPP_LBL_DELETED){
				 Msg1 = new ApexPages.Message(ApexPages.severity.WARNING ,System.Label.CPP_WARN_002);
				ApexPages.addMessage(Msg1);
			}
			//if( mobileNo=='<deleted>'){
			if( mobileNo==System.Label.CPP_LBL_DELETED){
				Msg2 = new ApexPages.Message(ApexPages.severity.WARNING ,System.Label.CPP_WARN_003);
				ApexPages.addMessage(Msg2);
			}
			ApexPages.addMessage(MsgSpace);
		}
	}
	public PageReference resetValues(){
		//try{
			PageReference pgRef = new PageReference(esPortalNavigator.getCustomerPortalNavURL(esPortalNavigator.NAV_SVC_CPP_INPUT)+'?clientId='+ClientId);
			pgRef.setRedirect(true);
			return pgRef;
			

		//}catch(exception e){
			
		//}

	}
	public void AcknowledgeChanges(){
		String PolicyNum;
		String PolicyId;
		//system.debug('xxxxxxxxxxxxxxxxxxxxxxx '+validatePage());
		//if(validatePage()==true){
		//	validated = 'true';
			//wilson's method
		if(!preConfirmValidation()){
			system.debug('AcknowledgeChanges');	
			ChangeInfoRequest putXMLData = new ChangeInfoRequest();
			
			putXMLData.RequestType = 'S0001';
			
			Boolean isChecked=false;
			Integer polCtr=0;
			for(CustomPolicyObject pol:MyPolicies){
				if(pol.boolIsChecked == true && isChecked !=true){
					isChecked = true;
					// JC NACHOR 09/09/09 - truncate Policy Number to 10 characters if its length is > 10
					// if(pol.strPolicyNumber.length() > 8){
					//   PolicyNum = pol.strPolicyNumber.substring(0,8);
					if(pol.strPolicyNumber.length() > 10){
						PolicyNum = pol.strPolicyNumber.substring(0,10);
					}else{
						PolicyNum = pol.strPolicyNumber;
					}
					PolicyId = pol.policyId;	
				}
				if(pol.boolIsChecked == true){
					putXMLData.Policies.add(pol.strPolicyNumber);
					polCtr++;
				}
			}
			if(MyPolicies.size()==polCtr){
				putXMLData.Policies.clear();
				putXMLData.Policies.add('X');
			}
			
			String dateToday = datetime.now().format('yyMMdd');
			
			putXMLData.ReferenceNo = 'ECP-'+PolicyNum+'-'+dateToday;
			putXMLData.ReferenceNo = String.valueOf(putXMLData.ReferenceNo).toUpperCase();
			putXMLData.PolicyNo = PolicyNum;
			// JC NACHOR 09/09/09 - used HK format
			//putXMLData.SubmissionDateTime = datetime.now().format('dd/MM/yyyy kk:mm:ss');
			putXMLData.SubmissionDateTime = datetime.now().format('dd/MM/yyyy HH:mm', 'Asia/Hong_Kong');			
			
			//Home Phone no change phone has existing
			if(homeNumber==oldHomeNumber && oldHomeNumber!=''){
				putXMLData.HomePhone = homeNumber;
				putXMLData.homePhoneChange = 'true';
			
			//Home phone no change phone does not exist 	
			}else if(homeNumber=='' && (oldHomeNumber==''|| oldHomeNumber==null)){
				putXMLData.HomePhone = '';
				putXMLData.homePhoneChange = 'true';
			
			//Home phone changed old exists
			}else if(homeNumber!=oldHomeNumber && oldHomeNumber!='' && homeNumber!=''){
				putXMLData.HomePhone = homeNumber;
				putXMLData.homePhoneChange = 'false';
			
			//Home phone changed old number blank
			}else if(homeNumber!='' && oldHomeNumber==''){
				putXMLData.HomePhone = homeNumber;
				putXMLData.homePhoneChange = 'false';
			
			//Home phone deleted
			}else if(homeNumber=='' && oldHomeNumber!=''){
				putXMLData.HomePhone = '*';
				putXMLData.homePhoneChange = 'false';
			}
			
			//Home Phone no change phone has existing
			if(officeNumber==oldOfficeNumber && oldOfficeNumber!=''){
				putXMLData.OfficePhone = OfficeNumber;
				putXMLData.OfficePhoneChange = 'true';
			
			//Home phone no change phone does not exist 	
			}else if(officeNumber=='' && (oldOfficeNumber==''|| oldOfficeNumber==null)){
				putXMLData.OfficePhone = '';
				putXMLData.OfficePhoneChange = 'true';
			
			//Home phone changed old exists
			}else if(officeNumber!=oldOfficeNumber && oldOfficeNumber!='' && officeNumber!=''){
				putXMLData.officePhone = officeNumber;
				putXMLData.officePhoneChange = 'false';
			
			//Home phone changed old number blank
			}else if(officeNumber!='' && oldOfficeNumber==''){
				putXMLData.OfficePhone = OfficeNumber;
				putXMLData.officePhoneChange = 'false';
			
			//Home phone deleted
			}else if(OfficeNumber=='' && oldOfficeNumber!=''){
				putXMLData.OfficePhone = '*';
				putXMLData.OfficePhoneChange = 'false';
			}
			//Home Phone no change phone has existing
			if(mobileNumber==oldMobileNumber && oldMobileNumber!=''){
				putXMLData.MobilePhone = MobileNumber;
				putXMLData.MobilePhoneChange = 'true';
			
			//Home phone no change phone does not exist 	
			}else if(mobileNumber=='' && (oldMobileNumber==''|| oldMobileNumber==null)){
				putXMLData.MobilePhone = '';
				putXMLData.MobilePhoneChange = 'true';
			
			//Home phone changed old exists
			}else if(mobileNumber!=oldMobileNumber && oldMobileNumber!='' && mobileNumber!=''){
				putXMLData.MobilePhone = mobileNumber;
				putXMLData.MobilePhoneChange = 'false';
			
			//Home phone changed old number blank
			}else if(mobileNumber!='' && oldMobileNumber==''){
				putXMLData.MobilePhone = mobileNumber;
				putXMLData.mobilePhoneChange = 'false';
			
			//Home phone deleted
			}else if(mobileNumber=='' && oldMobileNumber!=''){
				putXMLData.MobilePhone = '*';
				putXMLData.MobilePhoneChange = 'false';
			}
			
			if(emailAddress!=oldEmailAddress){
				putXMLData.EmailAddValue = emailAddress;
				putXMLData.EmailAddChange = 'false';
			}else{
				putXMLData.EmailAddValue = emailAddress;
				putXMLData.EmailAddChange = 'true';
			}
			if(renderNoChange != true){
				putXMLData.AddressChange = 'false';
				putXMLData.AddressLine1 = address1;
				putXMLData.AddressLine2 = address2;
				putXMLData.AddressLine3 = address3;
				putXMLData.AddressLine4 = address4;
				putXMLData.AddressLine5 = address5;
				
				putXMLData.AddressCity = address6;
				putXMLData.AddressZip = address7;
				
				if(address1!=''){
					putXMLData.AddressCountry = countryTemp;
					putXMLData.AddressProv = provinceTemp;
				}
			}else{
				putXMLData.AddressChange = 'true';
			}
			ChangeInfoXMLGen XMLWrite = new ChangeInfoXMLGen();
			
			String writeTxn = XMLWrite.writeCPPXML(putXMLData);
			
			
			Transaction__c txnRecord = new Transaction__c(TxnType__c = 'CP', TxnData__c = writeTxn, 
										Source__c='SF',SORStatus__c = 'Submitted', Request_Type__c='S0001',
										Reference_Number__c = putXMLData.ReferenceNo, Client_Id__c = ClientId, 
										Client__c = myAccount.Id, Policy_Id__c = PolicyNum, policy__c = PolicyId, 
										CommStatus__c = 'Open');
			try{
				insert txnRecord;
			}catch (exception e){
				
			}
			
			if(txnRecord!=null){
				system.debug('txnRecord '+txnRecord);
				transactId = txnRecord.Id;
				system.debug('transactId '+transactId);
			}
			System.debug('TransactionDebugg ' + txnRecord + transactId);
		}
	}
	public pageReference preAckConfirm(){
		return null;
	}
	public string getmyURL(){
		return URLNav;
	}
	
	public string getclientName(){
		String clientName = CU.formatName(clientFirstName, clientLastName);
		return clientName;
	}
	
	public string getChangedHomeNo(){
		String HomeNo='';
		system.debug('homeNumber '+homeNumber);
		system.debug('oldHomeNumber '+oldHomeNumber);
		if(homeNumber==oldHomeNumber && oldHomeNumber!=''){
			system.debug('1');
			HomeNo = homeNumber;
			
		//Home phone no change phone does not exist 	
		}else if(homeNumber=='' && (oldHomeNumber==''|| oldHomeNumber==null)){
			system.debug('2');
			HomeNo='';
		
		//Home phone changed old exists
		}else if(homeNumber!=oldHomeNumber && oldHomeNumber!='' && homeNumber!=''){
			system.debug('3');
			HomeNo= homeNumber;
		
		//Home phone changed old number blank
		}else if(homeNumber!='' && (oldHomeNumber==''|| oldHomeNumber==null)){
			system.debug('4');
			HomeNo = homeNumber;
		
		//Home phone deleted
		}else if(homeNumber=='' && oldHomeNumber!=''){
			system.debug('5');
			// JC NACHOR 10/05/2009 - changed '<deleted>' string to a custom label
			//HomeNo = '<deleted>';
			HomeNo = System.Label.CPP_LBL_DELETED;
		}
		system.debug('6');
		return HomeNo;
	}
	
	public string getChangedOfficeNo(){
		String OfficeNo='';
		if(officeNumber==oldOfficeNumber && oldOfficeNumber!=''){
			OfficeNo = officeNumber;
			
		//Home phone no change phone does not exist 	
		}else if(officeNumber=='' && (oldOfficeNumber=='' || oldOfficeNumber==null)){
			OfficeNo='';
		
		//Home phone changed old exists
		}else if(officeNumber!=oldOfficeNumber && oldOfficeNumber!='' && officeNumber!=''){
			OfficeNo= officeNumber;
		
		//Home phone changed old number blank
		}else if(officeNumber!='' && (oldOfficeNumber=='' || oldOfficeNumber==null)){
			OfficeNo = officeNumber;
		
		//Home phone deleted
		}else if(officeNumber=='' && oldOfficeNumber!=''){
			// JC NACHOR 10/05/2009 - changed '<deleted>' string to a custom label
			//OfficeNo = '<deleted>';
			OfficeNo = System.Label.CPP_LBL_DELETED;
		}
		return OfficeNo;
	}
	public string getChangedMobileNo(){
		system.debug('mobileNumber '+mobileNumber);
		system.debug('oldMobileNumber '+oldMobileNumber);
		String MobileNo='';
		if(mobileNumber==oldMobileNumber && oldMobileNumber!=''){
			system.debug('1');
			mobileNo = mobileNumber;
			
		//Home phone no change phone does not exist 	
		}else if(mobileNumber=='' && (oldMobileNumber==''|| oldMobileNumber==null)){
			system.debug('2');
			MobileNo='';
		
		//Home phone changed old exists
		}else if(mobileNumber!=oldMobileNumber && oldMobileNumber!='' && mobileNumber!=''){
			system.debug('3');
			MobileNo= mobileNumber;
		
		//Home phone changed old number blank
		}else if(mobileNumber!='' && (oldMobileNumber==''|| oldMobileNumber==null)){
			system.debug('4');
			MobileNo = mobileNumber;
		
		//Home phone deleted
		}else if(mobileNumber=='' && oldMobileNumber!=''){
			system.debug('5');
			// JC NACHOR 10/05/2009 - changed '<deleted>' string to a custom label
			//MobileNo = '<deleted>';
			MobileNo = System.Label.CPP_LBL_DELETED;
		}
		system.debug('6');
		return MobileNo;
	}
	Public String getChangedEmailAddress(){
		String eAddr='';
		if(emailAddress!=oldEmailAddress){
			eAddr=emailAddress;
		}else{
			eAddr=oldEmailAddress;
		}	
		return eAddr;
	}
	Public List<CustomPolicyObject> getCheckedPolicies(){
		List<CustomPolicyObject> checkedCPOList = new List<CustomPolicyObject>();
		Integer PolCount = 0;
		if(listCPO.size()>0){
			for(CustomPolicyObject checkedCPO:listCPO){
				if(checkedCPO.boolIsChecked ==true){
					checkedCPOList.add(checkedCPO);
				}
			}
		}
		return checkedCPOList;
	}
	
	Public String getDisplayAddrOne(){
		String AddressOne ='';
		if(Address1!='' && Address2 != ''){
			// JC NACHOR 09/18/2009 - changed '/' to ' '
			//AddressOne = Address1+'/'+Address2; 
			AddressOne = Address1+' '+Address2;
		}else if(Address1!='' && Address2 == ''){
			AddressOne = Address1;
		}
		if(AddressOne!=''){
			return String.valueOf(AddressOne).toUpperCase();
		}
		return AddressOne;
	}
	Public String getDisplayAddrTwo(){
		String AddressTwo ='';
		if(Address3!='' && Address4 != '' && Address5!=''){
			AddressTwo = Address3+' '+Address4+' '+Address5;
		}else if (Address3!='' && Address4 != '' && Address5==''){
			AddressTwo = Address3+' '+Address4;
		}else if (Address3!='' && Address4 == '' && Address5==''){
			AddressTwo = Address3;
		}else if (Address3=='' && Address4 != '' && Address5==''){
			AddressTwo = Address4;
		}else if (Address3=='' && Address4 == '' && Address5!=''){
			AddressTwo = Address5;
		}else if (Address3!='' && Address4 == '' && Address5!=''){
			AddressTwo = Address3+' '+Address5;
		}else if (Address3=='' && Address4 != '' && Address5!=''){
			AddressTwo = Address4+' '+Address5;
		}
		if(AddressTwo!=''){
			return String.valueOf(AddressTwo).toUpperCase();
		}
		return AddressTwo;
	}
	Public  String getDisplayAddrThree(){
		String AddressThree='';
		System.debug('address6 '+address6);
		if(address6!=''&& address6!=null){
			System.debug('address6 '+address6);
			AddressThree = address6.toUpperCase();
		}
		return AddressThree;
	}
	Public  String getDisplayAddrFour(){
		String AddressFour = '';
		if(address7!=''){
			AddressFour = address7;
		}
		if(address1!=''){
			if(province!=''){
				
				AddressFour +=' '+province+', ';
			}
			if(country!=''){
				
				AddressFour +=country;
			}
		}
		system.debug('province '+province);
		system.debug('country '+country);
		if(AddressFour!=''){
			return String.valueOf(AddressFour).toUpperCase();
		}
		return AddressFour;
	}
	public String gettransactId(){
		return transactId;
	}

	
	public String gettempURLAcknowledgement(){
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_FINAL);
	}
	public String gettempURLCancel(){
		return ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_MAIN);
	}
	

	public Boolean preConfirmValidation(){
		ChangeInfoPreValidateCtrl validator = new ChangeInfoPreValidateCtrl();
		
		preconfirmError = false;
		renderErrorCastIron  = validator.renderErrorCastIron;
		renderCPErrorExistRequest = validator.renderCPErrorExistRequest;

		if(renderErrorCastIron || renderCPErrorExistRequest){
			preconfirmError = true;
		}
		system.debug('preconfirmError '+preconfirmError);
		return preconfirmError;
	}

}