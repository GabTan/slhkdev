@isTest 
private class testForgotPassController {

    static testMethod void testForgotPasswordController() 
	{
		User[] usrIsActive = [select id, username, Client_Id__c 
								from User 
								where isActive = true 
								and isPortalEnabled = true 
								and Client_Id__c != null
								limit 1]; 
														  
		if(usrIsActive.size()>0)
		{
			
				User_Extension__c ue = new User_Extension__c();
				try{ue = [select id, User__c from User_Extension__c where User__c = :usrIsActive[0].id];}
				catch(Exception e){System.debug(e);}
				
				if(ue != null)
				{		
					ue = new User_Extension__c(User__c = usrIsActive[0].id, Reset__c = false, 
													User_Deactivated__c = false);
					insert ue;
				}
		       ForgotPasswordController fpCon = new ForgotPasswordController();
		       ForgotPasswordController.generateRandom();
		       ForgotPasswordController.generateSecurityQuestion();
		       
		       // Variation 1
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = false;
		       fpCon.bOverride4 = false;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}		
		       // Variation 2
		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = false;
		       
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
		       // Variation 3
		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = true;
		       
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
		       // Variation 4
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = false;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
		       // Variation 5
		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}
		       // Variation 6
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}
		       // Variation 7
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}			   
		       // Variation 8
		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = false;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}				   
			   			   			   			   			   			   	   
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = false;			   
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}		
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
			   		   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}		

		       fpCon.bOverride1 = false;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}			   
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
			   ForgotPasswordController.strUserName = '';
			   fpCon.bOverride1 = true;
			   fpCon.bOverride2 = true;
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
			   
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = false;
		       fpCon.bOverride3 = false;
		       fpCon.bOverride4 = false;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Policy I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}	
			   	
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = true;
		       fpCon.bOverride4 = true;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}	
			    
		       fpCon.bOverride1 = true;
		       fpCon.bOverride2 = true;
		       fpCon.bOverride3 = true;
		       fpCon.bOverride4 = false;
			   try {    
			       fpCon.checkSecurityAnswer(usrIsActive[0].username, 'Legal I.D.', 'x');
			   } catch(Exception ex){System.debug(ex);}	
			   
			   
			   //-----------------		  
			   fpCon.bOverride1 = false;
			   fpCon.bOverride2 = true;
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	   
			   
			   fpCon.bOverride1 = false;
			   fpCon.bOverride2 = true;
			   fpCon.bOverride2 = false;
			   try {    
			       fpCon.doForgotPassword();
			   } catch(Exception ex){System.debug(ex);}	
			   	   			   			   		   		   			   		   		   
	    }
	}
}