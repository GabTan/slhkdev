/* Custom Policy Statement Place Holder */
 	public class customStatementsObject
 	{
 		public Id idStatementId {get; set;}
 		public String strStatementName {get; set;}
 		public String strCurrencyISOCode {get; set;}
 		public Date dateStatementFromDate {get; set;}
 		public String strStatementFromDate {get; set;}
 		public Date dateStatementToDate {get; set;}
 		public String strStatementToDate {get; set;} 
 		public Id idAttachmentId {get; set;} 
		public Double doubReadCount {get; set;}
		public String strReadCount{get; set;}
 		public customStatementsObject(ID idInputStatementId, String strInputStatementName, String strInputCurrencyISOCode, 
 									 Date dateInputStatementFromDate, Date dateInputStatementToDate,
 									 Id idInputAttachmentId, Double doubleInputReadCount, String strRecordTypeName)
 		{
 			idStatementId = idInputStatementId; 
	 		strStatementName = strInputStatementName;
	 		strCurrencyISOCode = strInputCurrencyISOCode;
	 		dateStatementFromDate = dateInputStatementFromDate;
	 		dateStatementToDate =  dateInputStatementToDate;
	 		idAttachmentId = idInputAttachmentId;
	 		if (dateStatementFromDate != null)
	 		{	
	 			strStatementFromDate = String.valueOf(dateFormat(dateStatementFromDate)); 	
	 			if(strRecordTypeName == 'Traditional')
	 			{	
	 				strStatementFromDate = String.valueOf(dateStatementFromDate.year());
	 			}
	 		}
	 		if (dateStatementToDate != null)
	 		{
	 			strStatementToDate = String.valueOf(dateFormat(dateStatementToDate));
	 		}
	 		doubReadCount = doubleInputReadCount;
	 		strReadCount = 	String.valueOf(doubleInputReadCount);
 		} 
 		/* Generic Date Format */
		public static String dateFormat(Date dateInput)
		{
			return String.valueOf(dateInput.format());
		}
 	}