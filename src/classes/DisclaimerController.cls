/*
Version   : 1.4
Company   : Sun Life Financial
Date    : 30.JUL.2009
Author    : 
Description : Controller for the Disclaimer page.
History   : 
            version 1.1 JC NACHOR 09/22/09 - use URL for change password container 
            version 1.2 W. Clavero 11/19/09 - SU login acceptance / rejection handling.
            version 1.3 Wilson Cheng 11/20/09 - Checks the global settings to determine if the check for SU logins is required.
            version 1.4 MARSO 05/23/13 - SR-ES-13005 e-Services PDPO Phase 2
*/
public class DisclaimerController {
    
    public String url = '';
    public Boolean showDisclaimer {get; set;}
    
    //START Wcheng enhancement 11/20/09
    public Boolean checkForSU {get; set;}
    //END Wcheng enhancement 11/20/09

    //S - MARSO - 20130523 - SR-ES-13005
    public Boolean optOutPrev {get; set;}
    public Boolean optOut {get; set;}
    public Integer count {get; set;}
    //E - MARSO - 20130523 - SR-ES-13005

    public String getUrl() {
        return url; 
    }
    public DisclaimerController()
    {
        this.showDisclaimer = false;
        
        //w.clavero 20091120: added Check_For_SU_Logins__c to SOQL  
        Config_Global_Settings__c globalConfig = [Select Check_For_SU_Logins__c From Config_Global_Settings__c LIMIT 1];

        //w. clavero 20091120: expose the option fo SU logins
        if (globalConfig != null) 
        {
            checkForSU = globalConfig.Check_For_SU_Logins__c;
        } 
        else 
        {
            checkForSU = true;
        }
        
        //S - MARSO - 20130523 - SR-ES-13005
        optOutPrev = false; //loadPreviousOptOut();
        count = 1;
        //E - MARSO - 20130523 - SR-ES-13005
    }
    
    public PageReference acceptDisclaimer()
    {
        
        String sessionId = UserInfo.getSessionId();

        User u = [select Id, Client_Id__c, Last_Disclaimer_Session__c from user where id = :UserInfo.getUserId()];
        updateLastAcceptTime(u.ID);
        //url='/home/home.jsp';
        //url = ESPortalNavigator.NAV_URL_HOM;
        PageReference pg = new PageReference(ESPortalNavigator.NAV_URL_HOM);
        pg.setRedirect(true);         
    
        return pg;
    }
    
    public PageReference doNotAccept()
    {
        //S - MARSO - 20130523 - SR-ES-13005
        User u = [select Client_Id__c, Last_Disclaimer_Accept_Time__c from user where id = :UserInfo.getUserId()];
        Account a = [select Id, Opt_Out__c, Opt_Out_DT__c, Opt_Out_Prev__c, Opt_Out_DT_Prev__c from Account where Client_Id__c = :u.Client_Id__c];
        a.Opt_Out_Prev__c = a.Opt_Out__c;
        a.Opt_Out_DT_Prev__c = a.Opt_Out_DT__c;

        update a;
        //E - MARSO - 20130523 - SR-ES-13005
        
        //url='/secur/logout.jsp';
        //url='/secur/login_portal.jsp?orgId=00D30000000KbT8&portalId=060300000002KUS';
        //url='http://sunlife.batch2.cs3.force.com/0';
        //return null;
        PageReference pg = new PageReference('/secur/logout.jsp');
        pg.setRedirect(true);         
        return pg;
        
    }
    
    public static void updateLastAcceptTime(ID userId)
    {
        User u = [select Id, Last_Disclaimer_Session__c from user where id = :userId];
        u.Last_Disclaimer_Accept_Time__c = System.now();
    
        
        update u;
    }
    
    public PageReference checkPasswordChange()
    { 
        User u = [select Id, Last_Disclaimer_Accept_Time__c, LastLoginDate, Last_Password_Change_Date__c
                    from user where id = :UserInfo.getUserId()];
                    
        if (u.Last_Password_Change_Date__c == null)
        {
            // This means the user has never changed their password - && u.Id == '005S0000000E36NIAS'
            // Direct to change password screen
            
            //w. clavero 20090810: modified to reflect changes to the UI 
            //url = '/_ui/system/security/ChangePassword?cancelURL=/secur/logout.jsp&retURL=/apex/PasswordChanged';
            
            // JC NACHOR 09/22/09 - comment out custom change password page
            // url = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_ACC_MAIN) + ESPortalNavigator.CHPWD_FIRST_EVENT;
            
            // JC NACHOR 09/22/09 - use URL for change password container
            url = '/apex/ChangePasswordContainer?reg=0';
         } else {
             //w. clavero 20090813: show the disclaimer form
             showDisclaimer = true;
         }
        return null;
    }
    
    public static TestMethod void testDisclaimer()
    {
        DisclaimerController controller = new DisclaimerController();
        controller.acceptDisclaimer();
        //controller.doNotAccept();
        controller.checkPasswordChange();
        controller.getUrl();
    }
    
    //S - MARSO - 20130523 - SR-ES-13005:
    /*public Boolean loadPreviousOptOut()
    {
        User u = [select Client_Id__c from user where id = :UserInfo.getUserId()];
        Account a = [select Id, Opt_Out__c, Opt_Out_DT__c from Account where Client_Id__c = :u.Client_Id__c];

        optOut = a.Opt_Out__c;
        optOutPrev = a.Opt_Out__c;

        return optOutPrev;
    }*/

    public PageReference updateOptOut()
    {
        User u = [select Client_Id__c from user where id = :UserInfo.getUserId()];
        Account a = [select Id, Opt_Out__c, Opt_Out_DT__c, Opt_Out_Prev__c, Opt_Out_DT_Prev__c from Account where Client_Id__c = :u.Client_Id__c];
    
    if (optOutPrev)
    {
            a.Opt_Out_DT_Prev__c = System.now();
            a.Opt_Out_Prev__c = optOutPrev;
        
            update a;
    }
    else if (!optOutPrev)
    {
            a.Opt_Out_DT_Prev__c = a.Opt_Out_DT__c;
            a.Opt_Out_Prev__c = a.Opt_Out__c;
        
            update a;
    }             

    return null;
    }

    public PageReference updatePrevOptOut()
    {
        if (count == 1)
        {
            User u = [select Client_Id__c, Last_Disclaimer_Accept_Time__c from user where id = :UserInfo.getUserId()];
            Account a = [select Id, Opt_Out__c, Opt_Out_DT__c, Opt_Out_Prev__c, Opt_Out_DT_Prev__c from Account where Client_Id__c = :u.Client_Id__c];
            a.Opt_Out_Prev__c = a.Opt_Out__c;
            a.Opt_Out_DT_Prev__c = a.Opt_Out_DT__c;
            a.Opt_Out__c = a.Opt_Out_Prev__c;
            a.Opt_Out_DT__c = a.Opt_Out_DT_Prev__c;
            update a;
        }

        return null;
    }

    //E - MARSO - 20130523: - SR-ES-13005
}