/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Ryan Dave Wilson
Description	: Storing Attribute values for Switch in Funds
History	:

*/

public class SwitchInRequest {
	public String code {get; set;}
	public String name {get; set;}
	public String percent {get; set;}
	public String estiamount {get; set;}
	public Decimal estAmntDecimal {get; set;}
	public Boolean createNew {get; set;}
	
}