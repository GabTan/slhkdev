/* 
Version		: 1.0
Company		: Sun Life Financial
Date		: 30.JULY.2009
Author		: Dorothy Anne L. Baltazar
Description	: Class for displaying Transaction Log History records.
History		: 
*/
public class TxnLogList {
	public String strId {get; set;}
	public String strRefNo {get; set;}
	public String strPolicyID {get; set;}
	public String strSubmitDate {get; set;}
	public String strChgRequestType {get; set;}
	public String strRequestStatus {get; set;}
	public String strCompletedDate {get; set;}
}