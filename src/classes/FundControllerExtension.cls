/*
Version : 1.6
Company : Sun Life Financial
Date    : Unknown
Author  : Unknown c/o Wilson Cheng
Description : This is used for the fund related list in New Policy Inquiry page.
History : 
         Version 1.1 <09/08/09 - Wcheng   - SIT_5>
         Version 1.2 <10/09/09 - Wcheng   - IL-ES-00036 Added method which handles the rendering of Change Alloc. link>
         Version 1.3 <10/15/09 - Wcheng   - IL-ES-00043 Change the query of the getDispCHMLink method>
         Version 1.4 <10/16/09 - Wcheng   - Added Query in checking if its allowed to change alloc. or fund switch
                                            Added method on hiding the fund switch link in new policy inquiry page>
		 Version 1.5 <10/26/09 - RAngeles - Added IsPortalUser check on change allocation and fund switching button rendering.
		 Version 1.6 <11/24/09 - RAngeles - Updated in lieu with recent changes to target controller
		 version 1.7 <01/14/10 - JNachor  - extended from NewPolicyInquiry to enable inheritance
*/
public with sharing class FundControllerExtension extends NewPolicyInquiry{
// JC NACHOR 01/14/2010
//public with sharing class FundControllerExtension {

	//RBA 07062009 Changed N/A values to NAValue translation
	Policy__c policy = new Policy__c(); 
	Decimal totalFundValue = 0;
	//START ADD Wcheng 08/10/09
	String formattedFundValue;
	//END ADD Wcheng 08/10/09
	String strPolicyId;
	//Misc Enhancement req3 start
	//Double totalfundValue; 
	//Misc Enhancement req3 end	
	
    /* Fund Information Disclaimer */
    public String policyID {get; set;}
    Boolean isPortalUser;
   
    public FundControllerExtension(NewPolicyInquiry controller)
    {
    	isPortalUser = controller.isPortalUser;
    	strPolicyId = ApexPages.currentPage().getParameters().get('policyID');
    }
    
    public String getFundInformationDisclaimer()
    { 
        String strValue;
        strValue = '<br /><center>';
        strValue += Label.Fund_Disclaimer;
        strValue += '</center>';
        return strValue;
    }

    // JC NACHOR - 01/14/2010
    //Boolean bIsFirstDatePriceDT = false;
    //Boolean bIsSecondDatePriceDT = false;   
    //Boolean bIsFirstCOVSkipFstCov = false;
    //Boolean bIsFirstCOVReplaceForbCov = false;
    
    static final String strPortalProfileId = '00e30000000w2RGAAY';
    static final String RecTypePersonAccountId = '012300000006IcSAAU';

    /* Funds */ 
    public customFundObject[] getFundValues()  
    {
    		Display_Translator[] ltFundTemp = new Display_Translator[]{};
			Map<String, String>  ltFundMap  = new Map<String, String>();
    		totalFundValue = 0; 
    		List<customFundObject> customFundArrayResult = new List<customFundObject>();
    		List<Fund_Detail__c> strListTempFunds = new List <Fund_Detail__c>();
            List<String> strConfigFundDataFundName = new List<String>();

	        List<Fund_Detail__c> listFunds = [select id, name, CurrencyIsoCode, Fund_Allocation__c, Fund_Code__c, Fund_Value__c,
	                                    Interest_Rate__c, Maturity_Date__c, Number_of_Units__c, Policy__c, Extraction_Date__c,
	                                    Unit_Price__c, Valuation_Date__c, Unit_Valuation_Date__c, Pending_Indicator__c, ACTIVE__C
	                                    from Fund_Detail__c where Fund_Detail__c.Policy__r.Policy_Number__c = :strPolicyId
	                                    and ACTIVE__C = true
	                                    and (Fund_Value__c > 0 or Fund_Allocation__c > 0)];                 
	        if (listFunds.size() > 0)
	        { 
	            Date dateInvalidDate = Date.newinstance(1901, 01, 01);
	            strListTempFunds = listFunds;                        
	            strConfigFundDataFundName = LookupFundNames(strListTempFunds);  
	            for (Integer a = 0; a<strListTempFunds.size(); a++)
	            {   
	                Date dateFundPriceDate; 
	                dateFundPriceDate = strListTempFunds[a].Valuation_Date__c;      
	                if(strListTempFunds[a].Valuation_Date__c == null || strListTempFunds[a].Valuation_Date__c == dateInvalidDate)
	                {
	                    dateFundPriceDate = strListTempFunds[a].Extraction_Date__c;
	                }
	                else if(    (bIsFirstDatePriceDT && !bIsFirstCOVReplaceForbCov && !bIsFirstCOVSkipFstCov)||
	                            (bIsFirstDatePriceDT && bIsFirstCOVReplaceForbCov)||
	                            (bIsSecondDatePriceDT && bIsFirstCOVSkipFstCov)
	                        )
	                {
	                    dateFundPriceDate = strListTempFunds[a].Unit_Valuation_Date__c;
	                }                                   
	                customFundObject fund = new customFundObject(strConfigFundDataFundName[a], strListTempFunds[a].CurrencyIsoCode, 
	                                                strListTempFunds[a].Fund_Allocation__c, strListTempFunds[a].Number_of_Units__c, 
	                                                dateFundPriceDate, strListTempFunds[a].Unit_Price__c,
	                                                strListTempFunds[a].Interest_Rate__c, strListTempFunds[a].Maturity_Date__c, 
	                                                strListTempFunds[a].Fund_Value__c, strListTempFunds[a].Fund_Code__c,
	                                                strListTempFunds[a].Pending_Indicator__c, strListTempFunds[a].Valuation_Date__c);
	               	if(strListTempFunds[a].Fund_Value__c==null){
                    	strListTempFunds[a].Fund_Value__c=0.0;
                    }
	               	totalFundValue+=strListTempFunds[a].Fund_Value__c;
	                customFundArrayResult.add(fund);
	                Display_Translator elem = new Display_Translator('FUND_NAME', strListTempFunds[a].Fund_Code__c);
					ltFundTemp.add(elem);
	            }
	        }
	        ltFundMap = Display_Translator.TranslateMap(ltFundTemp);
	        for(customFundObject cFO :customFundArrayResult){
				String tempStr = ltFundMap.get(cFO.strFundCode);
				if(tempStr!=null) cFO.strFundName = tempStr;
			}
	        return customFundArrayResult;    
    }
    
    /* Lookup Fund Names */
    public String[] LookupFundNames(List<Fund_Detail__c> funds)
    {
        List<Config_Fund_Data__c> cfd = new List<Config_Fund_Data__c>();
        List<String> strValues = new List<String>();
        String strName = 'N/A';
        Boolean boolFound;
        if(funds.size()>0)
        {
            List<String> strFundCode = new List<String>();
            for(Integer b=0; b<funds.size();b++)
            {
                strFundCode.add(funds[b].Fund_Code__c);
            }           
            cfd = [select id, Fund_Code__c, Fund_Name__c from Config_Fund_Data__c
                    where Fund_Code__c in :strFundCode];
            for (Integer i=0;i<funds.size();i++)
            {
                boolFound = false;
                for(Integer j=0;j<cfd.size();j++)
                {   
                    if (funds[i].Fund_Code__c == cfd[j].Fund_Code__c)
                    {
                        strName = cfd[j].Fund_Name__c;
                        strValues.add(strName);
                        boolFound = true;
                        break;
                    }
                }
                if (boolFound == false)
                {
                    //Unknown Fund Name
                    strValues.add('N/A');
                }
            }
        }   
        return strValues;
    }
    
    //START SIT_5 Wcheng 09/08/09
    public String getFSWSubURL() {
        string FSWreturn;
        Boolean isFSValid = false;
        
        //START Enhancement Wcheng 09/16/09
        List<Policy__c> curPolicies = [Select Id From Policy__c Where Policy_Number__c =:strPolicyId
                                                        AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES 
                                                        AND Policy_Insurance_Type__c in :TransactionsReqConstant.POLICY_UL_CODES 
                                                        AND Active__c = true];
        if(curPolicies.size() > 0) isFSValid = true;  
        //END Enhancement Wcheng 09/16/09  
        
        if(isFSValid){
            FSWreturn = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_NOTES);
        }
        else{
            FSWreturn = '#';
        }
        return FSWreturn;
    }
    //END SIT_5 Wcheng 09/08/09
    
    
    //START IL-ES-00036 Wcheng 10/09/09
    public String getCHMSubURL() {
        string CHMreturn;
        Boolean isCHMValid = false;
        
        //START Enhancement Wcheng 09/16/09
        List<Policy__c> curPolicies = [Select Id From Policy__c Where Policy_Number__c =:strPolicyId
                                                        AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES 
                                                        AND Policy_Insurance_Type__c in :TransactionsReqConstant.POLICY_UL_CODES
                                                        AND Billing_Method__c != :TransactionsReqConstant.POLICY_SINGLE_PREMIUM 
                                                        AND Active__c = true];
        if(curPolicies.size() > 0) isCHMValid = true;
        //END Enhancement Wcheng 09/16/09
        
        if(isCHMValid){
            CHMreturn = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_NOTES);
        }
        else{
            CHMreturn = '#';
        }
        return CHMreturn;
    }
    
    public boolean getDispCHMLink()
    {
        boolean displayLink = false;
        //R Angeles - VF Page button security update (added IsPortalUser check)10/26/2009
        if(isPortalUser){
	        //START EDIT Wcheng IL-ES-00043 10/15/09
	        List<Policy__c> checkPolicy = [Select RecordType.Name, RecordTypeId, Policy_Status_Picklist_Hidden__c, Payment_Method__c From Policy__c
	                                       where Policy_Insurance_Type__c IN: TransactionsReqConstant.POLICY_UL_CODES AND
	                                       Policy_Status_Hidden__c IN: TransactionsReqConstant.POLICY_INFORCE_STATUSES AND
	                                       Billing_Method__c <>: TransactionsReqConstant.POLICY_SINGLE_PREMIUM AND
	                                       Policy_Number__c =:strPolicyId AND Active__c = true];
	        //END EDIT Wcheng IL-ES-00043 10/15/09
	                                       
	       if(checkPolicy.size() > 0)
	       {
	           displayLink = true;
	       }
        }
       return displayLink;       	 	
    }
    //END IL-ES-00036 Wcheng 10/09/09
    
    //START Enhancement Wcheng 09/16/09
    public boolean getDispFSWLink()
    {
        boolean displayLink = false;
        //R Angeles - VF Page button security update (added IsPortalUser check)10/26/2009
        if(isPortalUser){
	        //START EDIT Wcheng IL-ES-00043 10/15/09
	        List<Policy__c> checkPolicy = [Select RecordType.Name, RecordTypeId, Policy_Status_Picklist_Hidden__c, Payment_Method__c From Policy__c
	                                       where Policy_Insurance_Type__c IN: TransactionsReqConstant.POLICY_UL_CODES AND
	                                       Policy_Status_Hidden__c IN: TransactionsReqConstant.POLICY_INFORCE_STATUSES AND
	                                       Policy_Number__c =:strPolicyId AND Active__c = true];
	        //END EDIT Wcheng IL-ES-00043 10/15/09
	                                       
	       if(checkPolicy.size() > 0)
	       {
	           displayLink = true;
	       }
        }
       return displayLink;              
    }
    //START Enhancement Wcheng 09/16/09
    
   	//Misc Enhancement req3 start
   	//START EDITED Wcheng 08/09/09
	public String gettotalFundValue(){
		formattedFundValue =  ESAmountFormatter.FormatAmount(totalFundValue);
		return formattedFundValue;
	}
	//END EDITED Wcheng 08/09/09
	//Misc Enhancement req3 end
	
	static testMethod void testFundControllerExtension()
	{
		NewPolicyInquiry temp = new NewPolicyInquiry();
		RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Traditional'];
		Fund_Detail__c fundDetail1 = new Fund_Detail__c();
		Fund_Detail__c fundDetail2 = new Fund_Detail__c();
		Policy__c policyTemp = new Policy__c();
        
        policyTemp.Name = 'testPolicy';
        policyTemp.CurrencyIsoCode = 'HKD';
        policyTemp.Active__c = true;
        policyTemp.Policy_Number__c = '1234';
        policyTemp.RecordTypeId = recordID.Id;
        policyTemp.Policy_Status_Hidden__c = '1';
        
        insert policyTemp;
        
        fundDetail1.CurrencyIsoCode ='HKD';
        fundDetail1.Fund_Allocation__c = 20;
        fundDetail1.Fund_Code__c = 'FAEPU';
        fundDetail1.Fund_Value__c = 120;
        fundDetail1.Interest_Rate__c = 2;
        fundDetail1.Maturity_Date__c = date.today()+7;
        fundDetail1.Number_of_Units__c = 2.0;
        fundDetail1.Policy__c = policyTemp.id;
        fundDetail1.Extraction_Date__c = date.today();
        fundDetail1.Unit_Price__c = 20;
        fundDetail1.Valuation_Date__c = date.today();
        fundDetail1.Pending_Indicator__c = '1';
        fundDetail1.Active__c = true;
        
        insert fundDetail1;
        
        fundDetail2.CurrencyIsoCode ='HKD';
        fundDetail2.Fund_Allocation__c = 20;
        fundDetail2.Fund_Value__c = 120;
        fundDetail2.Interest_Rate__c = 2;
        fundDetail2.Number_of_Units__c = 2.0;
        fundDetail2.Policy__c = policyTemp.id;
        fundDetail2.Extraction_Date__c = date.today();
        fundDetail2.Pending_Indicator__c = 'N';
        fundDetail2.Active__c = true;
        
        insert fundDetail2;
        
        PageReference pref = Page.NewPolicyInquiry;
        Test.setCurrentPage(pref);
        pref.getParameters().put('policyID', policyTemp.policy_number__c);
        FundControllerExtension tempfund = new FundControllerExtension(temp);
        
        // JC NACHOR - 01/14/2010
        //tempfund.bIsSecondDatePriceDT = true;   
        //tempfund.bIsFirstCOVSkipFstCov = true;
        //temp.bIsSecondDatePriceDT = true;   
        //temp.bIsFirstCOVSkipFstCov = true;
        
        tempfund.getFundInformationDisclaimer();
        tempfund.getFundValues();
        tempfund.gettotalFundValue();
        tempfund.getFSWSubURL();
        tempfund.getCHMSubURL();
        tempfund.getDispFSWLink();
        tempfund.getDispCHMLink();
	}
}