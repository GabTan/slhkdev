/*
    Company    : Sun Life Financial
    Date       : 6/15/2009
    Author     : Emmanuel Simon Esguerra
    Description: Welcome banner controller
    History    : 
    
    <Date Modified>    <Developer Name>             <Short description of modification>
    6/15/2009           Emmanuel Simon Esguerra      Initial Version
	7/24/2009			Emmanuel Simon Esguerra		 Revised the source code to cater to the bug
													 that does not change banner displayed in IE.
   
    */
public class WelcomeBannerCtrl {
    
    public String showBanner {get; set;}
    public String bnrObjStr {get; set;} 
    public String bannerURL {get; set;}
    public String bannerID  {get; set;}
    public String bannerName {get; set;}
    public Double bannerDuration {get; set;}
    public String userLang; 
            
    public WelcomeBannerCtrl(){     
        bnrObjStr = '';
        showBanner = 'display: none;';
        bannerURL = '';
        bannerID = '';
        bannerName = '';
        bannerDuration = 0;
        Boolean validBanner = false;
		List<Config_Marketing__c> bannerList = new List<Config_Marketing__c>();
        
        if(UserInfo.getLanguage()=='en_US'){
        	userLang = 'English [en_US]';
        }else if(UserInfo.getLanguage()=='zh_TW'){
        	userLang = 'Chinese (Traditional) [zh_TW]';
        }

        try {
            //query all for config marketing object
            	bannerList = [Select Name, URL__c, Language__c, Id, Effective_Start_Date__c, Effective_End_Date__c, Display_Duration__c, 
	                             (Select Id, Name From Attachments order by LastModifiedDate desc Limit 1) 
	                             From Config_Marketing__c where Effective_Start_Date__c <= TODAY and Effective_End_Date__c >=TODAY and Language__c =:userLang];  
	                             
     		
            for (Config_Marketing__c bnr: bannerList) {
            	system.debug('banner attachment size '+bnr.Attachments.size() +'Language '+bnr.Language__c+' userLang '+userLang);
                if (!validBanner) {
                    validBanner = (bnr.Attachments.size() > 0/* && bnr.Language__c.contains(userLang)*/);
                } else {
                    break;
                }
            }
    
            //get default banner if list is empty
            if ((bannerList.size() == 0) || (!validBanner)) {
            //if ((bannerList.size() == 0)){
                bannerList = [Select Name, URL__c, Language__c, Id, Effective_Start_Date__c, Effective_End_Date__c, Display_Duration__c, 
                             (Select Id, Name From Attachments order by LastModifiedDate desc Limit 1) 
                             From Config_Marketing__c where Name like 'Default Banner'];
            }
    
            //check if banner object contains an attachment
            for (Config_Marketing__c banner: bannerList) {
                if (banner.Attachments.size() > 0) {
                    bnrObjStr += '{duration:' + (banner.Display_Duration__c * 1000)+
                                ',name:\'' + banner.Name +  
                                '\',id:\'' + banner.Attachments[0].Id + 
                                '\',url:\'' + (banner.URL__c == null ? '' : banner.URL__c) + '\'},';
                    
                    if (bannerID.length() == 0) {
	                    bannerID = '/servlet/servlet.FileDownload?file=' + banner.Attachments[0].Id;
	                    bannerURL = (banner.URL__c == null ? '' : banner.URL__c);
	                    bannerName = banner.Name;
	                    bannerDuration = banner.Display_Duration__c * 1000;
                	} 
                    
                }
                
           
            }
            
            if (bnrObjStr.length() > 0) {
                //remove the last comma
                bnrObjStr = bnrObjStr.substring(0, bnrObjStr.length() - 1);
                showBanner = 'display: block;';
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Banners Currently Available'));
            }
        } catch (QueryException qe) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Banners Currently Available'));
        }        
    }

    public void insertClickRate(){
        try {
            String bnrName = ApexPages.currentPage().getParameters().get('bnrName');
            
            User usrClient = [Select id, Client_Id__c From User Where id = :UserInfo.getUserId()];
            String clID = usrClient.Client_Id__c;
            if (clID == null) {
                clID = 'NO CLIENT ID';
            }
              
            Marketing_Click_Rate__c mktClick = new Marketing_Click_Rate__c (Banner_Name__c = bnrName, Client_ID__c = clID);
            insert mktClick;
        /*    
        } catch (QueryException qe) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There was a problem associated with this banner. Please inform your Customer Representative regarding this issue. Thank You.'));
        } catch (DmlException dmle) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There was a problem associated with this banner. Please inform your Customer Representative regarding this issue. Thank You.'));
        */    
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There was a problem associated with this banner. Please inform your Customer Representative regarding this issue. Thank You.'));
        }
    }
    
    public static testMethod void WelcomeBanner_test(){
    	WelcomeBannerCtrl banner = new WelcomeBannerCtrl();
    	    	
    	banner.insertClickRate();
    }
    	
}