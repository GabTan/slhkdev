/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 19.AUG.2009
Author		: W. Clavero
Description	: Special handling of XML data.
History		: 
*/
public class XMLDataHandler {

	public static final String XML_COMMA_CHAR	= ',';
	public static final String XML_COMMA_ESCAPE	= '%COMMA%';
	
	public static String escapeXMLString(String value) {
		String s = value;
		if (s != null) {
			s = s.replace(XML_COMMA_CHAR, XML_COMMA_ESCAPE);
		}
		return s;
	}

	public static String unescapeXMLString(String value) {
		String s = value;
		if (s != null) {
			s = s.replace(XML_COMMA_ESCAPE, XML_COMMA_CHAR);
		}
		return s;
	}
}