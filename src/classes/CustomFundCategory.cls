/* 

		Version		: 1.0
		Company		: Sun Life Financial
		Date		: AUG.2009
		Author		: Robby Angeles
		Description	: Source class for Fund Switching Swtich In section
					  and Fund Allocation Input section.
		History	: 
				[Version after modification] <Date Modified - Author - Short description of modification>  
		
*/ 

public class CustomFundCategory {
    
    public String  fswFundCatId          {get; set;}
    public String  fswCategory           {get; set;}
    public List<CustomFundSwitching> fswCustomFS {get; set;} 

    public CustomFundCategory(String fswFundCatId, String fswCategory, List<CustomFundSwitching> fswCustomFS){
        this.fswFundCatId = fswFundCatId; 
        this.fswCategory  = fswCategory;
        this.fswCustomFS  = fswCustomFS;
    }
}