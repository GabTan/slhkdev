/* Custom Coverage Place Holder  */
	public class customCoverageObject 
	{
		public String strBenefit_Coverage {get; set;} 
		public String strAmountOfBenefit {get; set;}
		public String strDeathBenefit {get; set;}
		public String strInsured_Name {get; set;}
		public String strPlanCode {get; set;}
		public String strLookupKey {get; set;}
		public String strFirstThreeLetterPlanCode {get; set;}
		public Id idFirstInsuredName  {get; set;}
		public Id idSecondInsuredName  {get; set;}
		public String strCoverageStatus {get; set;}
		public Date dateMaturityDate {get; set;}
		public Integer iHiddenCoverageNumber {get; set;}
		public String strFirstInsuredFirstName {get; set;}
		public String strFirstInsuredLastName {get; set;}
		public String strSecondInsuredFirstName {get; set;}
		public String strSecondInsuredLastName {get; set;}
		public String strPlan_Class {get; set;}
		public String strSupplementalBenefitIndicator {get; set;}
		public Date datePolicyInforceDate {get; set;}
		public String cptValue {get; set;}//SAIII - Steph So - 05/04/2012
		public customCoverageObject()
		{
		}
		public customCoverageObject(String strInputBenefit_Coverage, String strCurrencyISOCode, Decimal decInputAmountOfBenefit, 
										Decimal decInputDeathBenefit, ID idInputFirstInsuredName, ID idInputSecondInsuredName,
										String strPlan_Code, String strLookup_Key, String strFirstThreeLetterPlan_Code, 
										String strCoverage_Status, Date dateMaturity_Date, String strInputCoverageNumber, 
										String strInputPlanClass, String strInputSupplementalBenefitIndicator, Date dateInputPolicyInforceDate,
										String cptValueVal)
		{
			datePolicyInforceDate = dateInputPolicyInforceDate;
			strSupplementalBenefitIndicator = strInputSupplementalBenefitIndicator;
			strBenefit_Coverage = strInputBenefit_Coverage;
			strAmountOfBenefit = String.valueOf(decInputAmountOfBenefit);
			strDeathBenefit = String.valueOf(decInputDeathBenefit);
			String strFirstInsuredName;
			if (idInputFirstInsuredName != null)
			{
				idFirstInsuredName = idInputFirstInsuredName;	
			}
			String strSecondInsuredName;
			if (idInputSecondInsuredName != null)
			{
				idSecondInsuredName = idInputSecondInsuredName;		
			}			
			if (idInputFirstInsuredName == null)
			{
				strInsured_Name = strSecondInsuredName;
				 
			}
			else if (idInputSecondInsuredName == null)
			{
				strInsured_Name = strFirstInsuredName;
			}
			else
			{
				strInsured_Name = strFirstInsuredName + ' & ' + strSecondInsuredName;
			}	
			strPlanCode = strPlan_Code;
			strLookupKey = strLookup_Key;
			strFirstThreeLetterPlanCode = strFirstThreeLetterPlan_Code;	
			strCoverageStatus = strCoverage_Status;
			dateMaturityDate = dateMaturity_Date;
			//R Angeles 12.03.2009: removed null or NA value checks to strInputPlanClass variable (moved checking to newpolicyinquiry screen)
			//strPlan_Class = '';
			//if(strInputPlanClass != null)
			//{
			//	if(strInputPlanClass != NAValue)
			//	{
					strPlan_Class = strInputPlanClass;
			//	}
			//}
			iHiddenCoverageNumber = 0;
			if(strInputCoverageNumber != null && strInputCoverageNumber.contains('COV'))
			{
				iHiddenCoverageNumber = Integer.valueOf(String.valueOf(strInputCoverageNumber).replace('COV',''));
			}
			//SAIII - 05/04/2012 - start
			if (cptValueVal != null)
			{
				cptValue = cptValueVal;
			}
			//SAIII - 05/04/2012 - end
		}
	}