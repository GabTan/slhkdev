/*
Company    : Sun Life Financial
Date       : August 10,2009
Author     : Simon Esguerra
Description: Test Coverage for the Change Contact Information
History    : 
             version 1.1 JC NACHOR 10/12/09 - changed Kowloon Province to Hong Kong
             version 2.2 JC NACHOR 10/20/09 - updated to increase coverage
*/

public class ChangeContactInfoCtrl_test {
@isTest

	public static void ChangeContactInfo_Test(){
		
			Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
    		Map<String,Schema.RecordTypeInfo>AccountMapByName = AccountSchema.getRecordTypeInfosByName();
    		Schema.RecordTypeInfo AccountRT = AccountMapByName.get('Person Account');
    		
    		String PersonAccountTypeId = '';
        	if(AccountRT != null){
        		 PersonAccountTypeId =AccountRT.getRecordTypeId();
        	}
        	
        	Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
					PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
					FirstName='AcME', LastName='GX', RecordTypeId =  PersonAccountTypeId, 
					Client_Id__c = '!@#$%^&*()', Client_ID__pc = 'abcde12345'
					);
					
			Insert accountSuccessTest;
			
			Schema.DescribeSObjectResult PolicySchema = Schema.SObjectType.Policy__c;
    		Map<String,Schema.RecordTypeInfo>PolicyMapByName = PolicySchema.getRecordTypeInfosByName();
    		Schema.RecordTypeInfo PolicyRT = PolicyMapByName.get('Traditional');
    		
    		String TradPolicyTypeId = '';
        	if(PolicyRT != null){
        		TradPolicyTypeId =PolicyRT.getRecordTypeId();
        	}
			
			Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountSuccessTest.id, policy_number__c = '887888877',
											RecordTypeId = TradPolicyTypeId, Policy_Status_Picklist_Hidden__c = '1', 
											Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
											Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
											Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
											Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
											Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
											Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
											Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = '1',
											Branch_Number__c = '234', Account_Number__c = 'Account 1', 
											Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
											Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
											Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
											Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
											Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
											Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
											Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
											Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
											Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
											Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
											Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
											Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
											Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
											Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
											Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00									
											);      
			insert policySuccessTest;
			

			
			pageReference pg = page.ChangeContactInfo;
			Test.setCurrentPage(pg);
			
			ApexPages.StandardController myCPP = new ApexPages.StandardController(accountSuccessTest);
			ApexPages.currentPage().getParameters().put('ClientId',accountSuccessTest.Client_Id__c);
			
			ChangeContactInfoCtrl CP = new ChangeContactInfoCtrl(myCPP);
			

			CP.retrieveMyPolicies();
			
			
			for(customPolicyObject testCPO:CP.listCPO){
				testCPO.boolIsChecked = true;
			}
			CP.getUserPolicies();
			
			CP.checkForChanges();
			CP.validatePage();
			
			CP.homeNumber ='';
			CP.officeNumber='';
			CP.mobileNumber='';
			CP.validatePage();
			
			CP.homeNumber = CP.oldHomeNumber;
			CP.officeNumber = CP.oldOfficeNumber;
			CP.mobileNumber = CP.oldMobileNumber;
			
			CP.Address1 = '';
			CP.Address2 = '';
			CP.Address3 = '';
			CP.Address4 = '';
			CP.Address5 = '';
			CP.Address6 = '';
			CP.Address7 = '';
			CP.Address8 = '';
			CP.country = '';
			CP.province = '';
			
			CP.validatePage();
			
			CP.resetValues();
			
      CP.getAddress1();
      CP.getAddress2();
      CP.getAddress3();
      CP.getAddress4();
      CP.getAddress5();
      CP.getAddress6();
      CP.getAddress7();
      CP.getAddress8();			
      CP.homeNumber = '@';
      CP.officeNumber = '@';
      CP.mobileNumber = '@';
      CP.setAddress1('@');
      CP.setAddress2('@');
      CP.setAddress3('@');
      CP.setAddress4('@');
      CP.setAddress5('@');
      CP.setAddress6('@');
      CP.setAddress7('@');
      CP.setAddress8('@');
      CP.country = '@';
      CP.province = '@';
      
      CP.validatePage();
      
      CP.resetValues();			
			
			CP.homeNumber = CP.oldHomeNumber;
			CP.officeNumber = CP.oldOfficeNumber;
			CP.mobileNumber = CP.oldMobileNumber;
			
			CP.Address1 = '1';
			CP.Address2 = 'F';
			CP.Address3 = 'Test';
			CP.Address4 = 'Test';
			CP.Address5 = 'Test';
			CP.Address6 = 'Test';
			CP.Address7 = 'Test';
			CP.Address8 = 'Test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			
			
			CP.AcknowledgeChanges();
			
			CP.homeNumber ='';
			CP.mobileNumber = '';
			CP.officeNumber = '';
			CP.showWarnings();
			
			CP.homeNumber = '';
			CP.AcknowledgeChanges();
			
			CP.oldHomeNumber = '';
			CP.AcknowledgeChanges();
			
			CP.homeNumber = '1';
			CP.AcknowledgeChanges();
			
			CP.oldHomeNumber = '12345678909';
			
			
			CP.homeNumber = '1';
			CP.officeNumber = '';
			CP.AcknowledgeChanges();
			
			CP.oldOfficeNumber = '';
			CP.AcknowledgeChanges();
			
			CP.officeNumber ='1';
			CP.AcknowledgeChanges();
			 
			CP.oldOfficeNumber = '1234567890';
			
			CP.homeNumber = CP.oldHomeNumber;
			CP.officeNumber = '1';
			CP.mobileNumber = '';
			CP.AcknowledgeChanges();
			
			CP.oldMobileNumber = '';
			CP.AcknowledgeChanges();
			
			CP.mobileNumber = '1';
			CP.AcknowledgeChanges();
			
			CP.oldMobileNumber = '1234567890';
			
			CP.officeNumber = CP.oldOfficeNumber;
			CP.mobileNumber = '1';
			CP.AcknowledgeChanges();
			

			
			CP.mobileNumber = CP.oldMobileNumber;
			CP.AcknowledgeChanges();
			
			
			CP.preAckConfirm();
			CP.getmyURL();
			CP.getclientName();
			CP.getChangedHomeNo();
			CP.homeNumber = '';
			CP.getChangedHomeNo();
			CP.homeNumber = '1';
			CP.getChangedHomeNo();
			CP.homeNumber = '';
			CP.oldHomeNumber = '';
			CP.getChangedHomeNo();
			CP.homeNumber = '1';
			CP.oldHomeNumber = '';
			CP.getChangedHomeNo();
			
			CP.getChangedOfficeNo();
			CP.officeNumber = '';
			CP.getChangedOfficeNo();
			CP.officeNumber = '1';
			CP.getChangedOfficeNo();
			CP.officeNumber = '';
			CP.oldOfficeNumber = '';
			CP.getChangedOfficeNo();
			CP.officeNumber = '1';
			CP.oldOfficeNumber = '';
			CP.getChangedOfficeNo();
			
			CP.getChangedMobileNo();
			CP.mobileNumber = '';
			CP.getChangedMobileNo();
			CP.mobileNumber = '1';
			CP.getChangedMobileNo();
			CP.mobileNumber = '';
			CP.oldMobileNumber = '';
			CP.getChangedMobileNo();
			CP.mobileNumber = '1';
			CP.oldMobileNumber = '';
			CP.getChangedMobileNo();
			
			CP.getChangedEmailAddress();
			CP.emailAddress = 'bigChill@tec.com';
			CP.getChangedEmailAddress();
			
			CP.Address1 = '1';
			CP.Address2 = '';
			CP.Address3 = 'test';
			CP.Address4 = 'test';
			CP.Address5 = 'test';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();


			
			CP.Address1 = '1';
			CP.Address2 = 'testF';
			CP.Address3 = 'test';
			CP.Address4 = 'test';
			CP.Address5 = 'test';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();
			
			CP.getCheckedPolicies();
			
			CP.Address1 = '1';
			CP.Address2 = 'testF';
			CP.Address3 = 'test';
			CP.Address4 = 'test';
			CP.Address5 = '';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();
			
			CP.Address1 = '1';
			CP.Address2 = 'testF';
			CP.Address3 = 'test';
			CP.Address4 = '';
			CP.Address5 = '';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();
			
			CP.Address1 = '1';
			CP.Address2 = 'testF';
			CP.Address3 = 'test';
			CP.Address4 = '';
			CP.Address5 = 'test';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();
			
			CP.Address1 = '1';
			CP.Address2 = 'testF';
			CP.Address3 = '';
			CP.Address4 = 'test';
			CP.Address5 = 'test';
			CP.Address6 = 'test';
			CP.Address7 = 'test';
			CP.Address8 = 'test';
			CP.country = 'Hong Kong';
			CP.province = 'Hong Kong';
			
			CP.getDisplayAddrOne();
			CP.getDisplayAddrTwo();
			CP.getDisplayAddrThree();
			CP.getDisplayAddrFour();
			
			
			
			CP.gettempURLAcknowledgement();
			CP.gettempURLCancel();
			CP.preConfirmValidation();
			CP.getTransactId();
			
			
			
					
	}
}