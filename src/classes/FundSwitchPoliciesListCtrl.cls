/* 
Version		: 1.4
Company		: Sun Life Financial
Date		: AUG.2009
Author		: Robby Angeles
Description	: Class for retrieving user policies available for Fund Switching
History		: 
		1.1 24.AUG.2009 - Robby Angeles - set listCPO to null if 
		 				  no records were found (for error block to appear on page)  
		1.2 09.OCT.2009 - W. Clavero - Replaced Wave 1 logic for determining the Plan Name. 
						  The COV01 record will always be the basis for transactions.
						  
		1.3 13.OCT.2009 - Robby Angeles - Reverted Coverage record retrieval. Removed COV01 reference  
		1.4 15.OCT.2009 - W. Clavero - Changed the conditions of the query for policies (IL-ES-00043).
*/ 

public without sharing class FundSwitchPoliciesListCtrl {
	public Boolean errorCastIron;
	
	public Boolean renderErrorCastIron {get; set;}
	
	public String UserId = UserInfo.getUserId(); 
	public String UserName = UserInfo.getName();
	//User u = [Select id,contactId,Contact.AccountId from User where id = :UserId];
	//public String AccountId = u.Contact.AccountId;
	/* JC NACHOR 07/27/09 - initializes myPolicies */
	public List<Policy__c> myPolicies = new List<Policy__c>();
	public List<customPolicyObject> listCPO = new List<customPolicyObject>();
	public Config_Utilities CU = new Config_Utilities();
	public String tempURL;
	
	/* JC NACHOR 07/27/09 - added Set<Id> setIds */
	Set<Id> setPolIds = new Set<Id>();
	
	private Config_Utilities confUtil = new Config_Utilities();  //w. clavero 20091009
	
	public FundSwitchPoliciesListCtrl() {
	   TransactionValidator CMTxnValidator = new TransactionValidator();
	   errorCastIron = !(CMTxnValidator.checkCastIron());
	
	   if (errorCastIron == false)
	   {
	        renderErrorCastIron = false;
	        initPolicyStmt();
	   }
	   else
	   {
	        renderErrorCastIron = errorCastIron;
	   }	   
	}
	
	public String getPortalUserName()
	{
		String strValue = '';
	 	strValue = CU.formatName(UserInfo.getFirstName(), UserInfo.getLastName());
		return strValue;
	}
	public void initPolicyStmt() {
		//where OwnerId = :u.Id and Active__c = true and (Policy_status_hidden__c = '1' or Policy_Status_Hidden__c = '2' or Policy_Status_Hidden__c = '3' or Policy_Status_Hidden__c = '4')\
		User    curUser    			= new User();
		List<Account> curAccount    = new List<Account>();
		 
		curUser    = [Select Client_Id__c From User Where id = :UserInfo.getUserId()];
		if(curUser.Client_Id__c != null){
			if(curUser.Client_Id__c != ''){
				curAccount = [Select Id, Client_Id__pc 
				              From Account 
				              Where Client_Id__pc = :curUser.Client_Id__c And Active__c = true 
				              LIMIT 1];
				if(curAccount.size() > 0){
					/* w. clavero 20091015: changed the query conditions
					myPolicies = [Select Id, Plan_Code__c, Policy_Inforce_Date__c,  Name, Policy_Number__c, 
								   Policy_Status_Picklist_Hidden__c, Policy_Status_Hidden__c, Policy_Issue_date__c
								   From Policy__c
								   Where Client__c = :curAccount[0].Id
								   AND Policy_Status_Hidden__c IN ('1', '2', '3', '4')
								   AND RecordType.Name = 'UL'
								   AND Active__c = true
								   Order by Policy_Issue_date__c desc, Name 
		                           limit 200
		                          ];
		            */
		            
					myPolicies = [Select Id, Plan_Code__c, Policy_Inforce_Date__c,  Name, Policy_Number__c, 
								   Policy_Status_Picklist_Hidden__c, Policy_Status_Hidden__c, Policy_Issue_date__c
								   From Policy__c
								   Where Client__c = :curAccount[0].Id
								   AND Policy_Status_Hidden__c in :TransactionsReqConstant.POLICY_INFORCE_STATUSES 
								   AND Policy_Insurance_Type__c in :TransactionsReqConstant.POLICY_UL_CODES 
								   AND Active__c = true
								   Order by Policy_Issue_date__c desc, Name 
		                           limit 200
		                          ];
				}
			}
		}        		
		// get Ids from retrieved Policy__c records and put in setPolIds to replace Ids used in policyMap
		for(Policy__c policyTemp : myPolicies){
			setPolIds.add(policyTemp.Id);
		}
		
		Map<String, String> planCodeMap=new Map<String, String>();
		Map<ID, String> insuredMap=new Map<ID, String>();
		String[] planCodes=new String[0];
		
		//Map to store valid plan codes for each policy
		Map<ID, List<String>> validPolicyPlanCodeMap=new Map<ID, List<String>>();	
		//Insured Names
		Coverage__c[] coverageList=new Coverage__c[0];
		coverageList=[Select Plan_Code__c, X2nd_Life_Insured__r.Name, X2nd_Life_Insured__r.FirstName, 
						X2nd_Life_Insured__r.LastName, X1st_Life_Insured__r.Name, X1st_Life_Insured__r.FirstName, 
						X1st_Life_Insured__r.LastName, Policy__c, Name  
						From Coverage__c c
						/* JC NACHOR 07/27/09 - changed source of Ids from policyMap.keySet() into setPolIds */ 
						//where Policy__c IN: policyMap.keySet() 
						where Policy__c IN: setPolIds
						and c.Coverage_Status__c in ('1', '2', '3', '4') and Active__c = true
						Order By Policy__c, Name];
		for(Coverage__c coverage:coverageList) 
		{
			//w. clavero 20091009: no longer applicable as COV01 will always be used as the basis for transactions
			//r. angeles 20091013: comments removed
			if(!validPolicyPlanCodeMap.containsKey(coverage.Policy__c)) {
					validPolicyPlanCodeMap.put(coverage.Policy__c, new String[]{coverage.Plan_Code__c});
			} else {
				validPolicyPlanCodeMap.get(coverage.Policy__c).add(coverage.Plan_Code__c);
			}
			
			
			planCodes.add(coverage.Plan_Code__c);	
			if(!insuredMap.containsKey(coverage.Policy__c)) {
				String strTempName='';
				if(replaceNull(coverage.X1st_Life_Insured__r.Name)!='')
				{
					strTempName = CU.formatName(replaceNull(coverage.X1st_Life_Insured__r.FirstName), replaceNull(coverage.X1st_Life_Insured__r.LastName));

				}
				if(replaceNull(coverage.X2nd_Life_Insured__r.Name)!='') 
				{
					strTempName +=', '+ CU.formatName(replaceNull(coverage.X2nd_Life_Insured__r.FirstName), replaceNull(coverage.X2nd_Life_Insured__r.LastName));
				
				}				
				insuredMap.put(coverage.Policy__c, strTempName);
			} 
		}
		
		/* Plan Code Retrieval */
		// w. clavero 20091009: no longer applicable as COV01 will always be used as the basis for transactions
		// r. angeles 20091013: comments removed
		String[] newPlanCodes=new String[0];
		Set<String> removalPlanCodes=new Set<String>();
		Set<String> removalPlanCodes_new=new Set<String>();
		Set<String> finalRemovalPlanCodes=new Set<String>();
		for(Config_Func_Plan_Map__c tempObj:[select Plan_Code__c from Config_Func_Plan_Map__c where Plan_Code__c IN :planCodes and Lookup_Key__c='SKIP_FST_COV' order by Lookup_Key__c desc])
			if(!removalPlanCodes.contains(tempObj.Plan_Code__c)) removalPlanCodes.add(tempObj.Plan_Code__c);
		
		for(Config_Func_Plan_Map__c tempObj:[select Plan_Code__c from Config_Func_Plan_Map__c where Plan_Code__c IN :removalPlanCodes and Lookup_Key__c='REPLACE_FORB_COV']) {
			removalPlanCodes_new.add(tempObj.Plan_Code__c);
		}
		
		for(String tempVal:removalPlanCodes) {
			if(!removalPlanCodes_new.contains(tempVal)) finalRemovalPlanCodes.add(tempVal);
		}
		
		for(String pcode:planCodes)
			if(!finalRemovalPlanCodes.contains(pcode)) newPlanCodes.add(pcode);
	
		// Plan Name
		Config_Product_Data__c[] planCodesList=[Select Plan_Name__c, Plan_Code__C from Config_Product_Data__c where Plan_Code__C IN: newPlanCodes order by Plan_Code__c];
		for(Config_Product_Data__c planCode:planCodesList) 
			if(!planCodeMap.containsKey(planCode.Plan_Code__c)) planCodeMap.put(planCode.Plan_Code__c, planCode.Plan_Name__c);
		// Populate Class
		Map<String, String>  ltPNameMap  = new Map<String, String>();
		
		//r.angeles 20091013 - commented Map<ID, String> ltPNameMap  = new Map<ID, String>();
		Map<String, String>  ltPStatMap  = new Map<String, String>();
		
		//w.clavero 20091009: no longer needed
		//r. angeles 20091013: comments removed
		Display_Translator[] ltPlanNameTemp = new Display_Translator[]{}; //for Plan Name Translation
		Display_Translator[] ltPolStatTemp  = new Display_Translator[]{}; //for Policy Status Translation	
		
		for(Policy__c p:myPolicies) 
		{	
			customPolicyObject cPO = new customPolicyObject(p.Policy_Issue_Date__c);
			cPO.policyId = p.Id;  //w. clavero 20091009: used to get plan names 
			cPO.strPolicyNumber = p.Policy_Number__c;
			if(p.Policy_Status_Hidden__c != null){
				cPO.strPolicyStatus = p.Policy_Status_Hidden__c;
				Display_Translator elemPolStat = new Display_Translator('SYS_LOOKUP_MST', 'POLSTAT_'+p.Policy_Status_Hidden__c);
				ltPolStatTemp.add(elemPolStat);
			}
			//w. clavero 20091009: no longer applicable as COV01 will always be used as the basis for transactions
			//r. angeles 20091013: comments removed
			if(validPolicyPlanCodeMap.get(p.Id) != null)
			{
				for(Integer x=0; x< validPolicyPlanCodeMap.get(p.Id).size(); x++) 
				{
					if(validPolicyPlanCodeMap.get(p.Id)[x] != null)
					{
						if(planCodeMap.containsKey(validPolicyPlanCodeMap.get(p.Id)[x])) 
						{
							Display_Translator elemPlanName = new Display_Translator('PLAN_NAME', validPolicyPlanCodeMap.get(p.Id)[x]);
							ltPlanNameTemp.add(elemPlanName);
							cPO.strPlanName = validPolicyPlanCodeMap.get(p.Id)[x];
							break;
						}
					}
				}	
			}			
			

			if(insuredMap.get(p.Id) != null)
			{
				cPO.strInsuredName = insuredMap.get(p.Id);
			}
			listCPO.add(cPO);
		}
		
		//w.clavero 20091009: already handled by Config_Util
		//r. angeles 20091013: comments removed, reference to config util removed
		ltPNameMap = Display_Translator.TranslateMap(ltPlanNameTemp);
		//ltPNameMap = confUtil.getPolicyPlanNames(setPolIds);
		ltPStatMap = Display_Translator.TranslateMap(ltPolStatTemp);
		for(customPolicyObject cPO :listCPO){
			if(ltPNameMap != null){
				//w. clavero: replaced by Config_Util map
				//r. angeles 20091013: comments removed
				if(cPO.strPlanName != null){
					String tempPName = ltPNameMap.get(cPO.strPlanName);
					if(tempPName != '') {
						cPO.strPlanName = tempPName;
					}
					else{
						 cPO.strPlanName = '';
					}
				}
				
				//r.angeles 10142009 - commented
				//cPO.strPlanName = ltPNameMap.get(cPO.policyId);
			}
			if(ltPStatMap != null){
				if(cPO.strPolicyStatus != null){
					String tempPStat = ltPStatMap.get('POLSTAT_'+cPO.strPolicyStatus);
					if(tempPStat != '') {
						cPO.strPolicyStatus = tempPStat;
					}
					else{
						cPO.strPolicyStatus = '';
					}
				}
			}
		}
	}
	public String replaceNull(String strVal) {
			if(strVal==null) return '';
			return strVal;
	}
	public String getTempURL()
	{
		tempURL = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_NOTES); //pre-validation page URL
		tempURL = tempURL + '?policyID=';
		return tempURL;
	}

	public List<customPolicyObject> getlistCPO() {
		if(listCPO.size() == 0) return null;
		return listCPO;
	}
	
	
}