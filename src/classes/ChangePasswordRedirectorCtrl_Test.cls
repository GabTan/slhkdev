/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 17.AUG.2009
Author		: W. Clavero
Description	: Test Coverage for the ChangePasswordRedirectorCtrl Apex Class.
History		: 
*/
@isTest
private class ChangePasswordRedirectorCtrl_Test {

    static testMethod void testChangePasswordRedirectorCtrl() {
        PageReference pgRef = Page.ChangePasswordRedirector;
        Test.setCurrentPage(pgRef);
        
        ApexPages.currentPage().getParameters().put('reg', '1');
        ChangePasswordRedirectorCtrl ctrlr = new ChangePasswordRedirectorCtrl();
        
        ApexPages.currentPage().getParameters().put('reg', '0');
        ctrlr = new ChangePasswordRedirectorCtrl();
    }	
}