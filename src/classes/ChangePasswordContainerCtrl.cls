/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 10.AUG.2009
Author		: W. Clavero
Description	: Controller for the ChangePasswordContainer page.
History		: 
*/
public class ChangePasswordContainerCtrl {

    public Boolean isRegularEvent {get; set;}
    
    public ChangePasswordContainerCtrl() {
        this.isRegularEvent = true;
        String regParam = ApexPages.currentPage().getParameters().get('reg'); 
        if (regParam != null) {
            this.isRegularEvent = (regParam.equals(ESPortalNavigator.CHPWD_REG_EVENT_VAL));
        }
    }
}