/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 06.AUG.2009
Author		: W. Clavero
Description	: Test Coverage for the ESPortalNavigator Apex Class.
History		: 
*/
@isTest
private class ESPortalNavigator_Test {

    static testMethod void testPortalNavigation() {
        List<String> menuList = ESPortalNavigator.getMenuItems('MENU1|MENU2|MENU3');
        
        String s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_MAIN);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_WELCOME);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_POLICY);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_PYH);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HOM_FTH);
        
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_MAIN);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_TXN);
        
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_NOTES);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_INPUT);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_FINAL);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CPP_PDF);

        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_POLICY);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_NOTES);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_INPUT);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_FINAL);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_FSW_PDF);

        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_POLICY);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_NOTES);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_INPUT);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_FINAL);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_SVC_CHM_PDF);

        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_MAIN);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_SEC);
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_HLP_SVC);
        
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_DWN_MAIN);
        
        s = ESPortalNavigator.getCustomerPortalNavURL(ESPortalNavigator.NAV_ACC_MAIN);
    }
}