/* 
Version     : 1.7
Company     : Sun Life Financial
Date        : 08.JUL.2009
Author      : Robert Andrew Almedina
Description : This class acts as the controller for the Payment History VF page
History :
              1.1  09/18/09 - Robert Andrew Almedina - Revised code for the new look of pagination links - commented
              1.2  09/22/09 - Wcheng                 - Added link pagination (e.g. previous, back)
              1.3  10/28/09 - R Angeles              - Added admin portal check on security method
              1.4  11/25/09 - JC NACHOR              - Added security checking for PolicyId
              1.5  03.DEC.2009 - W. Clavero          - created new methods for retrieving the Usage and Means descriptions
              1.6  12/03/2009 - JC NACHOR            - used the value in Config Global Settings in deducting years from the current year
              1.7  19.OCT.2010 - M. Elepano 		 - SR-IQ-10065 Change formatNameWithPolicyId that caters both English and Chinese Names        
*/ 

public with sharing class PaymentHstyViewCtrl {
    
    public String currPageNumber        {get; set;} // [**]
    public String firstPageNumber       {get; set;} // [**]
    public String lastPageNumber        {get; set;} // [**]
    public String prevPageNumber        {get; set;} // [**]
    public String nextPageNumber        {get; set;} // [**]
    
    // JC NACHOR - 11/25/2009 - global flag to determine if transaction is accessible to the user
    public boolean isPolicyAccessible = true;    
    
    public Integer getCurrPageNumberInt()
    {
        return Integer.valueOf(currPageNumber);
    }
    
    public Integer getFirstPageNumberInt()
    {
        return Integer.valueOf(firstPageNumber);
    }
    
    public Integer getLastPageNumberInt()
    {
        return Integer.valueOf(lastPageNumber);
    }
    
    public final Policy__c phFinalPolicy {get; set;}
    Policy__c qryPolicy;
    
    public Boolean isNoRecord           {get; set;}
    public Boolean render3rdRemark      {get; set;}

    public String tempURLCancel;
    public String HKD                   {get; set;} 
    public String PolicyID              {get; set;}
    public String PageNumber            {get; set;}
    
    public Integer currYear             {get; set;}
    
    public List<SelectOption> options {get; set;}
    public Config_Utilities configUtils = new Config_Utilities();
    
    Map<String, List<CustomPaymentHistoryWrapper>> PH_Map   = new Map<String, List<CustomPaymentHistoryWrapper>>();
    Integer MaxRecPerPage;
    
    Config_Global_Settings__c globalConfig;
    
    //w. clavero 20091203: for usage and means
    public static final String PYH_TYPE_BANK = 'Bank';
    public static final String PYH_TYPE_CARD = 'Credit Card';

    public static final String PYH_USAGE_TYPE = 'PYH_PAYUSAGE';
    public static final String PYH_USAGE_CODE = 'RNW_PREM';

    public static final String PYH_MEANS_TYPE = 'PYH_PAYMETHOD';
    public static final String PYH_MEANS_BANK = 'BNK_AP';
    public static final String PYH_MEANS_CARD = 'CRC_AP';
    
    public Map<String, String> usageMap = null;
    public Map<String, String> meansMap = null;
        
        
    public String gettempURLCancel(){
        return ESPortalNavigator.NAV_URL_HOM_POLICY;
    }
    
    /* This method creates the map that stores the Lists of records that should be displayed in every page
    //public void createMapforLists() */
    public PaymentHstyViewCtrl()
    {               
        globalConfig = [Select c.SLF_Portal_Profile_Names__c, c.PYH_Max_Records_Per_Page__c From Config_Global_Settings__c c Limit 1];
        List<Payment_History__c> PHistoryList   = new List<Payment_History__c>(); // master list
          options                                 = new List<SelectOption>();
          MaxRecPerPage = Integer.valueOf(globalConfig.PYH_Max_Records_Per_Page__c);
          firstPageNumber = '1'; // [**]
          prevPageNumber = '1'; // [**]
          nextPageNumber = '2'; // [**]
          currPageNumber = '1'; // [**]
          PageNumber = '1';
          lastPageNumber = '1'; // [**]        
                
          // JC NACHOR - 11/25/2009 - Skip processing if the user do not own or do not have the policy specified in the URL               
        if([Select p.Id From Policy__c p where p.Policy_Number__c = :ApexPages.currentPage().getParameters().get('policyID')].size() > 0){                               
                
            try
            {
                /*SR-IQ-10065 - Add Mikka 10-26-10 */
                qryPolicy = [Select p.Client__r.FirstName, p.Client__r.LastName, p.Client__r.First_Name_Chinese__c, p.Client__r.Last_Name_Chinese__c, p.Policy_Number__c, p.Id From Policy__c p where p.Policy_Number__c = :ApexPages.currentPage().getParameters().get('policyID')];
                /*SR-IQ-10065 - End Mikka 10-26-10 */
                
                globalConfig = [Select c.SLF_Portal_Profile_Names__c, c.PYH_Max_Records_Per_Page__c
                                // JC NACHOR 12/03/2009 - used the value in Config Global Settings in deducting years from the current year     
                                , Remark_Year_Offset__c
                                From Config_Global_Settings__c c Limit 1];
                
                PolicyID = qryPolicy.Policy_Number__c;
                
                HKD = Display_Translator.genericTranslation('SYS_LOOKUP_MST', 'CURRENCY_HKD');
                                
                MAp<String, String> CurrencyMap = CurrencyCodeHandler.getTranslatedCurrencyCodes();             
    
                //ORIGINAL CODE
                // JC NACHOR 12/03/2009 - remove remark
                //render3rdRemark = true;
                render3rdRemark = false;
                
                // JC NACHOR 12/03/2009 - used the value in Config Global Settings in deducting years from the current year                
                //currYear = (Datetime.now().year()-1);                            
                currYear = (Datetime.now().year()-globalConfig.Remark_Year_Offset__c.intValue());
                if (Datetime.now().year() > 2009)
                    render3rdRemark = false;
                
                if ((qryPolicy.Policy_Number__c).startsWith('I') == true) // if it is I Policy
                {
                    //add filtering for the date
                    DateTime limitDate = Datetime.newInstance(2008, 08, 23);
                    
                    //if (Datetime.now().year() > 2009)
                    //    limitDate = Datetime.newInstance(Datetime.now().year(), 01, 01);
                        
                    PHistoryList = [Select p.Usage_Code__c, p.Reject_Reason_Description__c, p.Reject_Reason_Code__c, p.Received_Date__c, 
                                    p.Rec_Status__c, p.Rec_No__c, p.Premium_Due_Date__c, p.Post_Status__c, p.Policy_Id__c, p.Policy_Current_Amount__c, 
                                    p.Policy_Currency__c, p.Payment_Type__c, p.Payment_Method__c, p.Payment_Id__c, p.Payment_Amount__c, p.Name, 
                                    p.Means_Type__c, p.Last_Update__c, p.Id, p.Exchange_Rate__c, p.Error_Log__c, p.Draw_Date__c, p.DDA_Required__c, 
                                    p.CurrencyIsoCode, p.Credit_Card_Type__c, p.Credit_Card_Account_No__c, p.CC_Account_No_Masked__c, 
                                    p.Bank_Confirmation_Status__c, p.Bank_Account_No_Masked__c, p.Autopay_Branch_ID__c, p.Autopay_Bank_Name__c, 
                                    p.Autopay_Bank_ID__c, p.Autopay_Account_No__c, p.App_No__c, p.Active__c, p.CreatedDate From Payment_History__c p 
                                    Where (p.Received_Date__c >= LAST_YEAR AND p.Received_Date__c <= THIS_YEAR) AND (Active__c = true) 
                                    AND (p.Received_Date__c >= :limitDate) //AND (p.CreatedDate >= :limitDate)
                                    AND ((p.Bank_Confirmation_Status__c = 'A' OR p.Bank_Confirmation_Status__c = '1') 
                                    OR (p.Post_Status__c = 'Y' and  p.Rec_Status__c = 'Normal'))
                                    AND p.Policy_Id__c = :qryPolicy.Id
                                    Order By Received_Date__c DESC, Payment_Method__c ASC Limit 1000]; // where p.Policy_Id == selected policy
                }
                else // if it is not an I Policy
                {
                    PHistoryList = [Select p.Usage_Code__c, p.Reject_Reason_Description__c, p.Reject_Reason_Code__c, p.Received_Date__c, 
                                    p.Rec_Status__c, p.Rec_No__c, p.Premium_Due_Date__c, p.Post_Status__c, p.Policy_Id__c, p.Policy_Current_Amount__c, 
                                    p.Policy_Currency__c, p.Payment_Type__c, p.Payment_Method__c, p.Payment_Id__c, p.Payment_Amount__c, p.Name, 
                                    p.Means_Type__c, p.Last_Update__c, p.Id, p.Exchange_Rate__c, p.Error_Log__c, p.Draw_Date__c, p.DDA_Required__c, 
                                    p.CurrencyIsoCode, p.Credit_Card_Type__c, p.Credit_Card_Account_No__c, p.CC_Account_No_Masked__c, 
                                    p.Bank_Confirmation_Status__c, p.Bank_Account_No_Masked__c, p.Autopay_Branch_ID__c, p.Autopay_Bank_Name__c, 
                                    p.Autopay_Bank_ID__c, p.Autopay_Account_No__c, p.App_No__c, p.Active__c From Payment_History__c p 
                                    Where (p.Received_Date__c >= LAST_YEAR AND p.Received_Date__c <= THIS_YEAR) AND (Active__c = true) 
                                    AND ((p.Bank_Confirmation_Status__c = 'A' OR p.Bank_Confirmation_Status__c = '1') 
                                    OR (p.Post_Status__c = 'Y' and  p.Rec_Status__c = 'Normal'))
                                    AND p.Policy_Id__c = :qryPolicy.Id
                                    Order By Received_Date__c DESC, Payment_Method__c ASC Limit 1000]; // where p.Policy_Id__c == selected policy
                }
        
                if (PHistoryList.size() < 1)
                {
                    isNoRecord = true;
                }
        
                /* This is to compute the number of pages that will be available for the user */ 
                Integer numberOfPages   = PHistoryList.size() / MaxRecPerPage;              // this is the number of pages available for the user
                
                if (Math.mod(PHistoryList.size(), MaxRecPerPage) != 0)
                {
                    numberOfPages++;
                }
                /* end computation of number of pages */  
                
        
                /* This part creates the list for page selection on the VF page */
                for (Integer i=1; i<=numberOfPages; i++)
                {
                    options.add(new SelectOption(i.format(),i.format()));
                }
                lastPageNumber = numberOfPages.format(); // [**]
        
        
                //w. clavero 20091203: get usage and means translations first
                getUsageAndMeans(PHistoryList);
                            
                /* this is the loop for the map items */
                for (Integer i=0; i<numberOfPages; i++)
                {
                    List<CustomPaymentHistoryWrapper> PHWrappedDisplay = new List<CustomPaymentHistoryWrapper>(); // temp list
                    /* This is the loop for the List items */
                    for (Integer x=0; x<MaxRecPerPage; x++)
                    {
                        Integer index = (i * MaxRecPerPage) + x;
                        
                        if (index < PHistoryList.size())
                        {
                            //PHWrappedDisplay.add(new CustomPaymentHistoryWrapper(PHistoryList[index], PHistoryList[index].Payment_Amount__c, PHistoryList[index].Received_Date__c, CurrencyMap.get(PHistoryList[index].CurrencyIsoCode)));
                            //w. clavero 20091203: explicitly assign the usage and means
                            CustomPaymentHistoryWrapper pHsty = new CustomPaymentHistoryWrapper(PHistoryList[index], PHistoryList[index].Payment_Amount__c, PHistoryList[index].Received_Date__c, CurrencyMap.get(PHistoryList[index].CurrencyIsoCode));
                            pHsty.Usage = getUsageDesc(PHistoryList[index].Payment_Type__c, PHistoryList[index].Usage_Code__c);
                            pHsty.Means = getMeansDesc(PHistoryList[index].Payment_Type__c, PHistoryList[index].Means_Type__c);
                            PHWrappedDisplay.add(pHsty);
                        }
                        else
                        {
                            break; // end the loop if index is already greater than the main list's size
                        }
                    }
                    PH_Map.put((i+1).format(), PHWrappedDisplay); // Insert to the map the page number and the list of records to display for that page number
                }
                if (PHForDisplay == null)
                {
                    getPHForDisplayfromMap(); // called for the initial display of the table
                }
            }
            catch (Exception e)
            {
                isNoRecord = true;
            }
        } else {
            isPolicyAccessible = false;
        }
    }
    
  // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed 
  public PageReference Security() 
  {
      
      // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
      PageReference pRefRedirect = Page.NoAccessToPolicy;
      Boolean isPortalUser = configUtils.isPortalUser(globalConfig.SLF_Portal_Profile_Names__c);
       
      String policyId = ApexPages.currentPage().getParameters().get('policyID');
      // R Angeles 10/28/09 - added admin portal checks
      System.debug('******* SEC POLID; ' + policyId);            
      if(isPortalUser){
          if(ESAccessVerifier.isCurrentUsersPolicy((policyId==null)?'':policyId)){
              System.debug('******* SEC TRUE; ');  
            return null;
          } else {
            System.debug('******* SEC FALSE; ');
            return pRefRedirect;
          }
      }
      else{
           return null;
      }
  }     
    
    List<CustomPaymentHistoryWrapper> PHForDisplay;
    
    public string getPolicyName(){
          if(isPolicyAccessible) {
          String fName = qryPolicy.Client__r.FirstName;
          String lName = qryPolicy.Client__r.LastName;
          String policyNo = qryPolicy.Policy_Number__c;
          /*SR-IQ-10065 Add Mikka 102610 */
          String ChifName = qryPolicy.Client__r.First_Name_Chinese__c;
          String ChilName = qryPolicy.Client__r.Last_Name_Chinese__c;
          String policyValue = '';
              if(qryPolicy != NULL){
                  policyValue = configUtils.formatNameWithPolicyId(fName, lName, policyNo,ChifName,ChilName);
              }
          /*SR-IQ-10065 End Mikka 102610 */
          return policyValue;
          } else {
            return '';
        }   
    }
    public void getPHForDisplayfromMap()
    {
        //PHForDisplay = PH_Map.get(currPageNumber); // [**]
        PHForDisplay = PH_Map.get(PageNumber);
    }

    public List<CustomPaymentHistoryWrapper> getPHForDisplay()
    {
        if(PHForDisplay == null){
            PHForDisplay = new List<CustomPaymentHistoryWrapper>();
        }
        return PHForDisplay;
    }
    
    public String AmountFormatter(Double amountToFormat)
    {
        Decimal temp = amountToFormat;
        return temp.format();
    }
    
    public void getUsageAndMeans(List<Payment_History__c> payList) {
        List<Display_Translator> ul = new List<Display_Translator>();
        List<Display_Translator> ml = new List<Display_Translator>();
        
        //bank and cc usage
        ul.add(new Display_Translator(PYH_USAGE_TYPE, PYH_USAGE_CODE));
        
        //bank and cc means
        ml.add(new Display_Translator(PYH_MEANS_TYPE, PYH_MEANS_BANK));
        ml.add(new Display_Translator(PYH_MEANS_TYPE, PYH_MEANS_CARD));
        
        //other usages and means
        Set<String> useCodes = new Set<String>();
        Set<String> meanCodes = new Set<String>();
        for (Payment_History__c pHsty: payList) {
            if (!useCodes.contains(pHsty.Usage_Code__c)) {
                useCodes.add(pHsty.Usage_Code__c);
                ul.add(new Display_Translator(PYH_USAGE_TYPE, pHsty.Usage_Code__c));                
            }
            if (!meanCodes.contains(pHsty.Means_Type__c)) {
                meanCodes.add(pHsty.Means_Type__c);
                ml.add(new Display_Translator(PYH_MEANS_TYPE, pHsty.Means_Type__c));
            }
        }
        
        //translate
        usageMap = Display_Translator.translateMap(ul);
        meansMap = Display_Translator.translateMap(ml);
    }
    
    public String getUsageDesc(String payType, String usageCode) {
        if (usageMap != null) {
            if ((payType == PYH_TYPE_BANK) || (payType == PYH_TYPE_CARD)) {
                return usageMap.get(PYH_USAGE_CODE);
            } else {
                return usageMap.get(usageCode);
            }
        } else {
            return '';
        }
    }
    
    public String getMeansDesc(String payType, String meansCode) {
        if (meansMap != null) {
            if (payType == PYH_TYPE_BANK) {
                return meansMap.get(PYH_MEANS_BANK);
            } else if (payType == PYH_TYPE_CARD) {
                return meansMap.get(PYH_MEANS_CARD);
            } else {
                return meansMap.get(meansCode);
            }
        } else {
            return '';
        }
    }
}