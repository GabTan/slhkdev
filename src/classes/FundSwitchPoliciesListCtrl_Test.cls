/* 
Version		   : 1.1
Company        : Sun Life Financial 
Date           : August 18, 2009 
Author         : Robby Angeles 
Description    : Test coverage for the FundSwitchPoliciesListCtrl class.
History        : <Date of Update>        <Author of Update>        <Short description of update> 

               : 
               
               		08.18.2009 - Robby Angeles - Created
               
               1.1  11.24.2009 - Robby Angeles - Updated in lieu of recent changes in target controller.
                                                                                 
*/ 
@isTest
private class FundSwitchPoliciesListCtrl_Test {

    static testMethod void policiesListCtrlTest() {
    	User curUser = new User(
    		Id 			 = UserInfo.getUserId(),
    		Client_Id__c = 'TESTCL1'
    	);
    	update curUser;    		
    	
    	Id policyULRecType;
    	Id personRecType;
    	
    	List<RecordType> RecTypes = new List<RecordType>([Select Id, IsPersonType 
    													  From RecordType 
    													  Where (SObjectType = 'Policy__c'
        												  And Name = 'UL' And IsActive= true) 
        												  Or (IsPersonType = True And IsActive= true)
       	 												]);
        
        if(RecTypes.size() > 0){
        	for(RecordType recType :RecTypes){
        		if(recType.IsPersonType == true){
        			personRecType   = recType.Id; 	
        		}
        		else{
        			policyULRecType = recType.Id;
        		}	
        	}
        	
        }
        
        Account insuredName1 = new Account(
        	RecordTypeId	=	personRecType,
        	LastName		=   'First',
        	FirstName       =   'Insured',
        	Client_Id__pc   =   curUser.Client_Id__c,
        	Active__c		=	true,
        	PersonEmail = 'a@b.com'	
        );
        
        Account insuredName2 = new Account(
        	RecordTypeId	=	personRecType,
        	LastName		=   'Second',
        	FirstName       =   'Insured',
        	Client_Id__pc   =   'TESTCL2',
        	Active__c		=	true,
        	PersonEmail = 'a@b.com'
        );
        
        insert insuredName1;
        insert insuredName2; 
        
        Policy__c testPolicy 		 = new Policy__c(
        	RecordTypeId 			 = policyULRecType,
        	Policy_Number__c		 = 'TEST1020',
        	Active__c				 = true,
        	Policy_Status_Hidden__c	 = '1',
        	Client__c				 = insuredName1.Id,
        	Policy_Insurance_Type__c = 'A'
        );
        
        insert testPolicy;
        
        Coverage__c testCoverage = new Coverage__c(
        	Policy__c 				= testPolicy.Id,
        	Coverage_Number__c		= 'TestCovNumber',
        	Coverage_Status__c		= '1',
        	Plan_Code__c			= 'TestPC',
        	X1st_Life_Insured__c    = insuredName1.Id,
        	X2nd_Life_Insured__c    = insuredName2.Id,
        	Active__c 				= true
        );
        
        insert testCoverage;
        
        Config_Func_Plan_Map__c testFuncPlanMap = new Config_Func_Plan_Map__c(
        	Plan_Code__c  = 'TestPC',
        	Lookup_Key__c = 'SKIP_FST_COV'
        );
        
        Config_Func_Plan_Map__c testFuncPlanMap2 = new Config_Func_Plan_Map__c(
        	Plan_Code__c  = 'TestPC',
        	Lookup_Key__c = 'REPLACE_FORB_COV'
        );
        
        insert testFuncPlanMap;
        insert testFuncPlanMap2;
        
        Config_Product_Data__c testProductData   = new Config_Product_Data__c(
        	Plan_Code__c		  = 'TestPC',
        	CHM_Min_Percentage__c = 20
        );
        
        insert testProductData;
        
        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'PLAN_NAME',
        	Code__c				= 'TestPC',
        	Description__c		= 'TestPC',
        	Primary_Key__c		= 'PLAN_NAME|TestPC|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup2 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'SYS_LOOKUP_MST',
        	Code__c				= 'POLSTAT_1',
        	Description__c		= 'TestPolStat',
        	Primary_Key__c		= 'SYS_LOOKUP_MST|POLSTAT_1|en_US',
        	Language__c			= 'en_US'
        );
        
        insert testLookup;
        insert testLookup2;
        
        Config_Global_Settings__c testGlobalSettings = new Config_Global_Settings__c(
        	TXN_Is_Cast_Iron_Available__c = true,
        	CHM_Min_Percentage__c 			= 30.25,
        	FSW_Gif_Min_Amount__c			= 7500.0000,
        	FSW_Switch_Out_Min_Amount__c	= 100,
        	SLF_Portal_Profile_Names__c		= 'TEST'
        );
        
        insert testGlobalSettings;
        
        Test.setCurrentPageReference(new PageReference('Page.FundSwitchPoliciesList'));
        
        FundSwitchPoliciesListCtrl fsPolListCtrl = new FundSwitchPoliciesListCtrl();
        
        fsPolListCtrl.getPortalUserName();
        fsPolListCtrl.getTempURL();
        fsPolListCtrl.getlistCPO();
        fsPolListCtrl.replaceNull(null);
        
        
        //force calls
        
    }
}