/* 
Company        : Sun Life Financial 
Date           : August 11, 2009 
Author         : Marian Baleros 
Description    : This class acts as a test coverage for the Fund Transaction History View page controller
History        : <Date of Update>        <Author of Update>        <Short description of update> 

*/ 
@isTest
private class FundTxnHstyView_Test {

    static testMethod void myUnitTest1() {
        Test.setCurrentPageReference(new PageReference('Page.FundTxnHstyView'));
        
        //Policy__c policyQuer;
        Set<String> POLICY_INFORCE_STATUSES = new Set<String>{'1', '2', '3', '4'};
        Set<String> POLICY_UL_CODES = new Set<String>{'A', 'B', 'C', 'D', 'E', 'F',  
                                                      'G', 'H', 'I', 'J', 'K', 'L', 
                                                      'M', 'N', 'O', 'P', 'Q', 'R', 
                                                      'S', 'T', 'U', 'V', 'W', 'X', 
                                                      'Y', 'Z'
                                                     };
        
        
        Id policyULRecType;
        
        Account acctTemp = new Account();
        
        RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Person Account'];
        
        acctTemp.FirstName = 'Wilson';
        acctTemp.LastName = 'Cheng';
        acctTemp.Client_Id__c = 'test';
        acctTemp.Client_ID__pc = 'test';
        acctTemp.CurrencyIsoCode = 'HKD';
        acctTemp.RecordTypeId = recordID.Id;
        acctTemp.PersonEmail = 'a@b.com';
        acctTemp.First_Name_Chinese__c = 'メカイラジン';
        acctTemp.Last_Name_Chinese__c = 'エレパニオ';
        
        insert acctTemp;
        
        List<RecordType> policyRecType = new List<RecordType>([Select Id From RecordType Where SObjectType = 'Policy__c'
            And Name = 'UL' And IsActive= true
        ]);
        
        if(policyRecType.size() > 0){
            policyULRecType = policyRecType[0].Id;
        } 
        
        Policy__c testPolicy        = new Policy__c(
                  RecordTypeId              = policyULRecType,
                  Policy_Number__c          = 'TESTPOLNUM',
                  Active__c                 = true,
                  Policy_Status_Hidden__c   = '1',
                  Policy_Insurance_Type__c  = 'A',
                  CurrencyISOCode           = 'HKD',
                  OwnerId = Userinfo.getUserId(),
                  UL_lapse_start_date__c = date.today()
               );
       insert testPolicy;
        
        Coverage__c testCoverage = new Coverage__c(
            Policy__c               = testPolicy.Id,
            Coverage_Number__c      = 'COV01',
            Name                    = 'COV01',
            Coverage_Status__c      = '1',
            Plan_Code__c            = 'TestPC',
            Active__c               = true
        );
        
        List<Fund_Detail__c> testListFundDetail             = new List<Fund_Detail__c>();
        List<Config_Plan_Fund_Map__c> testListPlanFundMap   = new List<Config_Plan_Fund_Map__c>();
        List<Config_Translation_Lookups__c> testListLookups = new List<Config_Translation_Lookups__c>();
        
        Fund_Detail__c testFundDetail = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC1',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        );
        
        Fund_Detail__c testFundDetail2 = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC1',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        );
        
        //Added for fund detail with same fund code scenario
        Fund_Detail__c testFundDetail3 = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC2',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        ); 
        
        testListFundDetail.add(testFundDetail);
        testListFundDetail.add(testFundDetail2);
        testListFundDetail.add(testFundDetail3);

        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_NAME',
            Code__c             = 'TestFC1',
            Description__c      = 'TestFC1',
            Primary_Key__c      = 'FUND_NAME|TestFC1|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup2 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_NAME',
            Code__c             = 'TestFC2',
            Description__c      = 'TestFC2',
            Primary_Key__c      = 'FUND_NAME|TestFC2|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup3 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_CATEGORY',
            Code__c             = '0',
            Description__c      = 'CAT1',
            Primary_Key__c      = 'FUND_CATEGORY| z0|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup4 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'PLAN_NAME',
            Code__c             = 'TestPC',
            Description__c      = 'TestPC',
            Primary_Key__c      = 'Plan_Name|TestPC|en_US',
            Language__c         = 'en_US'
        );
        
        testListLookups.add(testLookup);
        testListLookups.add(testLookup2);
        testListLookups.add(testLookup3);
        testListLookups.add(testLookup4);
        
        Config_Plan_Fund_Map__c testPlanFundMap = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '0',
            Fund_Code__c                    = 'TestFC1',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'DIF',
            Sequence_Number__c              = 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap2 = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '0',
            Fund_Code__c                    = 'TestFC2',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'Other',
            Sequence_Number__c              = 1
            
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap3 = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '10',
            Fund_Code__c                    = 'TestFC3',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'Other',
            Sequence_Number__c              = 2
        );
        
        testlistPlanFundMap.add(testPlanFundMap);
        testlistPlanFundMap.add(testPlanFundMap2);
        testlistPlanFundMap.add(testPlanFundMap3);
        
        insert testListLookups;
        insert testCoverage;
        insert testListFundDetail;
        insert testListPlanFundMap;        



        Date fundPriceDate = System.Today()-60;
        Config_Utilities configUtils = new Config_Utilities();
        List<Fund_Transaction_History__c> testListFundTrans = new List<Fund_Transaction_History__c>();
        
        Fund_Transaction_History__c fundTrans1 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000123',Policy__c=testPolicy.Id, Fund_Code__c='BEAEH', Transaction_Type__c='LSI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans1);
        Fund_Transaction_History__c fundTrans2 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000124',Policy__c=testPolicy.Id, Fund_Code__c='BHKCU', Transaction_Type__c='SO', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.5023, Number_of_Units__c=10.000000, Unit_Price__c=100.7509, Fund_Value__c=1172004.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans2);
        Fund_Transaction_History__c fundTrans3 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000125',Policy__c=testPolicy.Id, Fund_Code__c='FGRFH', Transaction_Type__c='RI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75,Fund_Source__c='F',Active__c = true );
        testListFundTrans.add(fundTrans3);
        Fund_Transaction_History__c fundTrans4 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000126',Policy__c=testPolicy.Id, Fund_Code__c='BHKCU', Transaction_Type__c='SI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=20.5700, Number_of_Units__c=50.000000, Unit_Price__c=500.0000, Fund_Value__c=100000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans4);
        Fund_Transaction_History__c fundTrans5 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000127',Policy__c=testPolicy.Id, Fund_Code__c='SHCPU', Transaction_Type__c='WDL', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=NULL, Number_of_Units__c=1.000000, Unit_Price__c=150.7500, Fund_Value__c=5200.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans5);
        Fund_Transaction_History__c fundTrans6 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000128',Policy__c=testPolicy.Id, Fund_Code__c='IFU07', Transaction_Type__c='SO', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=11.0000, Number_of_Units__c=NULL, Unit_Price__c=100.7509, Fund_Value__c=1172004,Fund_Source__c='C',Active__c = true);
        testListFundTrans.add(fundTrans6);
        Fund_Transaction_History__c fundTrans7 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000129',Policy__c=testPolicy.Id, Fund_Code__c='BEAEH', Transaction_Type__c='LSI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.0000, Number_of_Units__c=10.000000, Unit_Price__c=NULL, Fund_Value__c=-105750.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans7);
        Fund_Transaction_History__c fundTrans8 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000130',Policy__c=testPolicy.Id, Fund_Code__c='AGEBU', Transaction_Type__c='APH', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.0000, Number_of_Units__c=2.000000, Unit_Price__c=-250.7509, Fund_Value__c=-50000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans8);
        Fund_Transaction_History__c fundTrans9 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000131',Policy__c=testPolicy.Id, Fund_Code__c='IFH07', Transaction_Type__c='FYB', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=2.1010, Number_of_Units__c=20.000000, Unit_Price__c=100.0000, Fund_Value__c=10000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans9);
        Fund_Transaction_History__c fundTrans10 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000132',Policy__c=testPolicy.Id, Fund_Code__c='AGEBU', Transaction_Type__c='SI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.5023, Number_of_Units__c=10.000000, Unit_Price__c=100.7509, Fund_Value__c=NULL,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans10);
        
        insert testListFundTrans; 
        
        System.currentPageReference().getParameters().put('policyID', testPolicy.Policy_Number__c);
        
        FundTxnHstyViewCtrl fundTxnHstyCtrlTest = new FundTxnHstyViewCtrl();
        
        fundTxnHstyCtrlTest.setbyPassCheck(true);
        
        fundTxnHstyCtrlTest.FundsStartMonth = 01;
        fundTxnHstyCtrlTest.FundsStartYear = 2008;
        fundTxnHstyCtrlTest.FundsEndMonth = 10;
        fundTxnHstyCtrlTest.FundsEndYear = 2009;
        fundTxnHstyCtrlTest.checkDate = true;
        
        fundTxnHstyCtrlTest.FundsName = 'All';
        fundTxnHstyCtrlTest.TransType = 'All';
        fundTxnHstyCtrlTest.FromSort = 'Fund_Code__c';
        fundTxnHstyCtrlTest.SortBy = 'desc';
        fundTxnHstyCtrlTest.byPassCheck = true;
        fundTxnHstyCtrlTest.checkRecs = true;
        fundTxnHstyCtrlTest.fundTranMap = NULL;
 
        fundTxnHstyCtrlTest.constructorExt();
        fundTxnHstyCtrlTest.getPolicyNum();
        fundTxnHstyCtrlTest.getFundName();
        fundTxnHstyCtrlTest.getPolicyName();
        fundTxnHstyCtrlTest.getCashflowFundTransType();
        fundTxnHstyCtrlTest.getUtilizedFundTransType();
        fundTxnHstyCtrlTest.getFundTran();
        fundTxnHstyCtrlTest.getFundStartMonth();
        fundTxnHstyCtrlTest.getFundEndMonth();
        fundTxnHstyCtrlTest.getFundStartYear();
        fundTxnHstyCtrlTest.getFundEndYear();
        fundTxnHstyCtrlTest.getFundSort();
        fundTxnHstyCtrlTest.getFundSortBy();
        fundTxnHstyCtrlTest.getPolicyBack();
        fundTxnHstyCtrlTest.getfundYearPrevious();
        fundTxnHstyCtrlTest.Submit();
        fundTxnHstyCtrlTest.getPolicyBack();
        fundTxnHstyCtrlTest.pageRecords();
        fundTxnHstyCtrlTest.buildPageCount();
        fundTxnHstyCtrlTest.pagination();
        fundTxnHstyCtrlTest.onPageChange();
        fundTxnHstyCtrlTest.getcurFundTran();
        fundTxnHstyCtrlTest.getpagesOpt();
        fundTxnHstyCtrlTest.getCurrPageNumberInt();
        fundTxnHstyCtrlTest.getFirstPageNumberInt();
        fundTxnHstyCtrlTest.getLastPageNumberInt();
        fundTxnHstyCtrlTest.Security();
        
        fundTxnHstyCtrlTest.policyVal = configUtils.formatNameWithPolicyId(testPolicy.Client__r.FirstName, testPolicy.Client__r.LastName, testPolicy.Policy_Number__c,testPolicy.Client__r.First_Name_Chinese__c,testPolicy.Client__r.Last_Name_Chinese__c);
        
        fundTxnHstyCtrlTest.setDisp(true);
        fundTxnHstyCtrlTest.getDisp();
        
    }
    
    static testMethod void myUnitTest2() {
        Test.setCurrentPageReference(new PageReference('Page.FundTxnHstyView'));
        
        //Policy__c policyQuer;
        //policyQuer = [Select Id, Policy_Number__c, Client__r.FirstName, Client__r.LastName, Client__c from Policy__c limit 1];
        
        //Policy__c policyQuer;
        Set<String> POLICY_INFORCE_STATUSES = new Set<String>{'1', '2', '3', '4'};
        Set<String> POLICY_UL_CODES = new Set<String>{'A', 'B', 'C', 'D', 'E', 'F',  
                                                      'G', 'H', 'I', 'J', 'K', 'L', 
                                                      'M', 'N', 'O', 'P', 'Q', 'R', 
                                                      'S', 'T', 'U', 'V', 'W', 'X', 
                                                      'Y', 'Z'
                                                     };
        
        
        Id policyULRecType;
        
        List<RecordType> policyRecType = new List<RecordType>([Select Id From RecordType Where SObjectType = 'Policy__c'
            And Name = 'UL' And IsActive= true
        ]);
        
        if(policyRecType.size() > 0){
            policyULRecType = policyRecType[0].Id;
        } 
        
        Policy__c testPolicy        = new Policy__c(
                  RecordTypeId              = policyULRecType,
                  Policy_Number__c          = 'TESTPOLNUM',
                  Active__c                 = true,
                  Policy_Status_Hidden__c   = '1',
                  Policy_Insurance_Type__c  = 'A',
                  CurrencyISOCode           = 'HKD' 
               );
       insert testPolicy;
        
        Coverage__c testCoverage = new Coverage__c(
            Policy__c               = testPolicy.Id,
            Coverage_Number__c      = 'COV01',
            Name                    = 'COV01',
            Coverage_Status__c      = '1',
            Plan_Code__c            = 'TestPC',
            Active__c               = true
        );
        
        List<Fund_Detail__c> testListFundDetail             = new List<Fund_Detail__c>();
        List<Config_Plan_Fund_Map__c> testListPlanFundMap   = new List<Config_Plan_Fund_Map__c>();
        List<Config_Translation_Lookups__c> testListLookups = new List<Config_Translation_Lookups__c>();
        
        Fund_Detail__c testFundDetail = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC1',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        );
        
        Fund_Detail__c testFundDetail2 = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC1',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        );
        
        //Added for fund detail with same fund code scenario
        Fund_Detail__c testFundDetail3 = new Fund_Detail__c(
            Policy__c               = testPolicy.Id,
            Fund_Code__c            = 'TestFC2',
            Active__c               = true,
            Fund_Value__c           = 1000000.0000,
            Number_Of_Units__c      = 10,
            Interest_Rate__c        = 3,
            Unit_Price__c           = 1000000.0000,
            Fund_Allocation__c      = 1000000.0000
        ); 
        
        testListFundDetail.add(testFundDetail);
        testListFundDetail.add(testFundDetail2);
        testListFundDetail.add(testFundDetail3);

        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_NAME',
            Code__c             = 'TestFC1',
            Description__c      = 'TestFC1',
            Primary_Key__c      = 'FUND_NAME|TestFC1|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup2 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_NAME',
            Code__c             = 'TestFC2',
            Description__c      = 'TestFC2',
            Primary_Key__c      = 'FUND_NAME|TestFC2|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup3 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'FUND_CATEGORY',
            Code__c             = '0',
            Description__c      = 'CAT1',
            Primary_Key__c      = 'FUND_CATEGORY| z0|en_US',
            Language__c         = 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup4 = new Config_Translation_Lookups__c(
            Code_Type__c        = 'PLAN_NAME',
            Code__c             = 'TestPC',
            Description__c      = 'TestPC',
            Primary_Key__c      = 'Plan_Name|TestPC|en_US',
            Language__c         = 'en_US'
        );
        
        testListLookups.add(testLookup);
        testListLookups.add(testLookup2);
        testListLookups.add(testLookup3);
        testListLookups.add(testLookup4);
        
        Config_Plan_Fund_Map__c testPlanFundMap = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '0',
            Fund_Code__c                    = 'TestFC1',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'DIF',
            Sequence_Number__c              = 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap2 = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '0',
            Fund_Code__c                    = 'TestFC2',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'Other',
            Sequence_Number__c              = 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap3 = new Config_Plan_Fund_Map__c(
            Plan_Code__c                    = 'TestPC',
            Fund_Category_Code__c           = '10',
            Fund_Code__c                    = 'TestFC3',
            FSW_Allow_Switch_Out__c         = true,
            FSW_Allow_Switch_In__c          = true,
            CHM_Allow_Fund_Allocation__c    = true,
            Fund_Status__c                  = 'Active',
            Fund_Type__c                    = 'Other',
            Sequence_Number__c              = 2
        );
        
        testlistPlanFundMap.add(testPlanFundMap);
        testlistPlanFundMap.add(testPlanFundMap2);
        testlistPlanFundMap.add(testPlanFundMap3);
        
        insert testListLookups;
        insert testCoverage;
        insert testListFundDetail;
        insert testListPlanFundMap;        



        Date fundPriceDate = System.Today()-30;
        Config_Utilities configUtils = new Config_Utilities();
        List<Fund_Transaction_History__c> testListFundTrans = new List<Fund_Transaction_History__c>();
        
        Fund_Transaction_History__c fundTrans1 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000123',Policy__c=testPolicy.Id, Fund_Code__c='BEAEH', Transaction_Type__c='LSI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans1);
        Fund_Transaction_History__c fundTrans2 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000124',Policy__c=testPolicy.Id, Fund_Code__c='BHKCU', Transaction_Type__c='SO', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.5023, Number_of_Units__c=10.000000, Unit_Price__c=100.7509, Fund_Value__c=1172004.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans2);
        Fund_Transaction_History__c fundTrans3 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000125',Policy__c=testPolicy.Id, Fund_Code__c='FGRFH', Transaction_Type__c='RI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75,Fund_Source__c='F',Active__c = true );
        testListFundTrans.add(fundTrans3);
        Fund_Transaction_History__c fundTrans4 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000126',Policy__c=testPolicy.Id, Fund_Code__c='BHKCU', Transaction_Type__c='SI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=20.5700, Number_of_Units__c=50.000000, Unit_Price__c=500.0000, Fund_Value__c=100000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans4);
        Fund_Transaction_History__c fundTrans5 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000127',Policy__c=testPolicy.Id, Fund_Code__c='SHCPU', Transaction_Type__c='WDL', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=NULL, Number_of_Units__c=1.000000, Unit_Price__c=150.7500, Fund_Value__c=5200.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans5);
        Fund_Transaction_History__c fundTrans6 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000128',Policy__c=testPolicy.Id, Fund_Code__c='IFU07', Transaction_Type__c='SO', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=11.0000, Number_of_Units__c=NULL, Unit_Price__c=100.7509, Fund_Value__c=1172004,Fund_Source__c='C',Active__c = true);
        testListFundTrans.add(fundTrans6);
        Fund_Transaction_History__c fundTrans7 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000129',Policy__c=testPolicy.Id, Fund_Code__c='BEAEH', Transaction_Type__c='LSI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=5.0000, Number_of_Units__c=10.000000, Unit_Price__c=NULL, Fund_Value__c=-105750.75,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans7);
        Fund_Transaction_History__c fundTrans8 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000130',Policy__c=testPolicy.Id, Fund_Code__c='AGEBU', Transaction_Type__c='APH', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.0000, Number_of_Units__c=2.000000, Unit_Price__c=-250.7509, Fund_Value__c=-50000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans8);
        Fund_Transaction_History__c fundTrans9 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000131',Policy__c=testPolicy.Id, Fund_Code__c='IFH07', Transaction_Type__c='FYB', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=2.1010, Number_of_Units__c=20.000000, Unit_Price__c=100.0000, Fund_Value__c=10000.00,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans9);
        Fund_Transaction_History__c fundTrans10 = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000132',Policy__c=testPolicy.Id, Fund_Code__c='AGEBU', Transaction_Type__c='SI', Fund_Price_Date__c=fundPriceDate, Maturity_Date__c=fundPriceDate, Interest_Rate__c=10.5023, Number_of_Units__c=10.000000, Unit_Price__c=100.7509, Fund_Value__c=NULL,Fund_Source__c='C',Active__c = true );
        testListFundTrans.add(fundTrans10);
        
        insert testListFundTrans;        
        
        System.currentPageReference().getParameters().put('policyID', testPolicy.Policy_Number__c);
        
        FundTxnHstyViewCtrl fundTxnHstyCtrlTest = new FundTxnHstyViewCtrl();
        
        fundTxnHstyCtrlTest.setbyPassCheck(true);

        //FOR DISPLAY OF ERRORS
        //If From Date > To Date
        fundTxnHstyCtrlTest.FundsStartMonth = 01;
        fundTxnHstyCtrlTest.FundsStartYear = 2009;
        fundTxnHstyCtrlTest.FundsEndMonth = 08;
        fundTxnHstyCtrlTest.FundsEndYear = 2008;
        fundTxnHstyCtrlTest.checkDate = true;
        //If Future Date
        fundTxnHstyCtrlTest.FundsStartMonth = 01;
        fundTxnHstyCtrlTest.FundsStartYear = 2008;
        fundTxnHstyCtrlTest.FundsEndMonth = 11;
        fundTxnHstyCtrlTest.FundsEndYear = 2009;
        
        fundTxnHstyCtrlTest.FundsName = 'BEAEH';
        fundTxnHstyCtrlTest.TransType = 'WDL';
        fundTxnHstyCtrlTest.FromSort = 'Fund_Price_Date__c';
        fundTxnHstyCtrlTest.SortBy = 'asc';
        fundTxnHstyCtrlTest.byPassCheck = true;
        fundTxnHstyCtrlTest.checkRecs = true;
        fundTxnHstyCtrlTest.fundTranMap = NULL;
        
       
        fundTxnHstyCtrlTest.constructorExt();
        fundTxnHstyCtrlTest.getPolicyNum();
        fundTxnHstyCtrlTest.getFundName();
        fundTxnHstyCtrlTest.getPolicyName();
        fundTxnHstyCtrlTest.getCashflowFundTransType();
        fundTxnHstyCtrlTest.getUtilizedFundTransType();
        fundTxnHstyCtrlTest.getFundTran();
        fundTxnHstyCtrlTest.getFundStartMonth();
        fundTxnHstyCtrlTest.getFundEndMonth();
        fundTxnHstyCtrlTest.getFundStartYear();
        fundTxnHstyCtrlTest.getFundEndYear();
        fundTxnHstyCtrlTest.getFundSort();
        fundTxnHstyCtrlTest.getFundSortBy();
        fundTxnHstyCtrlTest.getPolicyBack();
        fundTxnHstyCtrlTest.getfundYearPrevious();
        fundTxnHstyCtrlTest.Submit();
        fundTxnHstyCtrlTest.getPolicyBack();
        fundTxnHstyCtrlTest.pageRecords();
        fundTxnHstyCtrlTest.buildPageCount();
        fundTxnHstyCtrlTest.pagination();
        fundTxnHstyCtrlTest.onPageChange();
        fundTxnHstyCtrlTest.getcurFundTran();
        fundTxnHstyCtrlTest.getpagesOpt();
        
        fundTxnHstyCtrlTest.policyVal = configUtils.formatNameWithPolicyId(testPolicy.Client__r.FirstName, testPolicy.Client__r.LastName, testPolicy.Policy_Number__c, testPolicy.Client__r.First_Name_Chinese__c,testPolicy.Client__r.Last_Name_Chinese__c);
        
        fundTxnHstyCtrlTest.setDisp(false);
        fundTxnHstyCtrlTest.getDisp();
        
        
    }
}