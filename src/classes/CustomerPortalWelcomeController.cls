/*
Version : 1.5
Company : Sun Life Financial
Date    : <Date of Creation>
Author  : <Author's Name>
Description : Controller for the Customer Portal Home Page. 
              Displays the Customer's Policies, Coverages and Statements
History : 
              <07/27/09 - JC NACHOR   -  Fixed sorting of Policies in My Policies grid by 
                                         policy Issue Date in Descending, then by Name in  initPolicyStmt() 
          1.1 <10/19/09 - Wcheng      -  Added additional checking old browsers. If its lower version, there will an info. message
                                         that will be displayed on the page>
          1.2 <11/11/09               -  Added custom label for 'No Policy found' and 'No Statement found'
          1.3 <12/22/09 - R Angeles   -  Change restricted billing code reference from system lookup to config translation
          1.4 <05/04/12 - Steph So    -  SR-ES-12037 SAIII  
          1.5 <19/11/10 & 25/07/12 - Mikka Elepano/Steph So - Too many SOQL;
          1.6 <23/05/13               - SR-ES-13005 ES PDPO P2 Steph So
*/

public without sharing class CustomerPortalWelcomeController {
    public String UserId = UserInfo.getUserId(); 
    public String UserName = UserInfo.getName();
    User u = [Select id,contactId,Contact.AccountId from User where id = :UserId];
    public String AccountId = u.Contact.AccountId;
    /*public String policyMessage='No Policies found';*/
    public String policyMessage= Label.HOM_NO_POLICY_FOUND;
    /*public String statementMessage='No Statements found';*/
    public String statementMessage= Label.HOM_NO_STMT_FOUND;
    /* JC NACHOR 07/27/09 - initializes myPolicies */
    public List<Policy__c> myPolicies = new List<Policy__c>();
    public boolean showPolicy=true;
    public List<Statement__c> myStatements;
    public boolean showStatements=true;
    public List<customPolicyObject> listCPO = new List<customPolicyObject>();
    public Config_Utilities CU = new Config_Utilities();
    public String displayCPTVal; //SAIII - Steph So - 05/04/2012
    public String cptVal; //SAIII - Steph So - 05/04/2012
    
    /* JC NACHOR 07/27/09 - added Set<Id> setIds */
    Set<Id> setPolIds = new Set<Id>();
    
    public CustomerPortalWelcomeController() {
       initPolicyStmt();
    }
    
    List<Config_System_Lookups__c> listCSLTranslate = new List<Config_System_Lookups__c>();
    
    public String getPortalUserName()
    {
        String strValue = '';
        strValue = CU.formatName(UserInfo.getFirstName(), UserInfo.getLastName());
        return strValue;
    }
    public void initPolicyStmt() {
        
        /* JC NACHOR 07/24/09 - changed implementation in retrieving Policy__c because Map.values() removes sorting */
        //Map<Id, Policy__c> policyMap=new Map<Id, Policy__c>([Select Plan_Code__c, Policy_Inforce_Date__c,  Name, Restrict_Billing_Code_2__c, Policy_Number__c, Id, Payment_Mode_Picklist__c, Application_Sign_Date__c, Policy_Status_Picklist_Hidden__c, Application_Received_Date__c, policy_issue_date__c From Policy__c where OwnerId = :u.Id and (Policy_status_hidden__c = '1' or Policy_Status_Hidden__c = '2' or Policy_Status_Hidden__c = '3' or Policy_Status_Hidden__c = '4') order by Name  limit 200]);       
        // myPolicies = policyMap.values();
        
        system.debug('user id inside method deb:'+ u.Id);
                        
        myPolicies = [Select Plan_Code__c, Policy_Inforce_Date__c,  Name, Restrict_Billing_Code_2__c, Policy_Number__c, Id, Payment_Mode_Picklist__c, Application_Sign_Date__c, Policy_Status_Picklist_Hidden__c, Application_Received_Date__c, policy_issue_date__c, Policy_Status_Hidden__c 
                      From Policy__c 
                      where OwnerId = :u.Id and Active__c = true and (Policy_Status_Hidden__c = '1' or Policy_Status_Hidden__c = '2' or Policy_Status_Hidden__c = '3' or Policy_Status_Hidden__c = '4') 
                      order by Policy_Issue_date__c desc, Name 
                      limit 200];                       
        
        system.debug('myPolicies size deb:'+ myPolicies.size());
        system.debug('myPolicies deb:'+ myPolicies);
        
        // get Ids from retrieved Policy__c records and put in setPolIds to replace Ids used in policyMap
        for(Policy__c policyTemp : myPolicies){
            setPolIds.add(policyTemp.Id);
        }
        
        if(myPolicies.size()==0) 
        {   
            showPolicy=false;
        }
        
        Map<String, String> planCodeMap=new Map<String, String>();
        Map<ID, List<String>> covTermMap=new  Map<ID, List<String>>(); //SAIII - Steph So - 05/04/2012
        Map<ID, String> insuredMap=new Map<ID, String>();
        String[] planCodes=new String[0];   
        
        //Map to store valid plan codes for each policy
        Map<ID, List<String>> validPolicyPlanCodeMap=new Map<ID, List<String>>();   
        //Map to store coverage payment terms for each coverage
        //Insured Names
        System.debug('+>>> Welcome POL IDS: ' + setPolIds);
        Coverage__c[] coverageList=new Coverage__c[0];
        coverageList=[Select Plan_Code__c, X2nd_Life_Insured__r.Name, X2nd_Life_Insured__r.FirstName, 
                        X2nd_Life_Insured__r.LastName, X1st_Life_Insured__r.Name, X1st_Life_Insured__r.FirstName, 
                        X1st_Life_Insured__r.LastName, Policy__c,
                        Contribution_Payment_Term__c  //SAIII - Steph So - 05/04/2012
                        From Coverage__c c
                        /* JC NACHOR 07/27/09 - changed source of Ids from policyMap.keySet() into setPolIds */ 
                        //where Policy__c IN: policyMap.keySet() 
                        where Policy__c IN: setPolIds
                        and c.Coverage_Status__c in ('1', '2', '3', '4') and Active__c = true
                        Order By Policy__c, Name];
        System.debug('+>>> Welcome COV RECS: ' + coverageList);
        for(Coverage__c coverage:coverageList) 
        {
            if(!validPolicyPlanCodeMap.containsKey(coverage.Policy__c)) {
                validPolicyPlanCodeMap.put(coverage.Policy__c, new String[]{coverage.Plan_Code__c});            
            } else {
                validPolicyPlanCodeMap.get(coverage.Policy__c).add(coverage.Plan_Code__c);
            }
            if(!covTermMap.containsKey(coverage.Policy__c)) {
                covTermMap.put(coverage.Policy__c, new String[]{coverage.Contribution_Payment_Term__c});            
            } else {
                covTermMap.get(coverage.Policy__c).add(coverage.Contribution_Payment_Term__c);
            }
            planCodes.add(coverage.Plan_Code__c);   
            if(!insuredMap.containsKey(coverage.Policy__c)) {
                System.debug('error');
                String strTempName='';
                if(replaceNull(coverage.X1st_Life_Insured__r.Name)!='')
                {
                    strTempName = CU.formatName(replaceNull(coverage.X1st_Life_Insured__r.FirstName), replaceNull(coverage.X1st_Life_Insured__r.LastName));

                }
                if(replaceNull(coverage.X2nd_Life_Insured__r.Name)!='') 
                {
                    strTempName +=', '+ CU.formatName(replaceNull(coverage.X2nd_Life_Insured__r.FirstName), replaceNull(coverage.X2nd_Life_Insured__r.LastName));
                
                }               
                insuredMap.put(coverage.Policy__c, strTempName);
            } 
        }
                
        System.debug('+>>> Welcome PLANCODES: ' + planCodes);
        /* Plan Code Retrieval */
        String[] newPlanCodes=new String[0];
        Set<String> removalPlanCodes=new Set<String>();
        Set<String> removalPlanCodes_new=new Set<String>();
        Set<String> finalRemovalPlanCodes=new Set<String>();
        
        //R Angeles 12-22-2009: new process for restricted billing code, via config translation lookups
        //                    : moved policy status translation outside the loop
        Display_Translator[] tempResBillCode = new Display_Translator[]{};
        Map<String, String>  billingCodeMap  = new Map<String, String>();
        
        Display_Translator[] tempPolStatCode = new Display_Translator[]{};
        Map<String, String>  polStatMap     = new Map<String, String>();
        
        for(Policy__c policy:myPolicies) {
            tempResBillCode.add(new Display_Translator('RESBILLCD2', policy.Restrict_Billing_Code_2__c));
            tempPolStatCode.add(new Display_Translator('SYS_LOOKUP_MST', 'POLSTAT_'+policy.Policy_Status_Hidden__c));
        }
        billingCodeMap = Display_Translator.TranslateMap(tempResBillCode);
        polStatMap     = Display_Translator.TranslateMap(tempPolStatCode);
        
        for(Config_Func_Plan_Map__c tempObj:[select Plan_Code__c from Config_Func_Plan_Map__c where Plan_Code__c IN :planCodes and Lookup_Key__c='SKIP_FST_COV' order by Lookup_Key__c desc])
            if(!removalPlanCodes.contains(tempObj.Plan_Code__c)) removalPlanCodes.add(tempObj.Plan_Code__c);
        
        for(Config_Func_Plan_Map__c tempObj:[select Plan_Code__c from Config_Func_Plan_Map__c where Plan_Code__c IN :removalPlanCodes and Lookup_Key__c='REPLACE_FORB_COV']) {
            removalPlanCodes_new.add(tempObj.Plan_Code__c);
        }
        
        for(String tempVal:removalPlanCodes) {
            if(!removalPlanCodes_new.contains(tempVal)) finalRemovalPlanCodes.add(tempVal);
        }
        System.debug('+>>> Welcome FINAL PLANCODES: ' + finalRemovalPlanCodes);
        for(String pcode:planCodes)
            if(!finalRemovalPlanCodes.contains(pcode)) newPlanCodes.add(pcode);
      
        // Plan Name
        Config_Product_Data__c[] planCodesList=[Select Plan_Name__c, Plan_Code__C from Config_Product_Data__c where Plan_Code__C IN: newPlanCodes order by Plan_Code__c];
        for(Config_Product_Data__c planCode:planCodesList) 
            if(!planCodeMap.containsKey(planCode.Plan_Code__c)) planCodeMap.put(planCode.Plan_Code__c, planCode.Plan_Name__c);
        
        // Populate Class   
        //Too many SOQL & SAIII - Steph So - 7/25/2012 - start
        Display_Translator[] ltTemp1 = new Display_Translator[]{};
        Display_Translator[] tempPlanName = new Display_Translator[]{};
        List<Config_Translation_Lookups__c> translationList = new List<Config_Translation_Lookups__c>();
        
        Display_Translator[] tempCPTVal = new Display_Translator[]{};
        Map<String,String> mapCPT = new Map<String,String>();
                
            
        for(Policy__c p:myPolicies)
        {
        if(validPolicyPlanCodeMap.get(p.Id) != null) {
            System.debug('+>>> Welcome p.Id: ' + p.Id);
            System.debug('+>>> Welcome POLICY PLAN CODES: ' + validPolicyPlanCodeMap.get(p.Id));
            System.debug('+>>> Welcome PLAN CODE MAP: ' + planCodeMap);
            System.debug('+>>> Welcome PLAN CODE MAP SIZE: ' + planCodeMap.size());
             for(Integer x=0; x< validPolicyPlanCodeMap.get(p.Id).size(); x++) 
                {
                    if(planCodeMap.containsKey(validPolicyPlanCodeMap.get(p.Id)[x])) 
                    {
                    System.debug('+>>> Welcome PLAN CODE NAME: ' + validPolicyPlanCodeMap.get(p.Id)[x]);
                    tempPlanName.add (new Display_Translator('PLAN_NAME', validPolicyPlanCodeMap.get(p.Id)[x]));
                    tempCPTVal.add (new Display_Translator(validPolicyPlanCodeMap.get(p.Id)[x], covTermMap.get(p.Id)[x]));
                    System.debug('+>>> Welcome LIST PLAN NAME: ' + tempPlanName);
                    break;
                    }
                }   
            
            }
        }
        translationList = Display_Translator.TranslateMe(tempPlanName);
        mapCPT = Display_Translator.TranslateCPTMapHomePage(tempCPTVal);
        Map<String,String> mainCPTMap = new Map<String,String>();
        Map<String,String> ltPNameMap = new Map<String,String>();
        Display_Translator[] ltPlanNameTemp = new Display_Translator[]{};
        
        //Too many SOQL & SAIII - Steph So - 7/25/2012 - end
        
        for(Policy__c p:myPolicies) 
        {   
            customPolicyObject cPO = new customPolicyObject(p.Policy_Issue_Date__c);
            cPO.strPolicyNumber = p.Policy_Number__c;
            //if (billingCodeMap.get(p.Restrict_Billing_Code_2__c)!=null) 
            //{
            //  p.Policy_Status_Picklist_Hidden__c = p.Policy_Status_Picklist_Hidden__c + ' ' + billingCodeMap.get(p.Restrict_Billing_Code_2__c);
            //} 
            //else 
            //{
            //  if(p.Policy_Status_Picklist_Hidden__c==null) 
            //  {
            //      p.Policy_Status_Picklist_Hidden__c = 'N/A';
            //  }
            //}
            //cPO.strPolicyStatus = p.Policy_Status_Picklist_Hidden__c;
            //RBA 07272009: FOR TRANSLATION
            //if(cPO.strPolicyStatus!='N/A'){
                    //listCSLTranslate = [select Lookup_Value__c 
                    //                  from Config_System_Lookups__c where Config_Area__c='POLSTAT' 
                    //                  and Lookup_Description__c = :cPO.strPolicyStatus limit 1];
                    //if(listCSLTranslate.size() > 0){
                        //cPO.strPolicyStatus = Display_Translator.genericTranslation('SYS_LOOKUP_MST', 'POLSTAT_'+p.Policy_Status_Hidden__c);
                        cPO.strPolicyStatus   = polStatMap.get('POLSTAT_'+p.Policy_Status_Hidden__c);
                        //R Angeles 12-22-2009: Added restricted billing code value to policy status
                        if(billingcodeMap.get(p.Restrict_Billing_Code_2__c) != null){
                            cPO.strPolicyStatus += ' ' + billingcodeMap.get(p.Restrict_Billing_Code_2__c); 
                        }           
                    //}
                    if(cPO.strPolicyStatus == '')cPO.strPolicyStatus = 'N/A';//p.Policy_Status_Picklist_Hidden__c;
            //}
            /*
            try
            {
                for(String tempPolicyCode:validPolicyPlanCodeMap.get(p.Id)) {
                    if(planCodeMap.containsKey(tempPolicyCode)) {
                        cPO.strPlanName = planCodeMap.get(tempPolicyCode);
                        break;
                    }
                }
            }
            catch (Exception e)
            {}
            */
                if(validPolicyPlanCodeMap.get(p.Id) != null)
                {
                    System.debug('+>>> Welcome p.Id: ' + p.Id);
                    System.debug('+>>> Welcome POLICY PLAN CODES: ' + validPolicyPlanCodeMap.get(p.Id));
                    System.debug('+>>> Welcome PLAN CODE MAP: ' + planCodeMap);
                    System.debug('+>>> Welcome PLAN CODE MAP SIZE: ' + planCodeMap.size());
                    for(Integer x=0; x< validPolicyPlanCodeMap.get(p.Id).size(); x++) 
                    {
                        cPO.strPlanName = '&nbsp;';
                        if(validPolicyPlanCodeMap.get(p.Id)[x] != null)
                        {
                            if(planCodeMap.containsKey(validPolicyPlanCodeMap.get(p.Id)[x])) 
                            {
                                //RBA 07272009: FOR TRANSLATION
                                //System.debug('+>>> Welcome PLAN CODE NAME: ' + validPolicyPlanCodeMap.get(p.Id)[x]);
                                //Too many SOQL & SAIII - Steph So - 7/25/2012 - start
                                Display_Translator elemPlanName = new Display_Translator('PLAN_NAME',validPolicyPlanCodeMap.get(p.Id)[x]);
                                ltPlanNameTemp.add(elemPlanName);
                                cPO.strPlanName = validPolicyPlanCodeMap.get(p.Id)[x];
                                String primaryKey = validPolicyPlanCodeMap.get(p.Id)[x] + '||' + covTermMap.get(p.Id)[x];
                                if (mapCPT.containsKey(primaryKey))
                                {
                                    if(!mainCPTMap.containsKey(cPO.strPolicyNumber)) {
                                        mainCPTMap.put(cPO.strPolicyNumber, mapCPT.get(primaryKey));            
                                    } else {
                                        mainCPTMap.get(cPO.strPolicyNumber);
                                    }
                                }
                                break;
                                /*//SAIII - Steph So - 05/04/2012 - start
                                String pName;
                                pName = Display_Translator.genericTranslation('PLAN_NAME', validPolicyPlanCodeMap.get(p.Id)[x]);
                                String cptVal='';
                                String planCode = validPolicyPlanCodeMap.get(p.Id)[x];
                                Coverage__c[] covList = new Coverage__c[0];
                                    covList=[Select Contribution_Payment_Term__c 
                                    From Coverage__c c 
                                    where Policy__c =: p.Id
                                    and c.Coverage_Status__c in ('1', '2', '3', '4') and Active__c = true
                                    Order By Policy__c, Name];
                                //if(covTermMap.containsKey(planCode)){
                                cptVal = ' ' + Display_Translator.cptTranslation(planCode, covList[0].Contribution_Payment_Term__c);//covTermMap.get(planCode)); //planCode, covTerm
                                    //if (cptVal != '000') cPO.strPlanName += cptVal;
                                //}
                                if(pName==''){
                                    pName = planCodeMap.get(validPolicyPlanCodeMap.get(p.Id)[x]);
                                }
                                displayCPTVal = getRenderIfSAProduct(planCode); 
                                if (displayCPTVal == 'true' && covList[0].Contribution_Payment_Term__c != '0')
                                {
                                    pName += cptVal;
                                }
                                cPO.strPlanName = pName;
                                //SAIII - Steph So - 05/04/2012 - end
                                break;*/
                            }
                        }
                    }   
                }           
            if(validPolicyPlanCodeMap.get(p.Id) == null)
            {
                cPO.strPlanName = '&nbsp;';
            }
            cPO.strInsuredName = '&nbsp;';
            if(insuredMap.get(p.Id) != null)
            {
                cPO.strInsuredName = insuredMap.get(p.Id);
            }
            listCPO.add(cPO);
        }
        
        ltPNameMap = Display_Translator.TranslateMap(ltPlanNameTemp);        
        for (customPolicyObject cPO :listCPO){
            if(ltPNameMap != null){
                if(cPO.strPlanName != null){
                    String tempPName = ltPNameMap.get(cPO.strPlanName);
                    if (tempPName != ''){
                        displayCPTVal = getRenderIfSAProduct(cPO.strPlanName); 
                        if (displayCPTVal == 'true')// && cptVal != null)
                        {
                            cptVal = 'val';
                        }
                        else {
                            cptVal = null;
                        }
                        if(mainCPTMap.containsKey(cPO.strPolicyNumber) && cptVal != null )
                        {
                            cptVal = mainCPTMap.get(cPO.strPolicyNumber);
                            tempPName = tempPName + ' ' + cptVal;
                        }
                        cPO.strPlanName = tempPName;
                    }
                    else{
                        cPO.strPlanName = '';
                    }
                }
            }
        }
        //Too many SOQL & SAIII - Steph So - 7/25/2012 - end
        
        // Statements
        myStatements=[Select s.Id, s.Statement_Type__c, s.Statement_From_Date__c, s.Statement_To_Date__c, s.Read__c,
                        s.Read_Count__c, s.Policy__c, p.Name, s.Name, s.Attachment_Id__c, 
                        p.Policy_Number__c, p.RecordType.name 
                        From Statement__c s, s.Policy__r p 
                        /*JC NACHOR 07/27/09 - changed source of Ids from policyMap.keySet() into setPolIds
                                              - added and Attachment_Deleted__c = false to filter only those statements with attachments*/ 
                        where p.Id IN: setPolIds and Attachment_Deleted__c = false
                        order by s.Statement_From_Date__c desc 
                        limit 200];
        if(myStatements.size()==0) showStatements=false;
        
    }
    public String replaceNull(String strVal) {
            if(strVal==null) return '';
            return strVal;
    } 
    public Boolean getShowPolicy() {
        return showPolicy;
    }
    public Boolean getShowStatements() {
        return showStatements;
    }
    public String getPolicyMessage() {
        return policyMessage;
    }
    public String getStatementMessage() {
        return statementMessage;
        
    }
    public List<customPolicyObject> getMyPolicies() {
        return listCPO;
    }
    public List<Statement__c> getMyStatements() {
        System.debug('STATEMENTS:'+myStatements);
        return myStatements;
    }
    /* Custom Policy Place Holder */
    public class customPolicyObject
    {
        public String strPolicyNumber  {get; set;}
        public String strPlanName {get; set;}   
        public String strInsuredName  {get; set;}
        public String strPolicyStatus {get; set;}
        public String strPolicyDate {get; set;} 
        public customPolicyObject(Date dateInputPolicyDate)
        {
            if(dateInputPolicyDate != null)
            {
                strPolicyDate = String.valueOf(dateInputPolicyDate.format());
            }
            strPolicyNumber = '&nbsp;';
            strPlanName = '&nbsp;';
            strInsuredName = '&nbsp;';
            strPolicyStatus = '&nbsp;';
        }   
    }
    /* Custom Statement Place Holder */
    public class customStatementObject
    {
        public Id idStatementId {get; set;}
        public String strStatementName {get; set;}
        public Date dateStatementFromDate {get; set;}
        public String strStatementFromDate {get; set;}
        public Date dateStatementToDate {get; set;}
        public String strStatementToDate {get; set;}
        public String strStatementDate {get; set;}
        public Id idAttachmentId {get; set;} 
        public Double doubReadCount {get; set;}
        public String strReadCount {get; set;}
        public String strPolicyName {get;set;}
        public String strPolicyNumber {get;set;}
        public String strPolicyRecordType {get;set;}
        public Boolean bRead {get;set;}
        public customStatementObject(ID idInputStatementId, String strInputStatementName,  
                                     Date dateInputStatementFromDate, Date dateInputStatementToDate,
                                     Id idInputAttachmentId, Double doubleInputReadCount, 
                                     String strInputPolicyName, String strInputPolicyNumber,
                                     String strInputRecordType, Boolean bInputRead)
        {
            bRead = bInputRead;
            strStatementDate = '&nbsp;';
            strStatementToDate = '&nbsp;';
            strStatementFromDate = '&nbsp;';
            idStatementId = idInputStatementId; 
            strStatementName = strInputStatementName;
            dateStatementFromDate = dateInputStatementFromDate;
            dateStatementToDate = dateInputStatementToDate;
            if(dateInputStatementToDate != null)
            {
                if (strInputRecordType != 'Traditional')
                {               
                    strStatementToDate = String.valueOf(dateStatementToDate.format());
                }
            }
            idAttachmentId = idInputAttachmentId;
            strPolicyNumber = strInputPolicyNumber;
            strPolicyRecordType = strInputRecordType;
            if (strInputRecordType == 'Traditional')
            {
                if (dateStatementFromDate != null)
                {
                    strStatementDate = String.valueOf(dateStatementFromDate.year());
                }
            }
            if (dateStatementFromDate != null)
            {       
                if (strInputRecordType != 'Traditional')
                {
                    strStatementFromDate = String.valueOf(dateStatementFromDate.format());
                }       
                
            }
            doubReadCount = doubleInputReadCount;
            strReadCount =  String.valueOf(doubleInputReadCount);
            strPolicyName=strInputPolicyName;
        } 
    }   
    public customStatementObject[] getMyCustomStatements() 
    {
        CustomStatementObject[] stmtlist=new CustomStatementObject[0];
        for(Statement__c stmt:myStatements) {
            customStatementObject statement = new customStatementObject(
                                                            stmt.id,
                                                            stmt.name,
                                                            stmt.Statement_From_Date__c,
                                                            stmt.Statement_To_Date__c,
                                                            stmt.Attachment_Id__c,
                                                            stmt.Read_Count__c,
                                                            stmt.Policy__r.Name, 
                                                            stmt.Policy__r.Policy_Number__c,
                                                            stmt.Policy__r.RecordType.Name,
                                                            stmt.Read__c);  
                stmtlist.add(statement);
        }
        return stmtlist;
    }
    /* Read counter update on Statements */
    String strStatementId;
    public String getStatementRecordIdValue()  
    {
        return strStatementId;
    }   
    public void setStatementRecordIdValue(String n)  
    {
        strStatementId = n;
    }
    public PageReference updateReadCount() 
    {
        Statement__c stmt=[select id, Read_count__c, First_Read_Date__c, Last_Read_Date__c,
                           Read__c  
                           from Statement__c where id=: strStatementId for update];
        DateTime dtToday = DateTime.now();
        DateTime dtFirstRead;
        if(stmt.First_Read_Date__c == null)
        {
            dtFirstRead = dtToday;
        }
        else
        {
            dtFirstRead = stmt.First_Read_Date__c;
        }   
        stmt.Read_Count__c = stmt.Read_Count__c+1;
        stmt.Read__c = true;
        stmt.First_Read_Date__c = dtFirstRead;
        stmt.Last_Read_Date__c = dtToday;
        update stmt;
        return null;
    }
    public PageReference checkDisclaimerNeeded()
    {           
          User u = [select Id, Client_Id__c, Last_Disclaimer_Accept_Time__c, LastLoginDate, Last_Password_Change_Date__c
                    from user where id = :UserInfo.getUserId()];
          //S - MARSO - 20130523 - SR-ES-13005:
    Account a = [select Id, Opt_Out__c, Opt_Out_DT__c, Opt_Out_Prev__c, Opt_Out_DT_Prev__c from Account where Client_Id__c = :u.Client_Id__c];

        if (u.Last_Disclaimer_Accept_Time__c >= a.Opt_Out_DT_Prev__c) //accept button clicked
        {
            a.Opt_Out__c = a.Opt_Out_Prev__c;
            a.Opt_Out_DT__c = a.Opt_Out_DT_Prev__c;
            update a;
        }
        else if (u.Last_Disclaimer_Accept_Time__c < a.Opt_Out_DT_Prev__c)
        {
            a.Opt_Out_Prev__c = a.Opt_Out__c;
            a.Opt_Out_DT_Prev__c = a.Opt_Out_DT__c;
            update a;
        }
    //E - MARSO - 20130523: - SR-ES-13005

          if (u.Last_Disclaimer_Accept_Time__c > u.LastLoginDate)
          {
                return null;
          }
          else
          {
                PageReference pRef = new PageReference('/apex/Disclaimer');
                pRef.setRedirect(true);
                
                return pRef;
          }
    }
    
    //START ADD Enhancement Wcheng 10/19/09
    public boolean getCheckBrowser()
    {
        Config_Global_Settings__c browserValue = [Select c.Check_Browser__c From Config_Global_Settings__c c];
        
        return browserValue.Check_Browser__c;
    }
    
    public string getSecurityPage()
    {
        string NavURL;
        
        NavURL = ESPortalNavigator.NAV_URL_HLP_SEC_MAIN;
        
        return NavURL;
    }
    //END ADD Enhancement Wcheng 10/19/09
    
    //SAIII - Steph So - 05/04/2012 - start
    public String getRenderIfSAProduct(String planCode)
    {
        String RenderIfSAProduct;
        planCode = '\'' + planCode + '\'';
        List<Config_Product_Data__c> displaySAPaymentTerm = new List<Config_Product_Data__c>();
        String mainQuery = 'Select DISPLAY_SA_PAYMENT_TERM__C from Config_Product_Data__c where Plan_Code__c =' + planCode;
        displaySAPaymentTerm = Database.query(mainQuery);
        for (Config_Product_Data__c c: displaySAPaymentTerm)
        {
            RenderIfSAProduct = String.valueOf(c.DISPLAY_SA_PAYMENT_TERM__C);
        }
     
        return RenderIfSAProduct;
    }
    //SAIII - Steph So - 05/04/2012 - end    
    
    public static testmethod void testCustomerPortalWelcomeController() {
        
        
        RecordType transacType;
        try{
            transacType =[Select Id, Name from RecordType where Name ='Person Account'];
        }catch(Exception e){
            system.debug('Error while retrieving record type. '+e);
        }
        
        Account accnt1 = new Account(FirstName = 'Test', LastName = 'UserA', Client_ID__pc='Test1111', PersonEmail = 'a@b.com',
                                        RecordTypeId=transacType.Id);
        insert accnt1;
        
        Account accnt2 = new Account(FirstName = 'Test', LastName = 'UserB', Client_ID__pc='Test2222', PersonEmail = 'a@b.com',
                                        RecordTypeId=transacType.Id);
        insert accnt2;
        
        Policy__c policy= new Policy__c(OwnerId = UserInfo.getUserId(), 
                                        Policy_Status_Hidden__c = '1', 
                                        Plan_Code__c = 'FSLH0A',
                                        Policy_Inforce_Date__c = date.today(),
                                        Policy_Number__c = '12345',
                                        Payment_Mode_Picklist__c = 'CQ',
                                        Application_Sign_Date__c = date.today(),
                                        Policy_Issue_date__c = date.today(),
                                        Active__c = true,
                                        Client__c = accnt1.Id);
        insert policy;
        
        system.debug('policy deb:'+ policy);
        
        Policy__c polTemp = [Select OwnerId from Policy__c where Id =:policy.Id];
        system.debug('Policy Return: '+polTemp);

        
        
        Coverage__c coverage = new Coverage__c(Plan_Code__c = 'FSLH0A', 
                                                X2nd_Life_Insured__c = accnt1.Id, 
                                                X1st_Life_Insured__c = accnt2.Id,
                                                Coverage_Status__c = '1',
                                                Active__c = true, 
                                                Policy__c = policy.Id);
        insert coverage;
        
        CustomerPortalWelcomeController mycontroller=new CustomerPortalWelcomeController();
        mycontroller.getMyPolicies();
        mycontroller.getShowPolicy();
        mycontroller.getMyStatements();
        mycontroller.getShowStatements();
        mycontroller.getStatementMessage();
        mycontroller.getPolicyMessage();
        mycontroller.getPortalUserName();
        //mycontroller.checkDisclaimerNeeded();
        mycontroller.setStatementRecordIdValue('ID');
        mycontroller.replaceNull('Testing');
        //mycontroller.checkDisclaimerNeeded();
        mycontroller.getStatementRecordIdValue();
        mycontroller.getMyCustomStatements();
        customPolicyObject policyObject = new customPolicyObject(date.today());
        customStatementObject statementObj = new customStatementObject(accnt1.Id, 'Test',  
                                     date.today(), date.today(),
                                     accnt2.Id, 2.0, 
                                     'Policy1', '123545',
                                     'Traditional', true);
        
        
    } 
}