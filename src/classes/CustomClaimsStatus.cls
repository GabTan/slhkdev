/*
Version : 1.0
Company : Sun Life Financial
Date    : 27.July.2009
Author  : Wilson Cheng
Description : This is used for temporary container for saving and retrieving data for view claim status history.
History : 
*/

public class CustomClaimsStatus
{
    public string claimNumber { get; private set; }
    
    public string claimType { get; private set; }
    
    public string claimStatus { get; private set; }
    
    public string payAmnt { get; private set; }
    
    public string payMethod { get; private set; }
    
    public string approvalDate { get; private set; }
    
    public string minorClaims { get; private set; }
    
    public Id payNotice { get; private set; }
    
    /* Class constructor */
    public CustomClaimsStatus (string clmNo, string clmType, string clmStat, string pAmnt, string pMethod, string apprvDate, String mClaims, Id pNotice)
    {
        claimNumber = clmNo;
        claimType = clmType;
        claimStatus = clmStat;
        payAmnt = pAmnt;
        payMethod = pMethod;
        approvalDate = apprvDate;
        minorClaims = mClaims;
        payNotice = pNotice;
    }
}