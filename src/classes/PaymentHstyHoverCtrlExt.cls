/* 
Version		: 1.0
Company		: Sun Life Financial
Date		: 13.JUL.2009
Author		: Robert Andrew Almedina
Description	: This class acts as a controller for the paymet history's (Bank Autopay/Credit Card Autopay) 
				 minipages for the amount to be displayed in the right format
History	:

*/ 

public with sharing class PaymentHstyHoverCtrlExt {
	
	public String HKD { get; set;}
	public String Credit_Card ='';
		
	private final Payment_History__c phFinal;
	public Payment_History__c PHToDisplay;
	
	// JC NACHOR 08/25/2009 - uncommented constructor because error occured in deployment, 
	// missing constructor in PaymentHstyHoverCtrlExt_Test
	public PaymentHstyHoverCtrlExt()
	{
	}
	
	public PaymentHstyHoverCtrlExt(ApexPages.StandardController stdController)
	{	 
		this.phFinal = (Payment_History__c)stdController.getRecord();
		PHToDisplay = [Select p.Usage_Code__c, p.Reject_Reason_Description__c, p.Reject_Reason_Code__c, p.Received_Date__c, p.Rec_Status__c, p.Rec_No__c, p.Premium_Due_Date__c, p.Post_Status__c, p.Policy_Id__c, p.Policy_Current_Amount__c, p.Policy_Currency__c, p.Payment_Type__c, p.Payment_Method__c, p.Payment_Id__c, p.Payment_Amount__c, p.Name, p.Means_Type__c, p.Last_Update__c, p.Id, p.Exchange_Rate__c, p.Error_Log__c, p.Draw_Date__c, p.DDA_Required__c, p.CurrencyIsoCode, p.Credit_Card_Type__c, p.Credit_Card_Account_No__c, p.CC_Account_No_Masked__c, p.Bank_Confirmation_Status__c, p.Bank_Account_No_Masked__c, p.Autopay_Branch_ID__c, p.Autopay_Bank_Name__c, p.Autopay_Bank_ID__c, p.Autopay_Account_No__c, p.App_No__c, p.Active__c From Payment_History__c p Where p.Id = :phFinal.Id Limit 1];
	}
	
	public CustomPaymentHistoryWrapper getPHToDisplay()
	{
		Map<String, String> currencyMap = CurrencyCodeHandler.getTranslatedCurrencyCodes(); 
		HKD = Display_Translator.genericTranslation('SYS_LOOKUP_MST', 'CURRENCY_HKD');
		return (new CustomPaymentHistoryWrapper(PHToDisplay, PHToDisplay.Payment_Amount__c, PHToDisplay.Received_Date__c, currencyMap.get(PHToDisplay.CurrencyIsoCode)));
	}
	
	public String getCreditCard(){
		Credit_Card = Display_Translator.genericTranslation('CRC_CARDTYPE', PHToDisplay.Credit_Card_Type__c);
		system.debug('Test CC_type: '+PHToDisplay.Credit_Card_Type__c);
		system.debug('Test CC: '+Credit_Card);
		return Credit_Card;
	}

}