/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 19.AUG.2009
Author		: W. Clavero
Description	: Test Coverage for XMLDataHandler.
History		: 
*/
@isTest
private class XMLDataHandler_Test {

    static testMethod void myUnitTest() {
        String str = 'This, is a comma,, delimited ; value,';
        //escape the commas
        str = XMLDataHandler.escapeXMLString(str);
        //reverse the escaping of commas
        str = XMLDataHandler.unescapeXMLString(str);
    }
}