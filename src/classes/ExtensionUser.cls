/*
Company : Sun Life Financial
Date    : 8/17/09
Author  : Wilson Cheng
Description : This class inserts a user extension after inserting a record in User Object.
History : 
*/
public class ExtensionUser 
{
	  @future
    public static void insertUserExtension (List<Id> policyRec)
    {   
    	List<User_Extension__c> tempUserExt = new List<User_Extension__c>();
    	 	
    	for(integer i = 0; i < policyRec.size(); i++)
    	{
    		User_Extension__c userExt = new User_Extension__c();
    		    		
	        userExt.User__c = policyRec[i];
	        userExt.Reset__c = FALSE;
	        userExt.User_Deactivated__c = FALSE;
	        userExt.OwnerId = policyRec[i];
	        
	        tempUserExt.add(userExt);
    	}
    	
    	if(tempUserExt.size() > 0)
    	{
    	   insert tempUserExt;
    	}
    }
    
    static testMethod void testExtensionUser() 
    {
    	List<id> userId = new List<id>();
    	User userTemp = new User();
    	
    	Profile profileTemp = [select id from Profile Limit 1];
    	
    	userTemp.FirstName = 'testUser';
    	userTemp.LastName = 'testUser';
    	userTemp.Username = 'testUser2009@test.com';
    	userTemp.Email = 'testEmail@test.com';
    	userTemp.IsActive = TRUE;
    	userTemp.Alias = 'test';
    	userTemp.TimeZoneSidKey = 'Asia/Hong_Kong';
    	userTemp.LocaleSidKey = 'en_US';
    	userTemp.EmailEncodingKey = 'ISO-8859-1';
    	userTemp.ProfileId = profileTemp.id;
    	userTemp.LanguageLocaleKey = 'en_US';
    	
    	insert userTemp;
    	
        userId.add(userTemp.id);
    	
    	ExtensionUser.insertUserExtension(userId);
    }
}