/* 

		Version		: 1.4
		Company		: Sun Life Financial
		Date		: AUG.2009
		Author		: Robby Angeles
		Description	: Source class for building the Switch Out table for Fund Switching
		History		: 
					  1.3  -  17.SEP.2009 - Robby Angeles          Bug # 35 -   Updated NumOfUnits from Double to Decimal type for scaling 
																				Added attribute fswstrNumOfUnits for proper formatting
																				(Updates needed on class FundSwitchInputCtrl and page FundSwitchInput)
																				
					  1.4  -  10.NOV.2009 - Robby Angeles          CR: 			Internal and External Fund References, added attribute fswExtFundCode
					  1.5  -  26.MAR.2012 - Mikka Elepano          SR:          Sun Future Enhancements, added the ff attributes: fswMaxEstRem,fswMinEstRem and fswSOInd  																				
*/ 

public with sharing class CustomSwitchInOut {

    public String  fswFundId                        {get; set;}
    public Boolean fswIsSelected                    {get; set;}
    public String  fswFundName                      {get; set;}
    public String  fswTranslatedFundName            {get; set;}
    public String  fswFundCode                      {get; set;}
    public String  fswExtFundCode                   {get; set;}
    public Integer fswSwitchOut                     {get; set;}
    public String  fswStrSwitchOut                  {get; set;}
    public Decimal fswNumOfUnits                    {get; set;}
    public String  fswstrNumOfUnits                 {get; set;}
    public String  fswCurr                          {get; set;}
    public String  fswLatestFundPrice               {get; set;}
    public Decimal fswFundValue                     {get; set;}
    public String  fswstrFundValue                  {get; set;}
    public Decimal fswEstSwitchOutAmt               {get; set;}
    public String  fswstrEstSwitchOutAmt            {get; set;}
    public Decimal fswEstRemFundValue               {get; set;}
    public String  fswstrEstRemFundValue            {get; set;}
    public Decimal fswMaxEstRem                     {get; set;}
    public String  fswStrMaxEstRem                  {get; set;}
    public Decimal fswMinEstRem                     {get; set;}
    public String  fswStrMinEstRem                  {get; set;}
    public Boolean fswSOInd                         {get; set;}

    public CustomSwitchInOut(String FundId, Boolean IsSelected, String FundName, String FundCode, String ExtFundCode, Integer SwitchOut, Decimal NumOfUnits, String strNumOfUnits, 
        String Curr, String LatestFundPrice, Decimal FundValue,String strFundValue, Decimal EstSwitchOutAmt, String strEstSwitchOutAmt, Decimal EstRemFundValue, String strEstRemFundValue,
        Decimal MaxEstRem,String StrMaxEstRem,Decimal MinEstRem,String StrMinEstRem,Boolean SOInd ){
        
        fswFundId             = FundId;
        fswIsSelected         = IsSelected;
        fswFundName           = FundName;
        fswFundCode           = FundCode;
        fswExtFundCode        = ExtFundCode;
        fswStrSwitchOut       = String.valueOf(SwitchOut)=='0'?'':String.valueOf(SwitchOut);
        fswNumOfUnits         = NumOfUnits;
        fswstrNumOfUnits      = strNumOfUnits;
        fswCurr               = Curr;
        fswLatestFundPrice    = LatestFundPrice;
        fswFundValue          = FundValue;
        fswstrFundValue       = strFundValue;
        fswEstSwitchOutAmt    = EstSwitchOutAmt.setScale(2);
        fswstrEstSwitchOutAmt = strEstSwitchOutAmt;
        fswEstRemFundValue    = EstRemFundValue.setScale(2);
        fswstrEstRemFundValue = strEstRemFundValue;
        fswMaxEstRem          = MaxEstRem.setScale(2);
        fswStrMaxEstRem       = StrMaxEstRem;
        fswMinEstRem          = MinEstRem.setScale(2);
        fswStrMinEstRem       = StrMinEstRem;
        fswSOInd              = SOInd;
        
    }
    
}