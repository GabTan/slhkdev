public class PolicyOwnership {
	
	public static String SMS_REMINDER_31_OVERDUE = 'Payment Overdue 31 Days';
	public static String SMS_REMINDER_15_OVERDUE = 'Payment Overdue 15 Days';
	private static List<SMS_Template__c> smsTemplates = null;
	
	
	public static void resetPortalUserLastPasswordChangedDate(Map<Id,Account> oldAccountMap, Map<Id,Account> newAccountMap)
	{
		List<Id> contactIdChangedList = new List<Id>();
		for (Id id: newAccountMap.keySet())
		{
			Account oldAccount = oldAccountMap.get(id);
			Account newAccount = newAccountMap.get(id);
			
			if (newAccount.e_Registration_Status__pc == 'Resend' && oldAccount.e_Registration_Status__pc != 'Resend')
			{
				// Status has changed - 
				if (newAccount.IsPersonAccount)
				{
					if (newAccount.PersonContactId != null)
					{
						contactIdChangedList.add(newAccount.PersonContactId);
					}			
				}
			}
		}
		
		if (contactIdChangedList.size() > 0)
		{
			// find the customer portal users linked to these accounts
			List<User> portalUsers2Update = [select Id, ContactId, IsActive, Last_Password_Change_Date__c from User where ContactId IN: contactIdChangedList];
			
			List<ID> users2Update = new List<Id>();
			for(User u: portalUsers2Update)
			{
				users2Update.add(u.Id);
			}
			
			System.debug('Found '+users2Update.size()+' users to update');
			if (users2Update.size() > 0)
			{
				PolicyOwnership.futureUpdateUser_Last_Password_Change_Date(users2Update);
			}
		}
	}
	
	@future
	public static void futureUpdateUser_Last_Password_Change_Date(List<ID> userIds)
	{
			// find the customer portal users linked to these accounts
			List<User> portalUsers2Update = [select Id, ContactId, IsActive, Last_Password_Change_Date__c from User where Id IN: userIds];
			
			for(User user: portalUsers2Update)
			{	
				user.Last_Password_Change_Date__c = null;
			}
			
			System.debug('Updating '+portalUsers2Update.size()+' users');
			if (portalUsers2Update.size() > 0)
			{
				Database.update(portalUsers2Update);
			}
	}
	
	private static Boolean policyPremium_Due_DateChanged(Policy__c newPolicy, Policy__c oldPolicy)
	{
	    if (oldPolicy.Premium_Due_Date__c == null && newPolicy.Premium_Due_Date__c == null)
        {
            return false;
        }
        
        if (oldPolicy.Premium_Due_Date__c == null && newPolicy.Premium_Due_Date__c != null)
        {
            return true;
        }
        
        if (oldPolicy.Premium_Due_Date__c != null && newPolicy.Premium_Due_Date__c == null)
        {
            return true;
        }
        
        if (oldPolicy.Premium_Due_Date__c != newPolicy.Premium_Due_Date__c)
        {
        	return true;
        }
           
        return false;
	}
	
	public static testmethod void testPolicyPremiumDueDateCheck() {
		Policy__c p1=new Policy__c();
		Policy__c p2=new Policy__c();
		 
		System.assert(PolicyOwnership.policyPremium_Due_DateChanged(p1, p2)==false, 'Failed here ...1');
		
		p2.Premium_Due_Date__c=date.today();
		System.assert(PolicyOwnership.policyPremium_Due_DateChanged(p1, p2)==true, 'Failed here ...2');
		
		p1.Premium_Due_Date__c=date.today();
		p2.Premium_Due_Date__c=null;
		System.assert(PolicyOwnership.policyPremium_Due_DateChanged(p1, p2)==true, 'Failed here ...3');
		
		p2.Premium_Due_Date__c=date.today()+1;
		System.assert(PolicyOwnership.policyPremium_Due_DateChanged(p1, p2)==true, 'Failed here ...4');
		
		p2.Premium_Due_Date__c=date.today();
		System.assert(PolicyOwnership.policyPremium_Due_DateChanged(p1, p2)==false, 'Failed here ...5');

	}
	
	public static TestMethod void testmask()
	{
		List<String> testPolIds = new List<String>();
		
		testPolIds.add('');
		testPolIds.add('1');
		testPolIds.add('11');
		testPolIds.add('222');
		testPolIds.add('333');
		testPolIds.add('4444');
		testPolIds.add('55555');
		testPolIds.add('666666');
		testPolIds.add('7777777');
		testPolIds.add('88888888');
		testPolIds.add('999999999');
		testPolIds.add('123456789000');
		
		String sms = 'Your Policy <number> is 31 days overdue';	
		
		for (String s: testPolIds)
		{
			System.debug(s+ ' becomes '+maskPolicyId(s));
			System.debug(sms+ ' becomes '+replaceWild(sms,maskPolicyId(s)));	
		}
	}
	
	private static String maskPolicyId(String polId)
	{
		if (null == polId)
		{
			return '';
		}
		
		String retPolid = '';
		Integer unMaskLen = 3;
		Integer maskLen = polId.length() - unMaskLen;
		
		for (Integer i = 0; i < polId.length(); i++)
		{
			if (i < maskLen)
			{
				retPolid += '*';
			}
			else
			{
				retPolid += polId.substring(i);
				break;
			}
		}
		
		return retPolid;
	}
	
	public static testMethod void testMaskPolicyId() {
		System.assert(PolicyOwnership.maskPolicyId(null)=='');
		System.assert(PolicyOwnership.maskPolicyId('44778')=='**778');
	}
	
	private static String replaceWild(String inStr, String mask)
	{
		return inStr.replace('<number>',mask);
	}
	
	private static String getSMSText(String lang, String templateType)
	{
		// We should Look at SMS_Template__c Table 
		
		// Select s.SMS_Type__c, s .SMS_Text__c, s.Language__c From SMS_Template__c s
		// Default to English for now!
		// Later look at Language field on User - or Contact?
		
		if (smsTemplates == null)
		{
			smsTemplates =  [Select SMS_Type__c, SMS_Text__c, Language__c From SMS_Template__c];
		}
		
		for (SMS_Template__c smsT : smsTemplates)
		{
			if (smsT.SMS_Type__c == templateType && smsT.Language__c == lang)
			{
				return smsT.SMS_Text__c;
			}
		}
		
		return null;
	}
	
	private static Boolean isActivePortalUser(List<User> users, Id personContactId)
	{
		for(User u: users)
		{
			
			if (u.ContactId == personContactId)
			{
				return u.isActive;
			}
		}
		
		return false;
	}
	
	public static void sendReminders(Map<ID,Policy__c> newPolicyValues, Map<ID,Policy__c> oldPolicyValues)
	{
		System.debug('In Send Reminders - number of records: '+newPolicyValues.size());
		// Put Send SMS Logic In here - 
		
		// p.Client__r.IsPersonAccount, p.Client__r.Id, p.Client__c
		
		Map<Id, Policy__c> relatedClientsMap = new Map<Id, Policy__c>([Select Id, p.Client__c, a.PersonContactId From Policy__c p, p.Client__r a where p.Client__r.IsPersonAccount = true and Id in :newPolicyValues.keySet()]);
		
		Set<Id> tempPersonContactIds=new Set<Id> ();
		for(Policy__c p: relatedClientsMap.values())
		{
			tempPersonContactIds.add(p.Client__r.PersonContactId);	 
		}
		
		List<User> validPortalUsers = [select Id, ContactId, IsActive from User where ContactId IN: tempPersonContactIds];
		
		// TODO - need to make sure this is a Active Portal User - 
		// This is  a safety net, as will add this logic in CastIron too
				
		System.debug('Found '+relatedClientsMap.size()+' related Person Accounts');
		
		// TODO
		// With a list we can the side effect of the ability to send multiple SMSs to the same client in this code
		// List<Contact> contactUpdateList = new List<Contact>();
		List<Account> personAccountUpdateList = new List<Account>();
		
		for (ID id: newPolicyValues.keySet())
		{
			Policy__c newPolicy = newPolicyValues.get(id);
			Policy__c oldPolicy = oldPolicyValues.get(id);
			// Add check to perform this only for Active Portal Users
			// check if it is a PersonAccount and an Active Portal User 
			// if((relatedClientsMap.get(id)!=null)&&(validPortalUsers.contains((relatedClientsMap.get(id)).Client__r.PersonContactId))) 
			
			if(relatedClientsMap.get(id)!=null && (isActivePortalUser(validPortalUsers, relatedClientsMap.get(id).Client__r.PersonContactId)))
			{	
				Policy__c linkedClient = relatedClientsMap.get(id);
				
				/* Old code commented by Dharamvir
				if (null == linkedClient)
				{					
					// not a person account - 
					System.debug('Not Linked to a Person Account');
					continue;
				}
				*/
				
				String smsText = null;
				
				System.debug('Send 15: '+newPolicy.Send_15_Day_Payment_Due_Reminder__c);
				System.debug('Send 31: '+newPolicy.Send_31_Day_Payment_Due_Reminder__c);
				
				
				// We Always Set These Back to False
				if (newPolicy.Send_15_Day_Payment_Due_Reminder__c)
				{
					newPolicy.X15_Day_Payment_Due_Reminder_Sent__c = true;
					smsText = getSMSText('English', SMS_REMINDER_15_OVERDUE);
				}
				//else
				//{
					// Inside an Else Statement because - 
					// These flags should not both be set at the same time!!
					if (newPolicy.Send_31_Day_Payment_Due_Reminder__c)
					{
						// Put Send SMS Logic In here - 
						newPolicy.X31_Day_Payment_Due_Reminder_Sent__c = true;
						smsText = getSMSText('English', SMS_REMINDER_31_OVERDUE);
					}
				//}
				
				if (null != smsText)
				{
					// Take Policy Numer 123456789
					// Mask The Value to give ******789				
					// Merge the masked policy Number into the Message.
					// Update the Related Contact Record 
					// Set Contact SMS_Text__c = text
					// Set Contact Send_SMS__c = true
					
					Account acc = new Account(Id=linkedClient.Client__c);
					// Contact c = new COntact(Id=linkedClient.Client__r.PersonContactId);
					smsText = replaceWild(smsText, maskPolicyId(newPolicy.Policy_Number__c));
					
					// Maybe put Sqware Peg fields here--
					acc.Send_SMS__pc = true;
					acc.SMS_Text__pc = smsText;
					personAccountUpdateList.add(acc);
					// contactUpdateList.add(c);
				}
				
				// We assume that this date does not change in the above code! 
				// or update transaction....
				if (policyPremium_Due_DateChanged(newPolicy,oldPolicy))
				{
					newPolicy.X31_Day_Payment_Due_Reminder_Sent__c = false;
					newPolicy.X15_Day_Payment_Due_Reminder_Sent__c = false;
				}
			}
			
			newPolicy.Send_15_Day_Payment_Due_Reminder__c = false;
			newPolicy.Send_31_Day_Payment_Due_Reminder__c = false;
		} // end for
		
		System.debug('Need to update '+personAccountUpdateList.size()+' contacts who will receive SMS');
		
		if (personAccountUpdateList.size() > 0)
		{
			Database.update(personAccountUpdateList);
		}
	}
	
    public static void updatePolicyOwner(Policy__c[] policyList) {
    
        // Map to store a list of Policy Objects for each Client as Key/Value pairs
        // for easy update
        Map<ID, Policy__c[]> policyClientMap=new Map<ID, Policy__c[]>();
        
        System.debug('Policy List has:'+policyList.size()+' objects');
        
        //Loop thru all the Policy objects being Created/Update to create the PolicyClient Map
        for(Policy__c policyObj:policyList) {
            // Add a new Key (Client ID) if it does not exist already
            System.debug('Policy Client is:'+policyObj.client__c);
            if (!policyClientMap.containsKey(policyObj.Client__c)) {
                policyClientMap.put(policyObj.Client__c, new List<Policy__c>{policyObj});
            } 
            // Add Policy Object to the existing array for the Client 
            else {
                policyClientMap.get(policyObj.Client__c).add(policyObj);
            }
        }
        
        System.debug('Policy Client Map'+policyClientMap);
        
        // Run a query to retrieve User IDs for all active users associate with these Clients (Customer Portal Users)
        for(User userObj:[Select u.Id, c.AccountId from User u, u.Contact c where IsActive=TRUE and c.AccountId IN: policyClientMap.keySet()]) {
          	System.debug('Policy user Obj'+userObj.Id);
          	System.debug('Policy Client Map Get Item'+userObj.Contact.AccountId);
          	System.debug('Policy Client Map Item'+policyClientMap.get(userObj.Contact.AccountId));  
            updatePolicyOwner(policyClientMap.get(userObj.Contact.AccountId), userObj);
        }
        
        // 
    }   
    
    /*
    
    //OLD
    @future
    public static void updatePolicyOwner(ID userId) {
        /*
        // The '@future' method updates the policy objects associated 
        // with a customer portal user being created/activated
        */
      /*
        // Get UserId and ContactId for the Active Customer Portal user
        User userObj=[select Id, ContactId from User where Id =: userId];
        // Retrieve all related Policy Objects for this User 
        Policy__c[] policyList=[Select p.Id, c.PersonContactId from Policy__c p, p.Client__r c where Policy__c.Client__r.PersonContactId = :userObj.ContactId for UPDATE];
        updatePolicyOwner(policyList, userObj);
        update policyList;
    }
    */
    
    
    //new
    @future
    public static void updatePolicyOwner(ID[] userIDList) {
        /*
        // The '@future' method updates the policy objects associated 
        // with a customer portal user being created/activated
        */
        // Get UserId and ContactId for the Active Customer Portal user
        User[] userList=[select Id, ContactId from User where Id IN : userIDList];
        
        ID[] contactIdList=new ID[0];
        
        for(User u:userList) contactIdList.add(u.ContactId);
        System.debug('updatePolicyOwner for users:'+userlist);
        // Retrieve all related Policy Objects for this User 
        Policy__c[] policyList=[Select p.Id, c.PersonContactId from Policy__c p, p.Client__r c where Policy__c.Client__r.PersonContactId IN :contactIdList for UPDATE];
        updatePolicyOwner(policyList);
        update policyList;
    }
    
    public static void updatePolicyOwner(Policy__c[] policyList, User u) {
        for(Policy__c policyObj:policyList) {
        	policyObj.ownerId=u.Id;
        	System.debug('Policy Owner set is:'+policyObj.ownerId);
        }
    }
    
    public static void changeAccountOwner(List<Account> acct)
    {
        Map<string, id> clientUserId = new Map<string, id>();
        Set<string> userId = New Set<string>();
        
        //put all the client id to one set which will be use for the query
        for(Account acctId: acct)
        {
            if(acctId.Client_Id__c != NULL)
            {
                userId.add(acctId.Client_Id__c);
            }
        }
        
        //query all user Id from user object
        List<User> tempUserId = [select Client_Id__c, Id from user where Client_Id__c IN:userId];
        
        for(User u:tempUserId)
        {
            clientUserId.put(u.Client_Id__c, u.Id);
        }
        
        //update the owner of the account
        for(Account a: acct)
        {
            if(a.Client_Id__c != NULL)
            {
                if (clientUserId.get(a.Client_Id__c) != NULL)
                {
                    a.OwnerId = clientUserId.get(a.Client_Id__c);
                }
            }
        }       
    }
    
    public static void changeAddressOwner(List<Address__c> addr)
    {
    	Map<string, id> policyId = new Map<string, id>();
    	Map<string, id> clientId = new Map<string, id>();
    	set<string> policyUserId = new set<string>();
    	set<string> clientUserId = new set<string>();
    	
    	//get the policy id
    	for(Address__c pol: addr)
    	{
    		policyUserId.add(pol.Policy__c);
    	}
    	
    	//get the clientUserId
        for(Address__c c: addr)
        {
            clientUserId.add(c.Account__c);
        }
        
        List<Policy__c> tempPol = [select Id, OwnerId from Policy__c where Id IN:policyUserId];

        clientId = retrieveClientId(clientUserId);
              
        for(Policy__c p: tempPol)
        {
        	policyId.put(p.Id, p.OwnerId);
        }
    	
    	for(Address__c a: addr)
    	{
    		if(a.Policy__c != NULL)
    		{
    			if(policyId.get(a.Policy__c) != NULL)
    			{
    			    a.OwnerId = policyId.get(a.Policy__c);
    			}
    		}
    		else if(a.Account__c !=  NULL)
    		{
    			if(clientId.get(a.Account__c) != NULL)
    			{
    			    a.OwnerId = clientId.get(a.Account__c);
    			}
    		}
    	}
    }
    
    public static Map<string, id> retrieveClientId(set<string> acctIds)
    {
    	List<Account> acctRec = [select id, Client_Id__c from Account where Id IN:acctIds];
    	Set<string> userId = New Set<string>();
    	Map<string, id> clientUserId = new Map<string, id>();
    	Map<string, id> acctUserId = new Map<string, id>();
    	
    	//put all the client id to one set which will be use for the query
        for(Account acctId: acctRec)
        {
            if(acctId.Client_Id__c != NULL)
            {
                userId.add(acctId.Client_Id__c);
            }
        }
        
        //query all user Id from user object
        List<User> tempUserId = [select Client_Id__c, Id from user where Client_Id__c IN:userId];
        
        for(User u:tempUserId)
        {
            clientUserId.put(u.Client_Id__c, u.Id);
        }
        
        //get the respective client id with the respective account
        for(Account a: acctRec)
        {
            if(clientUserId.containsKey(a.Client_Id__c))
            {
            	acctUserId.put(a.id, clientUserId.get(a.Client_Id__c));
            }
        }
        
    	return acctUserId;
    }
    
    // Mikka start 10/06/2011
    public static void ValidateInactivePolicyOwners(List<ID> userIDList)
    {
    	 List<Policy__c> InactivePolicyOwners = new List<Policy__c>();
    	 
	    // Query all Inactive Policy Owners
        InactivePolicyOwners = [Select p.OwnerId, p.Id 
                                       From Policy__c p 
                                       where Client__r.e_Registration_Status__pc != 'Deactivate' AND 
                                             Client__r.e_Registration_Status__pc != NULL AND 
                                             Client__r.e_Registration_Status__pc != '' AND
                                             Client__r.IsCustomerPortal = TRUE AND
                                             p.OwnerId != '00530000001lkDP'AND 
                                             Owner.IsActive = FALSE AND
                                             OwnerId IN :userIDList];
        
        // Reiterates the Policy object
        for (Policy__c InactiveOwner : InactivePolicyOwners)
        {
            // Set the Inactive Owner to SunLife Eservices Id
            InactiveOwner.OwnerId = '00530000001lkDP';
        }
        
        // Update the Owner to default SunLife Eservices
        if (InactivePolicyOwners.size() > 0) update InactivePolicyOwners;
    }
    // Mikka end 10/06/2011
    
    /* w. clavero: no longer applicable
    //w. clavero 20090922 - change account ownership coming from the User object
    public static void updateUserAccountOwnership(List<User> portalUsers) {
    	//get all users with Client IDs
    	Set<String> clientIDs = new Set<String>();
    	Map<String, ID> userClientMap = new Map<String, ID>(); 
    	for (User usr: portalUsers) {
    		if (usr.Client_Id__c != null) {
    			clientIDs.add(usr.Client_Id__c);
    			userClientMap.put(usr.Client_Id__c, usr.Id);
    		}
    	}
    	
    	try {
    		//get accounts that match the users' Client IDs
    		List<Account> clientRecs = [Select Id, Client_Id__c, OwnerId From Account Where Client_Id__c in :clientIDs];
    		
    		for (Account acct: clientRecs) {
    			ID usrID = userClientMap.get(acct.Client_Id__c);
    			if (acct.OwnerId != usrID) {
    				acct.OwnerId = usrID;
    			}
    		} 
    		//update the Account records
    		if (clientRecs.size() > 0) {
    			update clientRecs;
    		}
    	} catch (QueryException qe) {
    		//possibly exceeded Limit
    	} catch (DmlException dmle) {
    		//possibly exceeded Limit
    	} catch (Exception e) {
    		//unknown exception
    	}
    }
    */
    
    public static testMethod void testPolicyOwnership()
    {
    	Account acctTemp = new Account();
    	Policy__c policyTemp = new Policy__c();
    	User userTemp = new User();
    	Address__c addrTemp = new Address__c();
    	List<user> userList = new List<user>();
    	List<id> userIdList = new list<id>();
    	
    	User userTemp2 = new User();
		List<user> userList2 = new List<user>();
		List<id> userIdList2 = new list<id>();
    	
    	Profile profileTemp = [select id from Profile Limit 1];
        
        userTemp.FirstName = 'testUser';
        userTemp.LastName = 'testUser';
        userTemp.Username = 'testUser2009@test.com';
        userTemp.Email = 'testEmail@test.com';
        userTemp.IsActive = TRUE;
        userTemp.Alias = 'test';
        userTemp.TimeZoneSidKey = 'Asia/Hong_Kong';
        userTemp.LocaleSidKey = 'en_US';
        userTemp.EmailEncodingKey = 'ISO-8859-1';
        userTemp.ProfileId = profileTemp.id;
        userTemp.LanguageLocaleKey = 'en_US';
        userTemp.Client_Id__c = 'test1234';
        
        insert userTemp;
        
        userList.add(userTemp);
        
        User userIdtemp = [select id from User limit 1];
        userIdList.add(userIdtemp.id);
        
        userTemp2.FirstName = 'testUser2';
        userTemp2.LastName = 'testUser2';
        userTemp2.Username = 'testUser2011@test.com';
        userTemp2.Email = 'testEmail2@test.com';
        userTemp2.IsActive = FALSE;
        userTemp2.Alias = 'test2';
        userTemp2.TimeZoneSidKey = 'Asia/Hong_Kong';
        userTemp2.LocaleSidKey = 'en_US';
        userTemp2.EmailEncodingKey = 'ISO-8859-1';
        userTemp2.ProfileId = profileTemp.id;
        userTemp2.LanguageLocaleKey = 'en_US';
        userTemp2.Client_Id__c = 'test12345';
        
        insert userTemp2;
        
        userList2.add(userTemp2);
		
		User userIdtemp2 = [select id from User limit 1];
		userIdList2.add(userIdtemp2.id);
    	
    	RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Person Account'];
        
        acctTemp.FirstName = 'Wilson';
        acctTemp.LastName = 'Cheng';
        acctTemp.Client_Id__c = 'test';
        acctTemp.Client_ID__pc = 'test';
        acctTemp.CurrencyIsoCode = 'HKD';
        acctTemp.RecordTypeId = recordID.Id;
        acctTemp.Client_Id__c = 'test1234';
        acctTemp.PersonEmail = 'a@b.com';
        
        insert acctTemp;
        
        acctTemp.FirstName = 'Wilz';
        acctTemp.e_Registration_Status__pc = 'Resend';
        //acctTemp.IsPersonAccount = true;
        //acctTemp.PersonContactId = userTemp.Id;
        
        update acctTemp;
        
        recordID = [Select r.Name, r.Id From RecordType r where Name='Traditional'];
        
        policyTemp.Name = 'testPolicy';
        policyTemp.CurrencyIsoCode = 'HKD';
        policyTemp.Active__c = true;
        policyTemp.Policy_Number__c = '1234';
        policyTemp.RecordTypeId = recordID.Id;
        policyTemp.Policy_Status_Hidden__c = '1';
        
        insert policyTemp;
        
        policyTemp.Name = 'testPolicy';
        
        update policyTemp;
        
        addrTemp.Account__c = acctTemp.id;
        
        insert addrTemp;
        
        addrTemp.Policy__c = policyTemp.Id;
        
        update addrTemp; 
        
        PolicyOwnership.getSMSText(null,null);
        PolicyOwnership.isActivePortalUser(userList,userTemp.id);  
        PolicyOwnership.updatePolicyOwner(userIdList);
        PolicyOwnership.ValidateInactivePolicyOwners(userIDList2);
        //PolicyOwnership.futureUpdateUser_Last_Password_Change_Date(userIdList);
         	
    }
    /* w. clavero: no longer applicable
    //w.clavero 20090922
    public static testMethod void testUserAccountOwnership() {
    	Account acctTemp = new Account();
    	RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Person Account'];
        
        acctTemp.FirstName = 'Wilson';
        acctTemp.LastName = 'Cheng';
        acctTemp.Client_Id__c = 'test01234';
        acctTemp.Client_ID__pc = 'test01234';
        acctTemp.CurrencyIsoCode = 'HKD';
        acctTemp.RecordTypeId = recordID.Id;

        insert acctTemp;
    	
    	User userTemp = new User();
    	Profile profileTemp = [select id from Profile Limit 1];
    	
        userTemp.FirstName = 'Wilson';
        userTemp.LastName = 'Cheng';
        userTemp.Username = 'wilsc@test.com';
        userTemp.Email = 'wilsc@test.com';
        userTemp.IsActive = TRUE;
        userTemp.Alias = 'wilscx';
        userTemp.TimeZoneSidKey = 'Asia/Hong_Kong';
        userTemp.LocaleSidKey = 'en_US';
        userTemp.EmailEncodingKey = 'ISO-8859-1';
        userTemp.ProfileId = profileTemp.id;
        userTemp.LanguageLocaleKey = 'en_US';
        userTemp.Client_Id__c = 'test01234';
        
        insert userTemp;
    }
    */
}