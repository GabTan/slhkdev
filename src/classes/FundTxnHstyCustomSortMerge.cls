/*
	Version	   : 1.1
    Company    : Sun Life Financial
    Date       : 06.AUG.2009
    Author     : Walter Clavero, Marian Baleros
    Description: Combines and Sorts Fund Transaction History records.
    History    : 
    			 1.1	16.NOV.2009 - Robby Angeles  - Added sorting handle for maturity date
    
*/
public class FundTxnHstyCustomSortMerge {
	
	public static final String COLUMN_FUND_NAME 			= 'fundtransName';
	public static final String COLUMN_TXN_TYPE 				= 'transactType';
	public static final String COLUMN_MAT_DATE 				= 'maturityDate';	
	public static final String COLUMN_TRANSLATE_FUND 		= 'DPDesc';
	
	public Integer maxListSize = 500;
	
	public List<List<FundTxnHstyValues>> sortFundTxnHstyRecords(List<List<FundTxnHstyValues>> fthList, String stringField, Boolean sortAscending) {
		if (fthList != null) {
			if (fthList.size() > 1) {
				return sortMergeFundTxnHsty(fthList.get(0), fthList.get(1), stringField, sortAscending);
			} else {
				List<List<FundTxnHstyValues>> listF = new List<List<FundTxnHstyValues>>();
                List<FundTxnHstyValues> listG = new List<FundTxnHstyValues>(fthList.get(0));
                listF.add(stringSort(listG, stringField, sortAscending));
				
                return listF;
			} 
		} else {
			return null;
		}			
	}
	
    //handles the merging and sorting of lists
    public List<List<FundTxnHstyValues>> sortMergeFundTxnHsty(List<FundTxnHstyValues> listA, List<FundTxnHstyValues> listB, String stringField, Boolean sortAscending) {    
        if (listA != null && listB != null) {
            // if B > A, reverse the lists
            if (listB.size() > listA.size()) {
                return sortMergeFundTxnHsty(listB, listA, stringField, sortAscending);
            
            // both listA and listB contains more than 500 rows 
            } else if (listA.size() > maxListSize && listB.size() > maxListSize) {
                List<FundTxnHstyValues> listB_1 = getRange(listB, 0, maxListSize);
                List<FundTxnHstyValues> listB_2 = getRange(listB, maxListSize, maxListSize);
                List<List<FundTxnHstyValues>> listC = sortMergeFundTxnHsty(listA, listB_2, stringField, sortAscending);
                List<List<FundTxnHstyValues>> listD = sortMergeFundTxnHsty(listC.get(0), listB_1, stringField, sortAscending);
				
				if (listC.size() > 1) { //excess from 1,000
					if (listD.size() > 1) { //excess from 1,000
						List<List<FundTxnHstyValues>> listE = sortMergeFundTxnHsty(listD.get(1), listC.get(1), stringField, sortAscending);
						List<List<FundTxnHstyValues>> listF = new List<List<FundTxnHstyValues>>();
						listF.add(listD.get(0));
						listF.add(listE.get(0));
						
						return listF;
					} else {
						listD.add(listC.get(1));
						return listD;
					}
				} else {
					return listD;
				}
                
            // listA more than 500 rows, listB less than/equal 500 rows
            } else if (listA.size() > maxListSize) {
                List<FundTxnHstyValues> listA_1 = getRange(listA, 0, maxListSize);
                List<FundTxnHstyValues> listA_2 = getRange(listA, maxListSize, maxListSize);
                List<List<FundTxnHstyValues>> listD = sortMergeFundTxnHsty(listA_2, listB, stringField, sortAscending);
				
				//split the resulting list
				List<FundTxnHstyValues> listD_1 = getRange(listD.get(0), 0, maxListSize);
				List<FundTxnHstyValues> listD_2 = getRange(listD.get(0), maxListSize, maxListSize);
				List<List<FundTxnHstyValues>> listE = sortMergeFundTxnHsty(listA_1, listD_1, stringField, sortAscending);
				listE.add(listD_2);
				return listE;
            
            // both listA and listB contains less than 500 rows
            } else {
				List<List<FundTxnHstyValues>> listF = new List<List<FundTxnHstyValues>>();
                List<FundTxnHstyValues> listG = new List<FundTxnHstyValues>(listA);
                listG.addAll(listB);
                listF.add(stringSort(listG, stringField, sortAscending));
				
                return listF;
            }
        } else {
            return null;
        }
    }
    
    // list helper
    private List<FundTxnHstyValues> getRange(List<FundTxnHstyValues> srcList, Integer startIndex, Integer size) {
        List<FundTxnHstyValues> retList = new List<FundTxnHstyValues>();
        if(srcList != null && startIndex >= 0 && srcList.size() > startIndex) {
            Integer currIndex = startIndex;
            while (true) {
                if((retList.size() >= size) || (currIndex >= srcList.size()))
                    break;
                retList.add(srcList.get(currIndex++));
            }
        }
        return retList;
    }
    
    private List<FundTxnHstyValues> stringSort(List<FundTxnHstyValues> csList, String stringField, Boolean sortAscending) {
        List<FundTxnHstyValues> retList = new List<FundTxnHstyValues>();
        List<FundTxnHstyValues> nullsList = new List<FundTxnHstyValues>();
        // create a map to a collection
        Map<String, List<FundTxnHstyValues>> csMap = new Map<String, List<FundTxnHstyValues>>();
        //build the collection for sorting
        for(FundTxnHstyValues cs : csList) {
            String fldVal = null;
            if (stringField.equals(COLUMN_FUND_NAME)) {
                fldVal = cs.fundtransName;
            } else if (stringField.equals(COLUMN_TXN_TYPE)) {
                fldVal = cs.transactType;
            } else if (stringField.equals(COLUMN_MAT_DATE)) {
                fldVal = cs.maturityDate;
            }
            
            if (fldVal == null) {
                nullsList.add(cs);
            } else {
                // add to map
                if (csMap.get(fldVal) == null)
                    csMap.put(fldVal, new List<FundTxnHstyValues>());
                csMap.get(fldVal).add(cs);
            }
        }
        
        List<String> keys = new List<String>(csMap.keySet());
        keys.sort();
		
        if (sortAscending) {
            //add nulls first
            if (nullsList.size() > 0)
                retList.addAll(nullsList);
            //add sorted
            for (String key : keys)
                retList.addAll(csMap.get(key));     
        } else {
            //reverse sorted list
            for(Integer i = keys.size()-1; i >= 0; i--)
                retList.addAll(csMap.get(keys.get(i)));
            //add nulls last
            if (nullsList.size() > 0)
                retList.addAll(nullsList);
        }
        
        return retList;
    }
    
    public List<Display_Translator> translateFundNameSort(List<Display_Translator> csList, String stringField, Boolean sortAscending) {
        List<Display_Translator> retList = new List<Display_Translator>();
        List<Display_Translator> nullsList = new List<Display_Translator>();
        // create a map to a collection
        Map<String, List<Display_Translator>> csMap = new Map<String, List<Display_Translator>>();
        //build the collection for sorting
        for(Display_Translator cs : csList) {
            String fldVal = null;
            if (stringField.equals(COLUMN_TRANSLATE_FUND)) {
                fldVal = cs.DPDesc;
            }
            
            if (fldVal == null) {
                nullsList.add(cs);
            } else {
                // add to map
                if (csMap.get(fldVal) == null)
                    csMap.put(fldVal, new List<Display_Translator>());
                csMap.get(fldVal).add(cs);
            }
        }
        
        List<String> keys = new List<String>(csMap.keySet());
        keys.sort();
		
        if (sortAscending) {
            //add nulls first
            if (nullsList.size() > 0)
                retList.addAll(nullsList);
            //add sorted
            for (String key : keys)
                retList.addAll(csMap.get(key));     
        } else {
            //reverse sorted list
            for(Integer i = keys.size()-1; i >= 0; i--)
                retList.addAll(csMap.get(keys.get(i)));
            //add nulls last
            if (nullsList.size() > 0)
                retList.addAll(nullsList);
        }
        
        return retList;
    }
}