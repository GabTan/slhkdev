@isTest
private class testFTAController {
    static testMethod void testFTA()      
    {            
		FirstTimeAuthenticationController fta = new FirstTimeAuthenticationController();
		/*
		FirstTimeAuthenticationController.generateSecurityQuestion(false);
		FirstTimeAuthenticationController.generateRandom();	
		FirstTimeAuthenticationController.intSecurityGen = 0;
		FirstTimeAuthenticationController.generateSecurityQuestion(true);
		FirstTimeAuthenticationController.intSecurityGen = 1;
		FirstTimeAuthenticationController.generateSecurityQuestion(true);
		FirstTimeAuthenticationController.intSecurityGen = 2;
		FirstTimeAuthenticationController.generateSecurityQuestion(true);
		*/
		User[] usrIsActive = [select id, username, Client_Id__c 
								from User 
								where isActive = true 
								and isPortalEnabled = true 
								and Client_Id__c != null 
								limit 1];
		if(usrIsActive.size()>0)
		{
			SavePoint sv = Database.setSavepoint();
			User_Extension__c[] exts = [select User__c, id from User_Extension__c where User__c = :usrIsActive[0].id limit 1];
			User_Extension__c usrExtension = new User_Extension__c(); 
			Account[] accts = [select id, Client_Id__c, HKID__pc 
								from Account  
								where (Client_ID__pc = :usrIsActive[0].Client_Id__c
								or Client_ID__c = :usrIsActive[0].Client_Id__c) limit 1];
			if(exts.size()>0)
			{
				usrExtension = new User_Extension__c(
									User__c = usrIsActive[0].id, Reset__c = false, 
									id=exts[0].id, User_Deactivated__c = false);
				update usrExtension;
			}
			else
			{
				usrExtension = new User_Extension__c(User__c = usrIsActive[0].id, 
									Reset__c = false, User_Deactivated__c = false);
				insert usrExtension;				
			}
			if(accts.size()>0)
			{
				fta.checkSecurityAnswer(usrIsActive[0].Username, 'Legal I.D.', accts[0].HKID__pc);
				fta.checkSecurityAnswer(usrIsActive[0].Username, 'Legal I.D.', 'Wrong Legal Id');
				fta.checkSecurityAnswer(usrIsActive[0].Username, 'Policy I.D.', 'Wrong Policy Id');
				Policy__c[] policies = [select id, Policy_Number__c
										from Policy__c 
										where Policy_Status_Hidden__c in ('1', '2', '3', '4')
										and Client__c = :accts[0].id];
				if(policies.size()>0)
				{
					fta.checkSecurityAnswer(usrIsActive[0].Username, 'Policy I.D.', policies[0].Policy_Number__c);
				}						
				try{
					fta.doAuthentication();
				}catch (Exception ex){System.debug(String.valueOf(ex));}
				try{
					fta.doAuthentication();
				}catch (Exception ex){System.debug(String.valueOf(ex));}				
				fta.checkSecurityAnswer('x@ip-converge.com', 'Legal I.D.', accts[0].HKID__pc);
				
			}
			// User Extension Deactivated
			usrExtension.Reset__c  = false;
			usrExtension.User_Deactivated__c = true;
			update usrExtension;
			fta.checkSecurityAnswer(usrIsActive[0].Username, 'Legal I.D.', 'abcd');
			// User Extension Reset
			usrExtension.Reset__c  = true;
			usrExtension.User_Deactivated__c = false;
			update usrExtension;			
			fta.checkSecurityAnswer(usrIsActive[0].Username, 'Legal I.D.', 'abcd');
			// User Locked
			usrIsActive[0].Locked__c = true;
			update usrIsActive;			
			fta.checkSecurityAnswer(usrIsActive[0].Username, 'Legal I.D.', 'abcd');
			// Username not found
			try{
				fta.checkSecurityAnswer('Not found', 'Legal I.D.', 'abcd');
			}catch (Exception ex){System.debug(String.valueOf(ex));}
			// Overrides
			try{
				fta.bOverride1 = true;
				fta.bOverride2 = true;
				fta.bOverride3 = false;
				fta.doAuthentication();
			}catch (Exception ex){System.debug(String.valueOf(ex));}	
			try{
				fta.bOverride1 = false;
				fta.bOverride2 = true;
				fta.bOverride3 = false;
				fta.doAuthentication();
			}catch (Exception ex){System.debug(String.valueOf(ex));}
			try{
				fta.bOverride1 = true;
				fta.bOverride2 = false;
				fta.bOverride3 = false;
				fta.doAuthentication();
			}catch (Exception ex){System.debug(String.valueOf(ex));}
			try{
				fta.bOverride1 = true;
				fta.bOverride2 = true;
				fta.bOverride3 = true;
				fta.doAuthentication();
			}catch (Exception ex){System.debug(String.valueOf(ex));}
			try{
				fta.bOverride1 = true;
				fta.bOverride2 = false;
				fta.bOverride3 = true;				
				fta.checkSecurityAnswer('Not found', 'Legal I.D.', 'abcd');
			}catch (Exception ex){System.debug(String.valueOf(ex));}
												try{
				fta.bOverride1 = true;
				fta.bOverride2 = false;
				fta.bOverride3 = true;				
				fta.checkSecurityAnswer('Not found', 'Policy I.D.', 'abcd');
			}catch (Exception ex){System.debug(String.valueOf(ex));}
			//
			try{
				fta.intMaxNumberOfAttempts = 0;
				fta.intNumberOfFailedAttempts = 0;
				fta.doubFailedAttempts = 0;
			}catch (Exception ex){System.debug(String.valueOf(ex));}			
			Database.rollback(sv);
		}							
    }
}