public class TestPolicyInquiryControllerExtension {
	/* Unit Test */	
	public static testmethod void controllerTest() 
	{
		Date dateToday = System.today();
    	System.Debug('Unit Test: Policy Inquiry Controller');
		String rtName = 'Traditional';
		RecordType RecTypeTraditional = [select id, name from RecordType where SObjectType='Policy__c' and name=:rtName limit 1];
		rtName = 'FPUL';
		RecordType RecTypeFPUL = [select id, name from RecordType where SObjectType='Policy__c' and name=:rtName limit 1];
		rtName = 'UL';
		RecordType RecTypeUL = [select id, name from RecordType where SObjectType='Policy__c' and name=:rtName limit 1];
		rtName = 'Person Account';	
		RecordType RecTypePersonAccount = [select id, name from RecordType where SObjectType='Account' and name=:rtName limit 1];		
		/* Success Set */
		Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
											PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
											FirstName='AcME', LastName='GX', RecordTypeId =  RecTypePersonAccount.id, 
											Client_Id__c = '12345xoxox', Client_ID__pc = '12345xoxox'
										 );	
		insert accountSuccessTest;
		
		Account accountTest = [Select Id,Client_Id__c,OwnerId from Account where IsCustomerPortal = TRUE and e_Registration_Status__pc= 'Create' and Owner.IsActive = TRUE limit 1];
			
		Address__c addressSuccessTest = new Address__c(Address_Id__c = 'abc123',
											Address_1__c = 'a', Address_2__c = 'a', Address_3__c = 'a', Address_4__c = 'a',
											Address_5__c = 'a', Address_6__c = 'a', Address_7__c = 'a', Address_8__c = 'a',
											Address_Status__c = 'C');
		insert addressSuccessTest;							 
		Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountTest.Id, policy_number__c = '1234562009',
											RecordTypeId = RecTypeTraditional.id, Policy_Status_Picklist_Hidden__c = 'INFORCE', 
											Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
											Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
											Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
											Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
											Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
											Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
											Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = 'Not E',
											Branch_Number__c = '234', Account_Number__c = 'Account 1', 
											Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
											Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
											Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
											Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
											Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
											Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
											Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
											Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
											Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
											Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
											Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
											Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
											Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
											Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
											Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00,
											OwnerId = accountTest.OwnerId 									
											);      
		insert policySuccessTest;
		Agent__c agentSuccessTest = new Agent__c( Servicing_Agent_First_Name__c = 'Gilbert', Servicing_Agent_Last_Name__c = 'de Leon',
										   Agent_Status__c = 'A', Policy__c = policySuccessTest.id);	
		insert agentSuccessTest;
		Agent__c agentSuccessTest1 = new Agent__c( Servicing_Agent_First_Name__c = 'Gilbert',
										   Agent_Status__c = 'A', Policy__c = policySuccessTest.id);	
		insert agentSuccessTest1;		
		Agent__c agentSuccessTest2 = new Agent__c( Servicing_Agent_First_Name__c = 'G', Servicing_Agent_Last_Name__c = 'de Leon',
										   Agent_Status__c = 'A', Policy__c = policySuccessTest.id);	
		insert agentSuccessTest2;			
		Beneficiary__c beneficiarySuccessTest = new Beneficiary__c( Beneficiary_Name__c = 'May Ajero', Designation__c = 'Chemist',
																	Relationship_to_Insured__c = 'Wife', Trustee_Name__c = 'G',
																	Percentage__c = 20.00, Revocable__c = 'IRREVOCABLE',
																	Policy__c = policySuccessTest.id 
																  ); 
		insert beneficiarySuccessTest;
		Coverage__c coverageSuccessTest = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'SLCH0A', Coverage_Number__c = '01', Name='COV01',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest;
		Coverage__c coverageSuccessTest2 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'LD1H0A', Coverage_Number__c = '02', Name='COV02',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest2;	
		Coverage__c coverageSuccessTest3 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'FJPU0B', Coverage_Number__c = '03', Name='COV03',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest3;
		Coverage__c coverageSuccessTest4 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'ABCDEF', Coverage_Number__c = '04', Name='COV04',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest4;	
		Coverage__c coverageSuccessTest5 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'ECEHAA', Coverage_Number__c = '05', Name='COV05',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest5;
		Coverage__c coverageSuccessTest6 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'RISH0A', Coverage_Number__c = '06', Name='COV06',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest6;
		Coverage__c coverageSuccessTest7 = new Coverage__c(Policy__c = policySuccessTest.id, Coverage_Status__c = '1', 
											Plan_Code__c = 'AB2H1A', Coverage_Number__c = '07', Name='COV07',
											Coverage_Face_Amount__c = 123.00, Coverage_Sum_Insured_Amt__c = 123.00);
		insert coverageSuccessTest7;											
		Attachment attachmentSuccessTest = new Attachment(ParentId = policySuccessTest.id, 
												Name='Attachment 1', Body = Blob.valueOf('test'));
		insert attachmentSuccessTest;
		Statement__c statementSuccessTest = new Statement__c(Policy__c = policySuccessTest.id, 
												Attachment_ID__c = String.valueOf(attachmentSuccessTest.id));
		insert statementSuccessTest;
		Fund_Detail__c fundSuccessTest1 = new Fund_Detail__c(Policy__c = policySuccessTest.id, Fund_Value__c = 100.00, Valuation_Date__c = null, 
												Fund_Code__c ='BULH0A', Unit_Valuation_Date__c = System.today());
		insert fundSuccessTest1;
		Fund_Detail__c fundSuccessTest2 = new Fund_Detail__c(Policy__c = policySuccessTest.id, Fund_Value__c = 100.00,
												Valuation_Date__c = System.today());
		insert fundSuccessTest2;
		//11-24-2009 Robby Angeles: Added CHM_Min_Percentage field
		Config_Product_Data__c cpd = new Config_Product_Data__c(Plan_Code__c = 'abc123', Show_Total_Available_Balance__c = true,
																Coverage_Show_AOB_101__c = true,
																CHM_Min_Percentage__c	 = 20
														 		);
		insert cpd;						   									
		/* Set the request parameter */
        PageReference pref = Page.PolicyInquiry;
        pref.getParameters().put('policyID', policySuccessTest.policy_number__c);
  		Test.setCurrentPage(pref); 
 		/* Instantiate Controller */      	 	 
		ApexPages.StandardController conSuccess = new ApexPages.StandardController(policySuccessTest);
        PolicyInquiryControllerExtension extSuccess = new PolicyInquiryControllerExtension(conSuccess); 	
        /* Call Inner Classes */ 
        customBeneficiaryObject cBObject = new customBeneficiaryObject('strInputBeneficiaryName', 'strInputDesignation', 
        								'strInputRevocable', 'strInputRelationshipToInsured', 123.00, 'strInputTrusteeName');       								
        customCoverageObject cCObject = new customCoverageObject('strInputBenefit_Coverage','strCurrencyISOCode', 123.00, 
										123.00, accountSuccessTest.id, accountSuccessTest.id,'strPlan_Code', 'strLookup_Key',
										'strFirstThreeLetterPlan_Code', 'strCoverage_Status', System.today(), 'COV01', 
										'strPlan_Class', 'W', System.today(), NULL); 								     
 		customFundObject cFObject = new customFundObject( 'strInputFundName', 'strInputCurrencyISOCode', 123.00, 123.00, 
 										System.today(), 123.00, 123.00, System.today(), 123.00, 'GFTEST', 
 										'Pending indicator', System.today());													
 		customStatementsObject cSObject = new customStatementsObject(statementSuccessTest.id, 'strInputStatementName', 
 										'strInputCurrencyISOCode', System.today(), System.today(), attachmentSuccessTest.id, 123.00,'Traditional');									
 		cCObject.strBenefit_Coverage = ''; 
		cCObject.strBenefit_Coverage = '';  
		cCObject.strAmountOfBenefit = ''; 
		cCObject.strDeathBenefit = ''; 
		cCObject.strInsured_Name = ''; 
		cCObject.strPlanCode = ''; 
		cCObject.strLookupKey = 'FACE_AMT'; 
		cCObject.strFirstThreeLetterPlanCode = ''; 
		cCObject.idFirstInsuredName = null;  
		cCObject.idSecondInsuredName = null;  
		cCObject.strCoverageStatus = ''; 
		cCObject.dateMaturityDate = System.today(); 
		cCObject.iHiddenCoverageNumber = 1;  								        																	   
		/* Call Methods */
		extSuccess.Security();
		extSuccess.getLastModifiedDateValue();
		extSuccess.getModalPremiumRegularContributionLabel();
		extSuccess.getFirstBasicCoverageValue();
		extSuccess.getPolicyNameValue();
		extSuccess.getRenderAgents();
		extSuccess.getRecordTypeValue();
		extSuccess.getPolicyNameValue();
	 	extSuccess.getPolicyStatusValue();
		extSuccess.getPolicyInforceDateValue();
		extSuccess.getPolicyIssueDateValue();
		extSuccess.getCurrencyISOCodeValue();
		extSuccess.getPremiumDueDateValue();
		extSuccess.getPolicyInsuranceTypeValue();
		extSuccess.getPaymentMethodValue();
		extSuccess.getPaymentModeValue();
		extSuccess.getConsultantNameValue();
		extSuccess.getDeathBenefitOptionValue();
		extSuccess.getPolicyOwnerNameValue();
		extSuccess.getAccountMobileNumberValue();
		extSuccess.getAccountHomeNumberValue();
		extSuccess.getAccountBusinessNumberValue();
		extSuccess.getAccountPersonEmailValue();
		extSuccess.getMailingAddressValues();
		extSuccess.getBankAccountNumberValue();
		extSuccess.getLastAutopayDateValue();
		extSuccess.getDebitDateValue();
		extSuccess.getCreditCardNumberValue();
		extSuccess.getModalPremiumRegularContribution();
		extSuccess.getDividendOptionValue();
		extSuccess.getCouponOptionValue();
		extSuccess.getDividendBalanceValue();
		extSuccess.getCouponFundSuperDividendBalanceValue();
		extSuccess.getPDFBalanceValue();
		extSuccess.getECEJCIBalanceValue();
		extSuccess.getPUACashValue();
		extSuccess.getLoanAmountValue();
		extSuccess.getPremiumOnAccountBalanceValue();
		extSuccess.getGuaranteedCashValue();
		extSuccess.getMaturityDividendBalanceValue();
		extSuccess.getMaturityBonusValue();	
		extSuccess.getMaxLoanAmountValue();
		extSuccess.getTotalValue();
		extSuccess.getTotalAvailableBalanceValue();
		extSuccess.getTotalUnavailableBalanceValue();
		extSuccess.getCoverageValues(); 
		extSuccess.getFundValues();  
		extSuccess.getBeneficiariesValues();
		extSuccess.getStatementValues();
		extSuccess.getRenderBeneficiaries();
		extSuccess.getRenderAltBeneficiaries();
		extSuccess.getRenderIfULRecordType();
	 	extSuccess.getRenderIfULFPULRecordType();
		extSuccess.getRenderIfTradRecordType();
		extSuccess.getCreditCardLastAutopayDateValue();
		extSuccess.getRenderPaymentMode();
		extSuccess.getRenderFundInformation();
		extSuccess.getPolicyPageDisclaimer();
		extSuccess.getFundInformationDisclaimer();
		extSuccess.getPolicyValueDisclaimer();
		extSuccess.getAddressEscape();
		extSuccess.getMobileNumberEscape();
		extSuccess.getHomeNumberEscape();
		extSuccess.getBusinessNumberEscape();
		extSuccess.getRenderModalPremiumRegContrib();
		agentSuccessTest = new Agent__c(id = agentSuccessTest.id, Servicing_Agent_First_Name__c = '',
										   Servicing_Agent_Last_Name__c = '',
										   Agent_Status__c = 'A', Policy__c = policySuccessTest.id);
		update agentSuccessTest;
		pref = Page.PolicyInquiry;	
		pref.getParameters().put('policyID', policySuccessTest.policy_number__c);
  		Test.setCurrentPage(pref);		
  		conSuccess = new ApexPages.StandardController(policySuccessTest);
        extSuccess = new PolicyInquiryControllerExtension(conSuccess); 			   
		extSuccess.getConsultantNameValue();								   		
		/* Failure Set */
		Policy__c policyFailureTest = new Policy__c();
		insert policyFailureTest;
        PageReference prefFail = Page.PolicyInquiry;
		prefFail.getParameters().put('policyID', null);
  		Test.setCurrentPage(prefFail);
 		ApexPages.StandardController conFailure = new ApexPages.StandardController(policyFailureTest);
        PolicyInquiryControllerExtension extFailure = new PolicyInquiryControllerExtension(conFailure);
        /* Call Inner Classes */  
        customStatementsObject cSObjectFail = new customStatementsObject(null, null, null, null, null, null, null, null); 
        customBeneficiaryObject cBObjectFail = new customBeneficiaryObject(null, null, null, null, null, null);
        customCoverageObject cCObjectFail = new customCoverageObject(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, NULL);
        customFundObject cFObjectFail = new customFundObject(null, null, null, null, null, null, null, null, null, null, null, null); 
        /* Call Failure Methods */
        extFailure.Security();
		extFailure.getPolicyNameValue();
		extFailure.getRenderAgents();
		extFailure.getRecordTypeValue();
	 	extFailure.getPolicyStatusValue();
		extFailure.getPolicyInforceDateValue();
		extFailure.getPolicyIssueDateValue();
		extFailure.getCurrencyISOCodeValue();
		extFailure.getPolicyInsuranceTypeValue();
		extFailure.getPaymentMethodValue();
		extFailure.getPaymentModeValue();
		extFailure.getConsultantNameValue();
		extFailure.getPremiumDueDateValue();
		extFailure.getDeathBenefitOptionValue();
		extFailure.getPolicyOwnerNameValue();
		extFailure.getAccountMobileNumberValue();
		extFailure.getAccountHomeNumberValue();
		extFailure.getAccountBusinessNumberValue();
		extFailure.getAccountPersonEmailValue();
		extFailure.getMailingAddressValues();
		extFailure.getBankAccountNumberValue();
		extFailure.getLastAutopayDateValue();
		extFailure.getDebitDateValue();
		extFailure.getCreditCardNumberValue();
		extFailure.getModalPremiumRegularContribution();
		extFailure.getDividendOptionValue();
		extFailure.getCouponOptionValue();
		extFailure.getDividendBalanceValue();
		extFailure.getCouponFundSuperDividendBalanceValue();
		extFailure.getPDFBalanceValue();
		extFailure.getECEJCIBalanceValue();
		extFailure.getPUACashValue();
		extFailure.getLoanAmountValue();
		extFailure.getPremiumOnAccountBalanceValue();
		extFailure.getGuaranteedCashValue();
		extFailure.getMaturityDividendBalanceValue();
		extFailure.getMaturityBonusValue();	
		extFailure.getMaxLoanAmountValue();
		extFailure.getTotalValue();
		extFailure.getTotalAvailableBalanceValue();
		extFailure.getTotalUnavailableBalanceValue();
		extFailure.getCoverageValues(); 
		extFailure.getFundValues();  
		extFailure.getBeneficiariesValues();
		extFailure.getStatementValues();
		extFailure.getRenderBeneficiaries();
		extFailure.getRenderAltBeneficiaries();
		extFailure.getRenderIfULRecordType();
	 	extFailure.getRenderIfULFPULRecordType();
		extFailure.getRenderIfTradRecordType();
		extFailure.getCreditCardLastAutopayDateValue();
		extFailure.getRenderPaymentMode(); 
		extFailure.getAddressEscape();
		extFailure.getMobileNumberEscape();
		extFailure.getHomeNumberEscape();
		extFailure.getBusinessNumberEscape();
		extFailure.getRenderModalPremiumRegContrib();			
		/* Variation Set 1 */
        PageReference prefVariation1 = Page.PolicyInquiry;
		prefVariation1.getParameters().put('policyID','113456ABC');
		Test.startTest();
  		Test.setCurrentPage(prefVariation1);
 		PolicyInquiryControllerExtension extVariation1 = new PolicyInquiryControllerExtension();
 		extVariation1.Security();
 		Test.stopTest();		
 		/* Run as 
 		//Profile p = [select id from profile where id='00e30000000w2RGAAY'];
		User u = new User(alias = 'standt', email='standarduser@testorg.com',
							emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
							localesidkey='en_US', profileid = '00e30000000w2RG',
							timezonesidkey='America/Los_Angeles', username='standarduser@testorg.com');
		System.runAs(u) 
		{
	  		Test.setCurrentPage(prefVariation2);
	 		extVariation2 = new PolicyInquiryControllerExtension();
	 		extVariation2.Security();					 					
		}
		*/
		accountSuccessTest = new Account(id=accountSuccessTest.id, LastName='NO LAST NAME', FirstName='G',PersonEmail='a@b.com');
		update accountSuccessTest;	
		extSuccess.getRecordTypeValue();
		accountSuccessTest = new Account(id=accountSuccessTest.id, LastName='XS', FirstName='Gilbert',PersonEmail='a@b.com');
		update accountSuccessTest;		
		extSuccess.getRecordTypeValue();
		accountSuccessTest = new Account(id=accountSuccessTest.id, LastName='N', FirstName='',PersonEmail='a@b.com');
		update accountSuccessTest;		
		extSuccess.getRecordTypeValue();	
		/* Call static methods */
		PolicyInquiryControllerExtension.currencyFormat(123);
		PolicyInquiryControllerExtension.currencyFormat(123.0);	
		String strAttach = statementSuccessTest.id;
		extSuccess.setStatementRecordIdValue(strAttach);
		extSuccess.getStatementRecordIdValue();
		PageReference prefUpdateReadCount = extSuccess.UpdateReadCount();
		List<Fund_Detail__c> funds = new List<Fund_Detail__c> {fundSuccessTest1, fundSuccessTest2};
		List<customCoverageObject> cCO = new List<customCoverageObject> {cCObject, cCObjectFail};
		extSuccess.LookupFundNames(funds);
		extSuccess.LookupDeathBenefit(cCO);		
	}
}