/*

Version        : 1.2
Company        : Sun Life Financial 
Date           : 06.AUG.2009
Author         : Robert Andrew Almedina
Description    : This class acts as a test coverage for the ChangeAllocInputCtrl
History        : 
                1.1 20.OCT.2009 Robby Angeles - Updated policy record under context to match the latest policy record conditions.
                1.2 23.NOV.2009 Robby Angeles - Updated in lieu of recent changes to target controller.
                1.3 25.NOV.2009 Robby Angeles - Updated to cover changes for DIF GIF mapping reference on target controller

*/

public with sharing class ChangeAllocInputCtrl_Test {


	public static testmethod void test()
    {   
    	// create records for client
    	// create records for policy
    	// create records for coverage
    	// create records for fund detail
    	// create records for Config_Global_Settings__c
    	// create records for Config_Plan_Fund_Map__c
    	
    	Test.setCurrentPageReference(new PageReference('Page.ChangeAllocInput'));
		
		Id policyULRecType;
        
        List<RecordType> policyRecType = new List<RecordType>([Select Id From RecordType Where SObjectType = 'Policy__c'
        	And Name = 'UL' And IsActive= true
        ]);
        
        if(policyRecType.size() > 0){
        	policyULRecType = policyRecType[0].Id;
        }
		
		Policy__c testPolicy 		= new Policy__c(
        	      RecordTypeId 		        = policyULRecType,
		          Policy_Number__c		    = 'TESTPOLNUM',
		          Active__c				    = true,
		          Policy_Status_Hidden__c	= '1',
		          Policy_Insurance_Type__c  = 'A',
		          Billing_Method__c			= 'A',
		          CurrencyISOCode			= 'HKD'	
		       );
        insert testPolicy;
        
        Coverage__c testCoverage = new Coverage__c(
        	Policy__c 				= testPolicy.Id,
        	Coverage_Number__c		= 'COV01',
        	Name					= 'COV01',
        	Coverage_Status__c		= '1',
        	Plan_Code__c			= 'TestPC',
        	Active__c 				= true
        );
        
        Config_Product_Data__c testConProdData = new Config_Product_Data__c(
        	Plan_Code__c			= testCoverage.Plan_Code__c,
        	CHM_Min_Percentage__c	= 20
        );
        
        List<Fund_Detail__c> testListFundDetail				= new List<Fund_Detail__c>();
        List<Config_Plan_Fund_Map__c> testListPlanFundMap   = new List<Config_Plan_Fund_Map__c>();
        List<Config_Exchange_Rate__c> testListExchangeRate  = new List<Config_Exchange_Rate__c>();
        List<Config_Fund_Price__c>	  testListFundPrice     = new List<Config_Fund_Price__c>();
        List<Config_Translation_Lookups__c> testListLookups = new List<Config_Translation_Lookups__c>();
        
        Fund_Detail__c testFundDetail = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC1',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        );
        
        Fund_Detail__c testFundDetail2 = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC1',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        );
        
        //Added for fund detail with same fund code scenario
        Fund_Detail__c testFundDetail3 = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC2',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        );
        
        //FOR DIF GIF mapping reference
        Fund_Detail__c testFundDetail4 = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFCDIF',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 15.00000
        ); 
        
        testListFundDetail.add(testFundDetail);
        testListFundDetail.add(testFundDetail2);
        testListFundDetail.add(testFundDetail3);
        testListFundDetail.add(testFundDetail4);
        
        Config_Exchange_Rate__c testExchangeRate = new COnfig_Exchange_Rate__c(
        	CurrencyISOCode				= 'USD',
        	Conversion_Rate_to_HKD__c	= 30
        );
        
        Config_Exchange_Rate__c testExchangeRate2 = new COnfig_Exchange_Rate__c(
        	CurrencyISOCode				= 'HKD',
        	Conversion_Rate_to_HKD__c	= 1
        );
        
        testListExchangeRate.add(testExchangeRate);
        testListExchangeRate.add(testExchangeRate2);
        
        Config_Fund_Price__c  testFundPrice = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC1',
        	Latest_Fund_Price__c = 30000.0000//,
        	//Fund_Price_Date__c   = System.Now()
        );
        
        Config_Fund_Price__c  testFundPrice2 = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC2',
        	Latest_Fund_Price__c = 97000.0000//,
        	//Fund_Price_Date__c   = System.Now()
        );
        
        Config_Fund_Price__c  testFundPrice3 = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC3',
        	Latest_Fund_Price__c = 85500.0000//,
        	//Fund_Price_Date__c   = System.Now()
        );
        
        Config_Fund_Price__c  testFundPrice4 = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFCDIF',
        	Latest_Fund_Price__c = 85900.0000//,
        	//Fund_Price_Date__c   = System.Now()
        );
        
        testListFundPrice.add(testFundPrice);
        testListFundPrice.add(testFundPrice2);
        testListFundPrice.add(testFundPrice3);
        testListFundPrice.add(testFundPrice4);
        
        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFC1',
        	Description__c		= 'TestFC1',
        	Primary_Key__c		= 'FUND_NAME|TestFC1|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup2 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFC2',
        	Description__c		= 'TestFC2',
        	Primary_Key__c		= 'FUND_NAME|TestFC2|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup3 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFCDIF',
        	Description__c		= 'TestFCDIF',
        	Primary_Key__c		= 'FUND_NAME|TestFCDIF|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup4 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFCGIF',
        	Description__c		= 'TestFCGIF',
        	Primary_Key__c		= 'FUND_NAME|TestFCGIF|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup5 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_CATEGORY',
        	Code__c				= '0',
        	Description__c		= 'CAT1',
        	Primary_Key__c		= 'FUND_CATEGORY|0|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup6 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'PLAN_NAME',
        	Code__c				= 'TestPC',
        	Description__c		= 'TestPC',
        	Primary_Key__c		= 'Plan_Name|TestPC|en_US',
        	Language__c			= 'en_US'
        );
        
        testListLookups.add(testLookup);
        testListLookups.add(testLookup2);
        testListLookups.add(testLookup3);
        testListLookups.add(testLookup4);
        testListLookups.add(testLookup5);
        testListLookups.add(testLookup6);
        
        Config_Plan_Fund_Map__c testPlanFundMap = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '0',
        	Fund_Code__c					= 'TestFC1',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'Other',
        	Sequence_Number__c				= 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap2 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '0',
        	Fund_Code__c					= 'TestFC2',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'Other',
        	Sequence_Number__c				= 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap3 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '10',
        	Fund_Code__c					= 'TestFC3',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'Other',
        	Sequence_Number__c				= 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap4 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '10',
        	Fund_Code__c					= 'TestFCDIF',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'DIF',
        	Sequence_Number__c				= 1
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap5 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '10',
        	Fund_Code__c					= 'TestFCGIF',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'GIF',
        	Sequence_Number__c				= 1
        );
        
        testlistPlanFundMap.add(testPlanFundMap);
        testlistPlanFundMap.add(testPlanFundMap2);
        testlistPlanFundMap.add(testPlanFundMap3);
        testlistPlanFundMap.add(testPlanFundMap4);
        testlistPlanFundMap.add(testPlanFundMap5);
        
        Config_Global_Settings__c testGlobalSettings = new Config_Global_Settings__c(
        	CHM_Min_Percentage__c 			= 30.25,
        	FSW_Gif_Min_Amount__c			= 7500.0000,
        	FSW_Switch_Out_Min_Amount__c	= 100,
        	SLF_Portal_Profile_Names__c		= 'TEST'
        );
        
        Config_DIF_GIF_Map__c testDIFGIFMap = new Config_DIF_GIF_Map__c(
        	Active__c 		 = true,
        	DIF_Fund_Code__c = 'TestFCDIF',
        	GIF_Fund_Code__c = 'TestFCGIF'
        );
        
        insert testGlobalSettings;
        insert testDIFGIFMap;
        insert testListLookups;
        insert testListExchangeRate;
        insert testListFundPrice;
        insert testCoverage;
        insert testConProdData;
        insert testListFundDetail;
        insert testListPlanFundMap;
		
		System.currentPageReference().getParameters().put('policyID', testPolicy.Policy_Number__c);
    	
    	
    	
		ChangeAllocInputCtrl myCHM = new ChangeAllocInputCtrl();
		
		myCHM.renderErrorSinglePremium = false;
		myCHM.renderErrorCastIron 	= false;
		myCHM.renderErrorExistRequest = false;
		myCHM.renderErrorLapseStartDate = false;
		myCHM.gettransactId();
		
		myCHM.gettempURLAcknowledgement();
		myCHM.gettempURLCancel();
		
		
		myCHM.isAllocChanged = false;

		myCHM.PolicyID 		= '';
		myCHM.clientName 	= '';
		myCHM.minPrcnt		= '';

		//myCHM.FundList = new List<Fund_Detail__c>();
		myCHM.CurrFundCategoryNameandValue = new List<CustomFundSwitching>();
	
		//myCHM.CurrFundAllocCategories = new List<Display_Translator>();
		
		myCHM.percentage2 = 2;
		myCHM.Security();
		myCHM.retrieveServerDT();
		myCHM.getPolicyName();
		// 11/23/2009 Robby Angeles: to fill up at least one fund allocation percentage before calling checkChanges method
		myCHM.MainDisplay[0].fswCustomFS[0].fswFundAllocPercentage = '100';
		// end 11/23/2009 Robby Angeles
		myCHM.checkChanges();
		myCHM.getMainDisplay();
		myCHM.getModifiedDisplay();
		myCHM.percentage=2;
		myCHM.currPercentage=2;
		myCHM.computeCurrTotalAllocation();
		myCHM.getDateTime();
		myCHM.generateTransactionHeader();
		myCHM.preConfirmValidation();
		myCHM.validator = new ChangeAllocPreValidateCtrl();
		
		myCHM.detectChanges();
		myCHM.generateTransactionHeader();
		
    }
}