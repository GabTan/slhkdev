public class customPolicyObject{
	public Id policyId {get; set;}
	public Boolean boolIsChecked  {get; set;}
	public String strPolicyNumber  {get; set;}
 	public String strPlanName {get; set;}	
 	public String strInsuredName  {get; set;}
 	public String strPolicyStatus {get; set;}
 	public String strPolicyDate {get; set;} 
 	public String strFullAddress {get; set;} 
 	
 	public customPolicyObject(Date dateInputPolicyDate){
		if(dateInputPolicyDate != null)
	 	{
	 		strPolicyDate = String.valueOf(dateInputPolicyDate.format());
	 	}
	 	strPolicyNumber = '';
 		strPlanName = '';
 		strInsuredName = '';
 		strPolicyStatus = '';
 		boolIsChecked = false;
 	}	
 }