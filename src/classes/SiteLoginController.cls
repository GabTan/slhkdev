public class SiteLoginController {
    public String username {get; set;}
    public String password {get; set;}

    public PageReference login() {   
        String startUrl = '/apex/RedirectCP';
        return Site.login(username, password, startUrl);
    }
    
   	public SiteLoginController () {}
    
    public static testMethod void testSiteLoginController () {
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'gilbert.deleon@ip-converge.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}