/* 
	Version		   : 1.1
	Company        : Sun Life Financial 
	Date           : August 17 , 2009 
	Author         : Robby Angeles 
	Description    : Test coverage for the Display Value Translation class.
	History        : <Date of Update>        <Author of Update>        <Short description of update> 
	
	               : 	 17.AUG.2009  - Robby Angeles  - Created
				   : 1.1 07.OCT.2009  - Robby Angeles  - Added TranslateENGMap controller check.                                                                                 
*/ 
@isTest
private class DisplayTranslator_Test {
	
    static testMethod void displayTranslatorTest() {
        
        //calls for three constructors
        Display_Translator consCall  = new Display_Translator();
        Display_Translator consCall2 = new Display_Translator('TESTCODE', 'TEST');
        Display_Translator consCall3 = new Display_Translator('TESTCODE', 'TEST', 'en_US');
        
        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
        	Code__c 		= 'TEST',
        	Code_Type__c	= 'TESTCODE',
        	Description__c  = 'TESTDescription',
        	Language__c     = 'en_US',
        	Primary_Key__c  = 'TESTCODE|TEST|en_US'
        );
        
        insert testLookup;
        
        //test for generic translation and translate functions
        String testString = Display_Translator.genericTranslation(testLookup.Code_Type__c, testLookup.Code__c);
        
        //test for translateMap function
        Map<String, String>      testMapDP  = new Map<String, String>();
        List<Display_Translator> testListDP = new List<Display_Translator>();
        
        Display_Translator elemDP = new Display_Translator(
        	'TESTCODE', 'TEST'
        );
        
        //force other language test
        Display_Translator elemDP2 = new Display_Translator(
        	'TESTCODE', 'TEST', 'zh_TW'
        );
        
        //forced no matching descriptions
        Display_Translator elemDP3 = new Display_Translator(
        	'TESTCODENOTEXIST', 'TESTNOTEXIST'
        ); 
        
        testListDP.add(elemDP);
        testListDP.add(elemDP2);
        testListDP.add(elemDP3);
        
        Display_Translator[] ltTemp = new Display_Translator[]{};
		ltTemp.add(elemDP2);
		ltTemp.add(elemDP3);
		ltTemp = Display_Translator.Translate(ltTemp);
		
        testMapDP = Display_Translator.TranslateMap(testListDP);
        testMapDP = Display_Translator.TranslateENGMap(testListDP);
        
        //force zero records
        ltTemp.clear();
        ltTemp = Display_Translator.Translate(ltTemp);
        
        testListDP.clear();
        testMapDP = Display_Translator.TranslateMap(testListDP);
        
        //force null returns
        ltTemp = null;
        ltTemp = Display_Translator.Translate(ltTemp);
        
        testListDP = null;
        testMapDP = Display_Translator.TranslateMap(testListDP);
    }
}