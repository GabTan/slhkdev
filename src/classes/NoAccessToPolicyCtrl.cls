/*
Version   : 1.0
Company   : Sun Life Financial
Date    : 25.SEP.2009
Author    : JC NACHOR
Description : Controller for the NoAccessToPolicy page.
History   : 
            version 1.1 - JC NACHOR 10/15/09 - added error message for invalid client 
*/
public with sharing class NoAccessToPolicyCtrl {

    public Boolean isPolicyError {get; set;}
    public Boolean isTransactionError {get; set;}
    public Boolean isClientError {get; set;}
    
    public NoAccessToPolicyCtrl() {
        this.isPolicyError = false;
        String strParam = ApexPages.currentPage().getParameters().get('type'); 
        if (strParam != null) {
            this.isPolicyError = (strParam.equals('policy'));
            this.isTransactionError = (strParam.equals('transaction'));
            this.isClientError = (strParam.equals('client'));
        } else {
        	this.isPolicyError = true;
        	this.isTransactionError = false;
        	this.isClientError = false;
        }
    }

}