/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Ryan Dave Wilson
Description	: Storing Attribute values for Switch out Funds
History	:

*/

public class SwitchOutRequest {
	public String code {get; set;}
	public String name {get; set;}
	public String percent {get; set;}
}