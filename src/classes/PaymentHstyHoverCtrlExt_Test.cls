/* 
Version		: 1.0
Company		: Sun Life Financial
Date		: 21.JUL.2009
Author		: Robert Andrew Almedina
Description	: This class acts as a test coverage for the Payment History Hover page controller
History	:

*/ 

public class PaymentHstyHoverCtrlExt_Test {

	static testMethod void myUnitTest() {
		
		Test.setCurrentPageReference(new PageReference('Page.PaymentHstyCardHover'));
		
		List<Payment_History__c> PHistoryList 	= new List<Payment_History__c>();
		PHistoryList = [Select p.Usage_Code__c, p.Reject_Reason_Description__c, p.Reject_Reason_Code__c, p.Received_Date__c, 
						p.Rec_Status__c, p.Rec_No__c, p.Premium_Due_Date__c, p.Post_Status__c, p.Policy_Id__c, p.Policy_Current_Amount__c, 
						p.Policy_Currency__c, p.Payment_Type__c, p.Payment_Method__c, p.Payment_Id__c, p.Payment_Amount__c, p.Name, 
						p.Means_Type__c, p.Last_Update__c, p.Id, p.Exchange_Rate__c, p.Error_Log__c, p.Draw_Date__c, p.DDA_Required__c, 
						p.CurrencyIsoCode, p.Credit_Card_Type__c, p.Credit_Card_Account_No__c, p.CC_Account_No_Masked__c, 
						p.Bank_Confirmation_Status__c, p.Bank_Account_No_Masked__c, p.Autopay_Branch_ID__c, p.Autopay_Bank_Name__c, 
						p.Autopay_Bank_ID__c, p.Autopay_Account_No__c, p.App_No__c, p.Active__c, p.CreatedDate From Payment_History__c p 
						Limit 1];

		System.currentPageReference().getParameters().put('id', PHistoryList[0].Id);

		PaymentHstyHoverCtrlExt ctrlTest = new PaymentHstyHoverCtrlExt();
		ctrlTest.PHToDisplay = [Select p.Usage_Code__c, p.Reject_Reason_Description__c, p.Reject_Reason_Code__c, p.Received_Date__c, p.Rec_Status__c, p.Rec_No__c, p.Premium_Due_Date__c, p.Post_Status__c, p.Policy_Id__c, p.Policy_Current_Amount__c, p.Policy_Currency__c, p.Payment_Type__c, p.Payment_Method__c, p.Payment_Id__c, p.Payment_Amount__c, p.Name, p.Means_Type__c, p.Last_Update__c, p.Id, p.Exchange_Rate__c, p.Error_Log__c, p.Draw_Date__c, p.DDA_Required__c, p.CurrencyIsoCode, p.Credit_Card_Type__c, p.Credit_Card_Account_No__c, p.CC_Account_No_Masked__c, p.Bank_Confirmation_Status__c, p.Bank_Account_No_Masked__c, p.Autopay_Branch_ID__c, p.Autopay_Bank_Name__c, p.Autopay_Bank_ID__c, p.Autopay_Account_No__c, p.App_No__c, p.Active__c From Payment_History__c p Limit 1];
    	
    	ctrlTest.getPHToDisplay();
    	ctrlTest.getCreditCard();
	}
}