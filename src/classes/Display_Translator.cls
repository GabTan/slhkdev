/* 

        Version     : 1.3
        Company     : Sun Life Financial
        Date        : JUL.2009
        Author      : Robby Angeles
        Description : Class for value translation in reference to the
                      logged in user record's language
        History     :
        
                    : 1.1 07.OCT.2009  - Robby Angeles  - Added TranslateENGMap for transaction records. 
                      1.2 04.MAY.2012  - Steph So       - SR-ES-12037 SAIII
                      1.3 19.NOV.2010 / 25.JUL.2012  - Mikka Elepano/Steph So - Too many SOQL; Added TranslateMe and getTranslation method for translating records in one query only.
        
*/ 

public with sharing class Display_Translator {
    public String DPCodeType;
    public String DPCode;
    public String DPLang;
    public String DPDesc { get; set;}
    public String DPCPTVal { get; set;} //SAIII - Steph So - 05/04/2012
    
    
    public Display_Translator(){
        this.DPLang = UserInfo.getLanguage();
    }
    
    public Display_Translator(String codeType, String codeValue){
        this.DPLang     = UserInfo.getLanguage();
        this.DPCodeType = codeType;
        this.DPCode     = codeValue;
    }
    
    public Display_Translator(String codeType, String codeValue, String DPLang){ //planCode, fundCode, covTerm
        this.DPLang     = DPLang;
        this.DPCodeType = codeType;
        this.DPCode     = codeValue;
    }
        
    public static List<Display_Translator> Translate(List<Display_Translator> tempDP){
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = '(';
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Display_Translator dp :tempDP){
                    if(whereClause!='(')whereClause+=',';
                    String tempStr = '\'' + dp.DPCodeType + '|' + dp.DPCode + '|' + dp.DPLang + '\'';
                    // NOTE 1- For blank non en-US language translations, look for en-US value.
                    mapTempDP.put(tempStr.replace('\'', ''), dp);
                    if(dp.DPLang != 'en_US'){
                        tempStr += ', \'' + dp.DPCodeType + '|' + dp.DPCode + '|en_US\'';
                    }
                    // END NOTE 1
                    whereClause += tempStr;
                }
                whereClause += ')';
                
                Map<String, String> mapDescription = new Map<String, String>();
                List<Config_Translation_Lookups__c> translationList = new List<Config_Translation_Lookups__c>();
                String mainQuery = 'Select Id, Primary_Key__c, Language__c, Description__c, Code__c, Code_Type__c From Config_Translation_Lookups__c Where Primary_Key__c In ' + whereClause;
                translationList = Database.query(mainQuery);
                if(translationList.size()>0){
                    for(Config_Translation_Lookups__c ctl :translationList){
                        mapDescription.put(ctl.Primary_Key__c, ctl.Description__c);
                    }   
                }
                for(String mapKey :mapTempDP.keySet()){
                    String tempStr = mapDescription.get(mapKey);
                    Display_Translator dpElem = mapTempDP.get(mapKey);
                    dpElem.DPDesc = ''; 
                    if(tempStr!=null){
                        dpElem.DPDesc = tempStr;    
                    }
                    else{
                        String tempmapKey = mapKey.replace(dpElem.DPLang, 'en_US');
                        tempStr = mapDescription.get(tempmapKey);
                        if(tempStr!=null){
                            dpElem.DPDesc = tempStr;
                        }
                    }
                }
                return tempDP;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }       
    }
    
    public static Map<String, String> TranslateMap(List<Display_Translator> tempDP){
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = '(';
                Map<String, String> test = new Map<String, String>();
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Display_Translator dp :tempDP){
                    if(whereClause!='(')whereClause+=',';
                    String tempStr = '\'' + dp.DPCodeType + '|' + dp.DPCode + '|' + dp.DPLang + '\'';
                    // NOTE 1- For blank non en-US language translations, look for en-US value.
                    mapTempDP.put(tempStr.replace('\'', ''), dp);
                    if(dp.DPLang != 'en_US'){
                        tempStr += ', \'' + dp.DPCodeType + '|' + dp.DPCode + '|en_US\'';
                    }
                    // END NOTE 1
                    whereClause += tempStr;
                }
                whereClause += ')';
                
                Map<String, String> mapTranslation = new Map<String, String>();
                Map<String, String> mapDescription = new Map<String, String>();
                List<Config_Translation_Lookups__c> translationList = new List<Config_Translation_Lookups__c>();
                String mainQuery = 'Select Id, Primary_Key__c, Language__c, Description__c, Code__c, Code_Type__c From Config_Translation_Lookups__c Where Primary_Key__c In ' + whereClause;
                translationList = Database.query(mainQuery);
                if(translationList.size()>0){
                    for(Config_Translation_Lookups__c ctl :translationList){
                        mapDescription.put(ctl.Primary_Key__c, ctl.Description__c);
                    }
                }
                for(String mapKey :mapTempDP.keySet()){
                    String tempStr = mapDescription.get(mapKey);
                    Display_Translator dpElem = mapTempDP.get(mapKey);
                    dpElem.DPDesc = ''; 
                    if(tempStr!=null){
                        mapTranslation.put(dpElem.DPCode, tempStr); 
                    }
                    else{
                        String tempmapKey = mapKey.replace(dpElem.DPLang, 'en_US');
                        tempStr = mapDescription.get(tempmapKey);
                        if(tempStr!=null){
                            mapTranslation.put(dpElem.DPCode, tempStr);
                        }
                        else{
                            mapTranslation.put(dpElem.DPCode, '');
                        }
                    }
                }
                return mapTranslation;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    public static Map<String, String> TranslateENGMap(List<Display_Translator> tempDP){
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = '(';
                Map<String, String> test = new Map<String, String>();
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Display_Translator dp :tempDP){
                    if(whereClause!='(')whereClause+=',';
                    String tempStr = '\'' + dp.DPCodeType + '|' + dp.DPCode + '|en_US\'';
                    mapTempDP.put(tempStr.replace('\'', ''), dp);               
                    whereClause += tempStr;
                }
                whereClause += ')';
                
                Map<String, String> mapTranslation = new Map<String, String>();
                Map<String, String> mapDescription = new Map<String, String>();
                List<Config_Translation_Lookups__c> translationList = new List<Config_Translation_Lookups__c>();
                String mainQuery = 'Select Id, Primary_Key__c, Language__c, Description__c, Code__c, Code_Type__c From Config_Translation_Lookups__c Where Primary_Key__c In ' + whereClause;
                translationList = Database.query(mainQuery);
                if(translationList.size()>0){
                    for(Config_Translation_Lookups__c ctl :translationList){
                        mapDescription.put(ctl.Primary_Key__c, ctl.Description__c);
                    }
                }
                for(String mapKey :mapTempDP.keySet()){
                    String tempStr = mapDescription.get(mapKey);
                    Display_Translator dpElem = mapTempDP.get(mapKey);
                    dpElem.DPDesc = ''; 
                    if(tempStr!=null){
                        mapTranslation.put(dpElem.DPCode, tempStr); 
                    }
                    else{
                        String tempmapKey = mapKey.replace(dpElem.DPLang, 'en_US');
                        tempStr = mapDescription.get(tempmapKey);
                        if(tempStr!=null){
                            mapTranslation.put(dpElem.DPCode, tempStr);
                        }
                        else{
                            mapTranslation.put(dpElem.DPCode, '');
                        }
                    }
                }
                return mapTranslation;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    public static String genericTranslation(String codetype, String code){
        Display_Translator genericDP = new Display_Translator(codetype, code);
        genericDP.DPDesc = '';
        Display_Translator[] ltTemp = new Display_Translator[]{};
        ltTemp.add(genericDP);
        ltTemp = Display_Translator.Translate(ltTemp);
        return genericDP.DPDesc;
    }
    //SAIII - Steph So - 05/04/2012 - start
    public static String cptTranslation(String planCode, String coverageTerm){
        Display_Translator cptDP = new Display_Translator(planCode, coverageTerm);
        cptDP.DPCPTVal = '';
        Display_Translator[] ltTemp = new Display_Translator[]{};
        ltTemp.add(cptDP);
        String ltReturn;
        ltReturn = Display_Translator.TranslateCPTMap(ltTemp);
        return ltReturn;
    }
    public static String TranslateCPTMap(List<Display_Translator> tempDP){
        String tempStr;
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = 'where Primary_Key__c in (';
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Integer x=0; x< tempDP.size(); x++) 
                {
                    if (x == tempDP.size()-1)   whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\'';
                    else whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\',';
                    mapTempDP.put(tempDP[x].DPCodeType + '||' + tempDP[x].DPCode, tempDP[x]);
                }
                whereClause += ')';
                
                Map<String, String> mapTranslation = new Map<String, String>();
                Map<String, String> mapDescription = new Map<String, String>();
                List<Config_Plan_CPT_Map__c> translationList = new List<Config_Plan_CPT_Map__c>();
                String mainQuery = 'Select Primary_Key__c, CPT_Value__c From Config_Plan_CPT_Map__c ' + whereClause;
                translationList = Database.query(mainQuery);
                if(translationList.size()>0){
                    for(Config_Plan_CPT_Map__c ctl :translationList){
                        mapDescription.put(ctl.Primary_Key__c, ctl.CPT_Value__c);
                    }
                }
                for(String mapKey :mapTempDP.keySet()){
                    tempStr = mapDescription.get(mapKey);
                    Display_Translator dpElem = mapTempDP.get(mapKey);
                    dpElem.DPCode = ''; 
                    if(tempStr!=null){
                        mapTranslation.put(dpElem.DPCode, tempStr); 
                    }
                }
                return tempStr;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }
    public static Map<String, String> TranslateCPTMapHomePage(List<Display_Translator> tempDP){
        List<Config_Plan_CPT_Map__c> translationList = new List<Config_Plan_CPT_Map__c>();
        Map<String, String> mapTranslation = new Map<String, String>();
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = 'where Primary_Key__c in (';
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Integer x=0; x< tempDP.size(); x++) 
                {
                    if (x == tempDP.size()-1)   whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\'';
                    else whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\',';
                    mapTempDP.put(tempDP[x].DPCodeType + '||' + tempDP[x].DPCode, tempDP[x]);
                }
                whereClause += ')';
                
                Map<String, String> mapDescription = new Map<String, String>();
                String mainQuery = 'Select Primary_Key__c, CPT_Value__c From Config_Plan_CPT_Map__c ' + whereClause;
                translationList = Database.query(mainQuery);
                               
                if(translationList.size()>0){
                   for(Config_Plan_CPT_Map__c ctl :translationList){
                       mapTranslation.put(ctl.Primary_Key__c, ctl.CPT_Value__c);
                	}
           		}
       		}
        }
		return mapTranslation;     
    }
    /*
    public static Map<String, String> TranslateCPTMap(List<Display_Translator> tempDP){
        if(tempDP != null){
            if(tempDP.size() > 0){
                String whereClause = 'where Primary_Key__c in (';
                Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
                for(Integer x=0; x< tempDP.size(); x++) 
                {
                    if (x == tempDP.size()-1)   whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\'';
                    else whereClause += '\'' + tempDP[x].DPCodeType + '||' + tempDP[x].DPCode + '\',';
                    mapTempDP.put(tempDP[x].DPCodeType + '||' + tempDP[x].DPCode, tempDP[x]);
                }
                whereClause += ')';
                
                Map<String, String> mapDescription = new Map<String, String>();
                List<Config_Plan_CPT_Map__c> translationList = new List<Config_Plan_CPT_Map__c>();
                String mainQuery = 'Select Primary_Key__c, CPT_Value__c From Config_Plan_CPT_Map__c ' + whereClause;
                translationList = Database.query(mainQuery);
                if(translationList.size()>0){
                    for(Config_Plan_CPT_Map__c ctl :translationList){
                        mapDescription.put(ctl.Primary_Key__c, ctl.CPT_Value__c);
                    }   
                }
                for(String mapKey :mapTempDP.keySet()){
                    String tempStr = mapDescription.get(mapKey);
                    Display_Translator dpElem = mapTempDP.get(mapKey);
                    dpElem.DPCPTVal = ''; 
                    if(tempStr!=null){
                        dpElem.DPCPTVal = tempStr;  
                    }
                }
                return tempDP;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }       
    }*/
    //SAIII - Steph So - 05/04/2012 - end
    
    //Too Many SOQL - Mikka Elepano/Steph So - 7/25/2012 - start
    public static List<Config_Translation_Lookups__c> TranslateMe(List<Display_Translator> tempDP){
		List<Config_Translation_Lookups__c> translationList = new List<Config_Translation_Lookups__c>();
		String whereClause;
		if(tempDP != null){
			if(tempDP.size() > 0){
				whereClause = '(';
				Map<String, Display_Translator> mapTempDP = new Map<String, Display_Translator>();
				for(Display_Translator dp :tempDP){
					if(whereClause!='(')whereClause+=',';
					String tempStr = '\'' + dp.DPCodeType + '|' + dp.DPCode + '|' + dp.DPLang + '\'';
					// NOTE 1- For blank non en-US language translations, look for en-US value.
					mapTempDP.put(tempStr.replace('\'', ''), dp);
					if(dp.DPLang != 'en_US'){
						tempStr += ', \'' + dp.DPCodeType + '|' + dp.DPCode + '|en_US\'';
					}
					// END NOTE 1
					whereClause += tempStr;
				}
				whereClause += ')';
				System.debug('whereClause:'+ whereClause);
				Map<String, String> mapDescription = new Map<String, String>();
				String mainQuery = 'Select Id, Primary_Key__c, Language__c, Description__c, Code__c, Code_Type__c From Config_Translation_Lookups__c Where Primary_Key__c In ' + whereClause;
				translationList = Database.query(mainQuery);
				//System.debug('+>>> TRANSLATION LIST: ' + translationList);
			}
		}
		return translationList;
	}
	
	public static String getTranslation ( String vPlanCode,Integer vSize,List<Config_Translation_Lookups__c> vTranslationList){
		string vname = '';
		string lang = UserInfo.getLanguage();
		for (integer i=0; i<vSize ; i++){
			if (vPlanCode.trim() == vTranslationList[i].Code__c && lang == vTranslationList[i].Language__c){
				System.debug('getPlanConfig:'+ vTranslationList[i].Code__c); 
				vname = vTranslationList[i].Description__c;
				return vname;
			}
			System.debug('getPlanParam:'+ vPlanCode); 
			
			System.debug('getTranslation:'+ vTranslationList); 		
		}
		
		return vname;
	}
    //Too Many SOQL - Mikka Elepano/Steph So - 7/25/2012 - end
}