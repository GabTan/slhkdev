/* 
	Version		   : 1.3
	Company        : Sun Life Financial 
	Date           : August 17, 2009 
	Author         : Robby Angeles 
	Description    : Test coverage for the FundSwitchInputCtrl class.
	History        : <Date of Update>        <Author of Update>        <Short description of update> 
	
	               : 		08-17-2009               Robby Angeles             Created
	               
	                 		09-30-2009				 Robby Angeles             Updated to cover more code after CR/Bug fix changes
                        
                     		10-19-2009				 Robby Angeles			   Fixed null pointer exceptions related to User/Client relationships
                     
                     1.3	11-23-2009				 Robby Angeles			   Updated in lieu with recent logic changes of target controller.
                     1.4    10-19-2011				 M.Elepano                 SR-ES-11033 First State Fund Closure Change                                                          
*/ 
@isTest
private class FundSwitchInputCtrl_Test {

    static testMethod void fundSwitchInputCtrlTest() {
        
        Set<String> POLICY_INFORCE_STATUSES = new Set<String>{'1', '2', '3', '4'};
        Set<String> POLICY_UL_CODES = new Set<String>{'A', 'B', 'C', 'D', 'E', 'F',  
													  'G', 'H', 'I', 'J', 'K', 'L', 
													  'M', 'N', 'O', 'P', 'Q', 'R', 
													  'S', 'T', 'U', 'V', 'W', 'X', 
													  'Y', 'Z'
													 };
        
        
        Id policyULRecType;
        
        List<RecordType> policyRecType = new List<RecordType>([Select Id From RecordType Where SObjectType = 'Policy__c'
        	And Name = 'UL' And IsActive= true
        ]);
        
        if(policyRecType.size() > 0){
        	policyULRecType = policyRecType[0].Id;
        } 
        
        Policy__c testPolicy 		= new Policy__c(
        	      RecordTypeId 		        = policyULRecType,
		          Policy_Number__c		    = 'TESTPOLNUM',
		          Active__c				    = true,
		          Policy_Status_Hidden__c	= '1',
		          Policy_Insurance_Type__c  = 'A',
		          CurrencyISOCode			= 'HKD'	
		       );
       insert testPolicy;
        
        Coverage__c testCoverage = new Coverage__c(
        	Policy__c 				= testPolicy.Id,
        	Coverage_Number__c		= 'COV01',
        	Name					= 'COV01',
        	Coverage_Status__c		= '1',
        	Plan_Code__c			= 'TestPC',
        	Active__c 				= true
        );
        
        List<Fund_Detail__c> testListFundDetail				= new List<Fund_Detail__c>();
        List<Config_Plan_Fund_Map__c> testListPlanFundMap   = new List<Config_Plan_Fund_Map__c>();
        List<Config_Exchange_Rate__c> testListExchangeRate  = new List<Config_Exchange_Rate__c>();
        List<Config_Fund_Price__c>	  testListFundPrice     = new List<Config_Fund_Price__c>();
        List<Config_Translation_Lookups__c> testListLookups = new List<Config_Translation_Lookups__c>();
        
        Fund_Detail__c testFundDetail = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC1',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        );
        
        Fund_Detail__c testFundDetail2 = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC1',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        );
        
        //Added for fund detail with same fund code scenario
        Fund_Detail__c testFundDetail3 = new Fund_Detail__c(
        	Policy__c				= testPolicy.Id,
        	Fund_Code__c			= 'TestFC2',
        	Active__c				= true,
        	Fund_Value__c			= 1000000.0000,
        	Number_Of_Units__c		= 10,
        	Interest_Rate__c		= 3,
        	Unit_Price__c			= 1000000.0000,
        	Fund_Allocation__c  	= 1000000.0000
        ); 
        
        testListFundDetail.add(testFundDetail);
        testListFundDetail.add(testFundDetail2);
        testListFundDetail.add(testFundDetail3);
        
        Config_Exchange_Rate__c testExchangeRate = new COnfig_Exchange_Rate__c(
        	CurrencyISOCode				= 'USD',
        	Conversion_Rate_to_HKD__c	= 30
        );
        
        Config_Exchange_Rate__c testExchangeRate2 = new COnfig_Exchange_Rate__c(
        	CurrencyISOCode				= 'HKD',
        	Conversion_Rate_to_HKD__c	= 1
        );
        
        testListExchangeRate.add(testExchangeRate);
        testListExchangeRate.add(testExchangeRate2);
        
        Config_Fund_Price__c  testFundPrice = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC1',
        	Latest_Fund_Price__c = 30000.0000//,
        	//Fund_Price_Date__c	 = System.Now()
        );
        
        Config_Fund_Price__c  testFundPrice2 = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC2',
        	Latest_Fund_Price__c = 97000.0000//,
        	//Fund_Price_Date__c	 = System.Now()
        );
        
        Config_Fund_Price__c  testFundPrice3 = new Config_Fund_Price__c(
        	Fund_Code__c		 = 'TestFC2',
        	Latest_Fund_Price__c = 85000.0000//,
        	//Fund_Price_Date__c	 = System.Now()
        );
        
        testListFundPrice.add(testFundPrice);
        testListFundPrice.add(testFundPrice2);
        testListFundPrice.add(testFundPrice3);
        
        Config_Translation_Lookups__c testLookup = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFC1',
        	Description__c		= 'TestFC1',
        	Primary_Key__c		= 'FUND_NAME|TestFC1|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup2 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_NAME',
        	Code__c				= 'TestFC2',
        	Description__c		= 'TestFC2',
        	Primary_Key__c		= 'FUND_NAME|TestFC2|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup3 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'FUND_CATEGORY',
        	Code__c				= '0',
        	Description__c		= 'CAT1',
        	Primary_Key__c		= 'FUND_CATEGORY|0|en_US',
        	Language__c			= 'en_US'
        );
        
        Config_Translation_Lookups__c testLookup4 = new Config_Translation_Lookups__c(
        	Code_Type__c 		= 'PLAN_NAME',
        	Code__c				= 'TestPC',
        	Description__c		= 'TestPC',
        	Primary_Key__c		= 'Plan_Name|TestPC|en_US',
        	Language__c			= 'en_US'
        );
        
        testListLookups.add(testLookup);
        testListLookups.add(testLookup2);
        testListLookups.add(testLookup3);
        testListLookups.add(testLookup4);
        
        Config_Plan_Fund_Map__c testPlanFundMap = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '0',
        	Fund_Code__c					= 'TestFC1',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'DIF',
        	Sequence_Number__c				= 1,
        	CHM_Soft_Fund_Closure__c        = true
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap2 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '0',
        	Fund_Code__c					= 'TestFC2',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'Other',
        	Sequence_Number__c				= 1,
        	CHM_Soft_Fund_Closure__c        = true
        );
        
        Config_Plan_Fund_Map__c testPlanFundMap3 = new Config_Plan_Fund_Map__c(
        	Plan_Code__c 					= 'TestPC',
        	Fund_Category_Code__c			= '10',
        	Fund_Code__c					= 'TestFC3',
        	FSW_Allow_Switch_Out__c			= true,
        	FSW_Allow_Switch_In__c			= true,
        	CHM_Allow_Fund_Allocation__c	= true,
        	Fund_Status__c					= 'Active',
        	Fund_Type__c					= 'Other',
        	Sequence_Number__c				= 1,
        	CHM_Soft_Fund_Closure__c        = true
        );
        
        testlistPlanFundMap.add(testPlanFundMap);
        testlistPlanFundMap.add(testPlanFundMap2);
        testlistPlanFundMap.add(testPlanFundMap3);
        
        Config_Global_Settings__c testGlobalSettings = new Config_Global_Settings__c(
        	CHM_Min_Percentage__c 			= 30.25,
        	FSW_Gif_Min_Amount__c			= 7500.0000,
        	FSW_Switch_Out_Min_Amount__c	= 100,
        	SLF_Portal_Profile_Names__c     = 'TEST'
        );
        
        insert testGlobalSettings;
        insert testListLookups;
        insert testListExchangeRate;
        insert testListFundPrice;
        insert testCoverage;
        insert testListFundDetail;
        insert testListPlanFundMap;
        
        Test.setCurrentPageReference(new PageReference('Page.FundSwitchInputCtrl'));
        
        System.currentPageReference().getParameters().put('policyID', testPolicy.Policy_Number__c);
        
        //getter calls
        
        FundSwitchInputCtrl fsCtrl = new FundSwitchInputCtrl();
        
        fsCtrl.getfundCategoryList();
        fsCtrl.getmainSwitchOutList();
        fsCtrl.getmainSwitchOutSelectedList();
        fsCtrl.getfundSwitchInSelectedList();
        fsCtrl.getuserName();
        fsCtrl.getpolicyID();
        fsCtrl.gettempURLAcknowledgement();
        fsCtrl.gettempURLTransactionLog();
        fsCtrl.gettempURLCancel();
        fsCtrl.gettransactId();
        fsCtrl.getconfirmDateTime();
        fsCtrl.getdateStr();
        
        //end getter calls
        
        //main methods call
        
        fsCtrl.delaySoNext();
        
        //force call before selecting at least one fund for the script to cover messaging when no fund
        //was selected for switch out
         
        fsCtrl.onSwitchOutNext();
        
        //select at least one fud for switch out with switch out amount equal to 50
        for(CustomSwitchInOut temp :fsCtrl.mainSwitchOutList){
        	temp.fswIsSelected   = true;
        	temp.fswStrSwitchOut = '50'; 
        }
        
        //another call this time with a selected fund for switch out
        fsCtrl.onSwitchOutNext();
        
     	for(CustomFundCategory tempFundCat: fsCtrl.fundCategoryList){
        	CustomFundSwitching[] tempListFS = tempFundCat.fswCustomFS;
            for(CustomFundSwitching tempFS: tempListFS){
                if(tempFS!=null){
                	tempFS.isGIF				= true; 
                	tempFS.fswEstSwitchValAmt	= 7000.0000;
      				tempFS.fswStrFSSwitchVal 	= '50';          	
                }
            }
     	}
        
        fsCtrl.onSwitchInNext();
        
        fsCtrl.onConfirm();
        fsCtrl.retrieveServerDT();
        fsCtrl.getPolicyName();
        //end main methods call
    }
}