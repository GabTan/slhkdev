/*
Version		: 1.0
Company 	: Sun Life Financial
Date    	: 24.SEP.2009
Author  	: W. Clavero
Description : Test coverage for the ESAccessVerifier class. 
History 	: 
*/
@isTest
private class ESAccessVerifier_Test {

    static testMethod void testAccess() {
    	/*
    	RecordType clientType = [Select Id, Name From RecordType Where Name = 'Person Account'];
        Account acct = new Account(LastName='Pportall', FirstName='Yussier', Client_Id__c='888xx1@11', 
        							Client_ID__pc='888xx1@11', CurrencyIsoCode='HKD', OwnerId=UserInfo.getUserId());
        insert acct;
		*/
		
    	Profile profileCP = [Select Id From Profile Where Name = 'SLF eServices Customer'];
    	
        User usr = [Select Id, ContactId, Client_Id__c From User Where (ProfileId = :profileCP.Id)  And (IsActive = TRUE) And (ContactId != NULL) And (Client_Id__c != NULL) Limit 1];
        
        Account acct = [Select Id, PersonContactId From Account Where PersonContactId = :usr.ContactId];

    	//UserRole role = [Select Id From UserRole Where Name like '%Customer Person Account' Limit 1];
    	 
    	/* 
    	User usr = new User();       
        usr.FirstName = 'Pportall';
        usr.LastName = 'Yussier';
        usr.Username = 'pportall.yussier2009@test.com';
        usr.Email = 'pportall.yussier2009@test.com';
        usr.IsActive = true;
        usr.Alias = 'porty';
        usr.TimeZoneSidKey = 'Asia/Hong_Kong';
        usr.LocaleSidKey = 'en_US';
        usr.EmailEncodingKey = 'ISO-8859-1';
        usr.ProfileId = profileCP.Id;
        usr.UserRoleId = role.Id; 
        usr.LanguageLocaleKey = 'en_US';
        usr.Client_Id__c = '888xx1@11';
        usr.ContactId = customer.PersonContactId;

		insert usr;
		*/
		
        Policy__c pol = new Policy__c(Name='888xx1@11', Policy_Number__c='888xx1@11', 
        								CurrencyIsoCode='HKD', Client__c=acct.Id, OwnerId=usr.Id);
        insert pol;
        
        Policy__c newPolicy = [Select Id, OwnerId From Policy__c Where Id = :pol.Id];
                
		Transaction__c txn = new Transaction__c(Reference_Number__c='888xx1@1t', Request_Type__c='FS', OwnerId=usr.Id);
		insert txn;                
                
        ESAccessVerifier.isUsersPolicy('888xx1@11', newPolicy.OwnerId);
        ESAccessVerifier.isCurrentUsersPolicy('888xx1@11');
        
        ESAccessVerifier.isUsersClient(usr.Client_Id__c, usr.Id);
        ESAccessVerifier.isCurrentUsersClient('888xx1@11');
        
        ESAccessVerifier.isUsersTransaction(txn.Id, usr.Id);
        ESAccessVerifier.isCurrentUsersTransaction(txn.Id);
    }
}