/*
Version   : 1.0
Company   : Sun Life Financial
Date    : 20.OCT.2009
Author    : JC NACHOR
Description : Test Class for the NoAccessToPolicy page.
History   : 
            
*/
@isTest
private with sharing class NoAccessToPolicyCtrl_test {

    static testmethod void NoAccessToPolicyCtrl() {
      pageReference pg = page.NoAccessToPolicy;
      Test.setCurrentPage(pg);            
      NoAccessToPolicyCtrl nap1 = new NoAccessToPolicyCtrl();
      
      ApexPages.currentPage().getParameters().put('type','policy');
      NoAccessToPolicyCtrl nap2 = new NoAccessToPolicyCtrl();      
      
    }

}