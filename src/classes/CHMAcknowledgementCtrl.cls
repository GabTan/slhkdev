/*
Version		: 1.3
Company		: Sun Life Financial
Date		: 27.JUL.2009
Author		: Ryan Dave Wilson
Description	: Change of fund allocation acknowledgement page controller.
History		: 

			  [1.2] <28.OCT.2009 - Robby Angeles - added method to check security for admin portal users (if page is Acknowledgement PDF).>
			  [1.3] <10.NOV.2009 - Robby Angeles - added config utility parseExtCode method to parse external fund code from TxnData name attribute.>
			  [1.4] <24.NOV.2009 - JC NACHOR - add parameter filtering to prevent cross site scripting

*/

public without sharing class CHMAcknowledgementCtrl{
	// JC NACHOR - change to String instead of Id
	//Id transId;
	String transId;
    Display_Translator[] changeAllocFunds = new Display_Translator[]{};
    Map<string, string> chmFundNameMap = new Map<string, string>();
	String fundName;
	
	Config_Utilities CU = new Config_Utilities();
	
    //public CHMAcknowledgementCtrl(ApexPages.StandardController controller){}
    
    // JC NACHOR - global flag to determine if transaction is accessible to the user
    public boolean isValidTxn = true;
    
    public PageReference Security() 
    {
      Boolean isPortalUser = true;
      // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
      // PageReference pRefRedirect = Page.NoAccessToPolicy;
      PageReference pRefRedirect = new Pagereference('/apex/NoAccessToPolicy?type=transaction');
      String strId = ApexPages.currentPage().getParameters().get('transId');            
      pRefRedirect.setRedirect(true);
      
      //R.Angeles bypass security if logged in user is admin and current page is PDF 
      String pageRef = ApexPages.currentPage().getURL();
      this.transId   = ApexPages.currentPage().getParameters().get('transId');
      if(pageRef.indexOf('PDF') != -1){
           isPortalUser = CU.isPortalUser('');
      }
      if(isPortalUser){
      	
        // START JC NACHOR - add parameter filtering to prevent cross site scripting
        if(strId.length() > 18 && strId.length() < 15){
          isValidTxn = false;
          return pRefRedirect;
        }       	
      	
	      if(ESAccessVerifier.isCurrentUsersTransaction((strId==null)?'':strId)){
	        return null;
	      } else {
	      	isValidTxn = false;
	        return pRefRedirect;
	      }
      }
	  else{
	  		return null;
	  }
	  //END
    }     
    
    // START JC NACHOR - add parameter filtering to prevent cross site scripting
    public CHMAcknowledgementCtrl(){
      String strId = ApexPages.currentPage().getParameters().get('transId');
	    if(strId.length() > 18 && strId.length() < 15){
	      isValidTxn = false;
	    }    
    }    
    
    public ChangeAllocationRequest getCHMxmlData(){
    	
    	if(isValidTxn){
    	
		  FundAllocation chmFunds = new FundAllocation();
	  	FundAllocation chmSpace = new FundAllocation();
	  	ChmTransactionXMLGen chmXmlMethod = new ChmTransactionXMLGen();
	  	ChangeAllocationRequest chmXmlOutput = new ChangeAllocationRequest();
		
	  	Transaction__c xmldata = [Select id, Client_Id__c, TxnData__c from Transaction__c where id =:transId];
		
	  	chmXmlOutput = chmXmlMethod.readCHMxml(xmldata.TxnData__c);
	  	//chmXmlOutput = chmXmlMethod.readCHMxml(sampleXml);
		
	  	for(integer x=0; x<chmXmlOutput.fundAlloc.size(); x++){
	  		changeAllocFunds.add(new Display_Translator(TransactionsReqConstant.FUND_NAME_DT, chmXmlOutput.fundAlloc[x].code));	
  		}
  		chmFundNameMap = Display_Translator.TranslateMap(changeAllocFunds);
		
	  	for(integer i=0; i<chmXmlOutput.fundAlloc.size(); i++){
			  fundName = chmFundNameMap.get(chmXmlOutput.fundAlloc[i].code);
			  chmXmlOutput.fundAlloc[i].name = fundName + CU.parseExtCode(chmXmlOutput.fundAlloc[i].name);
		  }
 		
		  chmSpace.percent ='*';
		  chmXmlOutput.fundAlloc.add(chmSpace);
		  chmFunds.name = 'Total';
		  chmFunds.percent = '100%';
		  chmXmlOutput.fundAlloc.add(chmFunds);
		  return chmXmlOutput;
		  
    	} else {
    		return null;
    	}
	  }
	
	public String getClientName(){
		// JC NACHOR - bypass query when the txn id is invalid
		if(isValidTxn){
		
		Transaction__c AcctName = [Select Client__r.Id, Client__r.FirstName, Client__r.LastName from Transaction__c where Id =:transId];
		//User uName = [Select FirstName, Id, LastName from User where Id=: UserInfo.getUserId()];
        string usrName;
        
        usrName = CU.formatName(AcctName.Client__r.FirstName, AcctName.Client__r.LastName);
        //usrName = formattedName.formatName(uName.FirstName, uName.LastName);

        return usrName;
		} else {
			return null;
		}   
	}
	
	public String getNavPreValidation(){
		String espParam = ESPortalNavigator.NAV_URL_FSW_POLICY;
		return espParam;
	}
	
	public String getNavTransactionLog(){
		String espParam = ESPortalNavigator.NAV_URL_SVC_TXN;
		return espParam;
	}
	
	// JC NACHOR - change to String instead of Id
	//public Id getTransactionId(){
	public String getTransactionId(){
		return transId;
	}
	
	public static testMethod void CHMAcknowledgementCtrl_test(){
		PageReference pg = Page.CHMAcknowledgement;
		Test.setCurrentPage(pg);
		PageReference pgPdf = Page.CHMAcknowledgementPDF;
		Test.setCurrentPage(pgPdf);
		
		RecordType transacType;
		try{
			transacType =[Select Id, Name from RecordType where Name ='Person Account'];
		}catch(Exception e){
			system.debug('Error while retrieving record type. '+e);
		}
		Account accTemp = new Account(LastName='TestAccount', CurrencyIsoCode='HKD', Client_ID__pc='Test0002', PersonEmail = 'a@b.com',
										RecordTypeId=transacType.Id);
		insert accTemp;
		Transaction__c transTemp = new Transaction__c(Reference_Number__c ='Ref0123456',
														Request_Type__c ='CHGALLOCAT',
														CurrencyIsoCode ='HKD',
														Client__c =accTemp.Id,
														TxnData__c ='<Txn RequestType="CHGALLOCAT" ReferenceNo="ECM-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="25" createnew="Y"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="25" createnew="N"/>'+
							'<fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="20" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund ryan wilson dave" percent="30" createnew="N"/></Txn>');
		insert transTemp;
		
    System.currentPageReference().getParameters().put('transId', transTemp.Id);		
		
		CHMAcknowledgementCtrl chmTest = new CHMAcknowledgementCtrl();
		chmTest.transId = transTemp.Id;
		chmTest.getCHMxmlData();
		chmTest.getClientName();
		chmTest.getNavPreValidation();
		chmTest.getNavTransactionLog();
		chmTest.Security();
	}

}