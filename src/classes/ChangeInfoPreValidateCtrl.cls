/* 
Version 	: 1.1
Company		: Sun Life Financial
Date		: 30.JULY.2009
Author		: Dorothy Anne L. Baltazar
Description	: Controller for Change Contact Information.
History		: 
			JC NACHOR  08/12/2009 added error checking if user have no active inforce Policy
			R  Angeles 10/28/2009 added admin portal checks
*/
public with sharing class ChangeInfoPreValidateCtrl {
	TransactionValidator CPTxnValidator = new TransactionValidator();
	
	//For checking if Transaction is valid
	public Boolean errorCastIron;
	public Boolean errorCPExistRequest;
	/* JC NACHOR 08/12/2009 added error checking if user have no active inforce Policy */
  	public Boolean errorNoActiveInforcePolicy;
  	 
	//For displaying error messages on the page; initially set to false
	public Boolean renderErrorCastIron {get; set;}
	public Boolean renderCPErrorExistRequest {get; set;}
	/* JC NACHOR 08/12/2009 added error checking if user have no active inforce Policy */
	public Boolean renderErrorNoActiveInforcePolicy {get; set;}
	
	public String policyNo;
	public String acctClientId;
	public String userId = userInfo.getUserId();
	public String tempURL {get; set;}
		
	public String getAcctClientId()
	{
		User userClientId = [Select u.Client_Id__c From User u where u.Id = :userId];
		
		List<Account> acctList = new List<Account>();
		acctList = [Select a.Client_Id__c, a.RecordType.name From Account a where a.Client_Id__c = :userClientId.Client_Id__c];
		if (acctList.size()>0)
		{
			if (acctList[0].Client_Id__c!=null)
			{
				acctClientId = acctList[0].Client_Id__c;
			}
		}
		
		return acctClientId;
	}
	
	public ChangeInfoPreValidateCtrl()
	{
		//String cId;
		acctClientId = getAcctClientId();
		
		String transType = 'CP';
		String transStatus = 'Submitted';
		//R Angeles 10282009 added admin portal checks
		Config_Utilities CU = new Config_Utilities();
		if(CU.isPortalUser('') == true){
			errorCastIron = !(CPTxnValidator.checkCastIron());
			errorCPExistRequest = CPTxnValidator.checkCPExistRequest(acctClientId, transType, transStatus);
			errorNoActiveInforcePolicy = CPTxnValidator.checkNoActiveInforcePolicy(userId);
		}
		else{
			errorCastIron = false;
			errorCPExistRequest = false;
			errorNoActiveInforcePolicy = false;
		}
		//END
		/* JC NACHOR 08/12/2009 added error checking if user have no active inforce Policy */
		if ((errorCastIron == false) && (errorCPExistRequest == false) && (errorNoActiveInforcePolicy == false) && (errorNoActiveInforcePolicy) == false)
		{
			//proceed to next page
			renderErrorCastIron = false;
			renderCPErrorExistRequest = false;
			tempURL = ESPortalNavigator.NAV_URL_CPP_INPUT;
			tempURL = tempURL + '?ClientId=' + acctClientId;
		}
		else
		{
			renderErrorNoActiveInforcePolicy = errorNoActiveInforcePolicy;
			renderErrorCastIron = errorCastIron;
			renderCPErrorExistRequest = errorCPExistRequest;
		}
	}
	
	public static testMethod void testChangeInfoPreValidateCtrl()
	{
		Schema.DescribeSObjectResult AccountSchema = Schema.SObjectType.Account;
    	Map<String,Schema.RecordTypeInfo>AccountMapByName = AccountSchema.getRecordTypeInfosByName();
    	Schema.RecordTypeInfo AccountRT = AccountMapByName.get('Person Account');
    	
    	String PersonAccountTypeId = '';
        if(AccountRT != null){
        	 PersonAccountTypeId =AccountRT.getRecordTypeId();
        }
        
        Account accountSuccessTest = new Account(  PersonBirthdate=System.today(), PersonHomePhone='12345678909', 
				PersonMobilePhone='1234567890', Phone='1234567890', PersonEmail='acmegx@gmail.com', 
				FirstName='AcME', LastName='GX', RecordTypeId =  PersonAccountTypeId, 
				Client_Id__c = '!@#$%^&*()', Client_ID__pc = 'abcde12345'
				);
					
		Insert accountSuccessTest;		
		
		Schema.DescribeSObjectResult PolicySchema = Schema.SObjectType.Policy__c;
    	Map<String,Schema.RecordTypeInfo>PolicyMapByName = PolicySchema.getRecordTypeInfosByName();
    	Schema.RecordTypeInfo PolicyRT = PolicyMapByName.get('Traditional');
    		
    	String TradPolicyTypeId = '';
        if(PolicyRT != null){
        	TradPolicyTypeId =PolicyRT.getRecordTypeId();
        }
			
		Policy__c policySuccessTest = new Policy__c(name ='b1234', client__c = accountSuccessTest.id, policy_number__c = '887888877',
										RecordTypeId = TradPolicyTypeId, Policy_Status_Picklist_Hidden__c = '1', 
										Policy_Issue_Date__c = System.today(), CurrencyIsoCode='HKD', 
										Policy_Inforce_Date__c = System.today(), Payment_Mode_Picklist__c = 'Single Premium', 
										Application_Received_Date__c = System.today(), Payment_Method_Picklist__c = 'Direct Bill', 
										Min_Requirement_on_Initial_Deposit__c = 100.00, Total_Mode_Premium__c = 100.00, 
										Premium_Due_Date__c = System.today(), Death_Benefit_Option_Picklist__c = 'FACE PLUS', 
										Plan_Code__c = 'abc123', HKID_no__c = 'HK123', Billing_Method__c = '4', 
										Policy_Insurance_Type__c = 'T',  Policy_Status_Hidden__c = '1',
										Branch_Number__c = '234', Account_Number__c = 'Account 1', 
										Payment_Method__c = '4', Last_Autopay_Date__c = System.today(), 
										Next_Autopay_Date__c = System.today(), Debit_Date__c = 123.00, 
										Credit_Card_Number__c = '123456', Policy_Premium_Type_Code__c = 'E', 
										Regular_Contribution__c = 123456, Restrict_Billing_Code_2__c = 'X', 
										Dividend_Option__c = '1', Contractual_Payout_Method__c = 'ABC', 
										Premium_Reduction_Indicator__c = 'ABC', Accumulated_Dividend__c = 123.0,  
										Accumulated_Dividend_Interest_Rate__c = 123.00, Coupon_Fund_Interest_Rate__c = 123.00, 
										Premium_Deposit_Funds_Interest_Rate__c = 123.00, Loan_Interest_Rate__c = 123.00, 
										Surrender_Value__c = 123.00, Premium_Suspense__c = 123, 
										Maximum_Loan__c = 123, Cash_Value__c = 123.0,  
										Maturity_Dividend_Surrendered__c = 123.00, Maturity_Balance_Indicator__c = 'Y', 
										Maturity_Balance__c = 123.00, ECE_JCI_Balance__c = 123.00, 
										Paid_Up_Addition_Cash_Value__c = 123.00, Policy_Country_Code__c = 'RF', 
										Policy_Owner_Address_Id__c = 'abc123', Policy_Payor_Address_Id__c = 'abc123',
										Credit_Card_Last_Autopay_Date__c = System.today(), Outstanding_Cash_Loan__c = 123.00									
										);      
		insert policySuccessTest;
		
		Transaction__c transTempCM = new Transaction__c(Reference_Number__c ='Ref0123456CM',
														Request_Type__c ='S0001',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="S0001" ReferenceNo="ECM-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempCM;
		
		Transaction__c transTempCP = new Transaction__c(Reference_Number__c ='Ref0123456CP',
														Request_Type__c ='CHGALLOCAT',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="CHGALLOCAT" ReferenceNo="ECP-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempCP;
		
		Transaction__c transTempFS = new Transaction__c(Reference_Number__c ='Ref0123456FS',
														Request_Type__c ='FUNDSW',
														CurrencyIsoCode ='HKD',
														SORStatus__c = 'Submitted',
														Active__c = true,
														Client_Id__c = accountSuccessTest.Client_Id__c,
														Client__c = accountSuccessTest.Id,
														TxnData__c ='<Txn RequestType="FUNDSW" ReferenceNo="EFS-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
							'<switchout><fund code="ACHF1" name="Sun Life Active Lifestyle Portfolio" percent="9"/>'+
							'<fund code="AENEH" name="Sun Life China Hong Kong Portfolio" percent="100"/></switchout>'+
							'<switchin><fund code="AENEU" name="Sun Life Stable Lifestyle Portfolio" percent="30" estamount="1234" createnew="Y"/>'+
							'<fund code="AEVEU" name="Sun Life-Schroder AS Commodity Fund" percent="70" estamount="5678"  createnew="Y"/></switchin></Txn>');
		insert transTempFS;
		
		Fund_Transaction_History__c fundTrans = new Fund_Transaction_History__c (Fund_Transaction_ID__c='000123',
													 Policy__c=policySuccessTest.Id, 
													 Fund_Code__c='BEAEH', Transaction_Type__c='LSI', 
													 Fund_Price_Date__c=System.today(), Maturity_Date__c=System.today(), 
													 Interest_Rate__c=5.5050, Number_of_Units__c=10.000000, Unit_Price__c=575.8068, Fund_Value__c=105750.75 );
        insert fundTrans;
        
        PageReference pgRef = Page.TxnLogHistory;
		Test.setCurrentPage(pgRef);

		ApexPages.currentPage().getParameters().put('defID', accountSuccessTest.Client_Id__c);
        
		ChangeInfoPreValidateCtrl temp = new ChangeInfoPreValidateCtrl();
		
		String policyNumber = policySuccessTest.policy_number__c;
		String tempId = temp.getAcctClientId();
		String tempUserId = accountSuccessTest.Client_Id__c;
		
		
		TransactionValidator tempTV = new TransactionValidator();
		tempTV.checkCPExistRequest(tempUserId, 'CP', 'Submitted');
		tempTV.checkNoActiveInforcePolicy(tempUserId);
		
		tempTV.checkExistRequest(policyNumber, 'FS', 'Submitted');
		tempTV.checkExistRequest(policyNumber, 'CM', 'Submitted');
		tempTV.checkLapseStartDate(policyNumber);
		tempTV.checkNonES(policyNumber);
		tempTV.checkPendingFundTxn(policyNumber);
		tempTV.checkSinglePremium(policyNumber);
	} 
}