/*
    Version     : 1.1
    Company     : Sun Life Financial
    Date        : 22.July.2009
    Author      : Wilson Cheng
    Description : This is used as a controller for both acknowledgement VF and PDF page.
    History     : 
    
                1.1 28.OCT.2009 - Robby Angeles - added method to check security for admin portal users (if page is Acknowledgement PDF)  
                1.2 24.NOV.2009 - JC NACHOR - add parameter filtering to prevent cross site scripting
*/

public without sharing class ChangeInfoAcknowledgeCtrl
{
	  // JC NACHOR - change to String instead of Id
    //Id transId;    
    String transId;
    boolean allPolicies;
    
    Config_Utilities CU = new Config_Utilities();
    
    // JC NACHOR - global flag to determine if transaction is accessible to the user
    public boolean isValidTxn = true;
    
    
    
    public PageReference Security() 
    {  
      Boolean isPortalUser = true; 
      // JC NACHOR 09/24/09 - determines if the current user has rights to the Policy being accessed
      //PageReference pRefRedirect = Page.NoAccessToPolicy;
      PageReference pRefRedirect = new Pagereference('/apex/NoAccessToPolicy?type=transaction');
      pRefRedirect.setRedirect(true);
      // START JC NACHOR - add parameter filtering to prevent cross site scripting
      Map<String,String> mapParams = ApexPages.currentPage().getParameters();
      String strId = ApexPages.currentPage().getParameters().get('transId');
      //R.Angeles bypass security if logged in user is admin and current page is PDF 
      String pageRef = ApexPages.currentPage().getURL();                  
      
      if(pageRef.indexOf('PDF') != -1){
           isPortalUser = CU.isPortalUser('');
      }
      if(isPortalUser){
      	
      	// START JC NACHOR - add parameter filtering to prevent cross site scripting
      	if(strId.length() > 18 && strId.length() < 15){
      		isValidTxn = false;
      		return pRefRedirect;
      	} 
     		this.transId   = ApexPages.currentPage().getParameters().get('transId');
      	
      	
	      if(ESAccessVerifier.isCurrentUsersTransaction((strId==null)?'':strId)){
	        return null;
	      } else {
	        isValidTxn = false;
	        return pRefRedirect;
	      }
      }
      else{
            return null;
      }
      //END 
    }      
    
    // START JC NACHOR - add parameter filtering to prevent cross site scripting
    /*
    public ChangeInfoAcknowledgeCtrl() {
    	    	  
    	  String strId = ApexPages.currentPage().getParameters().get('transId');
        if(strId.length() > 18 && strId.length() < 15){
          isValidTxn = false;
        }
        this.transId   = ApexPages.currentPage().getParameters().get('transId');
    	
    }
    */
    
    //for parsing the content of the xml and display it on the page
    public ChangeInfoRequest getDisplayXMLdata()
    {   
        
          // JC NACHOR
          if(isValidTxn) {
        
        ChangeInfoXMLGen xmlContainer = new ChangeInfoXMLGen();
        ChangeInfoRequest XMLoutput = new ChangeInfoRequest();

        Transaction__c xmldata = [Select id, Client_Id__c, Client__c, TxnData__c from Transaction__c where id =:transId];

        XMLoutput = xmlContainer.readCPPXML(xmldata.TxnData__c);
        
        if(XMLoutput.AddressLine1 != NULL)
        {
            XMLoutput.AddressLine1 = XMLoutput.AddressLine1.toUpperCase();
        }
        
        if(XMLoutput.AddressLine2 != NULL)
        {
            XMLoutput.AddressLine2 = XMLoutput.AddressLine2.toUpperCase();
        }
        
        if(XMLoutput.AddressLine3 != NULL)
        {
            XMLoutput.AddressLine3 = XMLoutput.AddressLine3.toUpperCase();
        }
        
        if(XMLoutput.AddressLine4 != NULL)
        {
            XMLoutput.AddressLine4 = XMLoutput.AddressLine4.toUpperCase();
        }
        
        if(XMLoutput.AddressLine5 != NULL)
        {
            XMLoutput.AddressLine5 = XMLoutput.AddressLine5.toUpperCase();
        }
        
        if(XMLoutput.AddressCity != NULL)
        {
            XMLoutput.AddressCity = XMLoutput.AddressCity.toUpperCase();
        }
        
        if(XMLoutput.AddressProv != NULL)
        {
            XMLoutput.AddressProv = XMLoutput.AddressProv.toUpperCase();
        }
        
        if(XMLoutput.Addresszip != NULL)
        {
            XMLoutput.Addresszip = XMLoutput.Addresszip.toUpperCase();
        }
        
        if(XMLoutput.AddressCountry != NULL)
        {
            XMLoutput.AddressCountry = XMLoutput.AddressCountry.toUpperCase();
        }
        
        //check if all policies are selected
        if(XMLoutput.Policies.size()>0)
        {
            //check if the first element of the policy in xml has the value 'X'
            if (XMLoutput.Policies[0] == TransactionsReqConstant.ALL_POLICIES)
            {
                allPolicies = true;
                /*List<Policy__c> policyList = [Select name, Policy_Inforce_Date__c, Policy_Number__c from Policy__c 
                                              where OwnerId =: userInfo.getUserId() 
                                              and (Policy_status_hidden__c = '1' or Policy_Status_Hidden__c = '2' or 
                                              Policy_Status_Hidden__c = '3' or Policy_Status_Hidden__c = '4') 
                                              order by Name, Policy_Inforce_Date__c desc  limit 200];
                XMLoutput.Policies.clear();
                
                system.debug('policyList size deb:'+ policyList.size());
                
                for(integer i = 0; i < policyList.size(); i++)
                {
                    XMLoutput.Policies.add(policyList[i].Policy_Number__c);
                }*/
            }
            else
            {
                allPolicies = false;
            }
        }
               
        return XMLoutput;
        
        } else {
            return null;
        }
    }
    
    //for displaying the first and last name of the user in correct format
    public string getDisplayName()
    {
        // JC NACHOR
        if(isValidTxn){
        
        Transaction__c AcctName = [Select Client__r.FirstName, Client__r.LastName from Transaction__c where id =:transId];
        string usrName;
        
        usrName = CU.formatName(AcctName.Client__r.FirstName, AcctName.Client__r.LastName);
        
        return usrName; 
        
        } else {
          return null;
        }  
        
    }
    
    // JC NACHOR - change to String instead of Id
    //public Id getTransactionId()
    public String getTransactionId()
    {
        return transId;
    }
    
    public string getTransitionLogNav()
    {
       string NavURL;
       
       NavURL = ESPortalNavigator.NAV_URL_SVC_TXN;
        
       return NavURL;   
    }
    
    public boolean getAllPolicies()
    {
        return  allPolicies;
    }
    
    static testMethod void testChangeInfoAcknowledgeCtrl() {
        
        Transaction__c tempTrans = new Transaction__c();
        Account acctTemp = new Account();
        ChangeInfoXMLGen demo = new ChangeInfoXMLGen();
        ChangeInfoRequest tempData = new ChangeInfoRequest();
        
        RecordType recordID = [Select r.Name, r.Id From RecordType r where Name='Person Account'];
        
        acctTemp.FirstName = 'Wilson';
        acctTemp.LastName = 'Cheng';
        acctTemp.Client_Id__c = 'test';
        acctTemp.Client_ID__pc = 'test';
        acctTemp.CurrencyIsoCode = 'HKD';
        acctTemp.RecordTypeId = recordID.Id;
        acctTemp.PersonEmail = 'a@b.com';
        
        insert acctTemp;
        
        tempTrans.TxnData__c = '<Txn RequestType="S0001" ReferenceNo="ECP-298000005-090421" PolicyNo="1234567890" SubmissionDateTime="21/04/2009 13:10:52">'+
                                 '<homephone value="21119999" nochange="true"/>'+
                                 '<officephone value="" nochange="true"/>'+
                                 '<mobilephone value="97778888" nochange="true"/>'+
                                 '<emailaddress value="peterchan@mailbox.com.hk" nochange="true"/>'+
                                 '<address nochange="false">'+
                                 '<line1 value="flat 34"></line1>'+
                                 '<line2 value="5th floor"/>'+
                                 '<line3 value="House ABC"/>'+
                                 '<line4 value="Salisbury Road"/>'+
                                 '<line5 value="Tsim Tsa Tsui"/>'+
                                 '<city value="Hong Kong City"/>'+
                                 '<prov value="Hong Kong"/>'+
                                 '<zip value="1234"/>'+
                                 '<country value="Hong Kong"/>'+
                                 '</address>'+
                                 '<policies>'+
                                 '<policyno value="1234567891"/>'+
                                 '<policyno value="1234567892"/>'+
                                 '<policyno value="1234567893"/>'+
                                 '</policies>'+
                                 '</Txn>';
                                 
        tempTrans.Client__c = acctTemp.id;
        tempTrans.Request_Type__c='S0001';
        tempTrans.Reference_Number__c='ECP-298000005-090421';
                                 
        insert tempTrans;

        Test.setCurrentPageReference(new PageReference('Page.ChangeInfoAcknowledgeCtrl'));
        
        System.currentPageReference().getParameters().put('transId', tempTrans.id);
        
        ChangeInfoAcknowledgeCtrl cppTemp = new ChangeInfoAcknowledgeCtrl();

        cppTemp.transId = tempTrans.id;
        cppTemp.getDisplayXMLdata();
        cppTemp.getDisplayName();
        cppTemp.getTransactionId();
        cppTemp.getTransitionLogNav();
        cppTemp.getAllPolicies();
        
        tempData.RequestType = 'S0001';
        tempData.ReferenceNo = 'ECP-298000005-090421';
        tempData.PolicyNo = '1234567890';
        tempData.SubmissionDateTime = '21/04/2009 13:10:52';
        tempData.HomePhone = '21119999';
        tempData.HomePhoneChange = 'true';
        tempData.OfficePhone = null;
        tempData.OfficePhoneChange = 'true';
        tempData.MobilePhone = '97778888';
        tempData.MobilePhoneChange = 'true';
        tempData.EmailAddValue = 'peterchan@mailbox.com.hk';
        tempData.EmailAddChange = 'true';
        tempData.AddressChange = 'false';
        tempData.AddressLine1 = 'Flat 123';
        tempData.AddressLine2 = '5th floor';
        tempData.AddressLine3 = 'House ABC';
        tempData.AddressLine4 = 'Salisbury Road';
        tempData.AddressLine5 = 'Tsim Tsa Tsui';
        tempData.AddressCity = 'Hong Kong City';
        tempData.AddressProv = 'Hong Kong';
        tempData.AddressZip = '1234';
        tempData.AddressCountry = 'Hong Kong';
        tempData.Policies.add('1234567891');
        tempData.Policies.add('1234567892');
        tempData.Policies.add('1234567893');
        
        demo.writeCPPXML(tempData);
        
        cppTemp.Security();
    }
}