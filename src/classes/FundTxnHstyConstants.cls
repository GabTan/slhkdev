/*
Version	   : 1.1
Company    : Sun Life Financial
Date       : 29.JUL.2009
Author     : Marian Baleros
Description: Fund Transaction History Constants
History    : 
	1.1 	16.OCT.2009 - W. Clavero - Added transaction type prefix constants and ADF constants.
						
*/


public class FundTxnHstyConstants 
{
	//for Fund Transaction Type
	public static final String FTH_CF_SI = 'CF_SI';
	public static final String FTH_CF_SO = 'CF_SO';
	public static final String FTH_CF_RO = 'CF_RO';
	public static final String FTH_CF_LSI = 'CF_LSI';
	public static final String FTH_CF_WDL = 'CF_WDL';
	public static final String FTH_CF_ADF = 'CF_ADF';  //w. clavero 20091016
	
	public static final String FTH_UF_SI = 'UF_SI';
	public static final String FTH_UF_SO = 'UF_SO';
	public static final String FTH_UF_RI = 'UF_RI';
	public static final String FTH_UF_LSI = 'UF_LSI';
	public static final String FTH_UF_WDL = 'UF_WDL';
	public static final String FTH_UF_APH = 'UF_APH';
	public static final String FTH_UF_AFB = 'UF_AFB';
	public static final String FTH_UF_FYB = 'UF_FYB';
	public static final String FTH_UF_LYB = 'UF_LYB';
	public static final String FTH_UF_ADF = 'UF_ADF';  //w. clavero 20091016
	
	public static final String FTH_CODE_TYPE = 'FTH_TXNTYPE';
	
	//for policies Fortune, Fortune Builder and Futurity
	public static final String POLICY_FORTUNE = 'FJFH0A;FJFH1A;FJFH2A;FJFH3A;FJFU0A;FJFU1A;FJFU2A;FJFU3A;FJPH0A;FJPH1A;FJPH2A;FJPH3A;FJPU0A;FJPU1A;FJPU2A;FJPU3A;FJSH0A;FJSH1A;FJSH2A;FJSH3A;FJSU0A;FJSU1A;FJSU2A;FJSU3A;FSLH0A;FSLH1A;FSLH2A;FSLH3A;FSLU0A;FSLU1A;FSLU2A;FSLU3A;FSPU0A;FSPU1A;FSPU2A;GIFH0A;GIFU0A;SJFH0A;SJFH1A;SJFH2A;SJFH3A;SJFU0A;SJFU1A;SJFU2A;SJFU3A;SJPH0A;SJPH1A;SJPH2A;SJPH3A;SJPU0A;SJPU1A;SJPU2A;SJPU3A;SJSH0A;SJSH1A;SJSH2A;SJSH3A;SJSU0A;SJSU1A;SJSU2A;SJSU3A;SSLH0A;SSLH1A;SSLH2A;SSLH3A;SSLU0A;SSLU1A;SSLU2A;SSLU3A;SSPU0A;SSPU1A;SSPU2A;FJFH0B;FJFU0B;FJPH0B;FJPU0B;FJSH0B;FJSU0B;FSLH0B;FSLU0B;GF5U0A;GF8U0A;GS5U0A;GS8U0A';
	//public static final String POLICY_FORTUNEBUILDER = 'FJFH0B;FJFU0B;FJPH0B;FJPU0B;FJSH0B;FJSU0B;FSLH0B;FSLU0B';
	//public static final String POLICY_FUTURITY = 'GF5U0A;GF8U0A;GS5U0A;GS8U0A';
	
	//w. clavero 20091016: transaction type constants
	public static final String FTH_TYPE_PREFIX_CASHFLOW = 'CF_';
	public static final String FTH_TYPE_PREFIX_UTILIZED = 'UF_';
	
}