/*
Version		: 1.0
Company		: Sun Life Financial
Date		: 12.AUG.2009
Author		: W. Clavero
Description	: Translates the currency codes.
History		:
*/
public class CurrencyCodeHandler {
	
	public static final String CURRENCY_CODE_TYPE 	= 'SYS_LOOKUP_MST';
	public static final String CURRENCY_CODE_PREFIX = 'CURRENCY_'; 
	
	public static List<String> getENCurrencyCodes() {
		String genCode = CURRENCY_CODE_PREFIX + '%';
		List<String> currencyList = new List<String>();
		
		for (Config_Translation_Lookups__c ctl: [Select Id, Description__c 
										From Config_Translation_Lookups__c 
										Where (Language__c = :ESPortalNavigator.LANG_ENG_USA)
										And (Code_Type__c = :CURRENCY_CODE_TYPE) 
										And (Code__c like :genCode)]) {
			currencyList.add(ctl.Description__c);
		}
		
		return currencyList;
	} 
	
	public static Map<String, String> getTranslatedCurrencyCodes() {
		Map<String, String> currencyMap = new Map<String, String>();
		List<String> currencyCodes = getENCurrencyCodes();
		List<Display_Translator> currencyList = new List<Display_Translator>();
		
		for (String isoCode: currencyCodes) {
			Display_Translator cdt = new Display_Translator(CURRENCY_CODE_TYPE, CURRENCY_CODE_PREFIX + isoCode);
			currencyList.add(cdt);
		} 
		
		if (currencyList.size() > 0) {
			Map<String, String> transMap = Display_Translator.translateMap(currencyList);
			
			for (String isoCode: currencyCodes) {
				currencyMap.put(isoCode, transMap.get(CURRENCY_CODE_PREFIX + isoCode));
			} 			
		}
		
		return currencyMap;
	}
}